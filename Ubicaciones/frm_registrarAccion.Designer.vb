﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_registrarAccion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lab_NombreOperario = New System.Windows.Forms.Label()
        Me.lab_IdOperario = New System.Windows.Forms.Label()
        Me.btn_aceptar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.list_accion = New System.Windows.Forms.ComboBox()
        Me.list_motivos = New System.Windows.Forms.ComboBox()
        Me.Lab_motivo = New System.Windows.Forms.Label()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.lab_maquina = New System.Windows.Forms.Label()
        Me.list_maquinas = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'lab_NombreOperario
        '
        Me.lab_NombreOperario.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_NombreOperario.Location = New System.Drawing.Point(69, 9)
        Me.lab_NombreOperario.Name = "lab_NombreOperario"
        Me.lab_NombreOperario.Size = New System.Drawing.Size(336, 23)
        Me.lab_NombreOperario.TabIndex = 0
        Me.lab_NombreOperario.Text = "Label1"
        Me.lab_NombreOperario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lab_IdOperario
        '
        Me.lab_IdOperario.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_IdOperario.Location = New System.Drawing.Point(12, 9)
        Me.lab_IdOperario.Name = "lab_IdOperario"
        Me.lab_IdOperario.Size = New System.Drawing.Size(51, 23)
        Me.lab_IdOperario.TabIndex = 1
        Me.lab_IdOperario.Text = "999"
        Me.lab_IdOperario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_aceptar
        '
        Me.btn_aceptar.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.btn_aceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_aceptar.Location = New System.Drawing.Point(259, 237)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(133, 40)
        Me.btn_aceptar.TabIndex = 2
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 24)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Acción:"
        '
        'list_accion
        '
        Me.list_accion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_accion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_accion.FormattingEnabled = True
        Me.list_accion.Location = New System.Drawing.Point(115, 67)
        Me.list_accion.Name = "list_accion"
        Me.list_accion.Size = New System.Drawing.Size(289, 32)
        Me.list_accion.TabIndex = 4
        '
        'list_motivos
        '
        Me.list_motivos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_motivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_motivos.FormattingEnabled = True
        Me.list_motivos.Location = New System.Drawing.Point(115, 119)
        Me.list_motivos.Name = "list_motivos"
        Me.list_motivos.Size = New System.Drawing.Size(289, 32)
        Me.list_motivos.TabIndex = 5
        '
        'Lab_motivo
        '
        Me.Lab_motivo.AutoSize = True
        Me.Lab_motivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lab_motivo.Location = New System.Drawing.Point(16, 122)
        Me.Lab_motivo.Name = "Lab_motivo"
        Me.Lab_motivo.Size = New System.Drawing.Size(77, 24)
        Me.Lab_motivo.TabIndex = 6
        Me.Lab_motivo.Text = "Motivo:"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.BackColor = System.Drawing.Color.LightSalmon
        Me.btn_cancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancelar.Location = New System.Drawing.Point(20, 237)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(133, 40)
        Me.btn_cancelar.TabIndex = 7
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'lab_maquina
        '
        Me.lab_maquina.AutoSize = True
        Me.lab_maquina.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_maquina.Location = New System.Drawing.Point(16, 175)
        Me.lab_maquina.Name = "lab_maquina"
        Me.lab_maquina.Size = New System.Drawing.Size(96, 24)
        Me.lab_maquina.TabIndex = 9
        Me.lab_maquina.Text = "Máquina:"
        Me.lab_maquina.Visible = False
        '
        'list_maquinas
        '
        Me.list_maquinas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_maquinas.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_maquinas.FormattingEnabled = True
        Me.list_maquinas.Items.AddRange(New Object() {"Impresora China 1", "Impresora China 2", "Impresora China 3", "Impresora China 4", "Rolland 1", "Rolland 2"})
        Me.list_maquinas.Location = New System.Drawing.Point(118, 165)
        Me.list_maquinas.Name = "list_maquinas"
        Me.list_maquinas.Size = New System.Drawing.Size(287, 32)
        Me.list_maquinas.TabIndex = 10
        Me.list_maquinas.Visible = False
        '
        'frm_registrarAccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(426, 308)
        Me.ControlBox = False
        Me.Controls.Add(Me.list_maquinas)
        Me.Controls.Add(Me.lab_maquina)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.Lab_motivo)
        Me.Controls.Add(Me.list_motivos)
        Me.Controls.Add(Me.list_accion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Controls.Add(Me.lab_IdOperario)
        Me.Controls.Add(Me.lab_NombreOperario)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frm_registrarAccion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_registrarAccion"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lab_NombreOperario As Label
    Friend WithEvents lab_IdOperario As Label
    Friend WithEvents btn_aceptar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents list_accion As ComboBox
    Friend WithEvents list_motivos As ComboBox
    Friend WithEvents Lab_motivo As Label
    Friend WithEvents btn_cancelar As Button
    Friend WithEvents lab_maquina As Label
    Friend WithEvents list_maquinas As ComboBox
End Class
