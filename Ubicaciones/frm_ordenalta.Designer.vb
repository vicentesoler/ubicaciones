﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ordenalta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_OtraUbi = New System.Windows.Forms.Button()
        Me.p_filtro = New System.Windows.Forms.Panel()
        Me.txt_filtro_codarticulo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lab_totunid = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lab_totdep = New System.Windows.Forms.Label()
        Me.btn_cerrar = New System.Windows.Forms.Button()
        Me.lab_tiempo = New System.Windows.Forms.Label()
        Me.lab_txttiempo = New System.Windows.Forms.Label()
        Me.lab_fecha = New System.Windows.Forms.Label()
        Me.btn_confirmarTodo = New System.Windows.Forms.Button()
        Me.lab_aplicacion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.list_ubicacion = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_articulo = New System.Windows.Forms.TextBox()
        Me.txt_descripcion = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.txt_cantidad = New System.Windows.Forms.TextBox()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.lab_almdestino = New System.Windows.Forms.Label()
        Me.lab_flecha = New System.Windows.Forms.Label()
        Me.list_codorden = New System.Windows.Forms.ComboBox()
        Me.lab_almorigen = New System.Windows.Forms.Label()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lab_CantUbiDest = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.p_filtro.SuspendLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.lab_CantUbiDest)
        Me.Panel1.Controls.Add(Me.btn_OtraUbi)
        Me.Panel1.Controls.Add(Me.p_filtro)
        Me.Panel1.Controls.Add(Me.btn_cerrar)
        Me.Panel1.Controls.Add(Me.lab_tiempo)
        Me.Panel1.Controls.Add(Me.lab_txttiempo)
        Me.Panel1.Controls.Add(Me.lab_fecha)
        Me.Panel1.Controls.Add(Me.btn_confirmarTodo)
        Me.Panel1.Controls.Add(Me.lab_aplicacion)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.list_ubicacion)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txt_articulo)
        Me.Panel1.Controls.Add(Me.txt_descripcion)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.btn_ok)
        Me.Panel1.Controls.Add(Me.txt_cantidad)
        Me.Panel1.Controls.Add(Me.imagen)
        Me.Panel1.Controls.Add(Me.lab_almdestino)
        Me.Panel1.Controls.Add(Me.lab_flecha)
        Me.Panel1.Controls.Add(Me.list_codorden)
        Me.Panel1.Controls.Add(Me.lab_almorigen)
        Me.Panel1.Controls.Add(Me.btn_finalizar)
        Me.Panel1.Controls.Add(Me.dg_lineas)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1250, 640)
        Me.Panel1.TabIndex = 25
        '
        'btn_OtraUbi
        '
        Me.btn_OtraUbi.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.btn_OtraUbi.Enabled = False
        Me.btn_OtraUbi.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OtraUbi.Location = New System.Drawing.Point(723, 81)
        Me.btn_OtraUbi.Name = "btn_OtraUbi"
        Me.btn_OtraUbi.Size = New System.Drawing.Size(92, 31)
        Me.btn_OtraUbi.TabIndex = 113
        Me.btn_OtraUbi.Text = "Otra Ubi."
        Me.btn_OtraUbi.UseVisualStyleBackColor = False
        '
        'p_filtro
        '
        Me.p_filtro.Controls.Add(Me.txt_filtro_codarticulo)
        Me.p_filtro.Controls.Add(Me.Label6)
        Me.p_filtro.Controls.Add(Me.Label4)
        Me.p_filtro.Controls.Add(Me.lab_totunid)
        Me.p_filtro.Controls.Add(Me.Label8)
        Me.p_filtro.Controls.Add(Me.lab_totdep)
        Me.p_filtro.Location = New System.Drawing.Point(374, 9)
        Me.p_filtro.Name = "p_filtro"
        Me.p_filtro.Size = New System.Drawing.Size(309, 82)
        Me.p_filtro.TabIndex = 91
        '
        'txt_filtro_codarticulo
        '
        Me.txt_filtro_codarticulo.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_filtro_codarticulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_filtro_codarticulo.Location = New System.Drawing.Point(12, 30)
        Me.txt_filtro_codarticulo.Name = "txt_filtro_codarticulo"
        Me.txt_filtro_codarticulo.Size = New System.Drawing.Size(128, 38)
        Me.txt_filtro_codarticulo.TabIndex = 89
        Me.txt_filtro_codarticulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label6.Location = New System.Drawing.Point(8, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 20)
        Me.Label6.TabIndex = 90
        Me.Label6.Text = "Buscar Ref."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(163, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(101, 25)
        Me.Label4.TabIndex = 56
        Me.Label4.Text = "Tot.Unid:"
        '
        'lab_totunid
        '
        Me.lab_totunid.AutoSize = True
        Me.lab_totunid.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_totunid.Location = New System.Drawing.Point(270, 9)
        Me.lab_totunid.Name = "lab_totunid"
        Me.lab_totunid.Size = New System.Drawing.Size(24, 25)
        Me.lab_totunid.TabIndex = 57
        Me.lab_totunid.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(163, 43)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(102, 25)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Tot.Dep.:"
        '
        'lab_totdep
        '
        Me.lab_totdep.AutoSize = True
        Me.lab_totdep.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_totdep.Location = New System.Drawing.Point(270, 43)
        Me.lab_totdep.Name = "lab_totdep"
        Me.lab_totdep.Size = New System.Drawing.Size(24, 25)
        Me.lab_totdep.TabIndex = 59
        Me.lab_totdep.Text = "0"
        '
        'btn_cerrar
        '
        Me.btn_cerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cerrar.Location = New System.Drawing.Point(684, 586)
        Me.btn_cerrar.Name = "btn_cerrar"
        Me.btn_cerrar.Size = New System.Drawing.Size(131, 48)
        Me.btn_cerrar.TabIndex = 62
        Me.btn_cerrar.Text = "Cerrar"
        Me.btn_cerrar.UseVisualStyleBackColor = True
        '
        'lab_tiempo
        '
        Me.lab_tiempo.AutoSize = True
        Me.lab_tiempo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tiempo.ForeColor = System.Drawing.Color.Teal
        Me.lab_tiempo.Location = New System.Drawing.Point(118, 67)
        Me.lab_tiempo.Name = "lab_tiempo"
        Me.lab_tiempo.Size = New System.Drawing.Size(17, 17)
        Me.lab_tiempo.TabIndex = 61
        Me.lab_tiempo.Text = "0"
        Me.lab_tiempo.Visible = False
        '
        'lab_txttiempo
        '
        Me.lab_txttiempo.AutoSize = True
        Me.lab_txttiempo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_txttiempo.ForeColor = System.Drawing.Color.Teal
        Me.lab_txttiempo.Location = New System.Drawing.Point(22, 67)
        Me.lab_txttiempo.Name = "lab_txttiempo"
        Me.lab_txttiempo.Size = New System.Drawing.Size(97, 17)
        Me.lab_txttiempo.TabIndex = 60
        Me.lab_txttiempo.Text = "Min.Usados:"
        Me.lab_txttiempo.Visible = False
        '
        'lab_fecha
        '
        Me.lab_fecha.AutoSize = True
        Me.lab_fecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_fecha.Location = New System.Drawing.Point(202, 74)
        Me.lab_fecha.Name = "lab_fecha"
        Me.lab_fecha.Size = New System.Drawing.Size(54, 15)
        Me.lab_fecha.TabIndex = 55
        Me.lab_fecha.Text = "Fecha: "
        '
        'btn_confirmarTodo
        '
        Me.btn_confirmarTodo.Enabled = False
        Me.btn_confirmarTodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_confirmarTodo.Location = New System.Drawing.Point(855, 587)
        Me.btn_confirmarTodo.Name = "btn_confirmarTodo"
        Me.btn_confirmarTodo.Size = New System.Drawing.Size(195, 48)
        Me.btn_confirmarTodo.TabIndex = 54
        Me.btn_confirmarTodo.Text = "Conf.Todo"
        Me.btn_confirmarTodo.UseVisualStyleBackColor = True
        '
        'lab_aplicacion
        '
        Me.lab_aplicacion.AutoSize = True
        Me.lab_aplicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_aplicacion.Location = New System.Drawing.Point(251, 598)
        Me.lab_aplicacion.Name = "lab_aplicacion"
        Me.lab_aplicacion.Size = New System.Drawing.Size(56, 31)
        Me.lab_aplicacion.TabIndex = 53
        Me.lab_aplicacion.Text = "xxx"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(18, 598)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(238, 31)
        Me.Label3.TabIndex = 52
        Me.Label3.Text = "Generada desde:"
        '
        'list_ubicacion
        '
        Me.list_ubicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_ubicacion.FormattingEnabled = True
        Me.list_ubicacion.Location = New System.Drawing.Point(592, 118)
        Me.list_ubicacion.Name = "list_ubicacion"
        Me.list_ubicacion.Size = New System.Drawing.Size(225, 62)
        Me.list_ubicacion.TabIndex = 50
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(589, 100)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 17)
        Me.Label7.TabIndex = 49
        Me.Label7.Text = "Ubi.Dest.:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(833, 100)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 17)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "Cant.:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(23, 100)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 17)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "Referencia:"
        '
        'txt_articulo
        '
        Me.txt_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_articulo.Location = New System.Drawing.Point(23, 119)
        Me.txt_articulo.MaxLength = 13
        Me.txt_articulo.Name = "txt_articulo"
        Me.txt_articulo.Size = New System.Drawing.Size(233, 60)
        Me.txt_articulo.TabIndex = 44
        '
        'txt_descripcion
        '
        Me.txt_descripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_descripcion.Location = New System.Drawing.Point(270, 119)
        Me.txt_descripcion.MaxLength = 13
        Me.txt_descripcion.Multiline = True
        Me.txt_descripcion.Name = "txt_descripcion"
        Me.txt_descripcion.ReadOnly = True
        Me.txt_descripcion.Size = New System.Drawing.Size(305, 61)
        Me.txt_descripcion.TabIndex = 46
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(268, 100)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 17)
        Me.Label5.TabIndex = 47
        Me.Label5.Text = "Descripción:"
        '
        'btn_ok
        '
        Me.btn_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ok.Location = New System.Drawing.Point(958, 118)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(92, 61)
        Me.btn_ok.TabIndex = 43
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'txt_cantidad
        '
        Me.txt_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad.Location = New System.Drawing.Point(835, 119)
        Me.txt_cantidad.Name = "txt_cantidad"
        Me.txt_cantidad.Size = New System.Drawing.Size(101, 60)
        Me.txt_cantidad.TabIndex = 41
        Me.txt_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(1068, 5)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(175, 175)
        Me.imagen.TabIndex = 40
        Me.imagen.TabStop = False
        '
        'lab_almdestino
        '
        Me.lab_almdestino.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_almdestino.ForeColor = System.Drawing.Color.OrangeRed
        Me.lab_almdestino.Location = New System.Drawing.Point(879, 17)
        Me.lab_almdestino.Name = "lab_almdestino"
        Me.lab_almdestino.Size = New System.Drawing.Size(106, 46)
        Me.lab_almdestino.TabIndex = 39
        Me.lab_almdestino.Text = "Código"
        Me.lab_almdestino.Visible = False
        '
        'lab_flecha
        '
        Me.lab_flecha.AutoSize = True
        Me.lab_flecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_flecha.Location = New System.Drawing.Point(803, 5)
        Me.lab_flecha.Name = "lab_flecha"
        Me.lab_flecha.Size = New System.Drawing.Size(77, 63)
        Me.lab_flecha.TabIndex = 38
        Me.lab_flecha.Text = "->"
        Me.lab_flecha.Visible = False
        '
        'list_codorden
        '
        Me.list_codorden.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_codorden.FormattingEnabled = True
        Me.list_codorden.IntegralHeight = False
        Me.list_codorden.Location = New System.Drawing.Point(199, 13)
        Me.list_codorden.Name = "list_codorden"
        Me.list_codorden.Size = New System.Drawing.Size(155, 59)
        Me.list_codorden.TabIndex = 37
        '
        'lab_almorigen
        '
        Me.lab_almorigen.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_almorigen.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lab_almorigen.Location = New System.Drawing.Point(706, 17)
        Me.lab_almorigen.Name = "lab_almorigen"
        Me.lab_almorigen.Size = New System.Drawing.Size(98, 46)
        Me.lab_almorigen.TabIndex = 36
        Me.lab_almorigen.Text = "Código"
        Me.lab_almorigen.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lab_almorigen.Visible = False
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(1068, 587)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(175, 48)
        Me.btn_finalizar.TabIndex = 33
        Me.btn_finalizar.Text = "Finalizar"
        Me.btn_finalizar.UseVisualStyleBackColor = True
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_lineas.Location = New System.Drawing.Point(20, 186)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(1223, 395)
        Me.dg_lineas.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(189, 46)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "N.Orden:"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'lab_CantUbiDest
        '
        Me.lab_CantUbiDest.AutoSize = True
        Me.lab_CantUbiDest.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_CantUbiDest.ForeColor = System.Drawing.Color.Black
        Me.lab_CantUbiDest.Location = New System.Drawing.Point(664, 97)
        Me.lab_CantUbiDest.Name = "lab_CantUbiDest"
        Me.lab_CantUbiDest.Size = New System.Drawing.Size(19, 20)
        Me.lab_CantUbiDest.TabIndex = 114
        Me.lab_CantUbiDest.Text = "0"
        '
        'frm_ordenalta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_ordenalta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_ordenalta"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.p_filtro.ResumeLayout(False)
        Me.p_filtro.PerformLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents list_ubicacion As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_articulo As System.Windows.Forms.TextBox
    Friend WithEvents txt_descripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents txt_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents imagen As System.Windows.Forms.PictureBox
    Friend WithEvents lab_almdestino As System.Windows.Forms.Label
    Friend WithEvents lab_flecha As System.Windows.Forms.Label
    Friend WithEvents list_codorden As System.Windows.Forms.ComboBox
    Friend WithEvents lab_almorigen As System.Windows.Forms.Label
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents dg_lineas As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lab_aplicacion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btn_confirmarTodo As System.Windows.Forms.Button
    Friend WithEvents lab_fecha As System.Windows.Forms.Label
    Friend WithEvents lab_totunid As System.Windows.Forms.Label
    Friend WithEvents lab_totdep As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lab_tiempo As System.Windows.Forms.Label
    Friend WithEvents lab_txttiempo As System.Windows.Forms.Label
    Friend WithEvents btn_cerrar As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_filtro_codarticulo As TextBox
    Friend WithEvents p_filtro As Panel
    Friend WithEvents btn_OtraUbi As Button
    Friend WithEvents lab_CantUbiDest As Label
End Class
