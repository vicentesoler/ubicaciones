﻿Public Class frm_nuevaubicacion
    Dim conexion As New Conector
    Private Sub frm_nuevaubicacion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        funciones.llenar_almacenes(list_almacen)
        list_almacen.SelectedValue = lab_almacen_aux.Text ' Siempre cuando abro este form, tengo que pasarle el almacen

    End Sub

    Private Sub txt_pasillo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_pasillo.KeyUp
        If txt_pasillo.Text.Length = 2 Then
            txt_portal.Focus()
            txt_portal.SelectAll()
        End If

    End Sub

    Private Sub txt_portal_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_portal.KeyUp
        If txt_portal.Text.Length = 2 Then
            txt_altura.Focus()
            txt_altura.SelectAll()
        End If

    End Sub
    Private Sub txt_altura_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_altura.KeyUp
        If txt_altura.Text.Length = 2 Then
            txt_tamaño.Focus()
            txt_tamaño.SelectAll()
        End If
    End Sub

    Private Sub btn_añadir_a_ubi_Click(sender As System.Object, e As System.EventArgs) Handles btn_añadir_a_ubi.Click
        Dim sql As String
        Dim resultado As String
        Dim vdatos As New DataTable

        If Not (txt_pasillo.Text.Length = 2) Or Not (txt_portal.Text.Length = 2) Or Not (txt_altura.Text.Length = 2) Or Not IsNumeric(txt_altura.Text) Or Not IsNumeric(txt_pasillo.Text) Or Not IsNumeric(txt_portal.Text) Or Not IsNumeric(txt_cantmax.Text) Or Not IsNumeric(txt_tamaño.Text) Or list_almacen.SelectedIndex = -1 Or list_tipo.SelectedIndex = -1 Then
            MsgBox("Hay algun campo vacio o con datos erroneos, revisar", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Comprobar que la nueva ubicacion de picking no existe
        sql = "select * from ubicaciones where codalmacen='" & list_almacen.SelectedValue & "' and codarticulo='" & lab_ref.Text & "' and ubicacion='" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count > 0 Then
            MsgBox("Esta ubicación ya existe para este artículo, no se puede crear.", MsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            'Crear la ubicación
            sql = "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
            sql &= "values ('" & list_almacen.SelectedValue & "','" & lab_ref.Text & "','" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "','" & list_tipo.Text & "'" & "," & txt_tamaño.Text & ",0,0," & txt_cantmax.Text & ",'Ubicaciones-Ubicaciones','" & frm_principal.lab_nomoperario.Text & "')"
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)

        Catch ex As Exception
            MsgBox("Se producjo un error al crear la ubicación", MsgBoxStyle.Critical)
        Finally
            Close()
        End Try
    End Sub

    Private Sub btn_cancelar_Click(sender As System.Object, e As System.EventArgs) Handles btn_cancelar.Click
        If MsgBox("Seguro que quiere ssalir?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

        txt_pasillo.Text = "" 'esto es para que luego compruebe si debe continuar o no
        Close()
    End Sub

    Private Sub btn_consolidacion_Click(sender As System.Object, e As System.EventArgs) Handles btn_consolidacion.Click
        If list_almacen.SelectedValue = "A01" Then
            txt_pasillo.Text = Strings.Left(constantes.consolidacion_A01, 2)
            txt_portal.Text = Strings.Mid(constantes.consolidacion_A01, 4, 2)
            txt_altura.Text = Strings.Right(constantes.consolidacion_A01, 2)
        End If
        If list_almacen.SelectedValue = "A07" Then
            txt_pasillo.Text = Strings.Left(constantes.consolidacion_A07, 2)
            txt_portal.Text = Strings.Mid(constantes.consolidacion_A07, 4, 2)
            txt_altura.Text = Strings.Right(constantes.consolidacion_A07, 2)
        End If
        list_tipo.Text = "Picking"
        txt_cantmax.Text = 0
        txt_tamaño.Text = 0
    End Sub
End Class