﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_inventarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.p_añadir = New System.Windows.Forms.Panel()
        Me.btn_NuevaUbi = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lab_pickingact_ubi = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lab_tit_ubi = New System.Windows.Forms.Label()
        Me.lab_stockact = New System.Windows.Forms.Label()
        Me.lab_txt_stock = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.list_ubi = New System.Windows.Forms.ComboBox()
        Me.lab_otrasact = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lab_cantnoprep = New System.Windows.Forms.Label()
        Me.lab_22 = New System.Windows.Forms.Label()
        Me.lab_canttotal_despues = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lab_almpick = New System.Windows.Forms.Label()
        Me.lab_ubipick = New System.Windows.Forms.Label()
        Me.lab_stockact_tot = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lab_pickingact = New System.Windows.Forms.Label()
        Me.lab_pulmonact = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_añadir = New System.Windows.Forms.Button()
        Me.txt_cantidadpick = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.txt_articulo = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lab_ultUbi = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lab_alm = New System.Windows.Forms.Label()
        Me.list_almacen = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lab_ultmodificacion = New System.Windows.Forms.Label()
        Me.lab_fechacreacion = New System.Windows.Forms.Label()
        Me.lab_creadopor = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_nuevo = New System.Windows.Forms.Button()
        Me.list_inventarios = New System.Windows.Forms.ComboBox()
        Me.dg_lineas_inventario = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        Me.p_añadir.SuspendLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dg_lineas_inventario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Bisque
        Me.Panel2.Controls.Add(Me.p_añadir)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.dg_lineas_inventario)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Location = New System.Drawing.Point(12, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1251, 640)
        Me.Panel2.TabIndex = 26
        '
        'p_añadir
        '
        Me.p_añadir.BackColor = System.Drawing.Color.PaleTurquoise
        Me.p_añadir.Controls.Add(Me.btn_NuevaUbi)
        Me.p_añadir.Controls.Add(Me.Label17)
        Me.p_añadir.Controls.Add(Me.Label20)
        Me.p_añadir.Controls.Add(Me.lab_pickingact_ubi)
        Me.p_añadir.Controls.Add(Me.Label19)
        Me.p_añadir.Controls.Add(Me.Label13)
        Me.p_añadir.Controls.Add(Me.lab_tit_ubi)
        Me.p_añadir.Controls.Add(Me.lab_stockact)
        Me.p_añadir.Controls.Add(Me.lab_txt_stock)
        Me.p_añadir.Controls.Add(Me.Label16)
        Me.p_añadir.Controls.Add(Me.list_ubi)
        Me.p_añadir.Controls.Add(Me.lab_otrasact)
        Me.p_añadir.Controls.Add(Me.Label15)
        Me.p_añadir.Controls.Add(Me.Label5)
        Me.p_añadir.Controls.Add(Me.lab_cantnoprep)
        Me.p_añadir.Controls.Add(Me.lab_22)
        Me.p_añadir.Controls.Add(Me.lab_canttotal_despues)
        Me.p_añadir.Controls.Add(Me.Label10)
        Me.p_añadir.Controls.Add(Me.lab_almpick)
        Me.p_añadir.Controls.Add(Me.lab_ubipick)
        Me.p_añadir.Controls.Add(Me.lab_stockact_tot)
        Me.p_añadir.Controls.Add(Me.Label11)
        Me.p_añadir.Controls.Add(Me.lab_pickingact)
        Me.p_añadir.Controls.Add(Me.lab_pulmonact)
        Me.p_añadir.Controls.Add(Me.Label9)
        Me.p_añadir.Controls.Add(Me.Label8)
        Me.p_añadir.Controls.Add(Me.Label6)
        Me.p_añadir.Controls.Add(Me.btn_añadir)
        Me.p_añadir.Controls.Add(Me.txt_cantidadpick)
        Me.p_añadir.Controls.Add(Me.Label12)
        Me.p_añadir.Controls.Add(Me.imagen)
        Me.p_añadir.Controls.Add(Me.txt_articulo)
        Me.p_añadir.Location = New System.Drawing.Point(7, 116)
        Me.p_añadir.Name = "p_añadir"
        Me.p_añadir.Size = New System.Drawing.Size(1235, 180)
        Me.p_añadir.TabIndex = 54
        '
        'btn_NuevaUbi
        '
        Me.btn_NuevaUbi.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_NuevaUbi.Location = New System.Drawing.Point(562, 7)
        Me.btn_NuevaUbi.Name = "btn_NuevaUbi"
        Me.btn_NuevaUbi.Size = New System.Drawing.Size(52, 46)
        Me.btn_NuevaUbi.TabIndex = 91
        Me.btn_NuevaUbi.Text = "+"
        Me.btn_NuevaUbi.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(591, 113)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(167, 29)
        Me.Label17.TabIndex = 90
        Me.Label17.Text = "Cantidad Act:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Red
        Me.Label20.Location = New System.Drawing.Point(710, 113)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(57, 29)
        Me.Label20.TabIndex = 88
        Me.Label20.Text = "A01"
        '
        'lab_pickingact_ubi
        '
        Me.lab_pickingact_ubi.AutoSize = True
        Me.lab_pickingact_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pickingact_ubi.ForeColor = System.Drawing.Color.Red
        Me.lab_pickingact_ubi.Location = New System.Drawing.Point(785, 113)
        Me.lab_pickingact_ubi.Name = "lab_pickingact_ubi"
        Me.lab_pickingact_ubi.Size = New System.Drawing.Size(67, 29)
        Me.lab_pickingact_ubi.TabIndex = 87
        Me.lab_pickingact_ubi.Text = "XXX"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Green
        Me.Label19.Location = New System.Drawing.Point(294, 61)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(125, 17)
        Me.Label19.TabIndex = 86
        Me.Label19.Text = "Stock en Almacén."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(591, 146)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(284, 29)
        Me.Label13.TabIndex = 77
        Me.Label13.Text = "Pulm.+Pick.+Otr.Desp.:"
        '
        'lab_tit_ubi
        '
        Me.lab_tit_ubi.AutoSize = True
        Me.lab_tit_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tit_ubi.ForeColor = System.Drawing.Color.Green
        Me.lab_tit_ubi.Location = New System.Drawing.Point(31, 61)
        Me.lab_tit_ubi.Name = "lab_tit_ubi"
        Me.lab_tit_ubi.Size = New System.Drawing.Size(144, 17)
        Me.lab_tit_ubi.TabIndex = 85
        Me.lab_tit_ubi.Text = "Stock en Ubicaciones"
        '
        'lab_stockact
        '
        Me.lab_stockact.AutoSize = True
        Me.lab_stockact.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stockact.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_stockact.Location = New System.Drawing.Point(480, 81)
        Me.lab_stockact.Name = "lab_stockact"
        Me.lab_stockact.Size = New System.Drawing.Size(64, 29)
        Me.lab_stockact.TabIndex = 84
        Me.lab_stockact.Text = "XXX"
        Me.lab_stockact.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_txt_stock
        '
        Me.lab_txt_stock.AutoSize = True
        Me.lab_txt_stock.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_txt_stock.Location = New System.Drawing.Point(292, 81)
        Me.lab_txt_stock.Name = "lab_txt_stock"
        Me.lab_txt_stock.Size = New System.Drawing.Size(136, 29)
        Me.lab_txt_stock.TabIndex = 83
        Me.lab_txt_stock.Text = "Stock Alm:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(299, 11)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(82, 39)
        Me.Label16.TabIndex = 82
        Me.Label16.Text = "Ubi:"
        '
        'list_ubi
        '
        Me.list_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_ubi.FormattingEnabled = True
        Me.list_ubi.Location = New System.Drawing.Point(384, 7)
        Me.list_ubi.Name = "list_ubi"
        Me.list_ubi.Size = New System.Drawing.Size(160, 46)
        Me.list_ubi.TabIndex = 81
        '
        'lab_otrasact
        '
        Me.lab_otrasact.AutoSize = True
        Me.lab_otrasact.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_otrasact.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_otrasact.Location = New System.Drawing.Point(193, 146)
        Me.lab_otrasact.Name = "lab_otrasact"
        Me.lab_otrasact.Size = New System.Drawing.Size(64, 29)
        Me.lab_otrasact.TabIndex = 80
        Me.lab_otrasact.Text = "XXX"
        Me.lab_otrasact.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(29, 146)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(150, 29)
        Me.Label15.TabIndex = 79
        Me.Label15.Text = "Otras Ubic.:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(591, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(117, 29)
        Me.Label5.TabIndex = 78
        Me.Label5.Text = "Ubi.Pick:"
        '
        'lab_cantnoprep
        '
        Me.lab_cantnoprep.AutoSize = True
        Me.lab_cantnoprep.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_cantnoprep.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_cantnoprep.Location = New System.Drawing.Point(480, 114)
        Me.lab_cantnoprep.Name = "lab_cantnoprep"
        Me.lab_cantnoprep.Size = New System.Drawing.Size(64, 29)
        Me.lab_cantnoprep.TabIndex = 76
        Me.lab_cantnoprep.Text = "XXX"
        Me.lab_cantnoprep.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_22
        '
        Me.lab_22.AutoSize = True
        Me.lab_22.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_22.Location = New System.Drawing.Point(292, 114)
        Me.lab_22.Name = "lab_22"
        Me.lab_22.Size = New System.Drawing.Size(122, 29)
        Me.lab_22.TabIndex = 75
        Me.lab_22.Text = "No prep.:"
        '
        'lab_canttotal_despues
        '
        Me.lab_canttotal_despues.AutoSize = True
        Me.lab_canttotal_despues.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_canttotal_despues.Location = New System.Drawing.Point(888, 146)
        Me.lab_canttotal_despues.Name = "lab_canttotal_despues"
        Me.lab_canttotal_despues.Size = New System.Drawing.Size(27, 29)
        Me.lab_canttotal_despues.TabIndex = 71
        Me.lab_canttotal_despues.Text = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Red
        Me.Label10.Location = New System.Drawing.Point(763, 82)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(22, 29)
        Me.Label10.TabIndex = 70
        Me.Label10.Text = "-"
        '
        'lab_almpick
        '
        Me.lab_almpick.AutoSize = True
        Me.lab_almpick.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_almpick.ForeColor = System.Drawing.Color.Red
        Me.lab_almpick.Location = New System.Drawing.Point(710, 82)
        Me.lab_almpick.Name = "lab_almpick"
        Me.lab_almpick.Size = New System.Drawing.Size(57, 29)
        Me.lab_almpick.TabIndex = 69
        Me.lab_almpick.Text = "A01"
        '
        'lab_ubipick
        '
        Me.lab_ubipick.AutoSize = True
        Me.lab_ubipick.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_ubipick.ForeColor = System.Drawing.Color.Red
        Me.lab_ubipick.Location = New System.Drawing.Point(785, 82)
        Me.lab_ubipick.Name = "lab_ubipick"
        Me.lab_ubipick.Size = New System.Drawing.Size(67, 29)
        Me.lab_ubipick.TabIndex = 68
        Me.lab_ubipick.Text = "XXX"
        '
        'lab_stockact_tot
        '
        Me.lab_stockact_tot.AutoSize = True
        Me.lab_stockact_tot.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stockact_tot.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_stockact_tot.Location = New System.Drawing.Point(480, 146)
        Me.lab_stockact_tot.Name = "lab_stockact_tot"
        Me.lab_stockact_tot.Size = New System.Drawing.Size(64, 29)
        Me.lab_stockact_tot.TabIndex = 67
        Me.lab_stockact_tot.Text = "XXX"
        Me.lab_stockact_tot.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(292, 146)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 29)
        Me.Label11.TabIndex = 66
        Me.Label11.Text = "Total:"
        '
        'lab_pickingact
        '
        Me.lab_pickingact.AutoSize = True
        Me.lab_pickingact.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pickingact.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_pickingact.Location = New System.Drawing.Point(193, 113)
        Me.lab_pickingact.Name = "lab_pickingact"
        Me.lab_pickingact.Size = New System.Drawing.Size(64, 29)
        Me.lab_pickingact.TabIndex = 65
        Me.lab_pickingact.Text = "XXX"
        Me.lab_pickingact.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_pulmonact
        '
        Me.lab_pulmonact.AutoSize = True
        Me.lab_pulmonact.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pulmonact.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_pulmonact.Location = New System.Drawing.Point(193, 81)
        Me.lab_pulmonact.Name = "lab_pulmonact"
        Me.lab_pulmonact.Size = New System.Drawing.Size(64, 29)
        Me.lab_pulmonact.TabIndex = 64
        Me.lab_pulmonact.Text = "XXX"
        Me.lab_pulmonact.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(29, 113)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(156, 29)
        Me.Label9.TabIndex = 63
        Me.Label9.Text = "Picking Act.:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(29, 81)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(158, 29)
        Me.Label8.TabIndex = 62
        Me.Label8.Text = "Pulmón Act.:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(708, 11)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(192, 39)
        Me.Label6.TabIndex = 61
        Me.Label6.Text = "Cant.Pick.:"
        '
        'btn_añadir
        '
        Me.btn_añadir.Enabled = False
        Me.btn_añadir.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_añadir.Location = New System.Drawing.Point(907, 61)
        Me.btn_añadir.Name = "btn_añadir"
        Me.btn_añadir.Size = New System.Drawing.Size(103, 71)
        Me.btn_añadir.TabIndex = 60
        Me.btn_añadir.Text = "OK"
        Me.btn_añadir.UseVisualStyleBackColor = True
        '
        'txt_cantidadpick
        '
        Me.txt_cantidadpick.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidadpick.Location = New System.Drawing.Point(907, 8)
        Me.txt_cantidadpick.MaxLength = 13
        Me.txt_cantidadpick.Name = "txt_cantidadpick"
        Me.txt_cantidadpick.Size = New System.Drawing.Size(103, 45)
        Me.txt_cantidadpick.TabIndex = 44
        Me.txt_cantidadpick.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(27, 11)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(93, 39)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Ref.:"
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(1051, 9)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(150, 150)
        Me.imagen.TabIndex = 41
        Me.imagen.TabStop = False
        '
        'txt_articulo
        '
        Me.txt_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_articulo.Location = New System.Drawing.Point(126, 8)
        Me.txt_articulo.MaxLength = 13
        Me.txt_articulo.Name = "txt_articulo"
        Me.txt_articulo.Size = New System.Drawing.Size(139, 45)
        Me.txt_articulo.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SandyBrown
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lab_ultUbi)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.lab_alm)
        Me.Panel1.Controls.Add(Me.list_almacen)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.lab_ultmodificacion)
        Me.Panel1.Controls.Add(Me.lab_fechacreacion)
        Me.Panel1.Controls.Add(Me.lab_creadopor)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btn_nuevo)
        Me.Panel1.Controls.Add(Me.list_inventarios)
        Me.Panel1.Location = New System.Drawing.Point(7, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1235, 98)
        Me.Panel1.TabIndex = 53
        '
        'lab_ultUbi
        '
        Me.lab_ultUbi.AutoSize = True
        Me.lab_ultUbi.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lab_ultUbi.Location = New System.Drawing.Point(193, 67)
        Me.lab_ultUbi.Name = "lab_ultUbi"
        Me.lab_ultUbi.Size = New System.Drawing.Size(90, 24)
        Me.lab_ultUbi.TabIndex = 64
        Me.lab_ultUbi.Text = "00-00-00"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label18.Location = New System.Drawing.Point(17, 67)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(172, 24)
        Me.Label18.TabIndex = 63
        Me.Label18.Text = "Última Ubicación:"
        '
        'lab_alm
        '
        Me.lab_alm.AutoSize = True
        Me.lab_alm.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_alm.Location = New System.Drawing.Point(341, -1)
        Me.lab_alm.Name = "lab_alm"
        Me.lab_alm.Size = New System.Drawing.Size(138, 63)
        Me.lab_alm.TabIndex = 62
        Me.lab_alm.Text = "XXX"
        '
        'list_almacen
        '
        Me.list_almacen.Enabled = False
        Me.list_almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almacen.FormattingEnabled = True
        Me.list_almacen.Location = New System.Drawing.Point(1143, 36)
        Me.list_almacen.Name = "list_almacen"
        Me.list_almacen.Size = New System.Drawing.Size(81, 33)
        Me.list_almacen.TabIndex = 61
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(1153, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(56, 25)
        Me.Label14.TabIndex = 60
        Me.Label14.Text = "Alm:"
        '
        'lab_ultmodificacion
        '
        Me.lab_ultmodificacion.AutoSize = True
        Me.lab_ultmodificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lab_ultmodificacion.Location = New System.Drawing.Point(683, 60)
        Me.lab_ultmodificacion.Name = "lab_ultmodificacion"
        Me.lab_ultmodificacion.Size = New System.Drawing.Size(55, 24)
        Me.lab_ultmodificacion.TabIndex = 59
        Me.lab_ultmodificacion.Text = "XXX"
        '
        'lab_fechacreacion
        '
        Me.lab_fechacreacion.AutoSize = True
        Me.lab_fechacreacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lab_fechacreacion.Location = New System.Drawing.Point(683, 34)
        Me.lab_fechacreacion.Name = "lab_fechacreacion"
        Me.lab_fechacreacion.Size = New System.Drawing.Size(55, 24)
        Me.lab_fechacreacion.TabIndex = 58
        Me.lab_fechacreacion.Text = "XXX"
        '
        'lab_creadopor
        '
        Me.lab_creadopor.AutoSize = True
        Me.lab_creadopor.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lab_creadopor.Location = New System.Drawing.Point(683, 6)
        Me.lab_creadopor.Name = "lab_creadopor"
        Me.lab_creadopor.Size = New System.Drawing.Size(55, 24)
        Me.lab_creadopor.TabIndex = 57
        Me.lab_creadopor.Text = "XXX"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(512, 60)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(103, 24)
        Me.Label4.TabIndex = 56
        Me.Label4.Text = "Ult.Modif.:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(512, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(165, 24)
        Me.Label3.TabIndex = 55
        Me.Label3.Text = "Fecha Creación:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(512, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 24)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "Creado por:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(187, 39)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Inventario:"
        '
        'btn_nuevo
        '
        Me.btn_nuevo.Enabled = False
        Me.btn_nuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nuevo.Location = New System.Drawing.Point(1013, 8)
        Me.btn_nuevo.Name = "btn_nuevo"
        Me.btn_nuevo.Size = New System.Drawing.Size(124, 67)
        Me.btn_nuevo.TabIndex = 52
        Me.btn_nuevo.Text = "Nuevo"
        Me.btn_nuevo.UseVisualStyleBackColor = True
        '
        'list_inventarios
        '
        Me.list_inventarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_inventarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_inventarios.FormattingEnabled = True
        Me.list_inventarios.Location = New System.Drawing.Point(207, 3)
        Me.list_inventarios.Name = "list_inventarios"
        Me.list_inventarios.Size = New System.Drawing.Size(128, 54)
        Me.list_inventarios.TabIndex = 51
        '
        'dg_lineas_inventario
        '
        Me.dg_lineas_inventario.AllowUserToAddRows = False
        Me.dg_lineas_inventario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas_inventario.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dg_lineas_inventario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_lineas_inventario.DefaultCellStyle = DataGridViewCellStyle2
        Me.dg_lineas_inventario.Location = New System.Drawing.Point(12, 338)
        Me.dg_lineas_inventario.MultiSelect = False
        Me.dg_lineas_inventario.Name = "dg_lineas_inventario"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas_inventario.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dg_lineas_inventario.RowTemplate.Height = 40
        Me.dg_lineas_inventario.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dg_lineas_inventario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas_inventario.Size = New System.Drawing.Size(1229, 285)
        Me.dg_lineas_inventario.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 310)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(176, 25)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Lineas inventario"
        '
        'frm_inventarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1275, 664)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_inventarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_inventarios"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.p_añadir.ResumeLayout(False)
        Me.p_añadir.PerformLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dg_lineas_inventario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dg_lineas_inventario As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents p_añadir As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btn_añadir As System.Windows.Forms.Button
    Friend WithEvents txt_cantidadpick As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents imagen As System.Windows.Forms.PictureBox
    Friend WithEvents txt_articulo As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lab_ultmodificacion As System.Windows.Forms.Label
    Friend WithEvents lab_fechacreacion As System.Windows.Forms.Label
    Friend WithEvents lab_creadopor As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_nuevo As System.Windows.Forms.Button
    Friend WithEvents list_inventarios As System.Windows.Forms.ComboBox
    Friend WithEvents lab_pickingact As System.Windows.Forms.Label
    Friend WithEvents lab_pulmonact As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lab_stockact_tot As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lab_ubipick As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lab_almpick As System.Windows.Forms.Label
    Friend WithEvents lab_canttotal_despues As System.Windows.Forms.Label
    Friend WithEvents lab_22 As System.Windows.Forms.Label
    Friend WithEvents lab_cantnoprep As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lab_otrasact As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents list_almacen As System.Windows.Forms.ComboBox
    Friend WithEvents lab_alm As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents list_ubi As System.Windows.Forms.ComboBox
    Friend WithEvents lab_stockact As System.Windows.Forms.Label
    Friend WithEvents lab_txt_stock As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lab_tit_ubi As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lab_pickingact_ubi As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btn_NuevaUbi As Button
    Friend WithEvents lab_ultUbi As Label
    Friend WithEvents Label18 As Label
End Class
