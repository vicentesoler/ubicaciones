﻿Public Class frm_reaprov
    Dim conexion As New Conector
    Private Sub frm_reaprov_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        limpiar()
        llenar_reaprov()
    End Sub
    Public Sub llenar_reaprov()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer

        list_codreaprov.DataSource = Nothing
        list_codreaprov.Items.Clear()
        sql = "select codreaprov from reaprov where procesado ='NO' order by codreaprov asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        cont = 0
        list_codreaprov.Text = ""
        list_codreaprov.Items.Clear()
        Do While cont < vdatos.Rows.Count
            list_codreaprov.Items.Add(vdatos.Rows(cont).Item("codreaprov"))
            cont += 1
        Loop
    End Sub
    Private Sub list_codreaprov_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles list_codreaprov.KeyUp
        If e.KeyValue = Keys.Enter Then
            If list_codreaprov.Text.Length = 13 Then
                list_codreaprov.Text = list_codreaprov.Text.Substring(7, 5)
            End If
            datos_cabecera()
        End If
    End Sub
    Public Sub datos_cabecera()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0

        sql = "select cab.codalmacenori,codalmacendest,procesado,origen "
        sql &= "from  reaprov cab  "
        sql &= "where cab.codreaprov=" & list_codreaprov.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows.Count = 1 Then
            If vdatos.Rows(0).Item("procesado") = "SI" Then
                If MsgBox("Este Reaprov. ya ha sido procesado, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    limpiar()
                    Exit Sub
                End If
            End If

            lab_almorigen.Text = vdatos.Rows(0).Item("codalmacenori")
            lab_almdestino.Text = vdatos.Rows(0).Item("codalmacendest")
            lab_almdestino.Visible = True
            lab_almorigen.Visible = True
            lab_flecha.Visible = True
            If vdatos.Rows(0).Item("origen") = "Ubicaciones" Then
                list_ubicacion_origen.DataSource = Nothing
                'No relleno el listbox porque en la linea de orden de reaprov viene la ubicacion destino.
            End If
            If vdatos.Rows(0).Item("origen") = "Lanz.Pedidos" Then
                list_ubicacion_destino.Items.Clear()
                If lab_almdestino.Text = "A07" Then list_ubicacion_destino.Items.Add(constantes.consolidacion_A07)
                If lab_almdestino.Text = "A01" Then list_ubicacion_destino.Items.Add(constantes.consolidacion_A01)
                If lab_almdestino.Text = "A08" Then list_ubicacion_destino.Items.Add(constantes.consolidacion_A08)
                list_ubicacion_destino.SelectedIndex = 0
            End If
            cargar_lineas(vdatos.Rows(0).Item("origen"))
        Else
            MsgBox("No existe el reaprovisionamiento seleccionado!.", MsgBoxStyle.Exclamation)
            limpiar()
            Exit Sub
        End If
    End Sub
    Public Sub cargar_lineas(ByVal origen As String)
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0
        Dim ubidestino As String
        If lab_almdestino.Text = "A01" Then ubidestino = constantes.traslados_A01
        If lab_almdestino.Text = "A07" Then ubidestino = constantes.traslados_A07
        If lab_almdestino.Text = "A08" Then ubidestino = constantes.traslados_A08


        sql = "select codlinea as 'Lin.',lin.codarticulo as 'Ref.',art.descripcion as 'Descripcion',lin.ubicacion_origen as 'Ubi.Ori.',lin.ubicacion_destino as 'Ubi.Dest.',"
        sql &= "lin.cantidad as 'Cant.',canttrasladada as 'Prep.' "
        sql &= "from reaprovlin lin "
        sql &= "inner join reaprov cab on cab.codreaprov =lin.codreaprov "
        sql &= "inner join inase.dbo.GesArticulos art on art.codempresa=1 and art.codarticulo collate Modern_Spanish_CI_AS = lin.codarticulo "
        sql &= "where lin.codreaprov=" & list_codreaprov.Text & " "
        sql &= "order by lin.codarticulo asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_lineas.DataSource = vdatos
            dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 20)
            dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 20, FontStyle.Bold)
            'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_lineas.Columns(0).Width = 50
            dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(1).Width = 150
            dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dg_lineas.Columns(2).Width = 350
            dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(3).Width = 150
            dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(4).Width = 150
            dg_lineas.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(5).Width = 100
            dg_lineas.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(6).Width = 100




        Else
            MsgBox("No se encontro el reaprov. especificado.", MsgBoxStyle.Critical)
            list_codreaprov.SelectAll()
        End If

        pintar_lineas_grid()
        'txt_articulo.Focus()
        'txt_articulo.SelectAll()
    End Sub

    Public Sub pintar_lineas_grid()
        Dim cont As Integer
        cont = 0
        While cont < dg_lineas.RowCount
            Select Case dg_lineas.Rows(cont).Cells("Prep.").Value
                Case 0
                Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
                Case Is > dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Red
                Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
            End Select

            'Situar el cursor en la linea q acabamos de marcar
            'If dg_lineasalb.Rows(cont).Cells("Referencia").Value = ult_ref_embalada And (linea_click = 0 Or dg_lineasalb.Rows(cont).Cells("Lin").Value = linea_click) Then
            '    dg_lineasalb.Item(0, dg_lineasalb.RowCount - 1).Selected = True
            '    dg_lineasalb.Item(0, cont).Selected = True
            'End If

            cont += 1
        End While
        'If ult_ref_embalada = "" Then dg_lineasalb.Item(0, dg_lineasalb.CurrentRow.Index).Selected = False

    End Sub

    Private Sub list_codreaprov_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_codreaprov.SelectedIndexChanged
        If list_codreaprov.SelectedIndex = -1 Then
            limpiar()
        Else
            datos_cabecera()
        End If
    End Sub


    Private Sub dg_lineas_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_lineas.CellEnter
        imagen.ImageLocation = "\\jserver\BAJA\" & dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value & ".jpg"
        imagen.SizeMode = PictureBoxSizeMode.StretchImage
        txt_articulo.Text = dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value
        lab_descripcion.Text = dg_lineas.Rows(e.RowIndex).Cells("Descripcion").Value
        txt_cantidad.Text = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value - dg_lineas.Rows(e.RowIndex).Cells("Prep.").Value
        Select Case dg_lineas.Rows(e.RowIndex).Cells("Prep.").Value
            Case 0 : txt_articulo.BackColor = Color.White
            Case Is = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Green
            Case Is < dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Yellow
            Case Is > dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Red
        End Select
        If dg_lineas.Rows(e.RowIndex).Cells("Ubi.Ori.").Value.ToString <> "" Then
            list_ubicacion_origen.DataSource = Nothing
            list_ubicacion_origen.Items.Clear()
            list_ubicacion_origen.Items.Add(dg_lineas.Rows(e.RowIndex).Cells("Ubi.Ori.").Value)
            list_ubicacion_origen.SelectedIndex = 0
        Else
            funciones.llenar_ubicaciones(list_ubicacion_origen, dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, lab_almorigen.Text, "")
        End If
        If dg_lineas.Rows(e.RowIndex).Cells("Ubi.Dest.").Value.ToString <> "" Then
            list_ubicacion_destino.Items.Clear()
            list_ubicacion_destino.Items.Add(dg_lineas.Rows(e.RowIndex).Cells("Ubi.Dest.").Value)
            list_ubicacion_destino.SelectedIndex = 0
        End If
        btn_ok.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub



    Private Sub txt_cantidad_Click(sender As System.Object, e As System.EventArgs) Handles txt_cantidad.Click
        txt_cantidad.SelectAll()
    End Sub

    Private Sub btn_ok_Click(sender As System.Object, e As System.EventArgs) Handles btn_ok.Click
        Dim sql As String = ""
        Dim sql2 As String = ""
        Dim resultado As String
        Dim vcant, vimporte, vubi, vdatosant As New DataTable
        Dim sinubi As Boolean = False
        Dim cantant As Integer = 0


        If dg_lineas.SelectedRows.Count = 0 Then Exit Sub

        If list_ubicacion_origen.Text = "" Then
            'MsgBox("La referencia no tiene ubicación en el almacen " & lab_almorigen.Text & ", no se puede confirmar esta referencia.", MsgBoxStyle.Information)
            'Exit Sub
            'No dejaba continuar si la ref no tiene ubicacion pero sergio me dijo que si, que deje continuar
            sinubi = True
        End If

        'If lab_almdestino.Text = "A01" Then consolidacion = constantes.consolidacion_A01
        'If lab_almdestino.Text = "A07" Then consolidacion = constantes.consolidacion_A07

        If txt_cantidad.Text = 0 Then
            If MsgBox("Seguro que quiere eliminar la cantidad preparada?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

            'Obtener la cantidad que tiene hasta el momento la linea preparada para sumarla al stock y dejar el stock en el picking cuadrado
            sql = "select canttrasladada from reaprovlin where codreaprov=" & list_codreaprov.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
            vcant = conexion.TablaxCmd(constantes.bd_intranet, sql)
            sql = ""
            If vcant.Rows.Count = 1 Then
                cantant = CInt(vcant.Rows(0)(0))
                'Dejar stock como estaba en la ubicacion origen
                If sinubi = False Then sql = "update ubicaciones set cantidad=cantidad+" & cantant & " where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & list_ubicacion_origen.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & list_ubicacion_origen.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad asc ) "

                'Dejar stock como estaba en la ubicacion destino. 
                sql &= "update ubicaciones set cantidad=cantidad-" & cantant & " where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & list_ubicacion_destino.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & list_ubicacion_destino.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad asc ) "

            End If
            'Actualizar la linea de reaprov
            sql &= "update reaprovlin set canttrasladada=0,ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultfecha=getdate() where codreaprov=" & list_codreaprov.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
            'Poner la cantidad en el grid
            dg_lineas.SelectedRows(0).Cells("Prep.").Value = 0
        Else
            'Actualizar el Stock en la ubicación origen
            If sinubi = False Then
                sql &= "update ubicaciones set cantidad=isnull(cantidad,0)-" & CInt(txt_cantidad.Text) & " where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & list_ubicacion_origen.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno.
                'Simon me pidio que se actualizara siempre el de menor cantidad
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & list_ubicacion_origen.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim.ToUpper & "' order by cantidad asc ) "
            End If


            'Mirar si existe la ref en la ubicación destino
            sql2 = "select id from ubicaciones where codalmacen='" & lab_almdestino.Text & "'  and ubicacion='" & list_ubicacion_destino.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
            vubi = conexion.TablaxCmd(constantes.bd_intranet, sql2)
            If vubi.Rows.Count = 0 Then
                'Crear el registro en la ubicación destino
                sql &= "insert into ubicaciones (codalmacen,ubicacion,codarticulo,tipo,cantidad,observaciones,ultusuario) values('" & lab_almdestino.Text & "','" & list_ubicacion_destino.Text & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim.ToUpper & "','Picking'," & CInt(txt_cantidad.Text) & ",'Ubicaciones-Reaprov','" & frm_principal.lab_nomoperario.Text & "')"
            Else
                'Actualizar la ubicacion destino
                sql &= "update ubicaciones set cantidad=isnull(cantidad,0)+" & CInt(txt_cantidad.Text) & " where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & list_ubicacion_destino.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & list_ubicacion_destino.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim.ToUpper & "' order by cantidad asc ) "
            End If

            'Actualizar la linea de reaprov
            sql &= "update reaprovlin set canttrasladada=" & CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Prep.").Value.ToString) & ",ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultfecha=getdate() "
            sql &= "where codreaprov=" & list_codreaprov.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
            'Poner la cantidad en el grid
            dg_lineas.SelectedRows(0).Cells("Prep.").Value = CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Prep.").Value.ToString)
        End If

        'Origen
        funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, (CInt(txt_cantidad.Text) * -1), lab_almorigen.Text, list_ubicacion_origen.Text, "Reaprov:" & list_codreaprov.Text, "Ubicaciones-Reaprov", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
        'Destino
        funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, CInt(txt_cantidad.Text) - cantant, lab_almdestino.Text, list_ubicacion_destino.Text, "Reaprov:" & list_codreaprov.Text, "Ubicaciones-Reaprov", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)

        If resultado <> "0" Then
            MsgBox("Se produjo un error al marcar la linea.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        'Pintar fila
        Select Case CInt(dg_lineas.SelectedRows(0).Cells("Prep.").Value.ToString)
            Case 0 : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow
            Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Red
        End Select

        If dg_lineas.CurrentRow.Index < dg_lineas.Rows.Count - 1 Then
            dg_lineas.Item(0, dg_lineas.CurrentRow.Index + 1).Selected = True
        Else
            dg_lineas.Item(0, dg_lineas.CurrentRow.Index).Selected = False
            txt_articulo.Text = ""
            txt_articulo.BackColor = Color.White
            lab_descripcion.Text = ""
            txt_cantidad.Text = ""
            list_ubicacion_origen.DataSource = Nothing
            list_ubicacion_origen.Items.Clear()
            btn_ok.Enabled = False
        End If
        btn_finalizar.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub

    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql As String = ""
        Dim resultado As String
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim proxmov As Integer = 0
        Dim numrefmov As String = ""
        Dim lineasprep As Integer = 0
        Dim pedidoincompleto As Boolean = False

        'Revisar si el pedido esta completamente servido
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("Prep.").Value Then
                pedidoincompleto = True
            End If
            If dg_lineas.Rows(cont).Cells("Prep.").Value > 0 Then
                lineasprep += 1
            End If
            cont += 1
        Loop


        If pedidoincompleto = True Then
            If MsgBox("El reaprov. no está completamente terminado, finalizar de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
        End If


        'Esto lo hacia antes para todos los reaprov., ya que en la linea del albaran no cambiaba el almacen y por tanto ahora tenia q pasar el stock de un almacen al otro
        'para dejar el stock bien. Ahora cambio el almacen en la linea del albaran , por tanto cuando es un reaprov. creado desde el lanz. de pedidos no hay que 
        'hacer el cambio de almacen, pero si es creado manualmente desde ubicaciones si

        sql = "select origen from Reaprov where codreaprov=" & list_codreaprov.Text & " "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows(0).Item("origen").ToString = "Ubicaciones" And lineasprep > 0 Then
            'Cabecera de movimiento. Solo si viene de un reaprov. creado desde ubicaciones y tiene alguna linea confirmada
            resultado = funciones.crear_movimiento_inase(frm_principal.codempresa, proxmov, "Movimiento para Reaprovisionamiento", numrefmov)
            If resultado <> "OK" Then
                MsgBox("Se produjo un error al crear la cabecara del movimiento", MsgBoxStyle.Critical)
                Exit Sub
            End If

            cont = 0
            sql = ""
            Do While cont < dg_lineas.Rows.Count
                If dg_lineas.Rows(cont).Cells("Prep.").Value > 0 Then
                    'Lineas Movimiento almacen
                    sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacenori,ubicacionori,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                    sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & frm_principal.codempresa & "," & Now.Year & "," & proxmov & "," & cont + 1 & ",'" & lab_almorigen.Text & "','XXX','" & lab_almdestino.Text & "','XXX','" & dg_lineas.Rows(cont).Cells("Ref.").Value & "'," & dg_lineas.Rows(cont).Cells("Prep.").Value & ",'Salida por Reaprov.') "
                    'Eliminar la referencia de la ubicacion origen si se ha quedado a 0. La cantidad ya se ha restado al marcar la linea.
                    'Solo si la ubicacion que se ha quedado a 0 es un pulmon. Pedido por Sergio
                    sql &= "delete intranet.dbo.ubicaciones where tipo='Pulmon' and codarticulo='" & dg_lineas.Rows(cont).Cells("Ref.").Value & "' and codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & dg_lineas.Rows(cont).Cells("Ubi.Ori.").Value & "' and cantidad=0 "

                End If
                cont += 1
            Loop

            resultado = conexion.Executetransact(constantes.bd_inase, sql)
            If resultado <> "0" Then
                resultado = funciones.borrar_movimiento_inase(frm_principal.codempresa, numrefmov)
                MsgBox("Se produjo un error al insertar las lineas del movimiento. El Reaprov. no ha sido finalizado.", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If


        'Marcar como procesado el reaprov
        sql = "update reaprov set procesado='SI',ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultfecha=getdate() where codreaprov=" & list_codreaprov.Text
        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado = "0" Then
            MsgBox("Movimiento de reaprovisionamiento finalizado correctamente", MsgBoxStyle.Information)
        Else
            MsgBox("Se produjo un error al marcar el reaprov. como procesado. Avisar a Informática", MsgBoxStyle.Information)
        End If

        'Borrar todos los registros de la tabla ubicaciones que sean de la ubicacion de consolidado/traslados con cantidad a 0
        'Esto es para que no se quede basura
        'Eliminado porque a veces se crea una ubicación con cantidad a 0 para usarla luego y se borraba al momento. 19/07/2019. 
        'Ahora lo borro por las noches
        'sql = "delete ubicaciones where ((codalmacen='A01' and ubicacion='" & constantes.consolidacion_A01 & "' ) or (codalmacen='A07' and ubicacion='" & constantes.consolidacion_A07 & "')  or (codalmacen='A08' and ubicacion='" & constantes.consolidacion_A08 & "') or (codalmacen='A01' and ubicacion='" & constantes.traslados_A01 & "' ) or (codalmacen='A07' and ubicacion='" & constantes.traslados_A07 & "') or (codalmacen='A08' and ubicacion='" & constantes.traslados_A08 & "') ) and cantidad=0 "
        'resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)


        limpiar()
        llenar_reaprov()
    End Sub

    Public Sub limpiar()
        lab_flecha.Visible = False
        lab_almdestino.Visible = False
        lab_almorigen.Visible = False
        txt_articulo.Text = ""
        txt_articulo.BackColor = Color.White
        lab_descripcion.Text = ""
        txt_cantidad.Text = ""
        list_ubicacion_origen.DataSource = Nothing
        list_ubicacion_origen.Items.Clear()
        btn_ok.Enabled = False
        'btn_finalizar.Enabled = False
        dg_lineas.DataSource = Nothing
        dg_lineas.Rows.Clear()
        imagen.ImageLocation = ""
        list_codreaprov.SelectedIndex = -1
    End Sub

    Private Sub btn_nuevoreaprov_Click(sender As System.Object, e As System.EventArgs) Handles btn_nuevoreaprov.Click
        frm_nuevoreaprov.ShowDialog()
    End Sub

End Class