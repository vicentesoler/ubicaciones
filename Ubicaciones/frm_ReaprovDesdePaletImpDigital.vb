﻿Public Class frm_ReaprovDesdePaletImpDigital
    Dim conexion As New Conector
    Private Sub frm_ReaprovDesdePaletImpDigital_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Public Sub llenar_palets()
        Dim vdatos As New DataTable
        Dim sql As String

        If list_accion.Text = "Sacar Fuera" Then
            'sql = "select '" & constantes.ImpDigital_A01_UBI & "' as ubicacion,'" & constantes.ImpDigital_A01_UBI & " A01 UBI' as descripcion union select '" & constantes.ImpDigital_A01_CONS & "' as ubicacion,'" & constantes.ImpDigital_A01_CONS & " A01 CONS' as descripcion union select '" & constantes.ImpDigital_A07_CONS & "' as ubicacion,'" & constantes.ImpDigital_A07_CONS & " A07 CONS' as descripcion "
            sql = "select '" & constantes.ImpDigital_A01_UBI & "' as ubicacion,'" & constantes.ImpDigital_A01_UBI & " A01 UBI' as descripcion union select '" & constantes.ImpDigital_A07_CONS & "' as ubicacion,'" & constantes.ImpDigital_A07_CONS & " A07 CONS' as descripcion "
        End If

        If list_accion.Text = "Subir Palet" Then
            'sql = "select '" & constantes.ImpDigital_A01_UBI_FUERA & "' as ubicacion,'" & constantes.ImpDigital_A01_UBI_FUERA & " A01 UBI' as descripcion union select '" & constantes.ImpDigital_A01_CONS_FUERA & "' as ubicacion,'" & constantes.ImpDigital_A01_CONS_FUERA & " A01 CONS' as descripcion union select '" & constantes.ImpDigital_A07_CONS_FUERA & "' as ubicacion,'" & constantes.ImpDigital_A07_CONS_FUERA & " A07 CONS' as descripcion "
            sql = "select '" & constantes.ImpDigital_A01_UBI_FUERA & "' as ubicacion,'" & constantes.ImpDigital_A01_UBI_FUERA & " A01 UBI' as descripcion  union select '" & constantes.ImpDigital_A07_CONS_FUERA & "' as ubicacion,'" & constantes.ImpDigital_A07_CONS_FUERA & " A07 CONS' as descripcion "
        End If
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        list_palet.DataSource = vdatos

        list_palet.DisplayMember = "descripcion"
        list_palet.ValueMember = "ubicacion"
        list_palet.DataSource = vdatos
        list_palet.SelectedIndex = -1
    End Sub
    Public Sub llenar_lineas()
        Dim sql As String
        Dim vdatos, vpick, vpulm As New DataTable
        Dim cont = 0

        dg_lineas.DataSource = Nothing

        sql = "select ubi.id as 'Id', ubi.codarticulo as 'Ref.' ,art.Descripcion as 'Descripción',ubi.ubicacion as 'Ubi.Origen',"
        Select Case list_palet.SelectedValue
            Case constantes.ImpDigital_A01_CONS_FUERA : sql &= "'99-99-93' as 'Ubi.Dest.',"
            Case constantes.ImpDigital_A07_CONS_FUERA : sql &= "'99-99-91' as 'Ubi.Dest.',"
            Case constantes.ImpDigital_A01_UBI_FUERA : sql &= "(select top 1 ubicacion from ubicaciones ubid where ubid.codarticulo=ubi.codarticulo and ubid.codalmacen='A01' and ubid.tipo='Picking' order by id asc) as 'Ubi.Dest.',"
            Case constantes.ImpDigital_A01_CONS : sql &= "'" & constantes.ImpDigital_A01_CONS_FUERA & "' as 'Ubi.Dest.',"
            Case constantes.ImpDigital_A07_CONS : sql &= "'" & constantes.ImpDigital_A07_CONS_FUERA & "' as 'Ubi.Dest.',"
            Case constantes.ImpDigital_A01_UBI : sql &= "'" & constantes.ImpDigital_A01_UBI_FUERA & "' as 'Ubi.Dest.',"
        End Select

        sql &= "ubi.cantidad as 'Cant.', 0 as 'Cant.Mover' "
        sql &= "from ubicaciones ubi "
        sql &= "inner join inase.dbo.GesArticulos art on art.CodArticulo collate Modern_Spanish_CI_AS =ubi.codarticulo and art.CodEmpresa =1 "
        sql &= "where ubi.ubicacion ='" & list_palet.SelectedValue & "' and ubi.cantidad >0 "
        sql &= "order by ubi.codarticulo asc "

        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_lineas.DataSource = vdatos
            dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14)
            dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
            'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_lineas.Columns(0).Width = 85
            dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(1).Width = 100
            dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(2).Width = 440
            dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dg_lineas.Columns(3).Width = 120
            dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(4).Width = 110
            dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(5).Width = 70
            dg_lineas.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(6).Width = 150
            dg_lineas.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            btn_finalizar.Enabled = True
            text_articulo.Text = ""
            text_cantidad.Text = ""
            dg_lineas.CurrentCell = Nothing
            imagen.ImageLocation = ""
            list_accion.Enabled = False
        Else
            MsgBox("Este palet no tiene referencias.", MsgBoxStyle.Critical)
            text_articulo.Text = ""
            text_cantidad.Text = ""
            btn_finalizar.Enabled = False
            btn_ConfirmarTodo.Enabled = False
            list_accion.Enabled = True
        End If


    End Sub

    Private Sub dg_lineas_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_lineas.CellEnter
        imagen.ImageLocation = "\\jserver\BAJA\" & dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value & ".jpg"
        imagen.SizeMode = PictureBoxSizeMode.StretchImage
    End Sub

    Private Sub list_palet_DropDownClosed(sender As Object, e As EventArgs) Handles list_palet.DropDownClosed
        If list_palet.SelectedValue = "" Then Exit Sub
        text_articulo.Enabled = True
        text_cantidad.Enabled = True
        text_articulo.Focus()
        btn_ConfirmarTodo.Enabled = True
        btn_finalizar.Visible = True
        If list_accion.Text = "Subir Palet" Then btn_finalizar.Text = "Crear Orden"
        If list_accion.Text = "Sacar Fuera" Then btn_finalizar.Text = "Confirmar"
        llenar_lineas()
    End Sub

    Private Sub btn_finalizar_Click(sender As Object, e As EventArgs) Handles btn_finalizar.Click

        If list_accion.Text = "Subir Palet" Then
            If MsgBox("Si continua, se generará una orden que tendrá que confirmar al depositar. Aparecerá en los reaprovisioamientos. Continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                generar_reaprov()
            Else
                Exit Sub
            End If
        End If

        If list_accion.Text = "Sacar Fuera" Then
            MoverDePalet()
        End If

    End Sub
    Public Sub MoverDePalet()
        Dim sql, sql2 As String
        Dim resultado As String
        Dim cont As Integer
        Dim codreaprov As Integer = 0
        Dim vreaprov As New DataTable
        Dim CodAlmacenOri As String = "A01"
        Dim CodAlmacenDest As String = "A01"
        Dim UbicacionDestino As String = ""
        Dim vdatos As New DataTable

        Try

            Select Case list_palet.SelectedValue
                Case constantes.ImpDigital_A01_UBI : UbicacionDestino = constantes.ImpDigital_A01_UBI_FUERA
                Case constantes.ImpDigital_A01_CONS : UbicacionDestino = constantes.ImpDigital_A01_CONS_FUERA
                Case constantes.ImpDigital_A07_CONS : UbicacionDestino = constantes.ImpDigital_A07_CONS_FUERA
            End Select

            If UbicacionDestino = "" Then
                MsgBox("No se puede determinar la ubicación destino, avisar a informática.")
                Exit Sub
            End If

            sql = ""
            cont = 0
            Do While cont < dg_lineas.Rows.Count
                If dg_lineas.Rows(cont).Cells("Cant.mover").Value > 0 Then
                    'Si no existe la ubicacion destino, se crea
                    sql2 = "select * from ubicaciones where codalmacen='" & CodAlmacenDest & "' and ubicacion='" & UbicacionDestino & "' and codarticulo='" & dg_lineas.Rows(cont).Cells("Ref.").Value & "' "
                    vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql2)
                    If vdatos.Rows.Count = 0 Then
                        sql2 = "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
                        sql2 &= "values ('" & CodAlmacenDest & "','" & dg_lineas.Rows(cont).Cells("Ref.").Value & "','" & UbicacionDestino & "','Picking'" & ",0,0,'',0,'Ubicaciones-Mover palet (No existe Ubi destino) ','" & frm_principal.lab_nomoperario.Text & "')"
                        conexion.TablaxCmd(constantes.bd_intranet, sql2)
                    End If


                    'Mover la cantidad de la ubicacion origen a destino
                    sql &= "update ubicaciones set cantidad=isnull(cantidad,0) - " & dg_lineas.Rows(cont).Cells("Cant.mover").Value & " where codarticulo='" & dg_lineas.Rows(cont).Cells("Ref.").Value & "' and codalmacen='" & CodAlmacenOri & "' and ubicacion='" & list_palet.SelectedValue & "'; "
                    sql &= "update ubicaciones set cantidad=isnull(cantidad,0) + " & dg_lineas.Rows(cont).Cells("Cant.mover").Value & " where codarticulo='" & dg_lineas.Rows(cont).Cells("Ref.").Value & "' and codalmacen='" & CodAlmacenOri & "' and ubicacion='" & UbicacionDestino & "'; "

                    'Registrar movimientos
                    funciones.añadir_reg_mov_ubicaciones(dg_lineas.Rows(cont).Cells("Ref.").Value.ToString.Trim, (CInt(dg_lineas.Rows(cont).Cells("Cant.mover").Value) * -1), CodAlmacenOri, list_palet.SelectedValue, "", "Ubicaciones-Sacar Palet", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
                    funciones.añadir_reg_mov_ubicaciones(dg_lineas.Rows(cont).Cells("Ref.").Value.ToString.Trim, (CInt(dg_lineas.Rows(cont).Cells("Cant.mover").Value)), CodAlmacenDest, UbicacionDestino, "", "Ubicaciones-Sacar Palet", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
                End If
                cont += 1
            Loop
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If resultado = "0" Then
                MsgBox("Cantidad sacada correctamente.")
                dg_lineas.DataSource = Nothing
                btn_finalizar.Enabled = False
                list_palet.SelectedIndex = -1
            Else
                MsgBox("Error al sacar la cantidad fuera.")
            End If
        Catch ex As Exception
            MsgBox("Error al sacar la cantidad.")
        End Try
    End Sub
    Public Sub generar_reaprov()
        Dim sql As String
        Dim resultado As String
        Dim cont As Integer
        Dim codreaprov As Integer = 0
        Dim vreaprov As New DataTable
        Dim CodAlmacenOri As String
        Dim CodAlmacenDest As String = ""

        CodAlmacenOri = "A01"
        Try

            If list_palet.SelectedValue = constantes.ImpDigital_A01_CONS_FUERA Or list_palet.SelectedValue = constantes.ImpDigital_A01_UBI_FUERA Then
                CodAlmacenDest = "A01"
            End If
            If list_palet.SelectedValue = constantes.ImpDigital_A07_CONS_FUERA Then CodAlmacenDest = "A07"

            sql = "insert into reaprov_ubi(Codalmacen,ultusuario,Tipo,observaciones,Confirmado) "
            sql &= "values('" & CodAlmacenDest & "',0,'','Generado desde Ubicaciones/Reaprov desde palet.',1) "
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)

            'Recuperar el codreaprov
            sql = "select top 1 codreaprov from reaprov_ubi order by codreaprov desc "
            vreaprov = conexion.TablaxCmd(constantes.bd_intranet, sql)
            codreaprov = vreaprov.Rows(0)(0)

            sql = ""
            cont = 0
            Do While cont < dg_lineas.Rows.Count
                If dg_lineas.Rows(cont).Cells("Cant.mover").Value > 0 Then
                    'El reaprov se marca como recogido, ya q simplemente cargan el palet en el montacargas y lo suben al muelle. Por tanto nadie tiene q volver a validar la recogida
                    sql &= "insert into reaprov_ubi_lin(codreaprov,codlinea ,codarticulo ,cantidad,codalmacen_recogida ,ubicacion_recogida,tipo_recogida,cantidad_recogida,usuario_recogida,fecha_recogida ,codalmacen_depositada ,ubicacion_depositada,tipo_depositada,UbiPickPrincipal,Ucaja,CantNoServida,CantNoPre,CantPendOrden,CantMax,CantPick,CantALlenarPick,CantALlenarPulmon,Cajas) "
                    sql &= "values (" & codreaprov & "," & cont + 1 & ",'" & dg_lineas.Rows(cont).Cells("Ref.").Value & "'," & dg_lineas.Rows(cont).Cells("Cant.mover").Value & ",'" & CodAlmacenOri & "','" & list_palet.SelectedValue & "','Pulmon'," & dg_lineas.Rows(cont).Cells("Cant.mover").Value & "," & frm_principal.lab_idoperario.Text & ",getdate(),'" & CodAlmacenDest & "','" & dg_lineas.Rows(cont).Cells("Ubi.Dest.").Value & "','Picking','',0,0,0,0,0,0,0,0,0 ); "

                    'Eliminar la cantidad de la ubicacion
                    sql &= "update ubicaciones set cantidad=isnull(cantidad,0) - " & dg_lineas.Rows(cont).Cells("Cant.mover").Value & " where codarticulo='" & dg_lineas.Rows(cont).Cells("Ref.").Value & "' and codalmacen='" & CodAlmacenOri & "' and ubicacion='" & list_palet.SelectedValue & "'; "

                    'Registrar movimiento
                    funciones.añadir_reg_mov_ubicaciones(dg_lineas.Rows(cont).Cells("Ref.").Value.ToString.Trim, (CInt(dg_lineas.Rows(cont).Cells("Cant.mover").Value) * -1), CodAlmacenOri, list_palet.SelectedValue, "Reaprov_ubi:" & codreaprov, "Ubicaciones-Reaprov_ubi", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
                End If
                cont += 1
            Loop
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If resultado = "0" Then
                MsgBox("Reaprov. " & codreaprov & " generado correctamente.")
                dg_lineas.DataSource = Nothing
                btn_finalizar.Enabled = False
                list_palet.SelectedIndex = -1
            Else
                MsgBox("Error al generar el reaprov.")
                If codreaprov <> 0 Then conexion.ExecuteCmd(constantes.bd_intranet, "delete reaprov_ubi where codreaprov=" & codreaprov)
            End If
        Catch ex As Exception
            MsgBox("Error al generar el reaprov.")
            If codreaprov <> 0 Then conexion.ExecuteCmd(constantes.bd_intranet, "delete reaprov_ubi where codreaprov=" & codreaprov)
        End Try

    End Sub

    Private Sub btn_salir_Click(sender As Object, e As EventArgs) Handles btn_salir.Click
        Close()
    End Sub

    Private Sub text_articulo_KeyUp(sender As Object, e As KeyEventArgs) Handles text_articulo.KeyUp
        Dim sql As String
        Dim vdatos, valbaran, vbultos As DataTable
        Dim cont As Integer
        Dim codarticulo As String = ""

        If e.KeyCode = Keys.Enter And text_articulo.Text <> "" Then
            If Len(Trim(text_articulo.Text)) = 7 Then
                codarticulo = Trim(text_articulo.Text)
            End If
            If Len(Trim(text_articulo.Text)) = 13 Then
                sql = "select codarticulo from gesarticulos "
                sql &= "where codempresa=1 and unidventaean='" & text_articulo.Text.Trim & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                If vdatos.Rows.Count = 0 Then
                    MsgBox("Este EAN no pertenece a ninguna referencia.")
                    Exit Sub
                ElseIf vdatos.Rows.Count > 1 Then
                    MsgBox("Este EAN pertenece a más de una referencia. Seleccione la referencia manualmente de la lista.")
                    Exit Sub
                Else
                    codarticulo = vdatos.Rows(0)(0)
                End If
            End If

            'Buscar la referencia en el grid
            cont = 0
            Do While cont < dg_lineas.Rows.Count
                If dg_lineas.Rows(cont).Cells("Ref.").Value = codarticulo Then

                    If dg_lineas.Rows(cont).Cells("Cant.").Value > dg_lineas.Rows(cont).Cells("Cant.mover").Value Then
                        dg_lineas.Rows(cont).Selected = True
                        dg_lineas.CurrentCell = dg_lineas.Item(0, cont)
                        dg_lineas.FirstDisplayedCell = dg_lineas.CurrentCell
                        text_cantidad.Text = 1
                        text_articulo.Text = codarticulo
                        btn_ok_Click(sender, e)
                        'Dim arg = New DataGridViewCellEventArgs(cont, 1)
                        'dg_lineas_CellClick(sender, arg)
                        Exit Sub
                    Else
                        MsgBox("Esta referencia ya ha sido añadida completamente")
                        Exit Sub
                    End If

                End If
                cont += 1
            Loop
            'Si no la encuentra
            MsgBox("la referencia " & codarticulo & " no aparece en la lista.")
            dg_lineas.CurrentCell = Nothing
            Exit Sub
        End If
    End Sub

    Private Sub dg_lineas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dg_lineas.CellClick
        text_articulo.Text = dg_lineas.SelectedRows(0).Cells("Ref.").Value
        text_cantidad.Text = dg_lineas.SelectedRows(0).Cells("Cant.").Value - dg_lineas.SelectedRows(0).Cells("Cant.mover").Value
        btn_ok.Enabled = True
        text_cantidad.SelectAll()
        text_cantidad.Focus()
    End Sub

    Private Sub btn_ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click
        If dg_lineas.SelectedCells.Count = 0 Then
            MsgBox("No hay ninguna fila seleccionada.")
        Else
            If text_cantidad.Text = 0 Then
                dg_lineas.SelectedRows(0).Cells("Cant.mover").Value = 0
            Else
                If dg_lineas.SelectedRows(0).Cells("Cant.mover").Value + text_cantidad.Text > dg_lineas.SelectedRows(0).Cells("Cant.").Value Then
                    MsgBox("La cantidad a mover es superior a la cantidad en la ubicación, no es posible añadir la cantidad.")
                    text_cantidad.SelectAll()
                    text_cantidad.Focus()
                    Exit Sub
                End If
                dg_lineas.SelectedRows(0).Cells("Cant.mover").Value += text_cantidad.Text
                End If
                Select Case dg_lineas.SelectedRows(0).Cells("Cant.mover").Value
                Case 0 : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
                Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow
                Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Red
                Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            End Select
            text_cantidad.Text = ""
            text_articulo.Text = ""
            text_articulo.Focus()
            btn_ok.Enabled = False
        End If


    End Sub

    Private Sub text_articulo_Click(sender As Object, e As EventArgs) Handles text_articulo.Click
        text_articulo.SelectAll()
    End Sub

    Private Sub Btn_ConfirmarTodo_Click(sender As Object, e As EventArgs) Handles btn_ConfirmarTodo.Click
        Dim cont As Integer

        cont = 0
        Do While cont < dg_lineas.Rows.Count
            dg_lineas.Rows(cont).Cells("Cant.mover").Value = dg_lineas.Rows(cont).Cells("Cant.").Value
            dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
            cont += 1
        Loop

        dg_lineas.CurrentCell = Nothing
    End Sub

    Private Sub frm_ReaprovDesdePaletImpDigital_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub

    Private Sub list_accion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles list_accion.SelectedIndexChanged
        list_palet.Enabled = True
        llenar_palets()
    End Sub
End Class