﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_act_stock
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dg_noprep = New System.Windows.Forms.DataGridView()
        Me.lab_stock_pul = New System.Windows.Forms.Label()
        Me.lab_stock_A01 = New System.Windows.Forms.Label()
        Me.lab_stock_a07 = New System.Windows.Forms.Label()
        Me.lab_stock_pick = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Lab_codarticulo = New System.Windows.Forms.Label()
        Me.lab_stock_ubi = New System.Windows.Forms.Label()
        Me.lab_tot_stock = New System.Windows.Forms.Label()
        Me.lab_descripcion = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lab_stock_p1 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lab_OF = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lab_stock_a08 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lab_prev_manipulacion = New System.Windows.Forms.Label()
        Me.lab_cantajustar_stock = New System.Windows.Forms.Label()
        Me.lab_tot_stock_ajustada = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lab_stock_esp = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lab_alm_pick = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lab_stock_ubi_ajustada = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lab_cantajustar_ubi = New System.Windows.Forms.Label()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_aceptar = New System.Windows.Forms.Button()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.lab_noprep = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.list_almacen = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lab_stock_total = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lab_reaprov = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.lab_desgmont = New System.Windows.Forms.Label()
        CType(Me.dg_noprep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(249, 26)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Stock ubicación pulmón:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(33, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "A01"
        '
        'dg_noprep
        '
        Me.dg_noprep.AllowUserToAddRows = False
        Me.dg_noprep.AllowUserToDeleteRows = False
        Me.dg_noprep.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_noprep.Location = New System.Drawing.Point(12, 383)
        Me.dg_noprep.MultiSelect = False
        Me.dg_noprep.Name = "dg_noprep"
        Me.dg_noprep.Size = New System.Drawing.Size(1125, 177)
        Me.dg_noprep.TabIndex = 16
        '
        'lab_stock_pul
        '
        Me.lab_stock_pul.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_pul.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_pul.Location = New System.Drawing.Point(265, 32)
        Me.lab_stock_pul.Name = "lab_stock_pul"
        Me.lab_stock_pul.Size = New System.Drawing.Size(90, 31)
        Me.lab_stock_pul.TabIndex = 18
        Me.lab_stock_pul.Text = "XXX"
        Me.lab_stock_pul.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_stock_A01
        '
        Me.lab_stock_A01.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_A01.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_A01.Location = New System.Drawing.Point(8, 54)
        Me.lab_stock_A01.Name = "lab_stock_A01"
        Me.lab_stock_A01.Size = New System.Drawing.Size(88, 31)
        Me.lab_stock_A01.TabIndex = 19
        Me.lab_stock_A01.Text = "XXX"
        Me.lab_stock_A01.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_stock_a07
        '
        Me.lab_stock_a07.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_a07.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_a07.Location = New System.Drawing.Point(105, 54)
        Me.lab_stock_a07.Name = "lab_stock_a07"
        Me.lab_stock_a07.Size = New System.Drawing.Size(88, 31)
        Me.lab_stock_a07.TabIndex = 20
        Me.lab_stock_a07.Text = "XXX"
        Me.lab_stock_a07.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_stock_pick
        '
        Me.lab_stock_pick.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_pick.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_pick.Location = New System.Drawing.Point(265, 68)
        Me.lab_stock_pick.Name = "lab_stock_pick"
        Me.lab_stock_pick.Size = New System.Drawing.Size(90, 31)
        Me.lab_stock_pick.TabIndex = 22
        Me.lab_stock_pick.Text = "XXX"
        Me.lab_stock_pick.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(14, 70)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(245, 26)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Stock ubicacion picking:"
        '
        'Lab_codarticulo
        '
        Me.Lab_codarticulo.AutoSize = True
        Me.Lab_codarticulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lab_codarticulo.Location = New System.Drawing.Point(12, 9)
        Me.Lab_codarticulo.Name = "Lab_codarticulo"
        Me.Lab_codarticulo.Size = New System.Drawing.Size(126, 31)
        Me.Lab_codarticulo.TabIndex = 23
        Me.Lab_codarticulo.Text = "1017951"
        '
        'lab_stock_ubi
        '
        Me.lab_stock_ubi.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_ubi.Location = New System.Drawing.Point(265, 177)
        Me.lab_stock_ubi.Name = "lab_stock_ubi"
        Me.lab_stock_ubi.Size = New System.Drawing.Size(90, 29)
        Me.lab_stock_ubi.TabIndex = 24
        Me.lab_stock_ubi.Text = "XXX"
        Me.lab_stock_ubi.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_tot_stock
        '
        Me.lab_tot_stock.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_tot_stock.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tot_stock.Location = New System.Drawing.Point(392, 197)
        Me.lab_tot_stock.Name = "lab_tot_stock"
        Me.lab_tot_stock.Size = New System.Drawing.Size(88, 29)
        Me.lab_tot_stock.TabIndex = 25
        Me.lab_tot_stock.Text = "XXX"
        Me.lab_tot_stock.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_descripcion
        '
        Me.lab_descripcion.AutoSize = True
        Me.lab_descripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_descripcion.Location = New System.Drawing.Point(163, 15)
        Me.lab_descripcion.Name = "lab_descripcion"
        Me.lab_descripcion.Size = New System.Drawing.Size(60, 26)
        Me.lab_descripcion.TabIndex = 26
        Me.lab_descripcion.Text = "XXX"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.LightGreen
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.lab_reaprov)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.lab_stock_total)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.lab_stock_p1)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.lab_OF)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lab_stock_a08)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.lab_prev_manipulacion)
        Me.GroupBox1.Controls.Add(Me.lab_cantajustar_stock)
        Me.GroupBox1.Controls.Add(Me.lab_tot_stock_ajustada)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lab_stock_A01)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lab_tot_stock)
        Me.GroupBox1.Controls.Add(Me.lab_stock_a07)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(419, 54)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(502, 302)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Stock Alm."
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(326, 27)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(28, 20)
        Me.Label18.TabIndex = 41
        Me.Label18.Text = "P1"
        '
        'lab_stock_p1
        '
        Me.lab_stock_p1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_p1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_p1.Location = New System.Drawing.Point(296, 54)
        Me.lab_stock_p1.Name = "lab_stock_p1"
        Me.lab_stock_p1.Size = New System.Drawing.Size(88, 31)
        Me.lab_stock_p1.TabIndex = 40
        Me.lab_stock_p1.Text = "XXX"
        Me.lab_stock_p1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(153, 126)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(234, 26)
        Me.Label17.TabIndex = 38
        Me.Label17.Text = "Alb.OF Pend.Rec/Dep:"
        '
        'lab_OF
        '
        Me.lab_OF.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_OF.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_OF.Location = New System.Drawing.Point(392, 124)
        Me.lab_OF.Name = "lab_OF"
        Me.lab_OF.Size = New System.Drawing.Size(88, 31)
        Me.lab_OF.TabIndex = 39
        Me.lab_OF.Text = "XXX"
        Me.lab_OF.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(226, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 20)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "A08"
        '
        'lab_stock_a08
        '
        Me.lab_stock_a08.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_a08.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_a08.Location = New System.Drawing.Point(201, 54)
        Me.lab_stock_a08.Name = "lab_stock_a08"
        Me.lab_stock_a08.Size = New System.Drawing.Size(88, 31)
        Me.lab_stock_a08.TabIndex = 36
        Me.lab_stock_a08.Text = "XXX"
        Me.lab_stock_a08.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(130, 29)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(38, 20)
        Me.Label16.TabIndex = 34
        Me.Label16.Text = "A07"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(183, 91)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(204, 26)
        Me.Label15.TabIndex = 31
        Me.Label15.Text = "Prev. manipulacion:"
        '
        'lab_prev_manipulacion
        '
        Me.lab_prev_manipulacion.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_prev_manipulacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_prev_manipulacion.Location = New System.Drawing.Point(392, 89)
        Me.lab_prev_manipulacion.Name = "lab_prev_manipulacion"
        Me.lab_prev_manipulacion.Size = New System.Drawing.Size(88, 31)
        Me.lab_prev_manipulacion.TabIndex = 32
        Me.lab_prev_manipulacion.Text = "XXX"
        Me.lab_prev_manipulacion.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_cantajustar_stock
        '
        Me.lab_cantajustar_stock.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_cantajustar_stock.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_cantajustar_stock.Location = New System.Drawing.Point(392, 230)
        Me.lab_cantajustar_stock.Name = "lab_cantajustar_stock"
        Me.lab_cantajustar_stock.Size = New System.Drawing.Size(88, 29)
        Me.lab_cantajustar_stock.TabIndex = 30
        Me.lab_cantajustar_stock.Text = "XXX"
        Me.lab_cantajustar_stock.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab_tot_stock_ajustada
        '
        Me.lab_tot_stock_ajustada.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.lab_tot_stock_ajustada.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tot_stock_ajustada.ForeColor = System.Drawing.Color.White
        Me.lab_tot_stock_ajustada.Location = New System.Drawing.Point(392, 263)
        Me.lab_tot_stock_ajustada.Name = "lab_tot_stock_ajustada"
        Me.lab_tot_stock_ajustada.Size = New System.Drawing.Size(88, 29)
        Me.lab_tot_stock_ajustada.TabIndex = 28
        Me.lab_tot_stock_ajustada.Text = "XXX"
        Me.lab_tot_stock_ajustada.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(147, 264)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(240, 26)
        Me.Label9.TabIndex = 27
        Me.Label9.Text = "Total desp. de ajuste:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(209, 231)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(178, 26)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Cant. a Ajustar:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(316, 198)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 26)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Total:"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.PaleTurquoise
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.lab_desgmont)
        Me.GroupBox2.Controls.Add(Me.lab_stock_esp)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.lab_alm_pick)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.lab_stock_ubi_ajustada)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.lab_cantajustar_ubi)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.lab_stock_pick)
        Me.GroupBox2.Controls.Add(Me.lab_stock_ubi)
        Me.GroupBox2.Controls.Add(Me.lab_stock_pul)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(18, 54)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(377, 286)
        Me.GroupBox2.TabIndex = 29
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Stock Ubi."
        '
        'lab_stock_esp
        '
        Me.lab_stock_esp.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_esp.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_esp.Location = New System.Drawing.Point(265, 104)
        Me.lab_stock_esp.Name = "lab_stock_esp"
        Me.lab_stock_esp.Size = New System.Drawing.Size(90, 31)
        Me.lab_stock_esp.TabIndex = 42
        Me.lab_stock_esp.Text = "XXX"
        Me.lab_stock_esp.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(41, 106)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(218, 26)
        Me.Label13.TabIndex = 41
        Me.Label13.Text = "Stock ubicacion esp.:"
        '
        'lab_alm_pick
        '
        Me.lab_alm_pick.AutoSize = True
        Me.lab_alm_pick.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_alm_pick.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.lab_alm_pick.Location = New System.Drawing.Point(37, 216)
        Me.lab_alm_pick.Name = "lab_alm_pick"
        Me.lab_alm_pick.Size = New System.Drawing.Size(38, 20)
        Me.lab_alm_pick.TabIndex = 40
        Me.lab_alm_pick.Text = "A01"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(81, 213)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(178, 26)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "Cant. a Ajustar:"
        '
        'lab_stock_ubi_ajustada
        '
        Me.lab_stock_ubi_ajustada.BackColor = System.Drawing.Color.LightSeaGreen
        Me.lab_stock_ubi_ajustada.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_ubi_ajustada.ForeColor = System.Drawing.Color.White
        Me.lab_stock_ubi_ajustada.Location = New System.Drawing.Point(267, 248)
        Me.lab_stock_ubi_ajustada.Name = "lab_stock_ubi_ajustada"
        Me.lab_stock_ubi_ajustada.Size = New System.Drawing.Size(88, 29)
        Me.lab_stock_ubi_ajustada.TabIndex = 30
        Me.lab_stock_ubi_ajustada.Text = "XXX"
        Me.lab_stock_ubi_ajustada.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(188, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 26)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "Total:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(19, 248)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(240, 26)
        Me.Label12.TabIndex = 29
        Me.Label12.Text = "Total desp. de ajuste:"
        '
        'lab_cantajustar_ubi
        '
        Me.lab_cantajustar_ubi.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_cantajustar_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_cantajustar_ubi.Location = New System.Drawing.Point(265, 212)
        Me.lab_cantajustar_ubi.Name = "lab_cantajustar_ubi"
        Me.lab_cantajustar_ubi.Size = New System.Drawing.Size(90, 29)
        Me.lab_cantajustar_ubi.TabIndex = 35
        Me.lab_cantajustar_ubi.Text = "XXX"
        Me.lab_cantajustar_ubi.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(937, 60)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(200, 200)
        Me.imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imagen.TabIndex = 30
        Me.imagen.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(138, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(22, 29)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "-"
        '
        'btn_aceptar
        '
        Me.btn_aceptar.BackColor = System.Drawing.Color.YellowGreen
        Me.btn_aceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_aceptar.Location = New System.Drawing.Point(949, 571)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(188, 45)
        Me.btn_aceptar.TabIndex = 32
        Me.btn_aceptar.Text = "Aceptar"
        Me.btn_aceptar.UseVisualStyleBackColor = False
        '
        'btn_cancelar
        '
        Me.btn_cancelar.BackColor = System.Drawing.Color.Red
        Me.btn_cancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancelar.ForeColor = System.Drawing.Color.White
        Me.btn_cancelar.Location = New System.Drawing.Point(12, 571)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(174, 45)
        Me.btn_cancelar.TabIndex = 33
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'lab_noprep
        '
        Me.lab_noprep.AutoSize = True
        Me.lab_noprep.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_noprep.Location = New System.Drawing.Point(252, 355)
        Me.lab_noprep.Name = "lab_noprep"
        Me.lab_noprep.Size = New System.Drawing.Size(24, 25)
        Me.lab_noprep.TabIndex = 36
        Me.lab_noprep.Text = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 355)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(233, 25)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "Cantidad no preparada"
        '
        'list_almacen
        '
        Me.list_almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almacen.FormattingEnabled = True
        Me.list_almacen.Items.AddRange(New Object() {"AUTO"})
        Me.list_almacen.Location = New System.Drawing.Point(758, 566)
        Me.list_almacen.Name = "list_almacen"
        Me.list_almacen.Size = New System.Drawing.Size(144, 54)
        Me.list_almacen.TabIndex = 38
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(466, 574)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(286, 39)
        Me.Label4.TabIndex = 39
        Me.Label4.Text = "Almacén ajustar:"
        '
        'lab_stock_total
        '
        Me.lab_stock_total.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_stock_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stock_total.Location = New System.Drawing.Point(392, 54)
        Me.lab_stock_total.Name = "lab_stock_total"
        Me.lab_stock_total.Size = New System.Drawing.Size(88, 31)
        Me.lab_stock_total.TabIndex = 42
        Me.lab_stock_total.Text = "XXX"
        Me.lab_stock_total.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(417, 27)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(44, 20)
        Me.Label20.TabIndex = 43
        Me.Label20.Text = "Total"
        '
        'lab_reaprov
        '
        Me.lab_reaprov.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_reaprov.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_reaprov.Location = New System.Drawing.Point(392, 160)
        Me.lab_reaprov.Name = "lab_reaprov"
        Me.lab_reaprov.Size = New System.Drawing.Size(88, 31)
        Me.lab_reaprov.TabIndex = 44
        Me.lab_reaprov.Text = "XXX"
        Me.lab_reaprov.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(176, 162)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(210, 26)
        Me.Label19.TabIndex = 46
        Me.Label19.Text = "Pendiente Reaprov.:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(66, 141)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(192, 26)
        Me.Label21.TabIndex = 49
        Me.Label21.Text = "Desglose/Montaje:"
        '
        'lab_desgmont
        '
        Me.lab_desgmont.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lab_desgmont.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_desgmont.Location = New System.Drawing.Point(264, 141)
        Me.lab_desgmont.Name = "lab_desgmont"
        Me.lab_desgmont.Size = New System.Drawing.Size(90, 31)
        Me.lab_desgmont.TabIndex = 48
        Me.lab_desgmont.Text = "XXX"
        Me.lab_desgmont.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frm_act_stock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Moccasin
        Me.ClientSize = New System.Drawing.Size(1149, 628)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.list_almacen)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lab_noprep)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.imagen)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lab_descripcion)
        Me.Controls.Add(Me.Lab_codarticulo)
        Me.Controls.Add(Me.dg_noprep)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frm_act_stock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_act_stock"
        CType(Me.dg_noprep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dg_noprep As System.Windows.Forms.DataGridView
    Friend WithEvents lab_stock_pul As System.Windows.Forms.Label
    Friend WithEvents lab_stock_A01 As System.Windows.Forms.Label
    Friend WithEvents lab_stock_a07 As System.Windows.Forms.Label
    Friend WithEvents lab_stock_pick As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Lab_codarticulo As System.Windows.Forms.Label
    Friend WithEvents lab_stock_ubi As System.Windows.Forms.Label
    Friend WithEvents lab_tot_stock As System.Windows.Forms.Label
    Friend WithEvents lab_descripcion As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents imagen As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lab_tot_stock_ajustada As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lab_stock_ubi_ajustada As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btn_aceptar As System.Windows.Forms.Button
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents lab_cantajustar_ubi As System.Windows.Forms.Label
    Friend WithEvents lab_noprep As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents list_almacen As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lab_cantajustar_stock As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lab_alm_pick As System.Windows.Forms.Label
    Friend WithEvents lab_stock_esp As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lab_prev_manipulacion As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lab_stock_a08 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lab_OF As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lab_stock_p1 As System.Windows.Forms.Label
    Friend WithEvents lab_reaprov As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents lab_stock_total As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents lab_desgmont As Label
End Class
