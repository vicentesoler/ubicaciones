﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_contenedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_cerrar = New System.Windows.Forms.Button()
        Me.lab_stockA01 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lab_pendserv_hoy = New System.Windows.Forms.Label()
        Me.lab2 = New System.Windows.Forms.Label()
        Me.lab_pendserv_tot = New System.Windows.Forms.Label()
        Me.lab1 = New System.Windows.Forms.Label()
        Me.btn_eliminar = New System.Windows.Forms.Button()
        Me.list_tipo = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_altura = New System.Windows.Forms.TextBox()
        Me.txt_portal = New System.Windows.Forms.TextBox()
        Me.txt_pasillo = New System.Windows.Forms.TextBox()
        Me.btn_trasladar = New System.Windows.Forms.Button()
        Me.txt_cantidad = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.list_almacen = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dg_ubicacion_destino = New System.Windows.Forms.DataGridView()
        Me.list_periodo = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.list_empresa = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.list_contenedor = New System.Windows.Forms.ComboBox()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.dg_ubicacion_destino, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.btn_cerrar)
        Me.Panel1.Controls.Add(Me.lab_stockA01)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.lab_pendserv_hoy)
        Me.Panel1.Controls.Add(Me.lab2)
        Me.Panel1.Controls.Add(Me.lab_pendserv_tot)
        Me.Panel1.Controls.Add(Me.lab1)
        Me.Panel1.Controls.Add(Me.btn_eliminar)
        Me.Panel1.Controls.Add(Me.list_tipo)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txt_altura)
        Me.Panel1.Controls.Add(Me.txt_portal)
        Me.Panel1.Controls.Add(Me.txt_pasillo)
        Me.Panel1.Controls.Add(Me.btn_trasladar)
        Me.Panel1.Controls.Add(Me.txt_cantidad)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.list_almacen)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.dg_ubicacion_destino)
        Me.Panel1.Controls.Add(Me.list_periodo)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.list_empresa)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.list_contenedor)
        Me.Panel1.Controls.Add(Me.btn_finalizar)
        Me.Panel1.Controls.Add(Me.dg_lineas)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1250, 640)
        Me.Panel1.TabIndex = 26
        '
        'btn_cerrar
        '
        Me.btn_cerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cerrar.Location = New System.Drawing.Point(520, 587)
        Me.btn_cerrar.Name = "btn_cerrar"
        Me.btn_cerrar.Size = New System.Drawing.Size(219, 44)
        Me.btn_cerrar.TabIndex = 84
        Me.btn_cerrar.Text = "Cerrar"
        Me.btn_cerrar.UseVisualStyleBackColor = True
        '
        'lab_stockA01
        '
        Me.lab_stockA01.AutoSize = True
        Me.lab_stockA01.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_stockA01.Location = New System.Drawing.Point(1018, 589)
        Me.lab_stockA01.Name = "lab_stockA01"
        Me.lab_stockA01.Size = New System.Drawing.Size(19, 20)
        Me.lab_stockA01.TabIndex = 83
        Me.lab_stockA01.Text = "0"
        Me.lab_stockA01.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(921, 589)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(97, 20)
        Me.Label11.TabIndex = 82
        Me.Label11.Text = "Stock A01:"
        '
        'lab_pendserv_hoy
        '
        Me.lab_pendserv_hoy.AutoSize = True
        Me.lab_pendserv_hoy.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pendserv_hoy.Location = New System.Drawing.Point(874, 615)
        Me.lab_pendserv_hoy.Name = "lab_pendserv_hoy"
        Me.lab_pendserv_hoy.Size = New System.Drawing.Size(19, 20)
        Me.lab_pendserv_hoy.TabIndex = 81
        Me.lab_pendserv_hoy.Text = "0"
        Me.lab_pendserv_hoy.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab2
        '
        Me.lab2.AutoSize = True
        Me.lab2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab2.Location = New System.Drawing.Point(775, 615)
        Me.lab2.Name = "lab2"
        Me.lab2.Size = New System.Drawing.Size(96, 20)
        Me.lab2.TabIndex = 80
        Me.lab2.Text = "P.serv.hoy:"
        '
        'lab_pendserv_tot
        '
        Me.lab_pendserv_tot.AutoSize = True
        Me.lab_pendserv_tot.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pendserv_tot.Location = New System.Drawing.Point(863, 589)
        Me.lab_pendserv_tot.Name = "lab_pendserv_tot"
        Me.lab_pendserv_tot.Size = New System.Drawing.Size(19, 20)
        Me.lab_pendserv_tot.TabIndex = 79
        Me.lab_pendserv_tot.Text = "0"
        Me.lab_pendserv_tot.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lab1
        '
        Me.lab1.AutoSize = True
        Me.lab1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab1.Location = New System.Drawing.Point(775, 589)
        Me.lab1.Name = "lab1"
        Me.lab1.Size = New System.Drawing.Size(90, 20)
        Me.lab1.TabIndex = 78
        Me.lab1.Text = "P.serv.tot:"
        '
        'btn_eliminar
        '
        Me.btn_eliminar.BackColor = System.Drawing.Color.OrangeRed
        Me.btn_eliminar.Enabled = False
        Me.btn_eliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_eliminar.Location = New System.Drawing.Point(1081, 587)
        Me.btn_eliminar.Name = "btn_eliminar"
        Me.btn_eliminar.Size = New System.Drawing.Size(163, 48)
        Me.btn_eliminar.TabIndex = 77
        Me.btn_eliminar.Text = "Eliminar"
        Me.btn_eliminar.UseVisualStyleBackColor = False
        '
        'list_tipo
        '
        Me.list_tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_tipo.FormattingEnabled = True
        Me.list_tipo.Items.AddRange(New Object() {"Pulmon", "Picking"})
        Me.list_tipo.Location = New System.Drawing.Point(1055, 113)
        Me.list_tipo.Name = "list_tipo"
        Me.list_tipo.Size = New System.Drawing.Size(189, 46)
        Me.list_tipo.TabIndex = 76
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(969, 121)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 31)
        Me.Label10.TabIndex = 75
        Me.Label10.Text = "Tipo:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(1075, 185)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(24, 31)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "-"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(993, 185)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(24, 31)
        Me.Label8.TabIndex = 73
        Me.Label8.Text = "-"
        '
        'txt_altura
        '
        Me.txt_altura.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_altura.Location = New System.Drawing.Point(1099, 177)
        Me.txt_altura.MaxLength = 13
        Me.txt_altura.Name = "txt_altura"
        Me.txt_altura.Size = New System.Drawing.Size(54, 45)
        Me.txt_altura.TabIndex = 72
        '
        'txt_portal
        '
        Me.txt_portal.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_portal.Location = New System.Drawing.Point(1020, 177)
        Me.txt_portal.MaxLength = 13
        Me.txt_portal.Name = "txt_portal"
        Me.txt_portal.Size = New System.Drawing.Size(54, 45)
        Me.txt_portal.TabIndex = 71
        '
        'txt_pasillo
        '
        Me.txt_pasillo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pasillo.Location = New System.Drawing.Point(937, 177)
        Me.txt_pasillo.MaxLength = 13
        Me.txt_pasillo.Name = "txt_pasillo"
        Me.txt_pasillo.Size = New System.Drawing.Size(54, 45)
        Me.txt_pasillo.TabIndex = 70
        '
        'btn_trasladar
        '
        Me.btn_trasladar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_trasladar.Location = New System.Drawing.Point(1066, 239)
        Me.btn_trasladar.Name = "btn_trasladar"
        Me.btn_trasladar.Size = New System.Drawing.Size(178, 48)
        Me.btn_trasladar.TabIndex = 69
        Me.btn_trasladar.Text = "Trasladar"
        Me.btn_trasladar.UseVisualStyleBackColor = True
        '
        'txt_cantidad
        '
        Me.txt_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad.Location = New System.Drawing.Point(937, 239)
        Me.txt_cantidad.MaxLength = 13
        Me.txt_cantidad.Name = "txt_cantidad"
        Me.txt_cantidad.Size = New System.Drawing.Size(88, 45)
        Me.txt_cantidad.TabIndex = 68
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(771, 247)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(140, 31)
        Me.Label7.TabIndex = 67
        Me.Label7.Text = "Cantidad:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(771, 185)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(152, 31)
        Me.Label6.TabIndex = 66
        Me.Label6.Text = "Ubicación:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(769, 121)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 31)
        Me.Label5.TabIndex = 64
        Me.Label5.Text = "Alm."
        '
        'list_almacen
        '
        Me.list_almacen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almacen.FormattingEnabled = True
        Me.list_almacen.Items.AddRange(New Object() {"A01", "A07", "A06", "A08"})
        Me.list_almacen.Location = New System.Drawing.Point(842, 113)
        Me.list_almacen.Name = "list_almacen"
        Me.list_almacen.Size = New System.Drawing.Size(89, 46)
        Me.list_almacen.TabIndex = 63
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(772, 300)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(109, 25)
        Me.Label12.TabIndex = 62
        Me.Label12.Text = "Ubicados:"
        '
        'dg_ubicacion_destino
        '
        Me.dg_ubicacion_destino.AllowUserToAddRows = False
        Me.dg_ubicacion_destino.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_ubicacion_destino.Location = New System.Drawing.Point(777, 328)
        Me.dg_ubicacion_destino.MultiSelect = False
        Me.dg_ubicacion_destino.Name = "dg_ubicacion_destino"
        Me.dg_ubicacion_destino.ReadOnly = True
        Me.dg_ubicacion_destino.RowHeadersWidth = 10
        Me.dg_ubicacion_destino.RowTemplate.Height = 50
        Me.dg_ubicacion_destino.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_ubicacion_destino.Size = New System.Drawing.Size(467, 253)
        Me.dg_ubicacion_destino.TabIndex = 60
        '
        'list_periodo
        '
        Me.list_periodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_periodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_periodo.FormattingEnabled = True
        Me.list_periodo.Location = New System.Drawing.Point(777, 9)
        Me.list_periodo.Name = "list_periodo"
        Me.list_periodo.Size = New System.Drawing.Size(117, 46)
        Me.list_periodo.TabIndex = 59
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(619, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(152, 39)
        Me.Label4.TabIndex = 58
        Me.Label4.Text = "Periodo:"
        '
        'list_empresa
        '
        Me.list_empresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_empresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_empresa.FormattingEnabled = True
        Me.list_empresa.Location = New System.Drawing.Point(128, 9)
        Me.list_empresa.Name = "list_empresa"
        Me.list_empresa.Size = New System.Drawing.Size(419, 46)
        Me.list_empresa.TabIndex = 57
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 39)
        Me.Label3.TabIndex = 56
        Me.Label3.Text = "Emp.:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(190, 25)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "Lineas contenedor"
        '
        'list_contenedor
        '
        Me.list_contenedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.list_contenedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_contenedor.FormattingEnabled = True
        Me.list_contenedor.Location = New System.Drawing.Point(1116, 9)
        Me.list_contenedor.Name = "list_contenedor"
        Me.list_contenedor.Size = New System.Drawing.Size(121, 46)
        Me.list_contenedor.TabIndex = 37
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Enabled = False
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(19, 587)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(219, 44)
        Me.btn_finalizar.TabIndex = 33
        Me.btn_finalizar.Text = "Finalizar"
        Me.btn_finalizar.UseVisualStyleBackColor = True
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_lineas.Location = New System.Drawing.Point(20, 113)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        Me.dg_lineas.RowHeadersWidth = 10
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(719, 468)
        Me.dg_lineas.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(961, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 39)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "N.Cont.:"
        '
        'frm_contenedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_contenedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "  "
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dg_ubicacion_destino, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents list_contenedor As System.Windows.Forms.ComboBox
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents dg_lineas As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents list_empresa As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents list_periodo As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dg_ubicacion_destino As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents list_almacen As System.Windows.Forms.ComboBox
    Friend WithEvents btn_trasladar As System.Windows.Forms.Button
    Friend WithEvents txt_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_altura As System.Windows.Forms.TextBox
    Friend WithEvents txt_portal As System.Windows.Forms.TextBox
    Friend WithEvents txt_pasillo As System.Windows.Forms.TextBox
    Friend WithEvents btn_eliminar As System.Windows.Forms.Button
    Friend WithEvents list_tipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lab_pendserv_tot As System.Windows.Forms.Label
    Friend WithEvents lab_pendserv_hoy As System.Windows.Forms.Label
    Friend WithEvents lab2 As System.Windows.Forms.Label
    Friend WithEvents lab_stockA01 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lab1 As System.Windows.Forms.Label
    Friend WithEvents btn_cerrar As Button
End Class
