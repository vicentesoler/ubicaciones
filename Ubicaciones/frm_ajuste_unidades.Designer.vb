﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ajuste_unidades
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_cantmax = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lab_referencia = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_unicaja = New System.Windows.Forms.TextBox()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.lab_ubipick = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lab_codalmpick = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txt_cantmax
        '
        Me.txt_cantmax.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantmax.Location = New System.Drawing.Point(208, 110)
        Me.txt_cantmax.MaxLength = 13
        Me.txt_cantmax.Name = "txt_cantmax"
        Me.txt_cantmax.Size = New System.Drawing.Size(88, 45)
        Me.txt_cantmax.TabIndex = 70
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(118, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 31)
        Me.Label7.TabIndex = 69
        Me.Label7.Text = "Ref.:"
        '
        'lab_referencia
        '
        Me.lab_referencia.AutoSize = True
        Me.lab_referencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_referencia.Location = New System.Drawing.Point(202, 10)
        Me.lab_referencia.Name = "lab_referencia"
        Me.lab_referencia.Size = New System.Drawing.Size(126, 31)
        Me.lab_referencia.TabIndex = 71
        Me.lab_referencia.Text = "1017951"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(40, 118)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(156, 31)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "Cant.Max.:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(55, 169)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 31)
        Me.Label2.TabIndex = 74
        Me.Label2.Text = "Un./Caja:"
        '
        'txt_unicaja
        '
        Me.txt_unicaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_unicaja.Location = New System.Drawing.Point(208, 161)
        Me.txt_unicaja.MaxLength = 13
        Me.txt_unicaja.Name = "txt_unicaja"
        Me.txt_unicaja.Size = New System.Drawing.Size(88, 45)
        Me.txt_unicaja.TabIndex = 73
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(82, 229)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(234, 48)
        Me.btn_finalizar.TabIndex = 75
        Me.btn_finalizar.Text = "Guardar"
        Me.btn_finalizar.UseVisualStyleBackColor = True
        '
        'lab_ubipick
        '
        Me.lab_ubipick.AutoSize = True
        Me.lab_ubipick.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_ubipick.Location = New System.Drawing.Point(246, 57)
        Me.lab_ubipick.Name = "lab_ubipick"
        Me.lab_ubipick.Size = New System.Drawing.Size(130, 31)
        Me.lab_ubipick.TabIndex = 77
        Me.lab_ubipick.Text = "01-01-01"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(21, 57)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(141, 31)
        Me.Label4.TabIndex = 76
        Me.Label4.Text = "Ubi.Pick.:"
        '
        'lab_codalmpick
        '
        Me.lab_codalmpick.AutoSize = True
        Me.lab_codalmpick.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_codalmpick.Location = New System.Drawing.Point(168, 57)
        Me.lab_codalmpick.Name = "lab_codalmpick"
        Me.lab_codalmpick.Size = New System.Drawing.Size(65, 31)
        Me.lab_codalmpick.TabIndex = 78
        Me.lab_codalmpick.Text = "A01"
        '
        'frm_ajuste_unidades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(388, 304)
        Me.Controls.Add(Me.lab_codalmpick)
        Me.Controls.Add(Me.lab_ubipick)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btn_finalizar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_unicaja)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lab_referencia)
        Me.Controls.Add(Me.txt_cantmax)
        Me.Controls.Add(Me.Label7)
        Me.Name = "frm_ajuste_unidades"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_ajuste_unidades"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_cantmax As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lab_referencia As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_unicaja As System.Windows.Forms.TextBox
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents lab_ubipick As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lab_codalmpick As System.Windows.Forms.Label
End Class
