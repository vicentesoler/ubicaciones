﻿Public Class frm_Desg_mont
    Dim conexion As New Conector
    Dim finalizadaRecogida, finalizadaDepositada As Boolean
    Dim fecha_inicio, fecha_fin As DateTime
    Private Sub frm_Desg_mont_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        llenar_ordenes()
        limpiar(False)
        dg_lineas.DefaultCellStyle.SelectionBackColor = Color.Gray
        dg_lineas.DefaultCellStyle.SelectionForeColor = Color.White
        dg_ubi.DefaultCellStyle.SelectionBackColor = Color.Gray
        dg_ubi.DefaultCellStyle.SelectionForeColor = Color.White

    End Sub

    Public Sub llenar_ordenes()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer

        list_codorden.DataSource = Nothing
        list_codorden.Items.Clear()
        sql = "select cab.codorden from orden_desg_mont cab "
        sql &= "inner join orden_desg_mont_lin lin on lin.codorden=cab.codorden "
        sql &= "left join orden_desg_mont_refs refs on refs.codorden=cab.codorden "
        sql &= "where cab.codorden >=1698 and (cab.recogido=0 or cab.depositado=0) "
        If txt_filtro_codarticulo.Text.ToString.Length = 7 Then
            sql &= "and (lin.codarticulo='" & txt_filtro_codarticulo.Text & "' or isnull(refs.codarticulo,'')='" & txt_filtro_codarticulo.Text & "') "
        End If
        If list_filtro_almacen.Text <> "" Then
            sql &= "and lin.codarticulo collate Modern_Spanish_CI_AS in (select codarticulo from ubicaciones where tipo='Picking' and codalmacen='" & list_filtro_almacen.Text & "') "
        End If
        If cb_filtro_urgentes.Checked Then
            sql &= "and lin.codarticulo in (select distinct lin2.Articulo  "
            sql &= "from albaranes_impresos ap "
            sql &= "inner join inase.dbo.GesAlbaranes cab2 on cab2.Codempresa=ap.codempresa and cab2.CodPeriodo =ap.codperiodo and cab2.SerAlbaran =ap.seralbaran "
            sql &= "and cab2.CodAlbaran =ap.codalbaran  "
            sql &= "inner join inase.dbo.GesAlbaranesLin lin2 on lin2.RefCabecera =cab2.NumReferencia "
            sql &= "where ap.preparado ='NO' and ap.codperiodo >=2018 "
            sql &= "and (select sum(cantidad) from ubicaciones where tipo='Picking' and codalmacen ='A01' and codarticulo =lin2.Articulo collate Modern_Spanish_CI_AS )<=0) "
        End If
        If cb_FiltroStockA01.Checked Then
            sql &= "And (case cab.tipo when 'Desglose' then lin.codarticulo when 'Montaje' then isnull(refs.codarticulo,'')  end in (select CodArticulo  from inase.dbo.GesStocks where CodAlmacen ='A01' group by CodArticulo having  sum(stockactual) >0)) "
        End If
        If cb_FiltroStockA07.Checked Then
            sql &= "And (case cab.tipo when 'Desglose' then lin.codarticulo when 'Montaje' then isnull(refs.codarticulo,'')  end in (select CodArticulo  from inase.dbo.GesStocks where CodAlmacen ='A07' group by CodArticulo having  sum(stockactual) >0)) "
        End If
        If cb_FiltroPorRecoger.Checked Then sql &= "and cab.recogido=0 "
        If cb_FiltroPorDepositar.Checked Then sql &= "and cab.depositado=0 "
        sql &= "group by cab.codorden "
        sql &= "order by cab.codorden asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        cont = 0
        list_codorden.Text = ""
        list_codorden.Items.Clear()
        Do While cont < vdatos.Rows.Count
            list_codorden.Items.Add(vdatos.Rows(cont).Item("codorden"))
            cont += 1
        Loop

        If list_filtro_almacen.Text <> "" Or txt_filtro_codarticulo.Text <> "" Or cb_filtro_urgentes.Checked Or cb_FiltroStockA01.Checked Or cb_FiltroStockA07.Checked Or cb_FiltroPorDepositar.Checked Or cb_FiltroPorRecoger.Checked Then list_codorden.BackColor = Color.SteelBlue Else list_codorden.BackColor = Color.White


    End Sub
    Public Sub limpiar(Optional ByVal GuardarTiempos As Boolean = True, Optional ByVal ObsGuardarTiempos As String = "")
        'Guardar datos de operario
        If GuardarTiempos Then
            fecha_fin = Now
            Guardar_datos_tiempo(ObsGuardarTiempos)
        End If

        p_filtro.Enabled = True
        list_accion.SelectedIndex = -1
        dg_lineas.DataSource = Nothing
        dg_lineas.Enabled = False
        dg_ubi.DataSource = Nothing
        dg_ubi.Enabled = False
        txt_cantidad.Text = 0
        btn_ok.Enabled = False
        btn_finalizar.Enabled = False
        btn_finalizar.Visible = False
        btn_salirorden.Enabled = False
        lab_titulo.Text = ""
        lab_descripcion.Text = ""
        lab_fecha.Text = ""
        lab_origen.Text = ""
        lab_aplicacion.Text = ""
        list_accion.Enabled = False
        btn_OtraUbi.Enabled = True
        btn_OtraUbi.Enabled = False
        list_codorden.Text = ""
        list_codorden.Focus()
    End Sub
    Public Sub Guardar_datos_tiempo(Optional ByVal ObsGuardarTiempos As String = "")
        Dim TiempoEmpleado As Integer
        Dim sql As String
        Try
            TiempoEmpleado = DateDiff(DateInterval.Second, fecha_inicio, fecha_fin)
            sql = "insert into albaranes_tiempos (CodOrdenDesgMont,idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(" & list_codorden.Text & "," & frm_principal.lab_idoperario.Text & ",'DesgMont','" & fecha_inicio & "','" & fecha_fin & "'," & TiempoEmpleado & ",0,'" & ObsGuardarTiempos & "' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub list_codorden_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_codorden.SelectedIndexChanged
        seleccionar_orden()
    End Sub
    Public Function comprobar_ordenes() As Boolean
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0

        sql = "select count(*) from orden_desg_mont where recogido=1 and depositado=0 and codorden <> " & list_codorden.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows(0)(0) > 0 Then
            If MsgBox("Existen órdenes ya recogidas y que están pendientes de depositar, debería depositarlas antes de empezar una nueva. Continuar de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If

    End Function
    Public Sub seleccionar_orden()
        Dim sql As String
        Dim vdatos, vrefs As New DataTable
        Dim cont, cref As Integer

        'If comprobar_ordenes() = False Then
        '    limpiar(False)
        '    Exit Sub
        'End If

        'Guardar el momento de abrir la orden
        fecha_inicio = Now

        dg_lineas.DataSource = Nothing
        dg_ubi.DataSource = New DataTable
        list_accion.SelectedIndex = -1
        btn_ok.Enabled = False
        txt_cantidad.Text = 0
        btn_finalizar.Enabled = False
        btn_finalizar.Visible = False

        'Obtener el tipo de orden
        sql = "select dm.tipo,dm.depositado,dm.recogido,dm.fechaalta,dm.aplicacion,dm.estado,oa.nombre,dm.aplicacion "
        sql &= "from orden_desg_mont dm "
        sql &= "left join operarios_almacen oa on oa.id=dm.ultusuario "
        sql &= "where dm.codorden = " & list_codorden.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count = 0 Then
            MsgBox("No existe esta orden.", MsgBoxStyle.Exclamation)
            Exit Sub
        Else
            If vdatos.Rows(0).Item("estado").ToString = "Abierta" Then
                MsgBox("Esta orden ya está abierta por el usuario " & vdatos.Rows(0).Item("nombre"), MsgBoxStyle.Information)
                limpiar(True, "Esta orden ya está abierta por otro usuario")
                Exit Sub
            End If
            If vdatos.Rows(0).Item("depositado") = 1 And vdatos.Rows(0).Item("recogido") = 1 Then
                If MsgBox("Esta orden ya ha sido recogida y depositada, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    limpiar(True, "Esta orden ya ha sido recogida y depositada")
                    Exit Sub
                End If
            End If
        End If
        lab_fecha.Text = "Fecha: " & vdatos.Rows(0).Item("fechaalta").ToString
        lab_origen.Text = "Origen: " & vdatos.Rows(0).Item("aplicacion").ToString
        lab_titulo.Text = vdatos.Rows(0).Item("Tipo")
        lab_aplicacion.Text = vdatos.Rows(0).Item("aplicacion")
        If vdatos.Rows(0).Item("Tipo") = "Desglose" Then
            If list_accion.Text = "Recoger" Then
                lab_tit_lineas.Text = "Sets a recoger para desglosarlos:"
            End If
            If list_accion.Text = "Depositar" Then
                lab_tit_lineas.Text = "Trolleys desglosados a ubicar:"
            End If
        End If
        If vdatos.Rows(0).Item("Tipo") = "Montaje" Then
            If list_accion.Text = "Recoger" Then
                lab_tit_lineas.Text = "Referencias a recoger para montajes:"
            End If
            If list_accion.Text = "Depositar" Then
                lab_tit_lineas.Text = "kits/Montajes a ubicar:"
            End If

        End If

        'Antes de nada, hay que llenar la tabla(si no lo está ya ) con las referencias que apareceran al desglosar o las que se necesitan para montar.
        'Esto es necesario para poder preparar la orden
        sql = "select * from orden_desg_mont_refs where codorden=" & list_codorden.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count = 0 Then
            sql = "select codorden,codlinea,codarticulo,cantidad from orden_desg_mont_lin where codorden=" & list_codorden.Text
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            cont = 0
            Do While cont < vdatos.Rows.Count
                If lab_titulo.Text = "Desglose" Then sql = "select CodArticulo from ArticulosSetsLin where CodSet ='" & vdatos.Rows(cont).Item("codarticulo") & "' and tiposet=1 " 'Ivan me dijo que pusiera el 1 de momento y ya veriamos mas adelante.
                If lab_titulo.Text = "Montaje" Then sql = "select CodArticulo from ArticulosPacksLin where codpack ='" & vdatos.Rows(cont).Item("codarticulo") & "' "
                vrefs = conexion.TablaxCmd(constantes.bd_intranet, sql)
                cref = 0
                sql = ""
                Do While cref < vrefs.Rows.Count
                    sql &= "insert into orden_desg_mont_refs(codorden,codlinea,codarticulo,cantidad,ultusuario,origen_llenado) "
                    sql &= "values (" & vdatos.Rows(cont).Item("codorden") & "," & vdatos.Rows(cont).Item("codlinea") & ",'" & vrefs.Rows(cref).Item("CodArticulo").ToString & "'," & vdatos.Rows(cont).Item("cantidad") & "," & frm_principal.lab_idoperario.Text & ",'Ubicaciones')"
                    cref += 1
                Loop

                conexion.ExecuteCmd(constantes.bd_intranet, sql)

                cont += 1
            Loop
        End If

        'Marcar como abierta para que nadie pueda entrar
        sql = "update orden_desg_mont set estado='Abierta',ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text
        conexion.TablaxCmd(constantes.bd_intranet, sql)

        p_filtro.Enabled = False
        btn_salirorden.Enabled = True
        list_accion.Enabled = True
    End Sub
    Public Sub cargar_lineas()
        Dim sql As String
        Dim vdatos, vpick, vpulm As New DataTable
        Dim cont = 0

        If (lab_titulo.Text = "Desglose" And list_accion.Text = "Recoger") Or (lab_titulo.Text = "Montaje" And list_accion.Text = "Depositar") Then
            sql = "select convert(integer,lin.codlinea) as 'Lin.',lin.codarticulo as 'Ref.',lin.codalmacen as 'Alm.',lin.ubicacion as 'Ubicacion',lin.cantidad as 'Cant.',lin.cantidad_validada as 'Val.',NumRefLinMov as 'NR',NumRefLinMovAjuste as 'NRAJ'  "
            sql &= "from orden_desg_mont_lin lin "
            sql &= "where lin.codorden=" & list_codorden.Text & " "
            sql &= "order by convert(integer,lin.codlinea) asc "
        End If

        If (lab_titulo.Text = "Desglose" And list_accion.Text = "Depositar") Or (lab_titulo.Text = "Montaje" And list_accion.Text = "Recoger") Then
            sql = "select convert(integer,lin.codlinea) as 'Lin.',lin.codarticulo as 'Ref.',lin.codalmacen as 'Alm.',lin.ubicacion as 'Ubicacion',lin.cantidad as 'Cant.',lin.cantidad_validada as 'Val.',NumRefLinMov as 'NR',NumRefLinMovAjuste as 'NRAJ' "
            sql &= "from orden_desg_mont_refs lin "
            sql &= "where lin.codorden=" & list_codorden.Text & " "
            sql &= "order by convert(integer,codlinea) asc "
        End If

        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_lineas.DataSource = vdatos
            dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18)
            dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
            'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_lineas.Columns(0).Width = 60
            dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(1).Width = 130
            dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(2).Width = 100
            dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(3).Width = 180
            dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(4).Width = 80
            dg_lineas.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(5).Width = 80

            'Pintar
            cont = 0
            Do While cont < dg_lineas.Rows.Count
                Select Case dg_lineas.Rows(cont).Cells("Val.").Value
                    Case Is = 0 : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.White
                    Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
                    Case Is > dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Red
                    Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
                End Select
                cont += 1
            Loop

            dg_lineas.CurrentCell = Nothing
        Else
            MsgBox("La orden no tiene lineas", MsgBoxStyle.Critical)
            list_codorden.SelectAll()
        End If



    End Sub

    Public Sub cargar_lineas_ubicaciones(ByVal codarticulo As String, ByRef validar As Boolean)
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim fila_encontrada As Boolean = False
        Dim resultado As String
inicio:

        sql = "select codalmacen as 'Alm.',ubicacion as 'Ubicacion',tipo as 'Tipo',cantidad as 'Cant.' "
        sql &= "from ubicaciones where codarticulo='" & codarticulo & "' "
        'Pedido por Sergio. Q no tenga en cuenta devoluciones,saldos o muestras. 14/05/2019
        sql &= "and substring(codalmacen,1,1) not in ('D','S','M') "
        sql &= "order by tipo asc,codalmacen,ubicacion asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows.Count = 0 Then
            If MsgBox("La referencia no tiene ubicación, quiere ubicarla en consolidación de A01.", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                funciones.añadir_reg_mov_ubicaciones(codarticulo, 0, "A01", constantes.consolidacion_A01, "Al desg/mont y no encontrar ubi.", "Ubicaciones-Desg/Mont", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

                sql = "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
                sql &= "values ('A01','" & codarticulo & "','" & constantes.consolidacion_A01 & "','Picking'" & ",0,0,'',0,'Ubicaciones-Desg/Mont','" & frm_principal.lab_nomoperario.Text & "')"
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                If resultado <> "0" Then
                    MsgBox("Se producjo un error al crear la ubicación", MsgBoxStyle.Critical)
                    Exit Sub
                End If
                validar = True
                GoTo inicio
            Else
                Exit Sub
            End If
        End If

        If vdatos.Rows.Count() > 0 Then
            dg_ubi.DataSource = vdatos
            dg_ubi.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18)
            dg_ubi.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
            'dg_ubi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_ubi.Columns(0).Width = 60
            dg_ubi.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_ubi.Columns(1).Width = 130
            dg_ubi.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubi.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_ubi.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubi.Columns(2).Width = 100
            dg_ubi.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubi.Columns(3).Width = 80


            'Pintar y habilitar
            btn_ok.Enabled = False
            dg_ubi.Enabled = True
            cont = 0
            Do While cont < dg_ubi.Rows.Count
                If dg_ubi.Rows(cont).Cells("Alm.").Value = "A01" Then
                    dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.Blue
                    dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.White
                End If
                If dg_ubi.Rows(cont).Cells("Alm.").Value = "A07" Then
                    dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                    dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
                End If
                If dg_ubi.Rows(cont).Cells("Alm.").Value = "A08" Then
                    dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.LightPink
                    dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
                End If
                If dg_ubi.Rows(cont).Cells("Ubicacion").Value.ToString = dg_lineas.SelectedRows(0).Cells("Ubicacion").Value.ToString Then
                    dg_ubi.Rows(cont).Selected = True
                    fila_encontrada = True
                    If dg_lineas.SelectedRows(0).Cells("Val.").Value > 0 Then
                        dg_ubi.Enabled = False
                        btn_ok.Enabled = True
                    End If
                End If
                cont += 1
            Loop

            If fila_encontrada = False Then dg_ubi.CurrentCell = Nothing

        End If


    End Sub

    Private Sub btn_ok_Click(sender As System.Object, e As System.EventArgs) Handles btn_ok.Click
        Dim sql As String = ""
        Dim resultado As String
        Dim vcant As New DataTable
        Dim cantant As Integer
        Dim codarticulo As String = dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim


        If dg_ubi.SelectedRows.Count = 0 Then
            MsgBox("Debe seleccionar la ubicación", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Val.").Value.ToString) > CInt(dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString) Then
            MsgBox("La cantidad a validar no puede ser superior a la cantidad de la linea.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If txt_cantidad.Text = 0 Then
            If dg_lineas.SelectedRows(0).Cells("Alm.").Value.ToString.Trim = "" Then Exit Sub 'No continuo porque al no tener codalmacen daria error.

            If MsgBox("Seguro que quiere eliminar la cantidad preparada?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

            'Obtener la cantidad que tiene hasta el momento la linea para sumarla al stock y dejar el stock en el picking cuadrado
            If (lab_titulo.Text = "Desglose" And list_accion.Text = "Recoger") Or (lab_titulo.Text = "Montaje" And list_accion.Text = "Depositar") Then
                sql = "select cantidad_validada from orden_desg_mont_lin where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value
                vcant = conexion.TablaxCmd(constantes.bd_intranet, sql)
                cantant = CInt(vcant.Rows(0)(0))
                'Dejar stock como estaba en la ubicacion 
                If list_accion.Text = "Recoger" Then sql = "update ubicaciones set cantidad=cantidad+" & cantant & " "
                If list_accion.Text = "Depositar" Then sql = "update ubicaciones set cantidad=cantidad-" & cantant & " "
                sql &= "where codalmacen='" & dg_lineas.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' order by cantidad asc ) "

                sql &= "update orden_desg_mont_lin set cantidad_validada=0,codalmacen='',ubicacion='',ultmodificacion=getdate(),ultusuario=" & frm_principal.lab_idoperario.Text & " "
                sql &= "where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value
            End If

            If (lab_titulo.Text = "Desglose" And list_accion.Text = "Depositar") Or (lab_titulo.Text = "Montaje" And list_accion.Text = "Recoger") Then
                sql = "select cantidad_validada from orden_desg_mont_refs where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & " and codarticulo='" & codarticulo & "' "
                vcant = conexion.TablaxCmd(constantes.bd_intranet, sql)
                cantant = CInt(vcant.Rows(0)(0))
                'Dejar stock como estaba en la ubicacion 
                If list_accion.Text = "Recoger" Then sql = "update ubicaciones set cantidad=cantidad+" & cantant & " "
                If list_accion.Text = "Depositar" Then sql = "update ubicaciones set cantidad=cantidad-" & cantant & " "
                sql &= "where codalmacen='" & dg_lineas.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' order by cantidad asc ) "

                sql &= "update orden_desg_mont_refs set cantidad_validada=0,codalmacen='',ubicacion='',ultmodificacion=getdate(),ultusuario=" & frm_principal.lab_idoperario.Text & " "
                sql &= "where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & " and codarticulo='" & codarticulo & "' "
            End If
            'Actualizar datos grid
            dg_lineas.SelectedRows(0).Cells("Val.").Value = 0
            dg_lineas.SelectedRows(0).Cells("Ubicacion").Value = ""
            dg_lineas.SelectedRows(0).Cells("Alm.").Value = ""

        Else
            'Actualizar la ubicacion 
            If list_accion.Text = "Recoger" Then sql &= "update ubicaciones set cantidad=isnull(cantidad,0)-" & CInt(txt_cantidad.Text) & " "
            If list_accion.Text = "Depositar" Then sql &= "update ubicaciones set cantidad=isnull(cantidad,0)+" & CInt(txt_cantidad.Text) & " "
            sql &= "where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' "
            'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
            sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' order by cantidad asc ) "

            'Actualizar la linea de desg/mont
            If (lab_titulo.Text = "Desglose" And list_accion.Text = "Recoger") Or (lab_titulo.Text = "Montaje" And list_accion.Text = "Depositar") Then
                sql &= "update orden_desg_mont_lin set codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "',ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "',cantidad_validada=cantidad_validada+" & CInt(txt_cantidad.Text) & ",ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
            End If
            If (lab_titulo.Text = "Desglose" And list_accion.Text = "Depositar") Or (lab_titulo.Text = "Montaje" And list_accion.Text = "Recoger") Then
                sql &= "update orden_desg_mont_refs set codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "',ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "',cantidad_validada=cantidad_validada+" & CInt(txt_cantidad.Text) & ",ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " and codarticulo='" & codarticulo & "' "
            End If
            'Poner la cantidad 
            dg_lineas.SelectedRows(0).Cells("Val.").Value = CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Val.").Value.ToString)
            dg_lineas.SelectedRows(0).Cells("Ubicacion").Value = dg_ubi.SelectedRows(0).Cells("Ubicacion").Value.ToString
            dg_lineas.SelectedRows(0).Cells("Alm.").Value = dg_ubi.SelectedRows(0).Cells("Alm.").Value

            'Actualizar el almacén del movimiento que se hizo. Por si se ha elegido un almacén diferente dei que se ha hecho el mov.
            'Solo cuando se valida cantidad >0. en el caso de que sea 0 no hace falta, ya que el almacen no ha cambiado porque no deja elegir otra ubicacion 
            If dg_lineas.SelectedRows(0).Cells("NR").Value.ToString.Length = 36 Then sql &= "update inase.dbo.GesMovAlmLin set AlmacenDest='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' where NumReferencia='" & dg_lineas.SelectedRows(0).Cells("NR").Value.ToString & "' "
            If dg_lineas.SelectedRows(0).Cells("NRAJ").Value.ToString.Length = 36 Then sql &= "update inase.dbo.GesMovAlmLin set AlmacenDest='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' where NumReferencia='" & dg_lineas.SelectedRows(0).Cells("NRAJ").Value.ToString & "' "
        End If


        'Añadir registro. Se hace antes de ejecutar el insert para poder obtener la cantidad anterior antes de que sea modificada
        If list_accion.Text = "Recoger" Then funciones.añadir_reg_mov_ubicaciones(codarticulo, (CInt(txt_cantidad.Text) - cantant) * -1, dg_ubi.SelectedRows(0).Cells("Alm.").Value, dg_ubi.SelectedRows(0).Cells("Ubicacion").Value, "Desg/mont:" & list_codorden.Text, "Ubicaciones-Desg/Mont", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
        If list_accion.Text = "Depositar" Then funciones.añadir_reg_mov_ubicaciones(codarticulo, CInt(txt_cantidad.Text) - cantant, dg_ubi.SelectedRows(0).Cells("Alm.").Value, dg_ubi.SelectedRows(0).Cells("Ubicacion").Value, "Desg/mont:" & list_codorden.Text, "Ubicaciones-Desg/Mont", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado <> "0" Then
            MsgBox("Se produjo un error, avisar a Informática. Err 20180116", MsgBoxStyle.Exclamation)
        End If
        Select Case dg_lineas.SelectedRows(0).Cells("Val.").Value
            Case Is = 0 : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Red
            Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow
        End Select

        'Llenar de nuevo ubicaciones para actualizar las cantidades por ubicacion.
        If dg_lineas.SelectedRows(0).Cells("Cant.").Value <> dg_lineas.SelectedRows(0).Cells("Val.").Value Then
            cargar_lineas_ubicaciones(codarticulo, False)
            'No puede elegir otra ubicacion q no sea la que ya usado
            'Esto se hace dentro de la funcion q acabo de llamar
        Else
            dg_lineas.CurrentCell = Nothing
            dg_ubi.DataSource = Nothing
            dg_ubi.Enabled = False
            txt_cantidad.Text = 0
            btn_ok.Enabled = False
        End If


    End Sub

    Private Sub txt_cantidad_Click(sender As System.Object, e As System.EventArgs) Handles txt_cantidad.Click
        txt_cantidad.SelectAll()
    End Sub

    Private Sub txt_cantidad_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_cantidad.KeyUp
        If e.KeyCode = Keys.Enter Then
            If IsNumeric(txt_cantidad.Text) Then btn_ok.Focus()
        End If
    End Sub

    Private Sub dg_lineas_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_lineas.CellClick
        Dim sql As String
        Dim vdatos As New DataTable
        Dim validar As Boolean = False

        If e.RowIndex = -1 Then Exit Sub

        dg_ubi.DataSource = Nothing

        sql = "select descripcion from gesarticulos where codarticulo='" & dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value & "' and codempresa=1 "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        lab_descripcion.Text = Microsoft.VisualBasic.Left(vdatos.Rows(0)(0).ToString, 30)
        cargar_lineas_ubicaciones(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, validar)
        btn_OtraUbi.Enabled = True
        txt_cantidad.Text = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value - dg_lineas.Rows(e.RowIndex).Cells("Val.").Value

        If validar = True Then
            dg_ubi.Rows(0).Selected = True
            btn_ok_Click(sender, e)
        End If

        If finalizadaDepositada = True And list_accion.Text = "Depositar" Then
            btn_ok.Enabled = False
            txt_cantidad.Enabled = False
        Else
            btn_ok.Enabled = True
            txt_cantidad.Enabled = True
        End If
        If finalizadaRecogida = True And list_accion.Text = "Recoger" Then
            btn_ok.Enabled = False
            txt_cantidad.Enabled = False
        Else
            btn_ok.Enabled = True
            txt_cantidad.Enabled = True
        End If

    End Sub

    Private Sub list_codorden_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles list_codorden.KeyUp
        If e.KeyCode = Keys.Enter Then
            seleccionar_orden()
        End If
    End Sub


    Private Sub dg_ubi_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_ubi.CellClick
        btn_ok.Enabled = True
        txt_cantidad.SelectAll()
        txt_cantidad.Focus()
    End Sub

    Private Sub list_accion_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_accion.DropDownClosed
        Dim sql As String
        Dim vdatos As New DataTable
        sql = "select recogido,depositado from orden_desg_mont where codorden = " & list_codorden.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows(0).Item("recogido") = 1 Then finalizadaRecogida = True Else finalizadaRecogida = False
        If vdatos.Rows(0).Item("depositado") = 1 Then finalizadaDepositada = True Else finalizadaDepositada = False

        If list_accion.Text = "Recoger" Then
            If vdatos.Rows(0).Item("recogido") = 1 Then
                If MsgBox("Esta orden ya ha sido recogida, abrir de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                btn_finalizar.Enabled = False
            Else
                btn_finalizar.Enabled = True
            End If
            btn_finalizar.Text = "Finalizar Recoger"
            If lab_titulo.Text = "Desglose" Then lab_tit_lineas.Text = "Sets a desglosar"
            If lab_titulo.Text = "Montaje" Then lab_tit_lineas.Text = "Referencias necesarias para los montajes"
        End If

        If list_accion.Text = "Depositar" Then
            If vdatos.Rows(0).Item("depositado") = 1 Then
                If MsgBox("Esta orden ya ha sido depositada, abrir de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                btn_finalizar.Enabled = False
            Else
                btn_finalizar.Enabled = True
            End If
            btn_finalizar.Text = "Finalizar Depositar"
            If lab_titulo.Text = "Desglose" Then lab_tit_lineas.Text = "Referencias desglosadas a ubicar"
            If lab_titulo.Text = "Montaje" Then lab_tit_lineas.Text = "Referencias montadas a ubicar"
        End If


        dg_lineas.Enabled = True
        txt_cantidad.Text = 0
        dg_ubi.DataSource = Nothing
        cargar_lineas()
        btn_finalizar.Visible = True

    End Sub

    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql As String = ""
        Dim resultado As String
        Dim cont As Integer
        Dim sql_ajuste_mov As String = ""
        Dim completo As Boolean = True
        Dim numrefmov, numrefmov_bro, numrefmov_jou As String
        Dim nummov, nummov_bro, nummov_jou As Integer
        Dim emp_mov, contmov, contmov_bro, contmov_jou As Integer
        Dim cantidad_ajuste As Integer
        Dim vrefespbrothers As New DataTable


        If list_accion.Text = "Depositar" And finalizadaRecogida = False Then
            MsgBox("Debe finalizar la recogida antes.")
            Exit Sub
        End If

        'Comprobar si todas las líneas están recogidas/depositadas
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("Val.").Value Then
                If MsgBox("Existen líneas por completar. Si continua, continuar de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Exit Sub
                Else
                    Exit Do
                End If
            End If
            cont += 1
        Loop

        llenar_ref_esp_brothers(vrefespbrothers)

        'Movimiento de ajuste
        numrefmov = ""
        numrefmov_bro = ""
        numrefmov_jou = ""
        nummov = 0
        nummov_bro = 0
        contmov_jou = 1
        contmov_bro = 1

        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("Val.").Value Then
                If funciones.ref_esp_brothers(dg_lineas.Rows(cont).Cells("Ref.").Value, vrefespbrothers) = True Then
                    emp_mov = 3
                    If nummov_bro = 0 Then funciones.crear_movimiento_inase(emp_mov, nummov_bro, "Mov. Ajuste por desg/mont menos cantidad que la orden(" & list_codorden.Text & ").", numrefmov_bro)
                    numrefmov = numrefmov_bro
                    nummov = nummov_bro
                    contmov = contmov_bro
                End If
                If funciones.ref_esp_brothers(dg_lineas.Rows(cont).Cells("Ref.").Value, vrefespbrothers) = False Then
                    emp_mov = 1
                    If nummov_jou = 0 Then funciones.crear_movimiento_inase(emp_mov, nummov_jou, "Mov. Ajuste por desg/mont menos cantidad que la orden(" & list_codorden.Text & ").", numrefmov_jou)
                    numrefmov = numrefmov_jou
                    nummov = nummov_jou
                    contmov = contmov_jou
                End If

                If list_accion.Text = "Recoger" Then
                    cantidad_ajuste = dg_lineas.Rows(cont).Cells("Cant.").Value - dg_lineas.Rows(cont).Cells("Val.").Value
                End If
                If list_accion.Text = "Depositar" Then
                    cantidad_ajuste = dg_lineas.Rows(cont).Cells("Val.").Value - dg_lineas.Rows(cont).Cells("Cant.").Value
                End If

                If numrefmov <> "" Then
                    sql_ajuste_mov &= "insert into GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                    sql_ajuste_mov &= "values('" & numrefmov & "',0," & emp_mov & ",'" & Now.Year & "'," & nummov & "," & contmov & ",'" & funciones.almacen_articulo(dg_lineas.Rows(cont).Cells("Ref.").Value, "A01") & "','XXX','" & dg_lineas.Rows(cont).Cells("Ref.").Value & "'," & cantidad_ajuste & ",'Movimiento ajuste desde Ubicaciones/Desg_mont por cant. validada diferente de original (Orden:" & list_codorden.Text & ".') "
                    contmov += 1
                Else
                    funciones.Enviar_correo("vicentesoler@joumma.com", "Se producjo un error en una orden de desg/mont.", "Error creando cabecera movimiento ajuste de desglose/packs por cantidad diferente al confirmar orden")
                End If

                If emp_mov = 1 Then contmov_jou = contmov
                If emp_mov = 3 Then contmov_bro = contmov

            End If
            cont += 1
        Loop

        resultado = conexion.ExecuteCmd(constantes.bd_inase, sql_ajuste_mov)


        If list_accion.Text = "Recoger" Then
            sql = "update orden_desg_mont set estado='',recogido=1,usuario_recoger=" & frm_principal.lab_idoperario.Text & " ,fecha_recoger=getdate(),ultmodificacion=getdate(),ultusuario=" & frm_principal.lab_idoperario.Text & " "
            sql &= "where codorden=" & list_codorden.Text
        End If
        If list_accion.Text = "Depositar" Then
            sql = "update orden_desg_mont set estado='',depositado=1,usuario_depositar=" & frm_principal.lab_idoperario.Text & " ,fecha_depositar=getdate(),ultmodificacion=getdate(),ultusuario=" & frm_principal.lab_idoperario.Text & " "
            sql &= "where codorden=" & list_codorden.Text
        End If
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
        If resultado <> "0" Then
            MsgBox("Error al guardar los datos.", MsgBoxStyle.Critical)
        Else
            limpiar(True, "Finalizar")
            llenar_ordenes()
            Exit Sub
        End If
    End Sub

    Private Sub list_codorden_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_codorden.DropDownClosed
        Dim sql

        'Al desplegar el combo y seleccionar una orden, el evento se produce cuando todavia el combo tiene el valor anterior, por tanto es lo que busco, ya que quiero
        'que la orden que se estaba preparando quede marcada como no abierta. 
        sql = "update orden_desg_mont set estado='',ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text
        conexion.ExecuteCmd(constantes.bd_intranet, sql)

    End Sub

    Private Sub frm_Desg_mont_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Dim sql
        If list_codorden.Text.ToString <> "" Then
            sql = "update orden_desg_mont set estado='',ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If

    End Sub

    Private Sub btn_salirorden_Click(sender As System.Object, e As System.EventArgs) Handles btn_salirorden.Click
        Dim sql
        If list_codorden.Text.ToString <> "" Then
            sql = "update orden_desg_mont set estado='',ultusuario=" & frm_principal.lab_idoperario.Text & ",ultmodificacion=getdate() where codorden=" & list_codorden.Text
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If
        limpiar(True, "Salir")
        llenar_ordenes()

    End Sub

    Private Sub txt_codarticulo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_filtro_codarticulo.KeyUp
        Dim sql
        If e.KeyCode = Keys.Enter Then
            If txt_filtro_codarticulo.Text.ToString.Length = 13 Then funciones.buscar_descripcion(txt_filtro_codarticulo.Text)
            If txt_filtro_codarticulo.Text.ToString.Length = 7 Then
                If list_codorden.Text.ToString <> "" Then
                    sql = "update orden_desg_mont set estado='',ultusuario=" & frm_principal.lab_idoperario.Text & ",ultmodificacion=getdate() where codorden=" & list_codorden.Text
                    conexion.ExecuteCmd(constantes.bd_intranet, sql)
                End If
                llenar_ordenes()
                limpiar(False)
            End If
        End If
    End Sub

    Private Sub list_filtro_almacen_DropDownClosed(sender As Object, e As EventArgs) Handles list_filtro_almacen.DropDownClosed
        Dim sql
        If list_codorden.Text.ToString <> "" Then
            sql = "update orden_desg_mont set estado='',ultusuario=" & frm_principal.lab_idoperario.Text & ",ultmodificacion=getdate() where codorden=" & list_codorden.Text
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub cb_FiltroPorRecoger_Click(sender As Object, e As EventArgs) Handles cb_FiltroStockA01.Click
        llenar_ordenes()
    End Sub

    Private Sub CheckBox1_Click(sender As Object, e As EventArgs) Handles cb_FiltroStockA07.Click
        llenar_ordenes()
    End Sub

    Private Sub cb_filtro_urgentes_Click(sender As Object, e As EventArgs) Handles cb_filtro_urgentes.Click
        llenar_ordenes()
    End Sub

    Private Sub cb_PorRecoger_Click(sender As Object, e As EventArgs) Handles cb_FiltroPorRecoger.Click
        llenar_ordenes()
    End Sub

    Private Sub cb_PorDepositar_Click(sender As Object, e As EventArgs) Handles cb_FiltroPorDepositar.Click
        llenar_ordenes()
    End Sub

    Private Sub btn_OtraUbi_Click(sender As Object, e As EventArgs) Handles btn_OtraUbi.Click
        Dim validar As Boolean
        frm_nuevaubicacion.txt_cantmax.Text = 0
        frm_nuevaubicacion.txt_pasillo.Text = ""
        frm_nuevaubicacion.txt_portal.Text = ""
        frm_nuevaubicacion.txt_altura.Text = ""
        frm_nuevaubicacion.txt_tamaño.Text = 0
        frm_nuevaubicacion.lab_ref.Text = dg_lineas.SelectedRows(0).Cells("Ref.").Value
        frm_nuevaubicacion.ShowDialog()
        cargar_lineas_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value, validar)

    End Sub



    Private Sub btn_limpiar_Click(sender As System.Object, e As System.EventArgs) Handles btn_limpiar.Click
        Dim sql
        list_filtro_almacen.SelectedIndex = -1
        txt_filtro_codarticulo.Text = ""
        cb_filtro_urgentes.Checked = False
        cb_FiltroPorRecoger.Checked = False
        cb_FiltroPorDepositar.Checked = False
        cb_FiltroStockA01.Checked = False
        cb_FiltroStockA07.Checked = False
        list_filtro_almacen.SelectedIndex = -1
        If list_codorden.Text.ToString <> "" Then
            sql = "update orden_desg_mont set estado='',ultusuario=" & frm_principal.lab_idoperario.Text & ",ultmodificacion=getdate() where codorden=" & list_codorden.Text
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If
        llenar_ordenes()
        limpiar(False)
    End Sub


    Private Sub btn_filtro_ok_Click(sender As System.Object, e As System.EventArgs) Handles btn_filtro_ok.Click
        Dim sql
        If list_codorden.Text.ToString <> "" Then
            sql = "update orden_desg_mont set estado='',ultusuario=" & frm_principal.lab_idoperario.Text & ",ultmodificacion=getdate() where codorden=" & list_codorden.Text
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If
        llenar_ordenes()
        limpiar(False)
    End Sub



End Class