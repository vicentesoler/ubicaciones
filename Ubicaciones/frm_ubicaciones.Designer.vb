﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ubicaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txt_pasillo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_portal = New System.Windows.Forms.TextBox()
        Me.txt_altura = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dg_contenido_ubicacion = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_tamaño = New System.Windows.Forms.TextBox()
        Me.btn_modificar_guardar_tamaño_ubicacion = New System.Windows.Forms.Button()
        Me.btn_cancel_nuevo_ubicacion = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btn_mostrar_ubicacion = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.List_tipo = New System.Windows.Forms.ComboBox()
        Me.btn_ubic_vacias = New System.Windows.Forms.Button()
        Me.list_almacen = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.list_filtro_tipo = New System.Windows.Forms.ComboBox()
        Me.btn_eliminar_fila = New System.Windows.Forms.Button()
        Me.btn_eliminar = New System.Windows.Forms.Button()
        Me.p_añadir_a_ubi = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_añadir_cantmax = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.list_añadir_tipo = New System.Windows.Forms.ComboBox()
        Me.txt_añadir_articulo = New System.Windows.Forms.TextBox()
        Me.btn_añadir_a_ubi = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_descripcion_ubicacion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_añadir_tamaño = New System.Windows.Forms.TextBox()
        Me.txt_añadir_cantidad = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        CType(Me.dg_contenido_ubicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.p_añadir_a_ubi.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_pasillo
        '
        Me.txt_pasillo.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pasillo.Location = New System.Drawing.Point(147, 51)
        Me.txt_pasillo.MaxLength = 7
        Me.txt_pasillo.Name = "txt_pasillo"
        Me.txt_pasillo.Size = New System.Drawing.Size(70, 53)
        Me.txt_pasillo.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 31)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Ubicación"
        '
        'txt_portal
        '
        Me.txt_portal.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_portal.Location = New System.Drawing.Point(250, 51)
        Me.txt_portal.MaxLength = 7
        Me.txt_portal.Name = "txt_portal"
        Me.txt_portal.Size = New System.Drawing.Size(70, 53)
        Me.txt_portal.TabIndex = 3
        '
        'txt_altura
        '
        Me.txt_altura.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_altura.Location = New System.Drawing.Point(353, 51)
        Me.txt_altura.MaxLength = 7
        Me.txt_altura.Name = "txt_altura"
        Me.txt_altura.Size = New System.Drawing.Size(70, 53)
        Me.txt_altura.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(220, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 39)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "/"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(323, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(27, 39)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "/"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(143, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 20)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Pasillo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(249, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 20)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Portal"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(352, 106)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 20)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Altura"
        '
        'dg_contenido_ubicacion
        '
        Me.dg_contenido_ubicacion.AllowUserToAddRows = False
        Me.dg_contenido_ubicacion.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_contenido_ubicacion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dg_contenido_ubicacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_contenido_ubicacion.DefaultCellStyle = DataGridViewCellStyle2
        Me.dg_contenido_ubicacion.Location = New System.Drawing.Point(20, 305)
        Me.dg_contenido_ubicacion.MultiSelect = False
        Me.dg_contenido_ubicacion.Name = "dg_contenido_ubicacion"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_contenido_ubicacion.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dg_contenido_ubicacion.Size = New System.Drawing.Size(1223, 265)
        Me.dg_contenido_ubicacion.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(451, 106)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 20)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Tamaño"
        '
        'txt_tamaño
        '
        Me.txt_tamaño.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_tamaño.Location = New System.Drawing.Point(437, 51)
        Me.txt_tamaño.Name = "txt_tamaño"
        Me.txt_tamaño.ReadOnly = True
        Me.txt_tamaño.Size = New System.Drawing.Size(99, 53)
        Me.txt_tamaño.TabIndex = 16
        Me.txt_tamaño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btn_modificar_guardar_tamaño_ubicacion
        '
        Me.btn_modificar_guardar_tamaño_ubicacion.BackColor = System.Drawing.Color.Yellow
        Me.btn_modificar_guardar_tamaño_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_modificar_guardar_tamaño_ubicacion.Location = New System.Drawing.Point(1082, 23)
        Me.btn_modificar_guardar_tamaño_ubicacion.Name = "btn_modificar_guardar_tamaño_ubicacion"
        Me.btn_modificar_guardar_tamaño_ubicacion.Size = New System.Drawing.Size(161, 45)
        Me.btn_modificar_guardar_tamaño_ubicacion.TabIndex = 18
        Me.btn_modificar_guardar_tamaño_ubicacion.Text = "Modif."
        Me.btn_modificar_guardar_tamaño_ubicacion.UseVisualStyleBackColor = False
        Me.btn_modificar_guardar_tamaño_ubicacion.Visible = False
        '
        'btn_cancel_nuevo_ubicacion
        '
        Me.btn_cancel_nuevo_ubicacion.BackColor = System.Drawing.Color.Cyan
        Me.btn_cancel_nuevo_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancel_nuevo_ubicacion.Location = New System.Drawing.Point(900, 88)
        Me.btn_cancel_nuevo_ubicacion.Name = "btn_cancel_nuevo_ubicacion"
        Me.btn_cancel_nuevo_ubicacion.Size = New System.Drawing.Size(148, 45)
        Me.btn_cancel_nuevo_ubicacion.TabIndex = 19
        Me.btn_cancel_nuevo_ubicacion.Text = "Limpiar"
        Me.btn_cancel_nuevo_ubicacion.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(539, 69)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 25)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Cm."
        '
        'btn_mostrar_ubicacion
        '
        Me.btn_mostrar_ubicacion.BackColor = System.Drawing.Color.Chartreuse
        Me.btn_mostrar_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_mostrar_ubicacion.Location = New System.Drawing.Point(900, 23)
        Me.btn_mostrar_ubicacion.Name = "btn_mostrar_ubicacion"
        Me.btn_mostrar_ubicacion.Size = New System.Drawing.Size(148, 45)
        Me.btn_mostrar_ubicacion.TabIndex = 21
        Me.btn_mostrar_ubicacion.Text = "Mostrar"
        Me.btn_mostrar_ubicacion.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(21, 277)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(242, 25)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Contenido de Ubicación"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.List_tipo)
        Me.Panel1.Controls.Add(Me.btn_ubic_vacias)
        Me.Panel1.Controls.Add(Me.list_almacen)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.list_filtro_tipo)
        Me.Panel1.Controls.Add(Me.btn_eliminar_fila)
        Me.Panel1.Controls.Add(Me.btn_eliminar)
        Me.Panel1.Controls.Add(Me.p_añadir_a_ubi)
        Me.Panel1.Controls.Add(Me.dg_contenido_ubicacion)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txt_portal)
        Me.Panel1.Controls.Add(Me.btn_mostrar_ubicacion)
        Me.Panel1.Controls.Add(Me.txt_altura)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txt_pasillo)
        Me.Panel1.Controls.Add(Me.btn_cancel_nuevo_ubicacion)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.btn_modificar_guardar_tamaño_ubicacion)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txt_tamaño)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1250, 640)
        Me.Panel1.TabIndex = 23
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(690, 108)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(39, 20)
        Me.Label19.TabIndex = 39
        Me.Label19.Text = "Tipo"
        '
        'List_tipo
        '
        Me.List_tipo.Enabled = False
        Me.List_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.List_tipo.FormattingEnabled = True
        Me.List_tipo.Items.AddRange(New Object() {" ", "Consolidacion", "Devoluciones", "Traslados", "OF"})
        Me.List_tipo.Location = New System.Drawing.Point(608, 50)
        Me.List_tipo.Name = "List_tipo"
        Me.List_tipo.Size = New System.Drawing.Size(262, 54)
        Me.List_tipo.TabIndex = 35
        '
        'btn_ubic_vacias
        '
        Me.btn_ubic_vacias.BackColor = System.Drawing.Color.Cyan
        Me.btn_ubic_vacias.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ubic_vacias.Location = New System.Drawing.Point(455, 587)
        Me.btn_ubic_vacias.Name = "btn_ubic_vacias"
        Me.btn_ubic_vacias.Size = New System.Drawing.Size(283, 45)
        Me.btn_ubic_vacias.TabIndex = 38
        Me.btn_ubic_vacias.Text = "Ver Ubic. vacias"
        Me.btn_ubic_vacias.UseVisualStyleBackColor = False
        '
        'list_almacen
        '
        Me.list_almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 29.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almacen.FormattingEnabled = True
        Me.list_almacen.Location = New System.Drawing.Point(22, 51)
        Me.list_almacen.Name = "list_almacen"
        Me.list_almacen.Size = New System.Drawing.Size(108, 52)
        Me.list_almacen.TabIndex = 37
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(34, 106)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(71, 20)
        Me.Label18.TabIndex = 36
        Me.Label18.Text = "Almacen"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(24, 595)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 31)
        Me.Label12.TabIndex = 34
        Me.Label12.Text = "Filtro"
        '
        'list_filtro_tipo
        '
        Me.list_filtro_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_filtro_tipo.FormattingEnabled = True
        Me.list_filtro_tipo.Items.AddRange(New Object() {"TODOS", "Pulmon", "Picking"})
        Me.list_filtro_tipo.Location = New System.Drawing.Point(111, 587)
        Me.list_filtro_tipo.Name = "list_filtro_tipo"
        Me.list_filtro_tipo.Size = New System.Drawing.Size(175, 46)
        Me.list_filtro_tipo.TabIndex = 33
        '
        'btn_eliminar_fila
        '
        Me.btn_eliminar_fila.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_eliminar_fila.Location = New System.Drawing.Point(1009, 587)
        Me.btn_eliminar_fila.Name = "btn_eliminar_fila"
        Me.btn_eliminar_fila.Size = New System.Drawing.Size(234, 45)
        Me.btn_eliminar_fila.TabIndex = 33
        Me.btn_eliminar_fila.Text = "Eliminar fila"
        Me.btn_eliminar_fila.UseVisualStyleBackColor = True
        '
        'btn_eliminar
        '
        Me.btn_eliminar.BackColor = System.Drawing.Color.Red
        Me.btn_eliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_eliminar.Location = New System.Drawing.Point(1082, 88)
        Me.btn_eliminar.Name = "btn_eliminar"
        Me.btn_eliminar.Size = New System.Drawing.Size(161, 45)
        Me.btn_eliminar.TabIndex = 32
        Me.btn_eliminar.Text = "Eliminar"
        Me.btn_eliminar.UseVisualStyleBackColor = False
        Me.btn_eliminar.Visible = False
        '
        'p_añadir_a_ubi
        '
        Me.p_añadir_a_ubi.BackColor = System.Drawing.Color.SkyBlue
        Me.p_añadir_a_ubi.Controls.Add(Me.Label16)
        Me.p_añadir_a_ubi.Controls.Add(Me.txt_añadir_cantmax)
        Me.p_añadir_a_ubi.Controls.Add(Me.Label7)
        Me.p_añadir_a_ubi.Controls.Add(Me.list_añadir_tipo)
        Me.p_añadir_a_ubi.Controls.Add(Me.txt_añadir_articulo)
        Me.p_añadir_a_ubi.Controls.Add(Me.btn_añadir_a_ubi)
        Me.p_añadir_a_ubi.Controls.Add(Me.Label11)
        Me.p_añadir_a_ubi.Controls.Add(Me.txt_descripcion_ubicacion)
        Me.p_añadir_a_ubi.Controls.Add(Me.Label13)
        Me.p_añadir_a_ubi.Controls.Add(Me.Label15)
        Me.p_añadir_a_ubi.Controls.Add(Me.txt_añadir_tamaño)
        Me.p_añadir_a_ubi.Controls.Add(Me.txt_añadir_cantidad)
        Me.p_añadir_a_ubi.Controls.Add(Me.Label14)
        Me.p_añadir_a_ubi.Enabled = False
        Me.p_añadir_a_ubi.Location = New System.Drawing.Point(20, 149)
        Me.p_añadir_a_ubi.Name = "p_añadir_a_ubi"
        Me.p_añadir_a_ubi.Size = New System.Drawing.Size(1223, 113)
        Me.p_añadir_a_ubi.TabIndex = 31
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(986, 92)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(70, 17)
        Me.Label16.TabIndex = 34
        Me.Label16.Text = "Cant. Max"
        '
        'txt_añadir_cantmax
        '
        Me.txt_añadir_cantmax.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_añadir_cantmax.Location = New System.Drawing.Point(978, 36)
        Me.txt_añadir_cantmax.MaxLength = 4
        Me.txt_añadir_cantmax.Name = "txt_añadir_cantmax"
        Me.txt_añadir_cantmax.Size = New System.Drawing.Size(82, 53)
        Me.txt_añadir_cantmax.TabIndex = 33
        Me.txt_añadir_cantmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(628, 92)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 17)
        Me.Label7.TabIndex = 32
        Me.Label7.Text = "Tipo"
        '
        'list_añadir_tipo
        '
        Me.list_añadir_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_añadir_tipo.FormattingEnabled = True
        Me.list_añadir_tipo.Location = New System.Drawing.Point(540, 35)
        Me.list_añadir_tipo.Name = "list_añadir_tipo"
        Me.list_añadir_tipo.Size = New System.Drawing.Size(216, 54)
        Me.list_añadir_tipo.TabIndex = 31
        '
        'txt_añadir_articulo
        '
        Me.txt_añadir_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_añadir_articulo.Location = New System.Drawing.Point(14, 35)
        Me.txt_añadir_articulo.MaxLength = 13
        Me.txt_añadir_articulo.Name = "txt_añadir_articulo"
        Me.txt_añadir_articulo.Size = New System.Drawing.Size(174, 53)
        Me.txt_añadir_articulo.TabIndex = 24
        '
        'btn_añadir_a_ubi
        '
        Me.btn_añadir_a_ubi.Enabled = False
        Me.btn_añadir_a_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_añadir_a_ubi.Location = New System.Drawing.Point(1083, 35)
        Me.btn_añadir_a_ubi.Name = "btn_añadir_a_ubi"
        Me.btn_añadir_a_ubi.Size = New System.Drawing.Size(134, 53)
        Me.btn_añadir_a_ubi.TabIndex = 30
        Me.btn_añadir_a_ubi.Text = "Añadir"
        Me.btn_añadir_a_ubi.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(9, 5)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(194, 25)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Añadir a Ubicación"
        '
        'txt_descripcion_ubicacion
        '
        Me.txt_descripcion_ubicacion.BackColor = System.Drawing.Color.SkyBlue
        Me.txt_descripcion_ubicacion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_descripcion_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_descripcion_ubicacion.ForeColor = System.Drawing.Color.DarkGreen
        Me.txt_descripcion_ubicacion.Location = New System.Drawing.Point(194, 35)
        Me.txt_descripcion_ubicacion.MaxLength = 2
        Me.txt_descripcion_ubicacion.Multiline = True
        Me.txt_descripcion_ubicacion.Name = "txt_descripcion_ubicacion"
        Me.txt_descripcion_ubicacion.Size = New System.Drawing.Size(367, 49)
        Me.txt_descripcion_ubicacion.TabIndex = 25
        Me.txt_descripcion_ubicacion.Text = "Descripción artículo"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(72, 93)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 17)
        Me.Label13.TabIndex = 25
        Me.Label13.Text = "Artículo"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(889, 92)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(64, 17)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = "Cantidad"
        '
        'txt_añadir_tamaño
        '
        Me.txt_añadir_tamaño.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_añadir_tamaño.Location = New System.Drawing.Point(774, 35)
        Me.txt_añadir_tamaño.MaxLength = 3
        Me.txt_añadir_tamaño.Name = "txt_añadir_tamaño"
        Me.txt_añadir_tamaño.Size = New System.Drawing.Size(90, 53)
        Me.txt_añadir_tamaño.TabIndex = 26
        Me.txt_añadir_tamaño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_añadir_cantidad
        '
        Me.txt_añadir_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_añadir_cantidad.Location = New System.Drawing.Point(880, 35)
        Me.txt_añadir_cantidad.MaxLength = 4
        Me.txt_añadir_cantidad.Name = "txt_añadir_cantidad"
        Me.txt_añadir_cantidad.Size = New System.Drawing.Size(82, 53)
        Me.txt_añadir_cantidad.TabIndex = 28
        Me.txt_añadir_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(772, 92)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 17)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "Tamaño palet"
        '
        'frm_ubicaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_ubicaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ubicaciones"
        CType(Me.dg_contenido_ubicacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.p_añadir_a_ubi.ResumeLayout(False)
        Me.p_añadir_a_ubi.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt_pasillo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_portal As System.Windows.Forms.TextBox
    Friend WithEvents txt_altura As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dg_contenido_ubicacion As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_tamaño As System.Windows.Forms.TextBox
    Friend WithEvents btn_modificar_guardar_tamaño_ubicacion As System.Windows.Forms.Button
    Friend WithEvents btn_cancel_nuevo_ubicacion As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btn_mostrar_ubicacion As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btn_añadir_a_ubi As System.Windows.Forms.Button
    Friend WithEvents txt_descripcion_ubicacion As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_añadir_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_añadir_tamaño As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txt_añadir_articulo As System.Windows.Forms.TextBox
    Friend WithEvents p_añadir_a_ubi As System.Windows.Forms.Panel
    Friend WithEvents list_añadir_tipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btn_eliminar As System.Windows.Forms.Button
    Friend WithEvents btn_eliminar_fila As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents list_filtro_tipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_añadir_cantmax As System.Windows.Forms.TextBox
    Friend WithEvents list_almacen As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents btn_ubic_vacias As System.Windows.Forms.Button
    Friend WithEvents Label19 As Label
    Friend WithEvents List_tipo As ComboBox
End Class
