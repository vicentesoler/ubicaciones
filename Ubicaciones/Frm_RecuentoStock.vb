﻿Public Class Frm_RecuentoStock
    Dim conexion As New Conector
    Dim cargainicial As Boolean
    Public fecha_inicio, fecha_fin As DateTime

    Private Sub Frm_RecuentoStock_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fecha_inicio = Now
        limpiar(False)
        funciones.llenar_almacenes(list_alm, False, False, False)
        llenar_grid()
    End Sub
    Public Sub llenar_grid()
        Dim sql As String
        Dim vdatos, vmovprev As New DataTable
        Dim cont As Integer

        'Consulta referencias por recontar
        sql = "select cas.id,cas.codarticulo as 'Ref.',art.Descripcion ,isnull(cas.existencias,0) as 'Exist.',cas.Recontado as 'R' "
        sql &= "from circuitoagotarstock cas "
        sql &= "inner join inase.dbo.GesArticulos art on art.CodArticulo =cas.codarticulo And art.CodEmpresa =1 "
        sql &= "inner join ubicaciones ubi on ubi.codarticulo =cas.codarticulo collate Modern_Spanish_CI_AS and ubi.codalmacen ='" & list_alm.SelectedValue & "' and ubi.tipo='Picking' "
        sql &= "And ubi.id = (select top 1 ubi2.id from ubicaciones ubi2 where ubi2.codarticulo=ubi.codarticulo And ubi2.codalmacen=ubi.codalmacen And ubi2.tipo=ubi.tipo order by ubi2.id asc ) "
        sql &= "inner join circuitoagotarstockLin lin on lin.idcab=cas.id and lin.codalmacen='" & list_alm.SelectedValue & "' and lin.recontado=0 "
        sql &= "where cas.recontado=0 and Circuito_Terminado=0 "

        sql &= "and cas.codarticulo collate Modern_Spanish_CI_AS in (select codarticulo from ubicaciones where codalmacen='" & list_alm.SelectedValue & "' and tipo='Picking') "
        If list_alm.SelectedValue = "A01" Then
            sql &= "order by convert(Integer, Left(isnull(ubi.ubicacion,'99-99-99'),2)) asc, "
            sql &= "case when convert(integer, Left(isnull(ubi.ubicacion,'99-99-99'),2)) in (1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99) then convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),4,2)) else 99-convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),4,2)) end asc, "
            sql &= "case when convert(integer, Left(isnull(ubi.ubicacion,'99-99-99'),2)) in (1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99) then convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),7,2)) else 99-convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),7,2)) end asc, "
            sql &= "cas.codarticulo asc "
        Else
            sql &= "order by ubi.ubicacion asc "
        End If

        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        dg_referencias.DataSource = vdatos
        dg_referencias.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold)
        dg_referencias.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 16)
        dg_referencias.Columns(0).Width = 40
        dg_referencias.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        dg_referencias.Columns(0).ReadOnly = True
        dg_referencias.Columns(1).Width = 110
        dg_referencias.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_referencias.Columns(1).ReadOnly = True
        dg_referencias.Columns(2).Width = 500
        dg_referencias.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomLeft
        dg_referencias.Columns(2).ReadOnly = True
        dg_referencias.Columns(3).Width = 100
        dg_referencias.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_referencias.Columns(3).ReadOnly = True
        dg_referencias.Columns(4).Width = 40
        dg_referencias.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_referencias.Columns(4).ReadOnly = True

        'dg_referencias.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells


        If vdatos.Rows.Count > 0 Then
            dg_referencias.Rows(0).Selected = True
            Dim evento As New System.Windows.Forms.DataGridViewCellEventArgs(1, 0)
            dg_referencias_CellClick(dg_referencias, evento)
            If dg_ubi.Rows.Count > 0 Then
                dg_ubi.Rows(0).Selected = True
                evento = New System.Windows.Forms.DataGridViewCellEventArgs(1, 0)
                dg_ubi_CellClick(dg_ubi, evento)
            End If
        End If


    End Sub
    Public Sub limpiar(Optional ByVal GuardarTiempos As Boolean = True, Optional ByVal ObsGuardarTiempos As String = "")
        'Guardar datos de operario
        fecha_fin = Now
        If GuardarTiempos Then Guardar_datos_tiempo(ObsGuardarTiempos)

        txt_articulo.Text = ""
        txt_articulo.BackColor = Color.White

        dg_ubi.DataSource = Nothing


    End Sub

    Private Sub Frm_RecuentoStock_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        limpiar(True, "Cerrar")
    End Sub

    Private Sub dg_referencias_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dg_referencias.CellClick
        Dim sql As String
        Dim resultado As String
        Dim cont As Integer
        Dim todasrecontadas As Boolean = True
        Dim vaux As New DataTable

        If e.RowIndex >= 0 Then
            txt_articulo.Text = dg_referencias.Rows(e.RowIndex).Cells("Ref.").Value
            dg_ubi.DataSource = Nothing
            llenar_grid_ubicaciones()

            'Puede que todas las ubicaciaones estén recontadas pero la referencia no esté marcada como completamente recontada
            'Esto puede pasar si al entrar la primera vez, aparecieron 2 o mas ubicaciones a recontar y se dejan alguna por recontar, por tanto la ref no queda marcada como completa
            'Al entrar una segunda vez, una de las referencias a recontar y a no existe y por tanto no se puede recontar, con lo que no se puede finalizar todas las ubcaciones
            'Aviso de si quieren marcarla como recontada
            'Guardar datos
            cont = 0
            Do While cont < dg_ubi.Rows.Count
                If dg_ubi.Rows(cont).Cells("Recont.").Value = 0 Then
                    todasrecontadas = False
                    Exit Do
                End If
                cont += 1
            Loop
            If todasrecontadas Then
                If MsgBox("Todas las ubicaciones de esta referencia han sido ya recontadas, quiere marcarla como completa?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    sql = "update circuitoagotarstocklin set recontado=1,recontadoFecha=getdate() "
                    sql &= "where iDCab=" & dg_referencias.Rows(e.RowIndex).Cells("id").Value & " and codalmacen='" & list_alm.SelectedValue & "' "
                    resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                    If resultado = "0" Then

                        dg_referencias.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGreen

                        'Si está recontada en todos los almacenes , marcar la ref como recontada
                        sql = "select * from circuitoagotarstocklin where IDCab=" & dg_referencias.Rows(e.RowIndex).Cells("id").Value & " and recontado=0 "
                        vaux = conexion.TablaxCmd(constantes.bd_intranet, sql)
                        If vaux.Rows.Count = 0 Then
                            'Guardar datos
                            sql = "update circuitoagotarstock set recontado=1,recontadoOperario=" & frm_principal.lab_idoperario.Text & ",recontadoFecha=getdate() "
                            sql &= "where id=" & dg_referencias.Rows(e.RowIndex).Cells("id").Value & ""
                            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                        End If
                    Else
                        MsgBox("Se produjo un error al guardar los datos en la tabla CircuitoAgotarStock.")
                    End If
                End If
            End If


            If dg_ubi.Rows.Count > 0 Then
                dg_ubi.Rows(0).Selected = True
                Dim evento As New System.Windows.Forms.DataGridViewCellEventArgs(1, 0)
                dg_ubi_CellClick(dg_ubi, evento)
            End If

            imagen.ImageLocation = "\\jserver\BAJA\" & dg_referencias.Rows(e.RowIndex).Cells("Ref.").Value & ".jpg"
            imagen.SizeMode = PictureBoxSizeMode.StretchImage

            txt_CantRecontada.Focus()
            txt_CantRecontada.SelectAll()
        End If

    End Sub

    Private Sub dg_ubi_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dg_ubi.CellClick
        txt_ubicacion.Text = dg_ubi.Rows(e.RowIndex).Cells("Ubicacion").Value
        txt_CantUbicacion.Text = dg_ubi.Rows(e.RowIndex).Cells("Cant.").Value
        If dg_ubi.Rows(e.RowIndex).Cells("Recont.").Value <> 0 Then
            txt_CantRecontada.Text = dg_ubi.Rows(e.RowIndex).Cells("Recont.").Value
            txt_CantRecontada.Enabled = False
            btn_ok.Enabled = False
        Else
            txt_CantRecontada.Enabled = True
            btn_ok.Enabled = True
            txt_CantRecontada.Text = "" 'Para obligar a que pongan una cantidad
            txt_CantRecontada.Focus()
            txt_CantRecontada.SelectAll()
        End If

    End Sub

    Public Sub llenar_grid_ubicaciones()
        Dim sql As String
        Dim vdatos, vrecontada As New DataTable
        Dim cont As Integer
        Dim fila_encontrada As Boolean = False

        sql = "select id as 'ID',ubicacion as 'Ubicacion',cantidad as 'Cant.',0 as 'Recont.','' as 'R' "
        sql &= "from ubicaciones where codarticulo='" & txt_articulo.Text & "' and codalmacen='" & list_alm.SelectedValue & "' "
        'Pedido por Sergio. Q no tenga en cuenta devoluciones,saldos o muestras. 14/05/2019
        sql &= "and substring(codalmacen,1,1) not in ('D','S','M') "
        sql &= "order by tipo asc,codalmacen,ubicacion asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)


        dg_ubi.DataSource = vdatos
        dg_ubi.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18)
        dg_ubi.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
        'dg_ubi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dg_ubi.Columns(0).Width = 10
        dg_ubi.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_ubi.Columns(1).Width = 120
        dg_ubi.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_ubi.Columns(2).Width = 85
        dg_ubi.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubi.Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
        dg_ubi.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubi.Columns(3).Width = 90
        dg_ubi.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubi.Columns(4).Width = 50

        'Colocar la cantidad recontada
        btn_ok.Enabled = False
        dg_ubi.Enabled = True
        cont = 0
        Do While cont < dg_ubi.Rows.Count
            sql = "select top 1 id,CantidadValidada from CircuitoAgotarStockUbicaciones where IDCircuitoAgotarStock=" & dg_referencias.SelectedRows(0).Cells("ID").Value & " "
            sql &= "and codarticulo='" & txt_articulo.Text & "' and ubicacion='" & dg_ubi.Rows(cont).Cells("Ubicacion").Value & "' and codalmacen='" & list_alm.SelectedValue & "' "
            sql &= "order by id desc"
            vrecontada = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vrecontada.Rows.Count = 1 Then
                dg_ubi.Rows(cont).Cells("R").Value = "S"
                dg_ubi.Rows(cont).Cells("Recont.").Value = CInt(vrecontada.Rows(0).Item("CantidadValidada"))
            Else
                dg_ubi.Rows(cont).Cells("R").Value = "N"
            End If
            cont += 1
        Loop

        'If fila_encontrada = False Then dg_ubi.CurrentCell = Nothing

    End Sub
    Private Sub list_alm_DropDownClosed(sender As Object, e As EventArgs) Handles list_alm.DropDownClosed
        txt_articulo.Text = ""
        dg_referencias.DataSource = Nothing
        dg_ubi.DataSource = Nothing
        llenar_grid()
    End Sub

    Private Sub txt_articulo_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_articulo.KeyUp
        Dim codarticulo As String = txt_articulo.Text.ToUpper
        Dim cont As Integer

        If e.KeyCode = Keys.Enter Then
            If txt_articulo.Text.Length = 7 Or txt_articulo.Text.Length = 13 Then
                funciones.buscar_descripcion(codarticulo) 'Para obtener el codarticulo

                'Buscar si la referencia existe en el grid
                cont = 0
                Do While cont < dg_referencias.Rows.Count
                    If dg_referencias.Rows(cont).Cells("Ref.").Value = codarticulo Then
                        txt_articulo.Text = codarticulo
                        dg_referencias.Rows(cont).Selected = True
                        Dim evento As New System.Windows.Forms.DataGridViewCellEventArgs(1, dg_referencias.SelectedRows(0).Index)
                        dg_referencias_CellClick(dg_referencias, evento)

                        'Hacer scroll hasta fila encontrada
                        dg_referencias.CurrentCell = dg_referencias.Item(0, cont)
                        dg_referencias.FirstDisplayedCell = dg_referencias.CurrentCell

                        If dg_ubi.Rows.Count > 0 Then
                            dg_ubi.Rows(0).Selected = True
                            evento = New System.Windows.Forms.DataGridViewCellEventArgs(1, 0)
                            dg_ubi_CellClick(dg_ubi, evento)
                        End If

                        Exit Sub
                        End If
                        cont += 1
                Loop

                'Como no ha encontrado , muestra mensaje
                MsgBox("La referencia indicada no está en la lista de referencias a recontar.")
                txt_articulo.Text = ""
                dg_referencias.ClearSelection()
                dg_referencias.CurrentCell = Nothing
                dg_ubi.DataSource = Nothing
                txt_CantRecontada.Text = ""
                txt_CantUbicacion.Text = ""
                txt_ubicacion.Text = ""
                txt_articulo.Focus()
            End If
        End If
    End Sub

    Private Sub list_ubicacion_DropDownClosed(sender As Object, e As EventArgs)
        txt_CantRecontada.Focus()
        txt_CantRecontada.SelectAll()
    End Sub

    Private Sub btn_ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click
        Dim sql, sqlinsert As String
        Dim resultado As String
        Dim cont As Integer
        Dim todorecontado As Boolean = True
        Dim vstock, vstockUbi, vaux As New DataTable
        Dim TodasUbiRecontadas As Boolean = False
        Dim borrarcon0 As Boolean


        If Not IsNumeric(txt_CantRecontada.Text) Then
            MsgBox("El valor introducido en el campo 'Cantidad Recontada' no es correcto.", MsgBoxStyle.Critical)
            Exit Sub
        End If

        sql = "select borrarcon0 from V_ubicaciones_almacen where ubicacion='" & txt_ubicacion.Text & "' and codalmacen='" & list_alm.SelectedValue & "' "
        vaux = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vaux.Rows.Count = 1 Then borrarcon0 = vaux.Rows(0)(0) Else borrarcon0 = False

        'Guardar dato de ubicacion en tabla registros
        sqlinsert = "insert into circuitoagotarstockUbicaciones(IDCircuitoAgotarStock,CodArticulo,CodAlmacen,Ubicacion,CantidadUbi,CantidadValidada,UsusarioValidacion) "
        sqlinsert &= "values(" & dg_referencias.Rows(dg_referencias.SelectedRows(0).Index).Cells("id").Value & ",'" & txt_articulo.Text & "','" & list_alm.SelectedValue & "','" & txt_ubicacion.Text & "'," & txt_CantUbicacion.Text & "," & txt_CantRecontada.Text & "," & frm_principal.lab_idoperario.Text & ")"
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sqlinsert)
        If resultado <> "0" Then
            MsgBox("Se produjo un error al guardar los datos.", MsgBoxStyle.Critical)
            Exit Sub
        End If

        'Cambiar el valor en el grid
        dg_ubi.SelectedRows(0).Cells("Recont.").Value = txt_CantRecontada.Text
        dg_ubi.SelectedRows(0).Cells("R").Value = "S"
        dg_ubi.SelectedRows(0).DefaultCellStyle.BackColor = Color.LightGreen

        'Se llama al formulario para hacer la actualización del stock
        frm_act_stock.Lab_codarticulo.Text = txt_articulo.Text
        frm_act_stock.lab_alm_pick.Text = list_alm.SelectedValue
        frm_act_stock.ShowDialog()


        funciones.añadir_reg_mov_ubicaciones(txt_articulo.Text, txt_CantRecontada.Text, list_alm.SelectedValue, txt_ubicacion.Text, "Cambio valor", "Ubicaciones-RecuentoStock", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
        If txt_CantRecontada.Text = 0 And borrarcon0 Then
            sqlinsert = "delete ubicaciones where id=" & dg_ubi.SelectedRows(0).Cells("ID").Value & " "
        Else
            sqlinsert = "update ubicaciones set cantidad=" & txt_CantRecontada.Text & " where id=" & dg_ubi.SelectedRows(0).Cells("ID").Value & " "
        End If
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sqlinsert)


        'Mirar si todas las ubicaciones de la referencia en este almacén seleccionada han sido recontadas
        TodasUbiRecontadas = True
        cont = 0
        Do While cont < dg_ubi.Rows.Count
            If dg_ubi.Rows(cont).Cells("R").Value = "N" Then TodasUbiRecontadas = False
            cont += 1
        Loop

        If TodasUbiRecontadas Then
            'Si todas las ubicaciones han sido recontadas, damos como recontada la referencia en la tabla circuitoagotarstocklin 

            'Guardar datos
            sql = "update circuitoagotarstocklin set recontado=1,recontadoFecha=getdate() "
            sql &= "where iDCab=" & dg_referencias.Rows(dg_referencias.SelectedRows(0).Index).Cells("id").Value & " and codalmacen='" & list_alm.SelectedValue & "' "
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If resultado = "0" Then
                dg_referencias.SelectedRows(0).DefaultCellStyle.BackColor = Color.LightGreen

                'Si está recontada en todos los almacenes , marcar la ref como recontada
                sql = "select * from circuitoagotarstocklin where IDCab=" & dg_referencias.Rows(dg_referencias.SelectedRows(0).Index).Cells("id").Value & " and recontado=0 "
                vaux = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vaux.Rows.Count = 0 Then
                    'Guardar datos
                    sql = "update circuitoagotarstock set recontado=1,recontadoOperario=" & frm_principal.lab_idoperario.Text & ",recontadoFecha=getdate() "
                    sql &= "where id=" & dg_referencias.Rows(dg_referencias.SelectedRows(0).Index).Cells("id").Value & ""
                    resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                End If


                'Si quedan refs en el grid, se pasa al siguiente
                If dg_referencias.Rows.Count - 1 > dg_referencias.SelectedRows(0).Index Then
                    dg_referencias.Rows(dg_referencias.SelectedRows(0).Index + 1).Selected = True
                    Dim evento As New System.Windows.Forms.DataGridViewCellEventArgs(1, dg_referencias.SelectedRows(0).Index)
                    dg_referencias_CellClick(dg_referencias, evento)
                Else
                    dg_referencias.ClearSelection()
                    dg_referencias.CurrentCell = Nothing
                End If
            Else
                MsgBox("Se produjo un error al guardar los datos en la tabla CircuitoAgotarStock.")
            End If


        Else
            'Pasar de ubicacion
            If dg_ubi.Rows.Count - 1 > dg_ubi.SelectedRows(0).Index Then
                dg_ubi.Rows(dg_ubi.SelectedRows(0).Index + 1).Selected = True
                Dim evento As New System.Windows.Forms.DataGridViewCellEventArgs(1, dg_ubi.SelectedRows(0).Index)
                dg_ubi_CellClick(dg_ubi, evento)
            Else
                dg_ubi.Rows(0).Selected = True
                Dim evento As New System.Windows.Forms.DataGridViewCellEventArgs(1, dg_ubi.SelectedRows(0).Index)
                dg_ubi_CellClick(dg_ubi, evento)
                dg_ubi.CurrentCell = dg_ubi.Rows(0).Cells(0)
            End If

        End If


    End Sub

    Private Sub dg_referencias_Sorted(sender As Object, e As EventArgs) Handles dg_referencias.Sorted
        ' dg_referencias.CurrentRow.Selected = False
        dg_referencias.Rows(0).Selected = True
        Dim evento As New System.Windows.Forms.DataGridViewCellEventArgs(1, dg_referencias.SelectedRows(0).Index)
        dg_referencias_CellClick(dg_referencias, evento)
        dg_referencias.CurrentCell = dg_referencias.Rows(0).Cells(0)


    End Sub

    Private Sub txt_CantRecontada_Click(sender As Object, e As EventArgs) Handles txt_CantRecontada.Click
        txt_CantRecontada.SelectAll()
    End Sub

    Public Sub Guardar_datos_tiempo(Optional ByVal ObsGuardarTiempos As String = "")
        Dim TiempoEmpleado As Integer
        Dim sql As String
        Try
            TiempoEmpleado = DateDiff(DateInterval.Second, fecha_inicio, fecha_fin)
            sql = "insert into albaranes_tiempos (idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(" & frm_principal.lab_idoperario.Text & ",'Recuento Stock','" & fecha_inicio & "','" & fecha_fin & "'," & TiempoEmpleado & ",0,'" & ObsGuardarTiempos & "' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        Catch ex As Exception

        End Try

    End Sub

End Class