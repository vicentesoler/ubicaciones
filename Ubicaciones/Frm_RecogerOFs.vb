﻿Public Class Frm_RecogerOFs
    Dim conexion As New Conector
    Dim fecha_inicio, fecha_fin As DateTime

    Private Sub Frm_RecogerOFs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sql As String
        'Registrar al operario en la tabla de operarios. 
        sql = "update operarios_almacen set IdUltimaAccion=3,FechaUltimaAccion=getdate() where Id=" & frm_principal.lab_idoperario.Text
        conexion.TablaxCmd(constantes.bd_intranet, sql)

        'Guardar el momento de entrar
        fecha_inicio = Now

        'Guardar_datos_tiempo(ObsGuardarTiempos)
    End Sub
    Public Sub Guardar_datos_tiempo(Optional ByVal ObsGuardarTiempos As String = "")
        Dim TiempoEmpleado As Integer
        Dim sql As String
        Try
            fecha_fin = Now()
            TiempoEmpleado = DateDiff(DateInterval.Second, fecha_inicio, fecha_fin)
            sql = "insert into albaranes_tiempos (idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(" & frm_principal.lab_idoperario.Text & ",'Recog.OFs','" & fecha_inicio & "','" & fecha_fin & "'," & TiempoEmpleado & ",0,'" & ObsGuardarTiempos & "' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        Catch ex As Exception

        End Try
    End Sub
    Public Sub llenar_referencias()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer

        sql = "select lin.articulo as 'Ref.',max(lin.deslinea) as 'Descripción', convert(integer,sum(ABS(lin.cantidad))) as 'Cant.', convert(integer,sum(abs(lin.CantidadValidada))) as 'Val.',case when min(cab.fase)=100 then 'ALTA' when min(cab.fase)=300 then 'MEDIA' when min(cab.fase)=500 then 'BAJA' end as 'Prior.' "
        sql &= "from GesAlbaranesProv cab "
        sql &= "inner join GesAlbaranesProvLin lin on lin.RefCabecera =cab.NumReferencia and abs(lin.CantidadValidada) <> abs(cantidad) "
        sql &= "where cab.estado in ('00','01') and cab.SerAlbaranProv ='OF' and lin.cantidad<0 "
        sql &= "and isnull(FinRecogida,0) =0 "
        If list_filtro_tipo.Text = "Gener." Then sql &= "and cab.proveedor=90000 "
        If list_filtro_tipo.Text = "Manip." Then sql &= "and cab.proveedor in (91000,93000) "
        If list_filtro_tipo.Text = "Perso." Then sql &= "and cab.proveedor=92000 "

        sql &= "group by lin.articulo "
        sql &= "order by min(cab.fase) asc,sum(lin.cantidad-lin.CantidadValidada) desc,lin.articulo asc "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)


        dg_lineas.DataSource = vdatos
        dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15)
        dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
        'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dg_lineas.Columns("Ref.").Width = 100
        dg_lineas.Columns("Ref.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_lineas.Columns("Descripción").Width = 280
        dg_lineas.Columns("Descripción").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_lineas.Columns("Descripción").DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 10)
        dg_lineas.Columns("Cant.").Width = 80
        dg_lineas.Columns("Cant.").DefaultCellStyle.Format = "N0"
        dg_lineas.Columns("Cant.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas.Columns("Val.").Width = 80
        dg_lineas.Columns("Val.").DefaultCellStyle.Format = "N0"
        dg_lineas.Columns("Val.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas.Columns("Prior.").Width = 80
        dg_lineas.Columns("Prior.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


        'Pintar
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            Select Case dg_lineas.Rows(cont).Cells("Val.").Value
                Case Is = 0 : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.White
                Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
            End Select
            cont += 1
        Loop

        dg_lineas.Enabled = True

        'If dg_lineas.Rows.Count > 0 Then dg_lineas.Rows(0).Selected = True
        'dg_lineas_CellEnter(Nothing, Nothing)
        'No dejo seleccionada una fila porque luego cuando lleno las ubicaciones no las pinta la primera vez.

        'Lo hago desde el botón, ya que sino no hace caso
        'dg_lineas.CurrentCell = Nothing

    End Sub

    Private Sub dg_lineas_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dg_lineas.CellEnter
        Dim sql As String
        Dim vdatos As New DataTable

        If dg_lineas.SelectedRows.Count = 0 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        dg_OFs.DataSource = Nothing
        cargar_albaranes(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value)
        dg_ubi.DataSource = Nothing
        cargar_lineas_ubicaciones(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value)
        dg_procesadas.DataSource = Nothing
        cargar_lineas_registro(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value)


        txt_cantidad.Text = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value - dg_lineas.Rows(e.RowIndex).Cells("Val.").Value

        funciones.mostrar_fotos(imagen, dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value)
        labStockA01.Text = funciones.stock(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", "A01")
        labStockA07.Text = funciones.stock(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", "A07")
        labStockA08.Text = funciones.stock(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", "A08")

        LabPdteServ.Text = funciones.pendiente_servir(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", True)
        LabPdtePrep.Text = funciones.pendiente_preparar("TODAS", "TODOS", dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value)

        btn_OtraUbi.Enabled = True
    End Sub

    Private Sub list_filtro_tipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles list_filtro_tipo.SelectedIndexChanged
        limpiar()
        llenar_referencias()
    End Sub
    Public Sub limpiar()
        'Guardar datos de operario
        imagen.Image = Nothing
        dg_lineas.DataSource = Nothing
        dg_lineas.Enabled = False
        dg_ubi.DataSource = Nothing
        dg_ubi.Enabled = False
        dg_procesadas.DataSource = Nothing
        dg_procesadas.Enabled = False
        txt_cantidad.Text = 0
        btn_ok.Enabled = False
        labStockA01.Text = 0
        labStockA07.Text = 0
        labStockA08.Text = 0
        LabPdtePrep.Text = 0
        LabPdteServ.Text = 0
    End Sub
    Public Sub cargar_albaranes(ByVal numreferencia As String)
        Dim sql As String
        Dim vdatos, vAlbCli As New DataTable
        Dim cont, cAlb As Integer

        sql = "select cab.codempresa as 'Emp.',cab.codperiodo as 'Per.',cab.seralbaranprov as 'Ser.', cab.codalbaranprov as 'Alb.',lin.codlinea as 'Lin.',prio.descripcion as 'Prioridad',sum(abs(lin.cantidad)) as 'Cant.',sum(abs(lin.CantidadValidada)) as 'Val.','' as 'Alb.Reserva' "
        sql &= "from GesAlbaranesProv cab "
        sql &= "inner join GesAlbaranesProvLin lin on lin.RefCabecera =cab.NumReferencia and abs(lin.CantidadValidada) <> abs(cantidad) "
        sql &= "left join intranet.dbo.PrioridadesOF prio on prio.id=cab.fase "
        sql &= "where lin.articulo='" & numreferencia & "' "
        sql &= "and cab.estado in ('00','01') and cab.SerAlbaranProv ='OF' and lin.cantidad<0 "
        sql &= "and isnull(FinRecogida,0) =0 "
        sql &= "group by cab.codempresa,cab.codperiodo,cab.seralbaranprov, cab.codalbaranprov,prio.descripcion,lin.codlinea,cab.fase "
        sql &= "order by cab.fase asc"
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)


        dg_OFs.DataSource = vdatos
        dg_OFs.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 12)
        dg_OFs.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 13, FontStyle.Bold)
        'dg_OFs.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dg_OFs.Columns("Emp.").Width = 60
        dg_OFs.Columns("Emp.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_OFs.Columns("Per.").Width = 70
        dg_OFs.Columns("Per.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_OFs.Columns("Ser.").Width = 60
        dg_OFs.Columns("Ser.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_OFs.Columns("Alb.").Width = 60
        dg_OFs.Columns("Alb.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_OFs.Columns("Lin.").Width = 50
        dg_OFs.Columns("Lin.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_OFs.Columns("Prioridad").Width = 100
        dg_OFs.Columns("Prioridad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_OFs.Columns("Cant.").Width = 60
        dg_OFs.Columns("Cant.").DefaultCellStyle.Format = "N0"
        dg_OFs.Columns("Cant.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_OFs.Columns("Val.").Width = 60
        dg_OFs.Columns("Val.").DefaultCellStyle.Format = "N0"
        dg_OFs.Columns("Val.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_OFs.Columns("Alb.Reserva").Width = 120
        dg_OFs.Columns("Alb.Reserva").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        dg_OFs.Columns("Alb.Reserva").DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 9)


        'Recorrer los Albaranes OF y llenar los albaranes de cliente asignados
        cont = 0
        Do While cont < dg_OFs.Rows.Count
            sql = "select distinct CodEmpresaAlbaran ,CodPeriodoAlbaran ,SerAlbaran ,CodAlbaran "
            sql &= "from MonitorBolsasReservasOF "
            sql &= "where CodEmpresaOF=" & dg_OFs.Rows(cont).Cells("Emp.").Value & " And CodperiodoOF=" & dg_OFs.Rows(cont).Cells("Per.").Value & " And SerAlbaranOF='" & dg_OFs.Rows(cont).Cells("Ser.").Value & "' and CodAlbaranOF=" & dg_OFs.Rows(cont).Cells("Alb.").Value & " "
            vAlbCli = conexion.TablaxCmd(constantes.bd_intranet, sql)
            cAlb = 0
            Do While cAlb < vAlbCli.Rows.Count
                dg_OFs.Rows(cont).Cells("Alb.Reserva").Value &= vAlbCli.Rows(cAlb).Item("CodEmpresaAlbaran").ToString.Trim & "/" & vAlbCli.Rows(cAlb).Item("CodPeriodoAlbaran").ToString.Trim & "/" & vAlbCli.Rows(cAlb).Item("SerAlbaran").ToString.Trim & "/" & vAlbCli.Rows(cAlb).Item("CodAlbaran").ToString.Trim & ", "
                cAlb += 1
            Loop
            cont += 1
        Loop

        dg_OFs.CurrentCell = Nothing
    End Sub

    Public Sub cargar_lineas_ubicaciones(ByVal codarticulo As String)
        Dim sql As String = ""
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim fila_encontrada As Boolean = False
        Dim valmacenes As New DataTable

inicio:


        sql = "select codalmacen as 'Alm.',Ubicacion as 'Ubicacion',Tipo as 'Tipo',sum(cantidad) as 'Cant.' ,min(orden) as orden from ( "
        'Si están depositando, debe aparecer la ubicación especial si o si
        'Depende del tipo de OF, saldrá una u otra. Pedido por Javier


        sql &= "select codalmacen ,ubicacion,tipo,cantidad,99 as orden "
        sql &= "from ubicaciones "
        sql &= "where codarticulo='" & codarticulo.ToUpper & "'  "

        'Pedido por Sergio. Q no tenga en cuenta devoluciones,saldos o muestras. 18/07/2019
        sql &= "and substring(codalmacen,1,1) not in ('D','S','M') "

        'Si es una personalizacion o una impresión , solo deben salir las ubicaciones especiales
        sql &= ") as tabla "
        sql &= "group by codalmacen,Ubicacion,Tipo  "
        sql &= "order by orden asc,tipo asc,Codalmacen,ubicacion asc "

        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        'Lo desactivé porque no veo claro que les ofrezca dar de alta una ubicación cuando están recogiendo, ya que no es real.
        'Lo activle solo para depositar. Pedido por Sergio 18/07/2019


        dg_ubi.DataSource = vdatos
        dg_ubi.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14
                                                    )
        dg_ubi.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold)
        'dg_ubi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dg_ubi.Columns(0).Width = 55
        dg_ubi.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_ubi.Columns(1).Width = 100
        dg_ubi.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubi.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
        dg_ubi.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubi.Columns(2).Width = 80
        dg_ubi.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubi.Columns(3).Width = 80
        dg_ubi.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubi.Columns(4).Width = 120



        'Pintar y habilitar
        btn_ok.Enabled = False
        dg_ubi.Enabled = True
        cont = 0
        Do While cont < dg_ubi.Rows.Count
            If dg_ubi.Rows(cont).Cells("Alm.").Value = "A01" Then
                dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.Blue
                dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.White
            End If
            If dg_ubi.Rows(cont).Cells("Alm.").Value = "A07" Then
                dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If
            If dg_ubi.Rows(cont).Cells("Alm.").Value = "A08" Then
                dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.LightPink
                dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If

            cont += 1
        Loop

        If fila_encontrada = False Then dg_ubi.CurrentCell = Nothing

    End Sub
    Public Sub cargar_lineas_registro(ByVal codarticulo As String)
        Dim sql As String
        Dim vdatos As New DataTable
        Dim fila_encontrada As Boolean = False
        Dim cont As Integer

        dg_procesadas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 12)
        dg_procesadas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold)


        sql = "select codalmacen as 'Alm.',ubicacion as 'Ubicacion',tipo as 'Tipo',cantidad as 'Cant.' "
        sql &= "from ordenesfabricacion "
        sql &= "where (1=0 "
        cont = 0
        Do While cont < dg_OFs.Rows.Count
            sql &= "or (codempresa =" & dg_OFs.Rows(cont).Cells("Emp.").Value & " And codperiodo=" & dg_OFs.Rows(cont).Cells("Per.").Value & " And seralbaranprov='" & dg_OFs.Rows(cont).Cells("Ser.").Value & "' and codalbaranprov=" & dg_OFs.Rows(cont).Cells("Alb.").Value & " and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "') "
            cont += 1
        Loop
        sql &= ") "
        'Lo puse porque vi que hay albaranes que tienen la misma referencia en diferentes líneas. Por ejemplo en impresion digital, se usa la misma base para producir otras
        sql &= "order by codalmacen,ubicacion asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        dg_procesadas.DataSource = vdatos

        If vdatos.Rows.Count() > 0 Then
            'dg_ubi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_procesadas.Columns(0).Width = 50
            dg_procesadas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_procesadas.Columns(1).Width = 85
            dg_procesadas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_procesadas.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_procesadas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_procesadas.Columns(2).Width = 60
            dg_procesadas.Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_procesadas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_procesadas.Columns(3).Width = 55
            'Pintar y habilitar
            btn_ok.Enabled = False
            dg_procesadas.Enabled = True


            'pintar_lineas()

        End If


    End Sub

    Private Sub dg_ubi_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dg_ubi.CellClick
        If dg_ubi.SelectedRows(0).Cells("Alm.").Value = "A01" Then
            btn_ok.Enabled = True
            txt_cantidad.Text = dg_lineas.SelectedRows(0).Cells("Cant.").Value - dg_lineas.SelectedRows(0).Cells("Val.").Value
            txt_cantidad.SelectAll()
            txt_cantidad.Focus()
        Else
            If MsgBox("Esta ubicación no es del A01, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                btn_ok.Enabled = True
                txt_cantidad.SelectAll()
                txt_cantidad.Focus()
            Else
                btn_ok.Enabled = False
                dg_ubi.CurrentCell = Nothing
            End If
        End If
    End Sub

    Private Sub Frm_RecogerOFs_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Dim sql As String
        Guardar_datos_tiempo("Salir")
        sql = "update operarios_almacen set IdUltimaAccion=0,FechaUltimaAccion=getdate() where id=" & frm_principal.lab_idoperario.Text
        conexion.Executetransact(constantes.bd_intranet, Sql)
    End Sub

    Private Sub btn_ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click
        Dim sql, ssel As String
        Dim resultado As String
        Dim vcant, vdatos As New DataTable
        Dim cantant As Integer
        Dim codarticulo As String
        Dim cont As Integer

        If dg_lineas.SelectedRows.Count = 0 Then
            MsgBox("Debe seleccionar la línea.", MsgBoxStyle.Exclamation)
            Exit Sub
        Else
            codarticulo = dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim.ToUpper
        End If


        If dg_ubi.SelectedRows.Count = 0 Then
            MsgBox("Debe seleccionar la ubicación", MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        If CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Val.").Value.ToString) > CInt(dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString) Then
            MsgBox("La cantidad a validar no puede ser superior a la cantidad de la linea.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'Si ya ha sido recogido/depositado algo en una ubicación, el almacén donde se va a depositar/ubicar ahora debe ser el mismo. Si no obliga a modificar el albarán en dos líneas
        If dg_procesadas.Rows.Count > 0 Then
            If dg_ubi.SelectedRows(0).Cells("Alm.").Value <> dg_procesadas.Rows(0).Cells("Alm.").Value Then
                MsgBox("Ya ha sido procesada una cantidad en otro almacén diferente al que está procesando ahora. No se permite.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
        End If



        If txt_cantidad.Text = 0 Then
            MsgBox("La cantidad debe ser superior a 0.", MsgBoxStyle.Information)
        Else
            'Actualizar la ubicacion 
            'Añado un registro a la tabla OrdenesFabricacion o actualizo el que ya existe
            'Para ello Tengo que recorrer las OF e ir descontando la cantidad 

            Dim CantADescontar As String = 0
            Dim CantPend As Integer
            sql = ""
            cont = 0
            Do While cont < dg_OFs.Rows.Count And txt_cantidad.Text > 0
                CantPend = dg_OFs.Rows(cont).Cells("Cant.").Value - dg_OFs.Rows(cont).Cells("Val.").Value
                Select Case CantPend
                    Case 0 : CantADescontar = 0
                    Case <= txt_cantidad.Text
                        CantADescontar = CantPend
                    Case > txt_cantidad.Text
                        CantADescontar = txt_cantidad.Text
                End Select
                If CantADescontar = 0 Then GoTo siguiente
                sql = "update ubicaciones set cantidad=isnull(cantidad,0)-" & CInt(CantADescontar) & " "
                sql &= "where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' order by cantidad asc ) "

                dg_OFs.Rows(cont).Cells("Val.").Value += CantADescontar

                ssel = "select cantidad,codalmacen from OrdenesFabricacion where codempresa=" & dg_OFs.Rows(cont).Cells("Emp.").Value & " and codperiodo=" & dg_OFs.Rows(cont).Cells("Per.").Value & " and SerAlbaranProv='" & dg_OFs.Rows(cont).Cells("Ser.").Value & "' and CodAlbaranProv=" & dg_OFs.Rows(cont).Cells("Alb.").Value & " and codlinea=" & dg_OFs.Rows(cont).Cells("Lin.").Value & " and CodAlmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and Ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, ssel)
                If vdatos.Rows.Count = 1 Then
                    'Actualizo
                    sql &= "update OrdenesFabricacion set cantidad=cantidad+(" & CantADescontar & "),ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codempresa=" & dg_OFs.Rows(cont).Cells("Emp.").Value & " and codperiodo=" & dg_OFs.Rows(cont).Cells("Per.").Value & " and SerAlbaranProv='" & dg_OFs.Rows(cont).Cells("Ser.").Value & "' and CodAlbaranProv=" & dg_OFs.Rows(cont).Cells("Alb.").Value & " and CodLinea='" & dg_OFs.Rows(cont).Cells("Lin.").Value & "' and CodAlmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and Ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' "
                Else
                    'Inserto
                    sql &= "insert into OrdenesFabricacion(codempresa,codperiodo,seralbaranprov,codalbaranprov,codlinea,codalmacen,ubicacion,codarticulo,cantidad,ultusuario,accion) values(" & dg_OFs.Rows(cont).Cells("Emp.").Value & "," & dg_OFs.Rows(cont).Cells("Per.").Value & ",'" & dg_OFs.Rows(cont).Cells("Ser.").Value & "'," & dg_OFs.Rows(cont).Cells("Alb.").Value & "," & dg_OFs.Rows(cont).Cells("Lin.").Value & ",'" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "','" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "'," & CantADescontar & ",'" & frm_principal.lab_idoperario.Text & "','Recoger') "
                End If

                'Actualizar la linea del albaran
                sql &= "update inase.dbo.gesalbaranesprovlin set  CantidadValidada=CantidadValidada + (" & CantADescontar & ") where codempresa=" & dg_OFs.Rows(cont).Cells("Emp.").Value & " and codperiodo=" & dg_OFs.Rows(cont).Cells("Per.").Value & " and SerAlbaranProv='" & dg_OFs.Rows(cont).Cells("Ser.").Value & "' and CodAlbaranProv=" & dg_OFs.Rows(cont).Cells("Alb.").Value & " and CodLinea='" & dg_OFs.Rows(cont).Cells("Lin.").Value & "' "


                '08/05/2019
                'Antes modificaba el almacén del albarán/movimiento para que el stock fuera al almacén correcto. 
                'Esto provoca que si depositan solo una parte, al modificar el almacén, se modifica el stock total y no solo de la cantidad que han depositado parcial
                'Ahora lo que hago es hacer un movimiento con la cantidad que acaban de recoger.

                Dim nummov As Integer
                Dim numrefmov As String

                'Consulta para obtener algunos datos que me hacen falta luego
                ssel = "select numreferencia,codlinea,articulo,isnull(cantidadvalidada,0) as cantidadvalidada,isnull(cantidad,0) as cantidad,almacen,CodEmpresaPed,CodPeriodoPed,SerPedidoProv,CodPedidoProv,linpedido "
                ssel &= "from GesAlbaranesProvlin where codempresa=" & dg_OFs.Rows(cont).Cells("Emp.").Value & " and codperiodo=" & dg_OFs.Rows(cont).Cells("Per.").Value & " and seralbaranprov='" & dg_OFs.Rows(cont).Cells("Ser.").Value & "' and codAlbaranProv=" & dg_OFs.Rows(cont).Cells("Alb.").Value & " and codlinea=" & dg_OFs.Rows(cont).Cells("Lin.").Value
                vdatos = conexion.TablaxCmd(constantes.bd_inase, ssel)

                'Buscar el pedidocompra
                Dim vped, vmov As New DataTable
                ssel = "select nummov,articulo,almacen from GesPedidosProvLin where codempresa=" & vdatos.Rows(0).Item("CodEmpresaPed") & " and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and serpedidoprov='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and codpedidoprov=" & vdatos.Rows(0).Item("CodPedidoProv") & " and codlinea= " & vdatos.Rows(0).Item("linpedido") & " "
                vped = conexion.TablaxCmd(constantes.bd_inase, ssel)

                'Si tiene un mov es porque es la que se consume. en este caso no habria que hacer nada , 
                'pero como puede darse el caso de que el almacén de recogida no sea el que tiene puesto el mov, lo cambio para que se haga la bajada de stock del alm. que toca
                '15/10/2020 Solo si no están haciendo la accion de imprimir
                If vped.Rows(0).Item("nummov") <> 0 Then
                    'Buscar en el movimiento para ver si el almacen es el mismo
                    ssel = "select nummov,codlinea,AlmacenOri from gesmovalmlin where codempresa=1 and nummov=" & vped.Rows(0).Item("nummov") & " and articulo='" & vped.Rows(0).Item("articulo") & "' "
                    vmov = conexion.TablaxCmd(constantes.bd_inase, ssel)
                    If dg_ubi.SelectedRows(0).Cells("Alm.").Value <> vmov.Rows(0).Item("AlmacenOri") Then
                        'Hasta el 08/05/2019 se hacia así
                        'sql &= "update inase.dbo.gesmovalmlin set AlmacenOri='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' where codempresa=1 and nummov=" & vmov.Rows(0).Item("nummov") & " and codlinea=" & vmov.Rows(0).Item("codlinea") & " "
                        funciones.crear_movimiento_inase(1, nummov, "Mov.creado al Recoger ref. en alm. distinto al mov. en " & dg_OFs.Rows(cont).Cells("Per.").Value.ToString.Trim & "/OF/" & dg_OFs.Rows(cont).Cells("Alb.").Value, numrefmov)
                        sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,AlmacenOri,UbicacionOri,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                        sql &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & nummov & ",1,'" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & vmov.Rows(0).Item("AlmacenOri") & "','XXX','" & codarticulo & "'," & txt_cantidad.Text & ",'Mov. causado por Alm. dif. al Recoger. OF:" & dg_OFs.Rows(cont).Cells("Per.").Value.ToString.Trim & "/" & dg_OFs.Rows(cont).Cells("Alb.").Value & ".') "
                    End If
                Else
                    funciones.crear_movimiento_inase(1, nummov, "Mov.creado al Recoger ref. " & dg_OFs.Rows(cont).Cells("Per.").Value.ToString.Trim & "/OF/" & dg_OFs.Rows(cont).Cells("Alb.").Value, numrefmov)
                    'sql &= "update inase.dbo.GesAlbaranesProvlin set ultmodificacion=getdate(), Almacen ='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' where numreferencia='" & vdatos.Rows(0).Item("numreferencia").ToString & "' "
                    sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,AlmacenOri,UbicacionOri,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                    sql &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & nummov & ",1,'P1','XXX','" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & codarticulo & "'," & txt_cantidad.Text & ",'Mov. causado al Recoger. OF:" & dg_OFs.Rows(cont).Cells("Per.").Value.ToString.Trim & "/" & dg_OFs.Rows(cont).Cells("Alb.").Value & ".') "
                End If

                'Poner la cantidad 
                dg_lineas.SelectedRows(0).Cells("Val.").Value = CInt(CantADescontar) + CInt(dg_lineas.SelectedRows(0).Cells("Val.").Value.ToString)

                'Si la OF está totalmente recogida, se marca como finalizada la recogida
                If dg_OFs.Rows(cont).Cells("Val.").Value = dg_OFs.Rows(cont).Cells("Cant.").Value Then
                    sql &= "update inase.dbo.GesAlbaranesProv set estado='00', ultoperario=" & frm_principal.lab_idoperario.Text & ",FechaRecogida=getdate(),FinRecogida=1,OperarioFinRecogida=" & frm_principal.lab_idoperario.Text & " where codempresa=" & dg_OFs.Rows(cont).Cells("Emp.").Value.ToString.Trim & " and codperiodo=" & dg_OFs.Rows(cont).Cells("Per.").Value.ToString.Trim & " and seralbaranprov='" & dg_OFs.Rows(cont).Cells("Ser.").Value.ToString.Trim & "' and codAlbaranProv=" & dg_OFs.Rows(cont).Cells("Alb.").Value.ToString.Trim & " "
                End If

                'Añadir registro. Se hace antes de ejecutar el insert para poder obtener la cantidad anterior antes de que sea modificada
                funciones.añadir_reg_mov_ubicaciones(codarticulo, (CInt(CantADescontar) - cantant) * -1, dg_ubi.SelectedRows(0).Cells("Alm.").Value, dg_ubi.SelectedRows(0).Cells("Ubicacion").Value, "OF:" & dg_OFs.Rows(cont).Cells("Alb.").ToString, "Ubicaciones-Orden Prod", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

                resultado = conexion.Executetransact(constantes.bd_intranet, sql)
                If resultado <> "0" Then
                    MsgBox("Se produjo un error, avisar a Informática. Err 20180116", MsgBoxStyle.Exclamation)
                End If
                txt_cantidad.Text -= CantADescontar
siguiente:
                cont += 1
            Loop
        End If



        Select Case dg_lineas.SelectedRows(0).Cells("Val.").Value
            Case Is = 0 : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Red
            Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow
        End Select

        'Llenar de nuevo ubicaciones para actualizar las cantidades por ubicacion.
        cargar_lineas_ubicaciones(codarticulo)
        'Llenar el registro de ubicaciones usadas
        cargar_lineas_registro(codarticulo)
    End Sub
End Class
