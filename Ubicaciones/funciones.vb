﻿Imports System.IO
Imports System.Net

Module funciones
    Dim conexion As New Conector
    Dim sql As String
    Dim resultado As String

    Public Function guardar_ubicacion_almacen(ByVal almacen As String, ByVal pasillo As Integer, ByVal portal As Integer, ByVal altura As Integer, ByVal tamaño As Integer, ByVal tipo As String) As String
        Try
            Dim vdatos As New DataTable
            'Consultar si existe
            sql = "select * from ubicaciones_almacen where codalmacen='" & almacen & "' and pasillo=" & pasillo & " and portal=" & portal & " and altura=" & altura
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 0 Then ' Crear
                sql = "insert into ubicaciones_almacen (codalmacen,pasillo,portal,altura,tamaño,tipo)"
                sql &= "values('" & almacen & "'," & pasillo & "," & portal & "," & altura & "," & tamaño & ",'" & tipo & "')"
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                If resultado <> "0" Then
                    MsgBox("Se produjo un error al crear la ubicación.", MsgBoxStyle.Critical)
                    Return "ERROR"
                    Exit Function
                Else
                    Return "OK"
                End If
            Else
                sql = "update ubicaciones_almacen set tamaño=" & tamaño & ",tipo='" & tipo & "' where codalmacen='" & almacen & "' and pasillo=" & pasillo & " and portal=" & portal & " and altura=" & altura
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                If resultado <> "0" Then
                    MsgBox("Se produjo un error al modificar la ubicación.", MsgBoxStyle.Critical)
                    Return "ERROR"
                Else
                    Return "OK"
                End If
            End If
        Catch ex As Exception
            MsgBox("Se produjo un error al guardar la ubicación.", MsgBoxStyle.Critical)
            Return "ERROR"
        End Try
    End Function
    Public Function consultar_tamaño_ubicacion(ByVal almacen As String, ByVal pasillo As Integer, ByVal portal As Integer, ByVal altura As Integer) As Integer
        Dim vdatos As New DataTable
        Try
            sql = "select tamaño from ubicaciones_almacen where codalmacen='" & almacen & "' and pasillo=" & pasillo & " and portal=" & portal & " and altura=" & altura
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 1 Then
                Return vdatos.Rows(0)(0)
            Else
                'MsgBox("No se encontró el tamaño de la ubicación.", MsgBoxStyle.Critical)
                Return -1
            End If
        Catch ex As Exception
            MsgBox("Se produjo un error al consultar el tamaño de la ubicación.", MsgBoxStyle.Critical)
            Return 0
        End Try
    End Function
    Public Function consultar_tamaño_ocupado(ByVal pasillo As String, ByVal portal As String, ByVal altura As String) As Integer
        Dim vdatos As New DataTable
        Try
            sql = "select isnull(sum(tamaño),0) from ubicaciones where ubicacion='" & pasillo & "-" & portal & "-" & altura & "'"
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 1 Then
                Return vdatos.Rows(0)(0)
            Else
                'MsgBox("No se encontró el tamaño de la ubicación.", MsgBoxStyle.Critical)
                Return 0
            End If
        Catch ex As Exception
            MsgBox("Se produjo un error al consultar el tamaño de la ubicación.", MsgBoxStyle.Critical)
            Return 0
        End Try
    End Function

    Public Function buscar_descripcion(ByRef articulo As String) As String
        Dim sql As String
        Dim verp As New DataTable
        articulo = Trim(articulo)
        buscar_descripcion = ""
        If Not (articulo.Length = 7 Or articulo.Length = 13) Then Exit Function
        'Primero buscar en erp
        sql = "select art.codarticulo, isnull(art.descripcion,'') as descripcion from gesarticulos art "
        sql &= "where art.codempresa=1 "
        'Estaba comentado , pero simon me dijo que descartara los 8X, porque no los usa. 21/03/2016
        'sql &= "and substring(art.codarticulo,6,1) not in ('8') "
        'Lo descomenté porque Domingo quería añadir referencias de la maquina de impresión digital y no le dejaba porque terminan en 8

        If Len(articulo) = 7 Then sql &= "and art.codarticulo='" & articulo & "' "
        If Len(articulo) = 13 Then sql &= "and art.unidventaean='" & articulo & "' "
        verp = conexion.TablaxCmd(constantes.bd_inase, sql)
        If verp.Rows.Count > 0 Then
            articulo = verp.Rows(0).Item("codarticulo")
            buscar_descripcion = verp.Rows(0).Item("descripcion")
        Else
            buscar_descripcion = ""
        End If
    End Function
    Public Function pendiente_recibir(ByVal articulo As String, ByVal empresa As String) As Integer
        Dim sql As String
        Dim vdatos As DataTable
        sql = "select isnull(sum(p_pro_ptes_recibir),0) from V_STOCK_POR_ARTICULO where CodArticulo ='" & articulo.Trim & "' "
        If empresa <> "TODAS" Then sql &= "and codempresa = " & empresa
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        pendiente_recibir = FormatNumber(vdatos.Rows(0)(0), 0)
    End Function
    Public Sub llenar_almacenes(ByRef ddl As Object, Optional ByVal Saldo As Boolean = False, Optional ByVal Muestras As Boolean = False, Optional ByVal Produccion As Boolean = False)
        Dim vitems_alm As New DataTable
        Dim ip As Net.Dns
        Dim nombrePC As String
        Dim entradasIP As Net.IPHostEntry
        Dim direccion_Ip As String
        nombrePC = Dns.GetHostName
        entradasIP = Dns.GetHostEntry(nombrePC)
        direccion_Ip = entradasIP.AddressList.FirstOrDefault(Function(i) i.AddressFamily = Sockets.AddressFamily.InterNetwork).ToString()
        vitems_alm.Columns.Add("almacen")
        If Microsoft.VisualBasic.Mid(direccion_Ip, 6, 1) = 0 Then
            vitems_alm.Rows.Add({"A01"})
            vitems_alm.Rows.Add({"A07"})
            vitems_alm.Rows.Add({"A06"})
            vitems_alm.Rows.Add({"A08"})
            If Saldo = True Then
                vitems_alm.Rows.Add({"S01"})
                vitems_alm.Rows.Add({"S07"})
                vitems_alm.Rows.Add({"S08"})
            End If
            If Muestras = True Then
                vitems_alm.Rows.Add({"M01"})
                vitems_alm.Rows.Add({"M07"})
                vitems_alm.Rows.Add({"M08"})
            End If
        Else
            vitems_alm.Rows.Add({"A07"})
            vitems_alm.Rows.Add({"A01"})
            vitems_alm.Rows.Add({"A06"})
            vitems_alm.Rows.Add({"A08"})
            If Saldo = True Then
                vitems_alm.Rows.Add({"S01"})
                vitems_alm.Rows.Add({"S07"})
                vitems_alm.Rows.Add({"S08"})
            End If
            If Muestras = True Then
                vitems_alm.Rows.Add({"M01"})
                vitems_alm.Rows.Add({"M07"})
                vitems_alm.Rows.Add({"M08"})
            End If
        End If
        If Produccion = True Then vitems_alm.Rows.Add({"P1"})

        ddl.DisplayMember = "almacen"
        ddl.ValueMember = "almacen"
        ddl.DataSource = vitems_alm

    End Sub
    Public Function crear_movimiento_inase(ByVal empresa As Integer, ByRef proxmov As Integer, ByVal descripcion As String, ByRef numrefmov As String, Optional ByVal RegStock As Integer = 0) As String
        Dim sql, resultado As String
        Dim vultmov As New DataTable
        numrefmov = ""
        'Obtener el siguiente movimiento
        sql = "select max(nummov)+1 from GesMovAlm where CodEmpresa=" & empresa & " "
        Try
            vultmov = conexion.TablaxCmd(constantes.bd_inase, sql)
            proxmov = vultmov.Rows(0)(0)
        Catch ex As Exception
            crear_movimiento_inase = "Error al obtener el próximo movimiento."
            Exit Function
        End Try

        'Inserto la cabecera del movimiento
        sql &= "insert into GesMovAlm (ultusuario,codempresa,codperiodo,nummov,observaciones,RegStock) values(" & frm_principal.usuario_inase & "," & empresa & "," & Now.Year & "," & proxmov & ",'" & descripcion & "'," & RegStock & ") "
        resultado = conexion.ExecuteCmd(constantes.bd_inase, sql)
        If resultado <> "0" Then
            crear_movimiento_inase = "Error al insertar el movimiento."
            Exit Function
        End If

        'Obtener el numref del movimiento
        Try
            sql = "select numreferencia from GesMovAlm where CodEmpresa=" & empresa & " and nummov=" & proxmov
            vultmov = conexion.TablaxCmd(constantes.bd_inase, sql)
            numrefmov = vultmov.Rows(0)(0).ToString
            crear_movimiento_inase = "OK"
        Catch ex As Exception
            crear_movimiento_inase = "Error al obtener el numref del movimiento"
        End Try
    End Function
    Public Function borrar_movimiento_inase(ByVal empresa As Integer, ByVal numref As String) As String
        Dim sql As String
        Dim resultado As String
        If numref = "" Then
            borrar_movimiento_inase = "OK"
            Exit Function
        End If
        Try
            sql = "delete GesMovAlmlin where codempresa=" & empresa & " and refcabecera='" & numref & "' "
            resultado = conexion.ExecuteCmd(constantes.bd_inase, sql)
            sql = "delete GesMovAlm where codempresa=" & empresa & " and numreferencia='" & numref & "' "
            resultado = conexion.ExecuteCmd(constantes.bd_inase, sql)
            If resultado = "0" Then
                borrar_movimiento_inase = "OK"
            Else
                borrar_movimiento_inase = resultado
            End If
        Catch ex As Exception
            borrar_movimiento_inase = "ERROR " & ex.ToString
        End Try

    End Function
    Public Sub añadir_reg_mov_ubicaciones(ByVal codarticulo As String, ByVal cantidad As Integer, ByVal codalmacen As String, ByVal ubicacion_articulo As String, ByVal documento As String, ByVal aplicacion As String, ByVal usuario As String, ByVal idoperario As Integer, Optional ByVal cantant As Integer = 0)
        Dim sql, sql_ant As String
        Dim vdatos, vdatosant As New DataTable
        Dim resultado As String
        Dim cant_antes As Integer

        If cantant = 0 Then
            'Obtener la cantidad que hay en la ubicacion antes
            sql_ant = "select isnull(sum(cantidad),0) from ubicaciones where codarticulo='" & codarticulo & "' and ubicacion= '" & ubicacion_articulo & "' and codalmacen='" & codalmacen & "' "
            vdatosant = conexion.TablaxCmd(constantes.bd_intranet, sql_ant)
            cant_antes = vdatosant.Rows(0)(0)
        Else
            cant_antes = cantant
        End If

        sql = "insert into ubicaciones_movimientos(codalmacen,ubicacion,codarticulo,cantidad_ant,cantidad,fecha,aplicacion,documento,usuario,idoperario) "
        sql &= "values ('" & codalmacen & "','" & ubicacion_articulo & "','" & codarticulo & "'," & cant_antes & "," & cantidad & ",getdate(),'" & aplicacion & "','" & documento & "','" & usuario & "'," & idoperario & ") "

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)

    End Sub
    Public Sub llenar_ubicaciones(ByRef ddl As Object, ByVal codarticulo As String, ByVal codalmacen As String, Optional ByVal tipo As String = "", Optional ByVal ubicacion As String = "", Optional ByVal tipoubic As String = "", Optional ByVal tipo2 As String = "", Optional ByVal espci As Boolean = False)
        Dim vdatos As New DataTable

        ddl.DataSource = Nothing
        ddl.Items.Clear()

        sql = "select distinct ubicacion,tipo from ubicaciones where codarticulo='" & codarticulo & "' and codalmacen='" & codalmacen & "' "
        If tipo <> "" Then sql &= "and tipo='" & tipo & "' "
        sql &= "and ubicacion not in ('" & constantes.ubicacion_devoluciones_A01 & "','" & constantes.ubicacion_devoluciones_A07 & "','" & constantes.ubicacion_devoluciones_A08 & "','" & constantes.traslados_A07 & "','" & constantes.traslados_A01 & "','" & constantes.traslados_A08 & "') "
        If tipo2 = "SI" And (tipo = "Pulmon" Or tipo = "Picking") Then
            sql &= "union select distinct ubicacion,tipo from ubicaciones where codarticulo='" & codarticulo & "' and codalmacen='" & codalmacen & "' "
            If tipo = "Picking" Then sql &= "and tipo='Pulmon' "
            If tipo = "Pulmon" Then sql &= "and tipo='Picking' "
            sql &= "and ubicacion not in ('" & constantes.ubicacion_devoluciones_A01 & "','" & constantes.ubicacion_devoluciones_A07 & "','" & constantes.ubicacion_devoluciones_A08 & "','" & constantes.consolidacion_A01 & "','" & constantes.consolidacion_A07 & "','" & constantes.consolidacion_A08 & "','" & constantes.traslados_A07 & "','" & constantes.traslados_A01 & "','" & constantes.traslados_A08 & "') "
        End If
        If espci = True Then
            sql &= "union select distinct ubicacion,tipo from ubicaciones where codarticulo='" & codarticulo & "' and codalmacen='" & codalmacen & "' "
            sql &= "and tipo='Esp.CI' "
        End If
        sql &= " order by tipo,ubicacion asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If ubicacion <> "" Then
            vdatos.Rows.Add({ubicacion, tipoubic})
        End If
        ddl.DisplayMember = "ubicacion"
        ddl.ValueMember = "ubicacion"
        ddl.DataSource = vdatos

    End Sub
    Public Function comprobar_nueva_version() As String
        Dim aplicacion, ruta_local, ruta_servidor As String
        Dim fecha_local, fecha_servidor As String
        Dim sLine As String = ""

        'obtener datos del ini
        Dim archivo As String
        archivo = "act_version.ini"
        Dim str As New StreamReader(archivo)
        Do
            sLine = str.ReadLine()
            If Not sLine Is Nothing Then
                If Strings.Left(sLine, 10) = "aplicacion" Then
                    aplicacion = Trim(Strings.Mid(sLine, 12))
                    If aplicacion = "NOACTUALIZAR" Then Exit Function
                End If
                If Strings.Left(sLine, 10) = "ruta_local" Then
                    ruta_local = Trim(Strings.Mid(sLine, 12))
                End If
                If Strings.Left(sLine, 13) = "ruta_servidor" Then
                    ruta_servidor = Trim(Strings.Mid(sLine, 15))
                End If
            End If
        Loop Until sLine Is Nothing

        fecha_local = File.GetLastWriteTime(ruta_local & aplicacion)

        If File.Exists(ruta_local & aplicacion) Then
            Try
                fecha_servidor = File.GetLastWriteTime(ruta_servidor & aplicacion)
                If CDate(fecha_local) < CDate(fecha_servidor) Then
                    MsgBox("Existe una nueva versión. El programa se cerrará, ábralo de nuevo para que se actualice.", MsgBoxStyle.Critical)
                    Return "SI"
                End If
        Catch ex As Exception
                If ex.ToString.Contains("No se ha encontrado la ruta de acceso de la red") Then
                    If MsgBox("No se ha podido conectar al servidor para comprobar la versión del programa. Desea continuar de todas formas? Comentarlo con Informática", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        End
                    End If
                End If
            End Try

        '    fecha_servidor = File.GetLastWriteTime(ruta_servidor & aplicacion)
        '    If CDate(fecha_local) < CDate(fecha_servidor) Then
        '        MsgBox("Existe una nueva versión. El programa se cerrará, ábralo de nuevo para que se actualice.", MsgBoxStyle.Critical)
        '        Return "SI"
        '    End If
        Else
        Return "NO"
        End If



    End Function
    Public Sub llenar_empresas(ByRef ddl As Object)
        Dim vdatos As New DataTable

        ddl.DataSource = Nothing
        ddl.Items.Clear()

        sql = "select CodEmpresa ,DesEmpresa from InaEmpresas "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        ddl.DisplayMember = "DesEmpresa"
        ddl.ValueMember = "CodEmpresa"
        ddl.DataSource = vdatos

    End Sub
    Public Sub llenar_periodos_contenedor(ByRef ddl As Object, ByVal codempresa As Integer)
        Dim vdatos As New DataTable

        ddl.DataSource = Nothing
        ddl.Items.Clear()

        sql = "select distinct rtrim(codperiodo) as codperiodo from gescontenedores where codempresa=" & codempresa & " order by rtrim(codperiodo) desc "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        ddl.DisplayMember = "codperiodo"
        ddl.ValueMember = "codperiodo"
        ddl.DataSource = vdatos

    End Sub
    Public Sub llenar_contenedores(ByRef ddl As Object, ByVal codempresa As Integer, ByVal codperiodo As Integer, ByVal ubicado As String)
        Dim vdatos As New DataTable

        ddl.DataSource = Nothing
        ddl.Items.Clear()

        sql = "select top 100 con.numcontenedor from gescontenedores con "
        If ubicado = "SI" Then sql &= "inner join intranet.dbo.contenedores_ubicados cub on cub.codempresa=con.codempresa and cub.codperiodo=con.codperiodo and cub.numcontenedor=con.numcontenedor and finalizado=1 "
        If ubicado = "NO" Then sql &= "left join intranet.dbo.contenedores_ubicados cub on cub.codempresa=con.codempresa and cub.codperiodo=con.codperiodo and cub.numcontenedor=con.numcontenedor "
        sql &= "where con.codempresa=" & codempresa & " and con.codperiodo=" & codperiodo & " "
        If ubicado = "NO" Then sql &= "and isnull(cub.finalizado,0)=0 "
        sql &= "order by numcontenedor desc "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        ddl.DisplayMember = "numcontenedor"
        ddl.ValueMember = "numcontenedor"
        ddl.DataSource = vdatos

    End Sub
    Public Sub Enviar_correo(destino As String, ByVal asunto As String, texto As String)
        Dim correo As New System.Net.Mail.MailMessage
        Dim smtp As New System.Net.Mail.SmtpClient

        smtp.Host = "smtp.gmail.com"
        smtp.Credentials = New System.Net.NetworkCredential("noreply@joumma.com", "Jo5mM1.B-@gsNo*R2ply+7")
        smtp.EnableSsl = True
        smtp.Port = "587"

        correo.From = New System.Net.Mail.MailAddress("noreply@joumma.com")
        correo.Subject = asunto
        correo.IsBodyHtml = True
        correo.Body = texto


        correo.IsBodyHtml = True
        correo.Priority = System.Net.Mail.MailPriority.Normal
        correo.To.Add(Replace(Replace(Replace(destino, ";", ","), "/", ","), ".com-", ".com,"))
        Try
            smtp.Send(correo)
        Catch ex As Exception
            MsgBox("Error al enviar el correo", MsgBoxStyle.Critical)
        End Try
    End Sub
    Public Sub llenar_ref_esp_brothers(ByRef vrefespbrothers As DataTable)
        Dim sql As String
        Dim vdatos As New DataTable
        Dim key(1) As DataColumn

        sql = "SELECT codarticulo_act FROM referencias_especiales  "
        vrefespbrothers = conexion.TablaxCmd(constantes.bd_intranet, sql)
        key(0) = vrefespbrothers.Columns(0)
        vrefespbrothers.PrimaryKey = key

    End Sub
    Public Function ref_esp_brothers(ByVal ref As String, ByVal vrefespbrothers As DataTable) As Boolean
        If IsNothing(vrefespbrothers.Rows.Find(ref)) Then Return False Else Return True
        'Select Case ref
        '    Case "4561451" : Return True
        '    Case "4561551" : Return True
        '    Case "4561651" : Return True
        '    Case "4562351" : Return True
        '    Case "4562451" : Return True
        '    Case "4563651" : Return True
        '    Case "4563751" : Return True
        '    Case "4563851" : Return True
        '    Case "4564251" : Return True
        '    Case "4564351" : Return True
        '    Case "4564451" : Return True
        '    Case "4565051" : Return True
        '    Case "4565651" : Return True
        '    Case "4565751" : Return True
        '    Case "4561452" : Return True
        '    Case "4561552" : Return True
        '    Case "4561652" : Return True
        '    Case "4562352" : Return True
        '    Case "4562452" : Return True
        '    Case "4563652" : Return True
        '    Case "4563752" : Return True
        '    Case "4563852" : Return True
        '    Case "4564252" : Return True
        '    Case "4564352" : Return True
        '    Case "4564452" : Return True
        '    Case "4565052" : Return True
        '    Case "4565652" : Return True
        '    Case "4565752" : Return True

        '    Case "2080361" : Return True
        '    Case "2080461" : Return True
        '    Case "2081061" : Return True
        '    Case "2082061" : Return True
        '    Case "2082161" : Return True
        '    Case "2083361" : Return True
        '    Case "2084161" : Return True
        '    Case "2084461" : Return True
        '    Case "2084861" : Return True
        '    Case "2084961" : Return True
        '    Case "2089061" : Return True
        '    Case "4211061" : Return True
        '    Case "4211161" : Return True
        '    Case "4211261" : Return True
        '    Case "4211361" : Return True
        '    Case "4212161" : Return True
        '    Case "4212261" : Return True
        '    Case "4213261" : Return True
        '    Case "4214161" : Return True
        '    Case "4214461" : Return True
        '    Case "4214861" : Return True
        '    Case "4214961" : Return True
        '    Case "4219061" : Return True

        '    Case "5448161" : Return True
        '    Case "5448261" : Return True
        '    Case "5452261" : Return True
        '    Case "5452361" : Return True
        '    Case "5453561" : Return True
        '    Case "5454461" : Return True
        '    Case "5455061" : Return True
        '    Case "5455161" : Return True
        '    Case "5455261" : Return True
        '    Case "5455661" : Return True
        '    Case "5458161" : Return True
        '    Case "5458261" : Return True
        '    Case "5461461" : Return True
        '    Case "5461462" : Return True
        '    Case "5461561" : Return True
        '    Case "5461562" : Return True
        '    Case "5461661" : Return True
        '    Case "5461662" : Return True
        '    Case "5470361" : Return True
        '    Case "5470362" : Return True
        '    Case "5470461" : Return True
        '    Case "5470462" : Return True
        '    Case "54810C1" : Return True
        '    Case "54810C2" : Return True
        '    Case "54899C1" : Return True
        '    Case "54899C2" : Return True
        '    Case "5492861" : Return True
        '    Case "5492862" : Return True
        '    Case "5492961" : Return True
        '    Case "5492962" : Return True
        '    Case "5519061" : Return True
        '    Case "5519062" : Return True
        '    Case "5519161" : Return True
        '    Case "5519162" : Return True

        '        'kuri kumi
        '    Case "2191461" : Return True
        '    Case "2191561" : Return True
        '    Case "2191661" : Return True
        '    Case "2192361" : Return True
        '    Case "2193361" : Return True
        '    Case "2194261" : Return True
        '    Case "2194861" : Return True
        '    Case "2194961" : Return True
        '    Case "2195361" : Return True
        '    Case "2195461" : Return True
        '    Case "2196461" : Return True


        '    Case Else : Return False
        'End Select
    End Function
    Public Function almacen_articulo(codarticulo As String, alm_pref As String) As String
        Dim vdatos As New DataTable
        Dim sql As String = ""
        Dim almacen As String = alm_pref

        sql = "select Composicion "
        sql &= "from gesarticulos where codempresa=1 and codarticulo='" & codarticulo & "' "


        sql = "select art.CodArticulo,art.Composicion,ubi01.ubicacion as ubi01,ubi07.ubicacion as ubi07,ubi08.ubicacion as ubi08 "
        sql &= "from gesarticulos art "

        'A01
        sql &= "left join intranet.dbo.ubicaciones ubi01 on ubi01.codarticulo =art.CodArticulo collate Modern_Spanish_CI_AS and ubi01.codalmacen ='A01' and ubi01.id = (select top 1 id from intranet.dbo.ubicaciones ubi2 where ubi2.codarticulo =ubi01.codarticulo and ubi2.codalmacen =ubi01.codalmacen "
        'No tengo en cuenta la ubicacion de consolidacion,
        sql &= "and ubi2.ubicacion not in ('" & constantes.consolidacion_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "') "
        'Solo tengo en cuenta el Picking. Pedido por Sergio/Domingo
        sql &= "and ubi2.tipo='Picking' order by tipo asc) "

        'A07
        sql &= "left join intranet.dbo.ubicaciones ubi07 on ubi07.codarticulo =art.CodArticulo collate Modern_Spanish_CI_AS and ubi07.codalmacen ='A07' and ubi07.id = (select top 1 id from intranet.dbo.ubicaciones ubi2 where ubi2.codarticulo =ubi07.codarticulo and ubi2.codalmacen =ubi07.codalmacen "
        'No tengo en cuenta la ubicacion de consolidacion,
        sql &= "and ubi2.ubicacion <>'" & constantes.consolidacion_A07 & "' "
        'Solo tengo en cuenta el Picking. Pedido por Sergio/Domingo
        sql &= "and ubi2.tipo='Picking' order by tipo asc) "

        'A08
        sql &= "left join intranet.dbo.ubicaciones ubi08 on ubi08.codarticulo =art.CodArticulo collate Modern_Spanish_CI_AS and ubi08.codalmacen ='A08' and ubi08.id = (select top 1 id from intranet.dbo.ubicaciones ubi2 where ubi2.codarticulo =ubi08.codarticulo and ubi2.codalmacen =ubi08.codalmacen "
        'No tengo en cuenta la ubicacion de consolidacion,
        sql &= "and ubi2.ubicacion <>'" & constantes.consolidacion_A08 & "' "
        'Solo tengo en cuenta el Picking. Pedido por Sergio/Domingo
        sql &= "and ubi2.tipo='Picking' order by tipo asc) "

        sql &= "where codempresa=1 and art.codarticulo='" & codarticulo & "' "
        'Ademas de saber si es un catalogo u otro , quedé con Chelo 24/06/2016 que miraríamos también si tiene ubicacíón en el almacén a priori obtenido

        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        If vdatos.Rows.Count = 0 Then
            almacen = "A01"
        Else
            ''Esto se consultaba al principio pero ahora prefieren que se elija el alamcen donde tiene el picking
            'Select Case vdatos.Rows(0).Item("Composicion").ToString.Trim
            '    Case "21", "22", "23", "41", "42", "43", "81", "82", "83", "16", "17"
            '        'Aunque el catalogo sea de A01, si no tiene ubicacion en A01 y si en A07, le paso A07
            '        If vdatos.Rows(0).Item("ubi01").ToString = "" And vdatos.Rows(0).Item("ubi07").ToString <> "" Then Return "A07" Else almacen = "A01"
            '    Case "104", "24", "44", "84", "31", "32", "33", "34"
            '        'Aunque el catalogo sea de A07, si no tiene ubicacion en A07 y si en A01, le paso A01
            '        If vdatos.Rows(0).Item("ubi07").ToString = "" And vdatos.Rows(0).Item("ubi01").ToString <> "" Then Return "A01" Else almacen = "A07"
            '    Case Else
            '        If vdatos.Rows(0).Item("ubi07").ToString <> "" And vdatos.Rows(0).Item("ubi01").ToString <> "" Then almacen = almacen_pref
            '        If vdatos.Rows(0).Item("ubi07").ToString = "" And vdatos.Rows(0).Item("ubi01").ToString = "" Then almacen = almacen_pref
            '        If vdatos.Rows(0).Item("ubi01").ToString <> "" And vdatos.Rows(0).Item("ubi07").ToString = "" Then almacen = "A01"
            '        If vdatos.Rows(0).Item("ubi07").ToString <> "" And vdatos.Rows(0).Item("ubi01").ToString = "" Then almacen = "A07"
            'End Select

            If vdatos.Rows(0).Item("ubi01").ToString <> "" Then almacen = "A01"
            If vdatos.Rows(0).Item("ubi07").ToString <> "" Then almacen = "A07"
            If vdatos.Rows(0).Item("ubi08").ToString <> "" Then almacen = "A08"
            If vdatos.Rows(0).Item("ubi01").ToString <> "" And vdatos.Rows(0).Item("ubi07").ToString <> "" And vdatos.Rows(0).Item("ubi08").ToString <> "" Then almacen = "AMBOS"
            If vdatos.Rows(0).Item("ubi01").ToString = "" And vdatos.Rows(0).Item("ubi07").ToString = "" And vdatos.Rows(0).Item("ubi08").ToString = "" Then

                ''Pedido por Sergio. Si es un montaje o un desglose , y no tiene picking en ninguno de los dos almacenes, se busca el picking de la referencia q no es carro
                ''en los montajes y el set en los desgloses
                Dim varticulo_stock As New DataTable
                sql = "select rel.codarticulo_stock,ubi.ubicacion ,ubi.codalmacen "
                sql &= "from V_Articulos_Relacionados rel "
                sql &= "left join ubicaciones ubi on ubi.tipo ='Picking' "
                'sql &= "and ubi.codarticulo collate Modern_Spanish_CI_AS =rel.codarticulo_stock "
                sql &= "and (ubi.codarticulo collate Modern_Spanish_CI_AS =rel.codarticulo_stock or ubi.codarticulo collate Modern_Spanish_CI_AS =rel.codarticulo_final ) "
                sql &= "and not (ubi.ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "') and ubi.codalmacen ='A01' ) "
                sql &= "and not (ubi.ubicacion in ('" & constantes.consolidacion_A07 & "') and ubi.codalmacen ='A07' ) "
                sql &= "and not (ubi.ubicacion in ('" & constantes.consolidacion_A08 & "') and ubi.codalmacen ='A08' ) "
                'sql &= "where rel.codarticulo_final ='" & codarticulo & "' "
                sql &= "where (rel.codarticulo_final ='" & codarticulo & "' or rel.codarticulo_stock ='" & codarticulo & "' ) "
                sql &= "and rel.codarticulo_stock not in (select art.codarticulo from inase.dbo.GesArticulos art where art.Codempresa=1 and art.TipoDeArticulo='07') "
                sql &= "and ubi.codalmacen is not null "
                sql &= "order by codalmacen desc "
                varticulo_stock = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If varticulo_stock.Rows.Count > 0 Then
                    almacen = varticulo_stock.Rows(0).Item("codalmacen").ToString
                Else
                    almacen = "AMBOS"
                End If
            End If
        End If
        If alm_pref <> "" And almacen = "AMBOS" Then almacen = alm_pref
        Return almacen

    End Function
    Public Function validar_ubicacion(ByVal codarticulo As String, ByVal codalmacen As String, ByVal ubicacion As String) As Boolean
        Dim vdatos As New DataTable
        Dim sql As String = ""

        sql = "select top 1 * from ubicaciones where codalmacen='" & codalmacen & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count >= 1 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub llenar_tipos_ubi(ByRef ddl As Object)
        Dim vdatos As New DataTable

        ddl.DataSource = Nothing
        ddl.Items.Clear()

        sql = "select tipo from ubicaciones_tipos order by orden asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        ddl.DisplayMember = "tipo"
        ddl.ValueMember = "tipo"
        ddl.DataSource = vdatos

    End Sub
    Public Function obtener_pendmontdesg(ByVal codarticulo As String) As Integer
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim pendmontdesg As Integer = 0
        sql = "Select isnull(sum(Case When cab.tipo='Desglose' then (lin.cantidad -lin.cantidad_validada)*-1 else lin.cantidad -lin.cantidad_validada end ),0) as cantidad "
        sql &= "from orden_desg_mont cab "
        sql &= "inner join orden_desg_mont_lin lin On lin.codorden =cab.codorden "
        sql &= "where (cab.recogido =0 or cab.depositado =0 ) and cab.fechaalta >='01/01/2018' "
        sql &= "and lin.codarticulo='" & codarticulo & "' "
        sql &= "union "
        sql &= "Select isnull(sum(case when cab.tipo='Montaje' then (ref.cantidad -ref.cantidad_validada)*-1 else ref.cantidad -ref.cantidad_validada end ),0) as cantidad "
        sql &= "from orden_desg_mont cab "
        sql &= "inner join orden_desg_mont_refs ref on ref.codorden =cab.codorden "
        sql &= "where (cab.recogido =0 Or cab.depositado =0) And cab.fechaalta >='01/01/2018' "
        sql &= "and ref.codarticulo='" & codarticulo & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        cont = 0
        Do While cont < vdatos.Rows.Count
            pendmontdesg += vdatos.Rows(cont)(0)
            cont += 1
        Loop
        Return pendmontdesg
    End Function
    Public Function obtener_pendreaprov(ByVal codarticulo As String, ByVal codalmacen As String, ByVal ubicacion As String, ByVal tipo As String) As Integer
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim pendmontdesg As Integer = 0
        If codalmacen <> "" And ubicacion <> "" And tipo <> "" Then
            sql = "Select isnull(sum(case when codalmacen_recogida='" & codalmacen & "' and ubicacion_recogida='" & ubicacion & "' and tipo_recogida ='" & tipo & "' then (cantidad-cantidad_recogida )*-1 end),0) "
            sql &= "+isnull(sum(case when codalmacen_depositada='" & codalmacen & "' and ubicacion_depositada='" & ubicacion & "' and tipo_depositada ='" & tipo & "' then cantidad-cantidad_depositada  end),0) as cantidad "
        Else
            sql = "Select isnull(sum(cantidad),0) as cantidad "
        End If
        sql &= "From reaprov_ubi cab "
        sql &= "inner Join reaprov_ubi_lin lin On lin.codreaprov =cab.codreaprov "
        sql &= "where cab.procesado = 0 And (cantidad_depositada = 0 Or cantidad_recogida = 0) "
        sql &= "and lin.codarticulo='" & codarticulo & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        cont = 0
        Do While cont < vdatos.Rows.Count
            pendmontdesg += vdatos.Rows(cont)(0)
            cont += 1
        Loop
        Return pendmontdesg
    End Function
    Public Function obtener_of(ByVal codarticulo As String) As Integer
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim penof As Integer = 0

        sql = "select isnull(sum(cantidadpdte),0) from GesPedidosProvLin where SerPedidoProv ='OF' and cantidadpdte<>0 and Articulo ='" & codarticulo & "' "
        sql &= "union "
        sql &= "select isnull(lin.cantidad - (case when lin.cantidad <1 then lin.cantidadvalidada else lin.cantidadvalidada * -1 end),0) from GesAlbaranesProvLin lin "
        sql &= "inner join GesAlbaranesProv cab on cab.NumReferencia =lin.RefCabecera "
        sql &= "where lin.Articulo ='" & codarticulo & "' and lin.SerAlbaranProv='OF' "
        'Solo se marca en estado 02 cuando finalizan, por tanto miro la cantidad validada que si que se actualiza enseguida que pulsan el ok
        'sql &= " and cab.Estado <> '02' "
        sql &= "and abs(lin.cantidadvalidada) <> abs(lin.cantidad) "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        cont = 0
        Do While cont < vdatos.Rows.Count
            penof += vdatos.Rows(cont)(0)
            cont += 1
        Loop

        Return penof
    End Function
    Public Function pendiente_servir(ByVal codarticulo As String, ByVal empresa As String, ByVal noreservas As Boolean) As Integer
        Dim sql As String
        Dim vdatos As DataTable
        sql = "select isnull(sum(cantidadpdte),0) "
        sql &= "from gespedidoslin lin "
        sql &= "inner join gespedidos cab on cab.numreferencia=lin.refcabecera "
        sql &= "where Articulo ='" & codarticulo.Trim & "' "
        If empresa <> "TODAS" Then sql &= "and codempresa = " & empresa & " "
        If noreservas = True Then
            sql &= "and cab.FormaPago not in (select formapago from intranet.dbo.V_Formas_Pago_Reserva) "
        End If
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        pendiente_servir = FormatNumber(vdatos.Rows(0)(0), 0)
    End Function
    Public Function stock(ByVal codarticulo As String, ByVal codempresa As String, ByVal codalmacen As String) As Integer
        Dim sql As String
        Dim vdatos As DataTable
        sql = "select isnull(sum(stockactual),0) "
        sql &= "from gesstocks "
        sql &= "where codempresa in (select CodEmpresa  from inaempresas where ControlStock =1 ) and CodArticulo ='" & codarticulo.Trim & "' "
        If codempresa <> "TODAS" Then sql &= "and codempresa = " & codempresa & " "
        If codalmacen <> "TODOS" Then sql &= "and codalmacen = '" & codalmacen & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        stock = FormatNumber(vdatos.Rows(0)(0), 0)
    End Function
    Public Function pendiente_preparar(ByVal codempresa As String, ByVal codalmacen As String, ByVal codarticulo As String) As Integer
        Dim sql As String
        Dim vdatos As DataTable
        'sql = "select isnull((select sum(l2.cantidad) from GesAlbaranesLin l2 where l2.codempresa=lin.CodEmpresa and l2.CodPeriodo =lin.codperiodo and l2.seralbaran=lin.SerAlbaran and l2.CodAlbaran =lin.CodAlbaran and l2.Articulo='" & codarticulo & "' and l2.almacen = lin.almacen),0) "
        'sql &= "-isnull(sum(pre.cantidadpreparada),0)  "
        'sql &= "from GesAlbaranesLin lin "
        'sql &= "inner join GesAlbaranes cab on cab.numreferencia=lin.refcabecera  "
        'sql &= "left join intranet.dbo.albaranes_preparados pre on pre.codempresa =lin.CodEmpresa and pre.codperiodo =lin.CodPeriodo and pre.seralbaran =lin.SerAlbaran "
        'sql &= "and pre.codalbaran =lin.CodAlbaran and pre.codlinea =lin.CodLinea and pre.codarticulo=lin.articulo "
        'sql &= "where lin.Articulo ='" & codarticulo & "' "
        'sql &= "and not ((cab.codempresa=1 and cab.cliente='99999' and (upper(cab.observaciones) like 'ALB. JOUMMA A NEXTDOOR%')) or (cab.cliente in ('99974','99964','90013') and cab.codempresa=1) or (cab.codempresa=2 and cab.cliente = '00006') or (cab.codempresa=2 and cab.cliente = '00100' and cab.observaciones='Ventas Web') or (cab.codempresa=3 and cab.cliente in ('00024','99999','99964','90013')) or (cab.observaciones like 'Devolucion-Abono%' or cab.observaciones like 'Devolucion-Cambio%'  or cab.observaciones like 'Devolucion-Reparacion%' or cab.observaciones like 'Devolucion desde TPV%' or cab.observaciones like 'Traspaso Devoluciones%') ) "
        'sql &= "And lin.codperiodo >=2017 And cab.seralbaran Not in ('AB','FR') and lin.Almacen in ('A01','A07','A08') " 'Solo los almacenens que se inventarian
        'sql &= "group by lin.codempresa,lin.codperiodo ,lin.seralbaran ,lin.codalbaran ,cab.cliente ,cab.observaciones,lin.almacen "
        'sql &= "having isnull((select sum(l2.cantidad) from GesAlbaranesLin l2 where l2.codempresa=lin.CodEmpresa and l2.CodPeriodo =lin.codperiodo and l2.seralbaran=lin.SerAlbaran and l2.CodAlbaran =lin.CodAlbaran and l2.Articulo='" & codarticulo & "' and l2.almacen = lin.almacen),0)  <> isnull(sum(pre.cantidadpreparada),0) "
        sql = "select isnull(sum(cantPendPrep),0) from intranet.dbo.V_pendientePreparar where codarticulo='" & codarticulo & "' "
        sql &= "and almacen <> 'Z01' " ' Pedido por sergio. 16/09/2019
        sql &= "and almacen <> 'A03' " ' Pedido por sergio. 19/01/2021
        If codempresa <> "TODAS" Then sql &= "codempresa =" & codempresa & " "
        If codalmacen <> "TODOS" Then sql &= "And almacen='" & codalmacen & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        If vdatos.Rows.Count = 1 Then pendiente_preparar = FormatNumber(vdatos.Rows(0)(0), 0)
    End Function
    Public Function consultar_tipo_ubicacion(ByVal almacen As String, ByVal pasillo As Integer, ByVal portal As Integer, ByVal altura As Integer) As String
        Dim vdatos As New DataTable
        Try
            sql = "select tipo from ubicaciones_almacen where codalmacen='" & almacen & "' and pasillo=" & pasillo & " and portal=" & portal & " and altura=" & altura
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 1 Then
                Return vdatos.Rows(0)(0)
            Else
                'MsgBox("No se encontró el tamaño de la ubicación.", MsgBoxStyle.Critical)
                Return ""
            End If
        Catch ex As Exception
            MsgBox("Se produjo un error al consultar el tamaño de la ubicación.", MsgBoxStyle.Critical)
            Return 0
        End Try
    End Function

    Public Sub mostrar_fotos(imgObject As PictureBox, codarticulo As String)
        Try
            imgObject.ImageLocation = "\\jserver\BAJA\" & codarticulo & ".jpg"
        Catch ex As Exception
        End Try
    End Sub

End Module
