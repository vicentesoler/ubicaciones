﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ReaprovDesdePaletImpDigital
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.list_palet = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.btn_salir = New System.Windows.Forms.Button()
        Me.text_articulo = New System.Windows.Forms.TextBox()
        Me.text_cantidad = New System.Windows.Forms.TextBox()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_ConfirmarTodo = New System.Windows.Forms.Button()
        Me.list_accion = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.AllowUserToResizeColumns = False
        Me.dg_lineas.AllowUserToResizeRows = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_lineas.DefaultCellStyle = DataGridViewCellStyle8
        Me.dg_lineas.Location = New System.Drawing.Point(12, 141)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(1179, 399)
        Me.dg_lineas.TabIndex = 16
        '
        'list_palet
        '
        Me.list_palet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_palet.Enabled = False
        Me.list_palet.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_palet.FormattingEnabled = True
        Me.list_palet.Location = New System.Drawing.Point(575, 15)
        Me.list_palet.Name = "list_palet"
        Me.list_palet.Size = New System.Drawing.Size(453, 50)
        Me.list_palet.TabIndex = 85
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(459, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 39)
        Me.Label1.TabIndex = 86
        Me.Label1.Text = "Palet:"
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Enabled = False
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(921, 551)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(270, 50)
        Me.btn_finalizar.TabIndex = 87
        Me.btn_finalizar.Text = "Crear Orden."
        Me.btn_finalizar.UseVisualStyleBackColor = True
        Me.btn_finalizar.Visible = False
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(1055, 12)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(136, 120)
        Me.imagen.TabIndex = 88
        Me.imagen.TabStop = False
        '
        'btn_salir
        '
        Me.btn_salir.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_salir.Location = New System.Drawing.Point(12, 551)
        Me.btn_salir.Name = "btn_salir"
        Me.btn_salir.Size = New System.Drawing.Size(146, 49)
        Me.btn_salir.TabIndex = 89
        Me.btn_salir.Text = "Cerrar"
        Me.btn_salir.UseVisualStyleBackColor = True
        '
        'text_articulo
        '
        Me.text_articulo.Enabled = False
        Me.text_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_articulo.Location = New System.Drawing.Point(93, 77)
        Me.text_articulo.MaxLength = 13
        Me.text_articulo.Name = "text_articulo"
        Me.text_articulo.Size = New System.Drawing.Size(170, 45)
        Me.text_articulo.TabIndex = 90
        '
        'text_cantidad
        '
        Me.text_cantidad.Enabled = False
        Me.text_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_cantidad.Location = New System.Drawing.Point(391, 77)
        Me.text_cantidad.Name = "text_cantidad"
        Me.text_cantidad.Size = New System.Drawing.Size(118, 45)
        Me.text_cantidad.TabIndex = 91
        Me.text_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btn_ok
        '
        Me.btn_ok.Enabled = False
        Me.btn_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ok.Location = New System.Drawing.Point(534, 77)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(92, 45)
        Me.btn_ok.TabIndex = 92
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(13, 82)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(78, 31)
        Me.Label12.TabIndex = 93
        Me.Label12.Text = "Ref.:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(293, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 31)
        Me.Label2.TabIndex = 94
        Me.Label2.Text = "Cant.:"
        '
        'btn_ConfirmarTodo
        '
        Me.btn_ConfirmarTodo.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.btn_ConfirmarTodo.Enabled = False
        Me.btn_ConfirmarTodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ConfirmarTodo.Location = New System.Drawing.Point(713, 77)
        Me.btn_ConfirmarTodo.Name = "btn_ConfirmarTodo"
        Me.btn_ConfirmarTodo.Size = New System.Drawing.Size(250, 45)
        Me.btn_ConfirmarTodo.TabIndex = 95
        Me.btn_ConfirmarTodo.Text = "Confirmar Todo"
        Me.btn_ConfirmarTodo.UseVisualStyleBackColor = False
        '
        'list_accion
        '
        Me.list_accion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_accion.Font = New System.Drawing.Font("Microsoft Sans Serif", 28.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_accion.FormattingEnabled = True
        Me.list_accion.Items.AddRange(New Object() {"Sacar Fuera", "Subir Palet"})
        Me.list_accion.Location = New System.Drawing.Point(164, 15)
        Me.list_accion.Name = "list_accion"
        Me.list_accion.Size = New System.Drawing.Size(261, 50)
        Me.list_accion.TabIndex = 97
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(22, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 39)
        Me.Label3.TabIndex = 96
        Me.Label3.Text = "Acción:"
        '
        'frm_ReaprovDesdePaletImpDigital
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1210, 606)
        Me.Controls.Add(Me.list_accion)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btn_ConfirmarTodo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.text_articulo)
        Me.Controls.Add(Me.text_cantidad)
        Me.Controls.Add(Me.btn_ok)
        Me.Controls.Add(Me.btn_salir)
        Me.Controls.Add(Me.imagen)
        Me.Controls.Add(Me.btn_finalizar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.list_palet)
        Me.Controls.Add(Me.dg_lineas)
        Me.Name = "frm_ReaprovDesdePaletImpDigital"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_ReaprovDesdePaletImpDigital"
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dg_lineas As DataGridView
    Friend WithEvents list_palet As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btn_finalizar As Button
    Friend WithEvents imagen As PictureBox
    Friend WithEvents btn_salir As Button
    Friend WithEvents text_articulo As TextBox
    Friend WithEvents text_cantidad As TextBox
    Friend WithEvents btn_ok As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btn_ConfirmarTodo As Button
    Friend WithEvents list_accion As ComboBox
    Friend WithEvents Label3 As Label
End Class
