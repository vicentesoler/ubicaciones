﻿Public Class frm_ordenalta
    Dim conexion As New Conector
    Dim tiempo As Integer 'Segundos
    Dim fecha_inicio, fecha_fin As DateTime

    Private Sub frm_ordenalta_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        limpiar(False)
        llenar_ordenes()
    End Sub
    Public Sub llenar_ordenes()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer

        list_codorden.DataSource = Nothing
        list_codorden.Items.Clear()
        sql = "select cab.codorden from orden_alta cab "
        If txt_filtro_codarticulo.Text.Trim <> "" Then sql &= "inner join orden_alta_lin lin on lin.codorden=cab.codorden and lin.codarticulo like '%" & txt_filtro_codarticulo.Text & "%' "
        sql &= "where cab.procesado ='NO' "
        sql &= "group by cab.codorden "
        sql &= "order by cab.codorden asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        cont = 0
        list_codorden.Text = ""
        list_codorden.Items.Clear()
        Do While cont < vdatos.Rows.Count
            list_codorden.Items.Add(vdatos.Rows(cont).Item("codorden"))
            cont += 1
        Loop

        If txt_filtro_codarticulo.Text <> "" Then list_codorden.BackColor = Color.SteelBlue Else list_codorden.BackColor = Color.White

    End Sub
    Private Sub list_codorden_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles list_codorden.KeyUp
        If e.KeyValue = Keys.Enter Then
            If list_codorden.Text.Length = 13 Then
                list_codorden.Text = list_codorden.Text.Substring(7, 5)
                list_codorden.Text = list_codorden.Text.Substring(7, 5)
            End If
            datos_cabecera()
        End If
    End Sub
    Public Sub datos_cabecera()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0

        'Guardar el momento de abrir el albarán
        fecha_inicio = Now

        p_filtro.Enabled = False

        sql = "Select codalmacen_origen, codalmacen_destino, procesado, aplicacion, Convert(date, fechaalta) As fecha, tiempo "
        sql &= "from orden_alta  "
        sql &= "where codorden=" & list_codorden.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows.Count = 1 Then
            If vdatos.Rows(0).Item("procesado") = "SI" Then
                If MsgBox("Esta orden de alta ya ha sido procesada, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    limpiar(True, "Esta orden de alta ya ha sido procesada")
                    Exit Sub
                End If
            End If

            conexion.ExecuteCmd(constantes.bd_intranet, "Update orden_alta set estado='Abierta',ultmodificacion=getdate(),ultusuario='" & frm_principal.lab_nomoperario.Text & "' where codorden=" & list_codorden.Text)

            lab_fecha.Text = "Fecha:" & vdatos.Rows(0).Item("fecha")
            lab_aplicacion.Text = vdatos.Rows(0).Item("aplicacion")
            lab_almorigen.Text = vdatos.Rows(0).Item("codalmacen_origen")
            lab_almdestino.Text = vdatos.Rows(0).Item("codalmacen_destino")
            lab_almdestino.Visible = True
            lab_almorigen.Visible = True
            lab_flecha.Visible = True
            If frm_principal.PermOrdenAltaVerTiempo = True Then
                lab_txttiempo.Visible = True
                lab_tiempo.Visible = True
                lab_tiempo.Text = FormatNumber(vdatos.Rows(0).Item("tiempo") / 60, 2) & " min"
            Else
                lab_txttiempo.Visible = False
                lab_tiempo.Visible = False
            End If
            cargar_lineas()
            tiempo = 0
            If frm_principal.lab_idoperario.Text <> 8 Then
                Timer1.Enabled = True
                Timer1.Start()
            End If
            btn_cerrar.Enabled = True
            btn_finalizar.Enabled = True
            'sergio pidió que no se pudiera usar.
            'btn_confirmarTodo.Enabled = True
            list_codorden.Enabled = False
        Else
            MsgBox("No existe la orden de alta seleccionada o no tiene datos.", MsgBoxStyle.Exclamation)
            limpiar(True, "No existe la orden de alta seleccionada o no tiene datos.")
            Exit Sub
        End If
    End Sub
    Public Sub cargar_lineas()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0


        sql = "select codlinea as 'Lin.',lin.codarticulo as 'Ref.',art.descripcion as 'Descripcion',lin.ubicacion_origen as 'Ubi.Ori.',case when lin.ubicacion_destino='99-99-99' then 'SIN UBI.' else lin.ubicacion_destino end as 'Ubi.Dest.',lin.cantidad as 'Cant.',cantidad_depositada as 'Depos.' "
        sql &= "from orden_alta_lin lin "
        sql &= "inner join orden_alta cab on cab.codorden =lin.codorden "
        sql &= "inner join inase.dbo.GesArticulos art on art.codempresa=1 and art.codarticulo collate Modern_Spanish_CI_AS = lin.codarticulo "
        'sql &= "left join ubicaciones ubi on lin.codarticulo collate Modern_Spanish_CI_AS=ubi.codarticulo and tipo='Picking' and ubi.codalmacen='" & lab_almorigen.Text & "' "
        'sql &= "and ubi.id = (select min(ubi2.id) from intranet.dbo.ubicaciones ubi2 where ubi2.codarticulo=ubi.codarticulo and ubi2.tipo='Picking' and ubi2.codalmacen='" & lab_almorigen.Text & "') "
        sql &= "where lin.codorden=" & list_codorden.Text & " "
        sql &= "order by convert(integer,left(isnull(lin.ubicacion_destino,'99-99-99'),2)) asc, "
        sql &= "case when convert(integer,left(isnull(lin.ubicacion_destino,'99-99-99'),2)) in (1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99) then convert(integer,substring(isnull(lin.ubicacion_destino,'99-99-99'),4,2)) "
        sql &= "else 99-convert(integer,substring(isnull(lin.ubicacion_destino,'99-99-99'),4,2)) end asc, "
        sql &= "case when convert(integer,left(isnull(lin.ubicacion_destino,'99-99-99'),2)) in (1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99) then convert(integer,substring(isnull(lin.ubicacion_destino,'99-99-99'),7,2)) "
        sql &= "else 99-convert(integer,substring(isnull(lin.ubicacion_destino,'99-99-99'),7,2)) end asc,lin.codarticulo asc "

        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_lineas.DataSource = vdatos
            dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 22)
            dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 22, FontStyle.Bold)
            'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_lineas.Columns(0).Width = 75
            dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(1).Width = 160
            dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dg_lineas.Columns(2).Width = 400
            dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(3).Width = 140
            dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(4).Width = 140
            dg_lineas.Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(5).Width = 110
            dg_lineas.Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(6).Width = 110
        Else
            MsgBox("La orden no tiene lineas", MsgBoxStyle.Critical)
            limpiar()
            list_codorden.SelectAll()
        End If

        pintar_lineas_grid()
        'txt_articulo.Focus()
        'txt_articulo.SelectAll()
    End Sub

    Public Sub pintar_lineas_grid()
        Dim cont As Integer
        Dim totunid As Integer = 0
        Dim totdep As Integer = 0
        cont = 0
        While cont < dg_lineas.RowCount
            totunid += dg_lineas.Rows(cont).Cells("Cant.").Value
            totdep += dg_lineas.Rows(cont).Cells("Depos.").Value
            Select Case dg_lineas.Rows(cont).Cells("Depos.").Value
                Case 0
                Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
                Case Is > dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Red
                Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
            End Select

            'Situar el cursor en la linea q acabamos de marcar
            'If dg_lineasalb.Rows(cont).Cells("Referencia").Value = ult_ref_embalada And (linea_click = 0 Or dg_lineasalb.Rows(cont).Cells("Lin").Value = linea_click) Then
            '    dg_lineasalb.Item(0, dg_lineasalb.RowCount - 1).Selected = True
            '    dg_lineasalb.Item(0, cont).Selected = True
            'End If

            cont += 1
        End While
        lab_totunid.Text = FormatNumber(totunid, 0)
        lab_totdep.Text = FormatNumber(totdep, 0)
        'If ult_ref_embalada = "" Then dg_lineasalb.Item(0, dg_lineasalb.CurrentRow.Index).Selected = False

    End Sub

    Private Sub list_codorden_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_codorden.SelectedIndexChanged
        If list_codorden.SelectedIndex = -1 Then
            limpiar()
        Else
            datos_cabecera()
        End If
    End Sub


    Private Sub dg_lineas_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_lineas.CellEnter
        seleccionar_linea(e.RowIndex)
    End Sub
    Public Sub seleccionar_linea(rowindex As Integer)
        imagen.ImageLocation = "\\jserver\BAJA\" & dg_lineas.Rows(rowindex).Cells("Ref.").Value & ".jpg"
        imagen.SizeMode = PictureBoxSizeMode.StretchImage
        txt_articulo.Text = dg_lineas.Rows(rowindex).Cells("Ref.").Value
        txt_descripcion.Text = dg_lineas.Rows(rowindex).Cells("Descripcion").Value
        txt_cantidad.Text = dg_lineas.Rows(rowindex).Cells("Cant.").Value - dg_lineas.Rows(rowindex).Cells("Depos.").Value
        Select Case dg_lineas.Rows(rowindex).Cells("Depos.").Value
            Case 0 : txt_articulo.BackColor = Color.White
            Case Is = dg_lineas.Rows(rowindex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Green
            Case Is < dg_lineas.Rows(rowindex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Yellow
            Case Is > dg_lineas.Rows(rowindex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Red
        End Select
        llenar_ubicaciones_articulo(dg_lineas.Rows(rowindex).Cells("Ref.").Value, rowindex)
        If dg_lineas.Rows(rowindex).Cells("Depos.").Value <> 0 Then
            list_ubicacion.Enabled = False
        Else
            list_ubicacion.Enabled = True
        End If
        btn_OtraUbi.Enabled = True
        btn_ok.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub
    Public Sub llenar_ubicaciones_articulo(ByVal codarticulo As String, ByVal fila As Integer)
        Dim cont As Integer
        Dim sql As String
        Dim vdatos As New DataTable

        list_ubicacion.DataSource = Nothing
        list_ubicacion.Items.Clear()


        'Añadir como primera opcion la ubicacion que pone en el grid, q es la que viene de la orden de alta
        'Ahora Domingo pidió que se muestren sola la ubicación/ubicaciones de picking que estén realmente dadas de alta .
        'Sucedía que cuando Dora hace la orden , en la orden se guarda una ubicación y luego cuando van a procesarla desde el programa de ubicaciones
        'la ubicación ya no era la misma y apareciían dos ubicaciones , una que ya no existe y la nueva, con lo que confundía


        sql = "select ubicacion from ubicaciones where codarticulo ='" & codarticulo & "' and codalmacen='" & lab_almdestino.Text & "' "
        If lab_almdestino.Text = "A01" Then sql &= "and ubicacion <> '" & constantes.ubicacion_devoluciones_A01 & "' "
        If lab_almdestino.Text = "A07" Then sql &= "and ubicacion <> '" & constantes.ubicacion_devoluciones_A07 & "' "
        If lab_almdestino.Text = "A08" Then sql &= "and ubicacion <> '" & constantes.ubicacion_devoluciones_A08 & "' "
        'sql &= "and ubicacion <> '" & dg_lineas.Rows(fila).Cells("Ubi.").Value & "' " 'Para que no muestre esta ubicacion repetida
        sql &= "and tipo='Picking' " 'Pedido por Domingo. 23/10/2017
        sql &= "order by tipo desc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count = 0 Then
            vdatos.Rows.Add({""})
        End If


        list_ubicacion.DisplayMember = "ubicacion"
        list_ubicacion.ValueMember = "ubicacion"
        list_ubicacion.DataSource = vdatos

        'list_ubicacion.SelectedIndex = vdatos.Rows.Count - 1

        list_ubicacion.SelectedValue = dg_lineas.Rows(fila).Cells("Ubi.Dest.").Value ' Si no exsite esta ubicación, quedará marcada la pirmera 
        If list_ubicacion.SelectedIndex = -1 Then list_ubicacion.SelectedIndex = 0

    End Sub

    Private Sub txt_cantidad_Click(sender As System.Object, e As System.EventArgs) Handles txt_cantidad.Click
        txt_cantidad.SelectAll()
    End Sub

    Private Sub btn_ok_Click(sender As System.Object, e As System.EventArgs) Handles btn_ok.Click
        confirmar_linea()
    End Sub
    Public Sub confirmar_linea()
        Dim sql As String = ""
        Dim sql2 As String = ""
        Dim resultado As String
        Dim vcant, vimporte, vubi As New DataTable
        Dim cantant As Integer = 0
        Dim nummov As Integer
        Dim numrefmov As String
        Dim ubicacion As String = list_ubicacion.SelectedValue

        If dg_lineas.SelectedRows.Count = 0 Then Exit Sub

        If ubicacion = "" Then
            If MsgBox("La referencia no tiene ubicación en el almacen " & lab_almdestino.Text & ", no se puede confirmar esta referencia. Quiere dar de alta la ubicación ahora?.", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                frm_nuevaubicacion.lab_ref.Text = dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim
                frm_nuevaubicacion.list_tipo.SelectedIndex = -1
                frm_nuevaubicacion.txt_altura.Text = ""
                frm_nuevaubicacion.txt_cantmax.Text = ""
                frm_nuevaubicacion.txt_pasillo.Text = ""
                frm_nuevaubicacion.txt_portal.Text = ""
                frm_nuevaubicacion.txt_tamaño.Text = ""
                frm_nuevaubicacion.lab_almacen_aux.Text = lab_almdestino.Text
                frm_nuevaubicacion.ShowDialog()
                If frm_nuevaubicacion.txt_pasillo.Text <> "" Then
                    ubicacion = frm_nuevaubicacion.txt_pasillo.Text & "-" & frm_nuevaubicacion.txt_portal.Text & "-" & frm_nuevaubicacion.txt_altura.Text
                Else
                    MsgBox("Como no se ha creado la nueva ubicación, la linea no será procesada.", MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
            Else
                Exit Sub
            End If

        End If



        'Mirar si existe la ubicacion origen
        'Esto no debería hacer falta. Lo dejo por si acaso
        sql = "select * from ubicaciones where codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubi.Ori.").Value.ToString.Trim & "' and codalmacen='" & lab_almorigen.Text & "' "
        vubi = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vubi.Rows.Count = 0 Then
            'Me di cuenta que si despues de confirmar una linea , se quiere anular, como la ubicación 
            'de devoluciones es eliminada si se queda a 0, no existe la ubicacion para dejar el stock, por tanto tengo crear la ubicacion
            sql = "insert into ubicaciones (codalmacen,ubicacion,codarticulo,tipo,tamaño,cantidad,observaciones,ultusuario) "
            sql &= "values('" & lab_almorigen.Text & "','" & dg_lineas.SelectedRows(0).Cells("Ubi.Ori.").Value.ToString.Trim & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "','Picking',0,0,'Ubicaciones-ordenalta','" & frm_principal.lab_nomoperario.Text & "') "
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If


        If txt_cantidad.Text = 0 Then
            If MsgBox("Seguro que quiere eliminar la cantidad preparada?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

            '19/06/2020 Si devulevo cantidad, tengo que devolver la cantidad a la ubicación que está en la linea de la orden de alta, que es la que se usó
            ubicacion = dg_lineas.SelectedRows(0).Cells("Ubi.Dest.").Value.ToString.Trim

            'Obtener la cantidad que tiene hasta el momento la linea preparada para sumarla al stock y dejar el stock en el picking cuadrado
            sql = "select cantidad_depositada from orden_alta_lin where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
            vcant = conexion.TablaxCmd(constantes.bd_intranet, sql)
            sql = ""
            If vcant.Rows.Count = 1 Then
                cantant = CInt(vcant.Rows(0)(0))
                'Dejar stock como estaba en la ubicacion origen
                sql = "update ubicaciones set cantidad+=" & cantant & " where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubi.Ori.").Value.ToString.Trim & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubi.Ori.").Value.ToString.Trim & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad desc ) "

                'Dejar stock como estaba en la ubicacion destino. 
                sql &= "update ubicaciones set cantidad-=" & cantant & " where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & ubicacion & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & ubicacion & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad desc ) "

            End If
            'Actualizar la linea de orden alta
            sql &= "update orden_alta_lin set cantidad_depositada=0,ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "

            'Poner la cantidad en el grid
            dg_lineas.SelectedRows(0).Cells("Depos.").Value = 0

            'mov para dejar el stock bien en el almacen 
            funciones.crear_movimiento_inase(1, nummov, "Mov. al eliminar cantidad en orden alta (" & list_codorden.Text & ").", numrefmov)
            If numrefmov <> "" Then
                sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacenori,ubicacionori,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                sql &= "values('" & numrefmov & "',0," & 1 & ",'" & Now.Year & "'," & nummov & ",1,'" & lab_almdestino.Text & "','XXX','" & lab_almorigen.Text & "','XXX','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "'," & cantant & ",'Movimiento ajuste desde Ubicaciones/OrdenAlta al eliminar la cantidad (Orden:" & list_codorden.Text & ".') "
            Else
                funciones.Enviar_correo("vicentesoler@joumma.com", "Se producjo un error en una orden de alta", "Error creando cabecera movimiento en orden de alta por cantidad diferente al confirmar orden. " & sql)
                MsgBox("Se producjo un error.")
                Exit Sub
            End If
        Else
            sql = ""
            'Actualizar el Stock en la ubicación origen
            sql &= "update ubicaciones set ultusuario='" & frm_principal.lab_nomoperario.Text & "',observaciones='Ubicaciones-OrdenAlta',fechaultmodificacion=getdate(), cantidad=isnull(cantidad,0)-(" & CInt(txt_cantidad.Text) & ") where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubi.Ori.").Value.ToString.Trim & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
            'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
            sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almorigen.Text & "' and ubicacion='" & dg_lineas.SelectedRows(0).Cells("Ubi.Ori.").Value.ToString.Trim & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad desc ) "

            'Actualizar la ubicacion destino
            sql &= "update ubicaciones set ultusuario='" & frm_principal.lab_nomoperario.Text & "',observaciones='Ubicaciones-OrdenAlta',fechaultmodificacion=getdate(), cantidad=isnull(cantidad,0)+(" & CInt(txt_cantidad.Text) & ") where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & ubicacion & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
            'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
            sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & ubicacion & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad desc ) "

            'Actualizar la linea de orden alta
            sql &= "update orden_alta_lin set ubicacion_destino='" & ubicacion & "', ubicacion_destino_propuesta='" & dg_lineas.SelectedRows(0).Cells("Ubi.Dest.").Value.ToString.Trim & "', cantidad_depositada=" & CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Depos.").Value.ToString) & ",ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultmodificacion=getdate() "
            sql &= "where codorden=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
            'Poner la cantidad en el grid
            dg_lineas.SelectedRows(0).Cells("Depos.").Value = CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Depos.").Value.ToString)

            'Actualizar en el grid la ubicación que ha elegido. Así queda visible la ubicación q ha elegido, por si es diferente a la que le ofrecía.
            'Además es necesario ya que si elimina la cantidad, debe saber el programa en que ubicaicón lo depositó, sino devolvería a la propuesta inicialmente
            dg_lineas.SelectedRows(0).Cells("Ubi.Dest.").Value = list_ubicacion.SelectedValue

            'mov para dejar el stock bien en el almacen 
            funciones.crear_movimiento_inase(1, nummov, "Mov.para cambiar stock de almacen en orden alta (" & list_codorden.Text & ").", numrefmov)
            If numrefmov <> "" Then
                sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacenori,ubicacionori,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                sql &= "values('" & numrefmov & "',0," & 1 & ",'" & Now.Year & "'," & nummov & ",1,'" & lab_almorigen.Text & "','XXX','" & lab_almdestino.Text & "','XXX','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "'," & CInt(txt_cantidad.Text) & ",'Movimiento ajuste desde Ubicaciones/OrdenAlta (Orden:" & list_codorden.Text & ").') "
            Else
                funciones.Enviar_correo("vicentesoler@joumma.com", "Se producjo un error en una orden de alta", "Error creando cabecera movimiento al confirmar linea. (" & list_codorden.Text & ")")
                MsgBox("Se producjo un error.")
                Exit Sub
            End If

        End If

        'Origen
        funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, (CInt(txt_cantidad.Text) * -1), "" & lab_almorigen.Text & "", dg_lineas.SelectedRows(0).Cells("Ubi.Ori.").Value.ToString.Trim, "Ord:" & list_codorden.Text, "Ubicaciones-Orden Alta", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
        'Destino
        funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, CInt(txt_cantidad.Text) - cantant, lab_almdestino.Text, ubicacion, "Ord:" & list_codorden.Text, "Ubicaciones-Orden Alta", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        'Eliminar de la ubicacion de devol , las lineas a 0, para q no moleste en el lanz. pedidos y asigne de esta ubicacion
        'sql &= "delete ubicaciones where codalmacen='A01' and ubicacion='" & constantes.ubicacion_devoluciones_A01 & "' and cantidad=0 "
        'sql &= "delete ubicaciones where codalmacen='A07' and ubicacion='" & constantes.ubicacion_devoluciones_A07 & "' and cantidad=0 "
        'sql &= "delete ubicaciones where codalmacen='A08' and ubicacion='" & constantes.ubicacion_devoluciones_A08 & "' and cantidad=0 "
        'Eliminado porque a veces se crea una ubicación con cantidad a 0 para usarla luego y se borraba al momento. 19/07/2019. 
        'Ahora lo borro por las noches

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)

        If resultado <> "0" Then
            MsgBox("Se produjo un error al marcar la linea.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        'Pintar fila
        Select Case CInt(dg_lineas.SelectedRows(0).Cells("Depos.").Value.ToString)
            Case 0 : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow
            Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Red
        End Select

        If dg_lineas.CurrentRow.Index < dg_lineas.Rows.Count - 1 Then
            dg_lineas.Item(0, dg_lineas.CurrentRow.Index + 1).Selected = True
        Else
            dg_lineas.Item(0, dg_lineas.CurrentRow.Index).Selected = False
            txt_articulo.Text = ""
            txt_articulo.BackColor = Color.White
            txt_descripcion.Text = ""
            txt_cantidad.Text = ""
            list_ubicacion.DataSource = Nothing
            list_ubicacion.Items.Clear()
            btn_ok.Enabled = False
        End If
        btn_finalizar.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub
    Public Sub parar_temporizador()
        Dim sql As String
        If IsNumeric(list_codorden.Text) Then
            sql = "update orden_alta set tiempo=isnull(tiempo,0) + " & tiempo & " where codorden=" & list_codorden.Text & " and procesado='NO' "
            conexion.Executetransact(constantes.bd_intranet, sql)
        End If

        Timer1.Stop()
        Timer1.Enabled = False
    End Sub
    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql As String = ""
        Dim resultado As String
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim proxmov As Integer = 0
        Dim numrefmov As String = ""
        Dim lineasprep As Integer = 0
        Dim pedidoincompleto As Boolean = False


        'Revisar si la orden esta completamente terminada
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("Depos.").Value Then
                pedidoincompleto = True
            End If
            If dg_lineas.Rows(cont).Cells("Depos.").Value > 0 Then
                lineasprep += 1
            End If
            cont += 1
        Loop


        If pedidoincompleto = True Then
            If MsgBox("La orden no está completamente terminada, finalizar de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
        End If

        'Lo hago antes de marcar como finalizado porque la condición para que guarde los datos del tiempo es que la orden no esté ya finalizada
        parar_temporizador()

        'Marcar como procesado la orden
        sql = "update orden_alta set procesado='SI',ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text
        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado = "0" Then
            MsgBox("Orden de alta finalizada correctamente", MsgBoxStyle.Information)
        Else
            MsgBox("Se produjo un error al marcar la orden como procesada. Avisar a Informática", MsgBoxStyle.Information)
        End If

        'Borrar todos los registros de la tabla ubicaciones que sean de la ubicacion de consolidado con cantidad a 0
        'Esto es para que no se quede basura
        'sql = "delete ubicaciones where ((codalmacen='A01' and ubicacion='" & constantes.consolidacion_A01 & "' ) or (codalmacen='A07' and ubicacion='" & constantes.consolidacion_A07 & "') or (codalmacen='A08' and ubicacion='" & constantes.consolidacion_A08 & "')) and cantidad=0 "
        'resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
        'Eliminado porque a veces se crea una ubicación con cantidad a 0 para usarla luego y se borraba al momento. 19/07/2019. 
        'Ahora lo borro por las noches

        limpiar()
        llenar_ordenes()
    End Sub

    Public Sub limpiar(Optional ByVal GuardarTiempos As Boolean = True, Optional ByVal ObsGuardarTiempos As String = "")
        'Guardar datos de operario
        fecha_fin = Now
        If GuardarTiempos Then Guardar_datos_tiempo(ObsGuardarTiempos)

        If list_codorden.Text <> "" Then conexion.ExecuteCmd(constantes.bd_intranet, "Update orden_alta set estado='Cerrada',ultmodificacion=getdate(),ultusuario='" & frm_principal.lab_nomoperario.Text & "' where codorden=" & list_codorden.Text)

        p_filtro.Enabled = True
        lab_aplicacion.Text = ""
        lab_flecha.Visible = False
        lab_almdestino.Visible = False
        lab_almorigen.Visible = False
        txt_articulo.Text = ""
        txt_articulo.BackColor = Color.White
        txt_descripcion.Text = ""
        txt_cantidad.Text = ""
        list_ubicacion.DataSource = Nothing
        list_ubicacion.Items.Clear()
        btn_ok.Enabled = False
        btn_finalizar.Enabled = False
        btn_cerrar.Enabled = False
        dg_lineas.DataSource = Nothing
        dg_lineas.Rows.Clear()
        imagen.ImageLocation = ""
        lab_fecha.Text = "Fecha: "
        btn_finalizar.Enabled = False
        btn_cerrar.Enabled = False
        btn_confirmarTodo.Enabled = False
        btn_OtraUbi.Enabled = False
        list_codorden.Enabled = True
        list_codorden.SelectedIndex = -1
    End Sub

    Public Sub Guardar_datos_tiempo(Optional ByVal ObsGuardarTiempos As String = "")
        Dim TiempoEmpleado As Integer
        Dim sql As String
        Try
            TiempoEmpleado = DateDiff(DateInterval.Second, fecha_inicio, fecha_fin)
            sql = "insert into albaranes_tiempos (codOrdenAlta,idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(" & list_codorden.Text & "," & frm_principal.lab_idoperario.Text & ",'OrdenAlta','" & fecha_inicio & "','" & fecha_fin & "'," & TiempoEmpleado & ",0,'" & ObsGuardarTiempos & "' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        Catch ex As Exception

        End Try

    End Sub

    Public Sub confirmar_todo()
        Dim cont As Integer
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Depos.").Value < dg_lineas.Rows(cont).Cells("Cant.").Value Then
                dg_lineas.CurrentCell = dg_lineas.Rows(cont).Cells(1)
                confirmar_linea()
            End If

            cont += 1
        Loop

    End Sub

    Private Sub dg_lineas_Sorted(sender As System.Object, e As System.EventArgs) Handles dg_lineas.Sorted
        pintar_lineas_grid()
    End Sub

    Private Sub txt_articulo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_articulo.KeyUp
        Dim codarticulo As String = txt_articulo.Text
        Dim cont As Integer


        If e.KeyCode = Keys.Enter Then
            If txt_articulo.Text.Length = 7 Or txt_articulo.Text.Length = 13 Then
                txt_descripcion.Text = funciones.buscar_descripcion(codarticulo)
                'Mirara si pertenece al pedido
                cont = 0
                Do While cont < dg_lineas.Rows.Count
                    If dg_lineas.Rows(cont).Cells("Ref.").Value = codarticulo Then
                        seleccionar_linea(cont)
                        'Situar en la linea
                        dg_lineas.CurrentCell = dg_lineas.Rows(cont).Cells(1)
                        dg_lineas.Rows(cont).Selected = True
                        Exit Sub
                    End If
                    cont += 1
                Loop
                MsgBox("Esta referencia no pertence a la orden.", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        tiempo += 1
    End Sub

    Private Sub frm_ordenalta_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        'Por si cambian de pestaña sin cerrar la orden
        conexion.ExecuteCmd(constantes.bd_intranet, "Update orden_alta set estado='Cerrada',ultmodificacion=getdate(),ultusuario='" & frm_principal.lab_nomoperario.Text & "' where codorden=" & list_codorden.Text)
        parar_temporizador()
    End Sub

    Private Sub btn_cerrar_Click(sender As Object, e As EventArgs) Handles btn_cerrar.Click
        limpiar()
    End Sub

    Private Sub btn_confirmarTodo_Click(sender As Object, e As EventArgs) Handles btn_confirmarTodo.Click
        If MsgBox("Todas las lineas serán confirmadas, seguro que quiere continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            confirmar_todo()
        End If
    End Sub

    Private Sub txt_filtro_codarticulo_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_filtro_codarticulo.KeyUp
        txt_filtro_codarticulo.Text = txt_filtro_codarticulo.Text.Trim.ToUpper
        If e.KeyCode = Keys.Enter Then
            llenar_ordenes()
        End If
    End Sub

    Private Sub btn_OtraUbi_Click(sender As Object, e As EventArgs) Handles btn_OtraUbi.Click
        frm_nuevaubicacion.lab_ref.Text = dg_lineas.SelectedRows(0).Cells("Ref.").Value
        frm_nuevaubicacion.ShowDialog()
        llenar_ubicaciones_articulo(dg_lineas.SelectedRows(0).Cells("Ref.").Value, dg_lineas.SelectedRows(0).Index)
    End Sub

    Private Sub list_ubicacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles list_ubicacion.SelectedIndexChanged
        Dim sql As String
        Dim vdatos As New DataTable
        If list_ubicacion.SelectedValue <> "" And txt_articulo.Text <> "" And lab_almdestino.Text <> "" Then
            sql = "select cantidad from ubicaciones where codarticulo='" & txt_articulo.Text & "' and codalmacen='" & lab_almdestino.Text & "' and ubicacion='" & list_ubicacion.SelectedValue & "'"
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 1 Then lab_CantUbiDest.Text = FormatNumber(vdatos.Rows(0)(0), 0) Else lab_CantUbiDest.Text = 0
        End If
    End Sub

    Private Sub list_codorden_DropDown(sender As System.Object, e As System.EventArgs) Handles list_codorden.DropDown
        parar_temporizador()
    End Sub
End Class