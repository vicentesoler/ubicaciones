﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.p_principal = New System.Windows.Forms.Panel()
        Me.btn_mostrar_ubicacion = New System.Windows.Forms.Button()
        Me.btn_articulo = New System.Windows.Forms.Button()
        Me.btn_reaprov = New System.Windows.Forms.Button()
        Me.lab_nomoperario = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_ordenalta = New System.Windows.Forms.Button()
        Me.lab_idoperario = New System.Windows.Forms.Label()
        Me.btn_inventarios = New System.Windows.Forms.Button()
        Me.btn_desg_mont = New System.Windows.Forms.Button()
        Me.btn_contenedor = New System.Windows.Forms.Button()
        Me.btn_ordensalida = New System.Windows.Forms.Button()
        Me.Timer_principal = New System.Windows.Forms.Timer(Me.components)
        Me.btn_OrdProd = New System.Windows.Forms.Button()
        Me.btn_RecuentoStocks = New System.Windows.Forms.Button()
        Me.btn_RegistrarAccion = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lab_maquina = New System.Windows.Forms.Label()
        Me.btn_RecogerOFs = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'p_principal
        '
        Me.p_principal.Location = New System.Drawing.Point(5, 81)
        Me.p_principal.Name = "p_principal"
        Me.p_principal.Size = New System.Drawing.Size(1255, 645)
        Me.p_principal.TabIndex = 0
        '
        'btn_mostrar_ubicacion
        '
        Me.btn_mostrar_ubicacion.BackColor = System.Drawing.Color.Teal
        Me.btn_mostrar_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_mostrar_ubicacion.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_mostrar_ubicacion.Location = New System.Drawing.Point(2, 0)
        Me.btn_mostrar_ubicacion.Name = "btn_mostrar_ubicacion"
        Me.btn_mostrar_ubicacion.Size = New System.Drawing.Size(98, 39)
        Me.btn_mostrar_ubicacion.TabIndex = 22
        Me.btn_mostrar_ubicacion.Text = "Ubic."
        Me.btn_mostrar_ubicacion.UseVisualStyleBackColor = False
        '
        'btn_articulo
        '
        Me.btn_articulo.BackColor = System.Drawing.Color.Teal
        Me.btn_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_articulo.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_articulo.Location = New System.Drawing.Point(100, 0)
        Me.btn_articulo.Name = "btn_articulo"
        Me.btn_articulo.Size = New System.Drawing.Size(127, 39)
        Me.btn_articulo.TabIndex = 23
        Me.btn_articulo.Text = "Artículo"
        Me.btn_articulo.UseVisualStyleBackColor = False
        '
        'btn_reaprov
        '
        Me.btn_reaprov.BackColor = System.Drawing.Color.Teal
        Me.btn_reaprov.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_reaprov.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_reaprov.Location = New System.Drawing.Point(227, 0)
        Me.btn_reaprov.Name = "btn_reaprov"
        Me.btn_reaprov.Size = New System.Drawing.Size(145, 39)
        Me.btn_reaprov.TabIndex = 25
        Me.btn_reaprov.Text = "Reaprov."
        Me.btn_reaprov.UseVisualStyleBackColor = False
        '
        'lab_nomoperario
        '
        Me.lab_nomoperario.AutoSize = True
        Me.lab_nomoperario.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_nomoperario.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_nomoperario.Location = New System.Drawing.Point(996, 49)
        Me.lab_nomoperario.Name = "lab_nomoperario"
        Me.lab_nomoperario.Size = New System.Drawing.Size(72, 22)
        Me.lab_nomoperario.TabIndex = 26
        Me.lab_nomoperario.Text = "Usuario"
        Me.lab_nomoperario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(870, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 22)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Operario:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_ordenalta
        '
        Me.btn_ordenalta.BackColor = System.Drawing.Color.Teal
        Me.btn_ordenalta.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ordenalta.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_ordenalta.Location = New System.Drawing.Point(372, 0)
        Me.btn_ordenalta.Name = "btn_ordenalta"
        Me.btn_ordenalta.Size = New System.Drawing.Size(138, 39)
        Me.btn_ordenalta.TabIndex = 28
        Me.btn_ordenalta.Text = "Ord.Alta"
        Me.btn_ordenalta.UseVisualStyleBackColor = False
        '
        'lab_idoperario
        '
        Me.lab_idoperario.AutoSize = True
        Me.lab_idoperario.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_idoperario.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_idoperario.Location = New System.Drawing.Point(958, 49)
        Me.lab_idoperario.Name = "lab_idoperario"
        Me.lab_idoperario.Size = New System.Drawing.Size(24, 22)
        Me.lab_idoperario.TabIndex = 29
        Me.lab_idoperario.Text = "id"
        Me.lab_idoperario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_inventarios
        '
        Me.btn_inventarios.BackColor = System.Drawing.Color.Teal
        Me.btn_inventarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventarios.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_inventarios.Location = New System.Drawing.Point(1146, 0)
        Me.btn_inventarios.Name = "btn_inventarios"
        Me.btn_inventarios.Size = New System.Drawing.Size(114, 39)
        Me.btn_inventarios.TabIndex = 30
        Me.btn_inventarios.Text = "Invent."
        Me.btn_inventarios.UseVisualStyleBackColor = False
        '
        'btn_desg_mont
        '
        Me.btn_desg_mont.BackColor = System.Drawing.Color.Teal
        Me.btn_desg_mont.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_desg_mont.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_desg_mont.Location = New System.Drawing.Point(642, 0)
        Me.btn_desg_mont.Name = "btn_desg_mont"
        Me.btn_desg_mont.Size = New System.Drawing.Size(175, 39)
        Me.btn_desg_mont.TabIndex = 31
        Me.btn_desg_mont.Text = "Desg./Mont"
        Me.btn_desg_mont.UseVisualStyleBackColor = False
        '
        'btn_contenedor
        '
        Me.btn_contenedor.BackColor = System.Drawing.Color.Teal
        Me.btn_contenedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_contenedor.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_contenedor.Location = New System.Drawing.Point(965, 0)
        Me.btn_contenedor.Name = "btn_contenedor"
        Me.btn_contenedor.Size = New System.Drawing.Size(179, 39)
        Me.btn_contenedor.TabIndex = 32
        Me.btn_contenedor.Text = "Contenedor"
        Me.btn_contenedor.UseVisualStyleBackColor = False
        '
        'btn_ordensalida
        '
        Me.btn_ordensalida.BackColor = System.Drawing.Color.Teal
        Me.btn_ordensalida.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ordensalida.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_ordensalida.Location = New System.Drawing.Point(510, 0)
        Me.btn_ordensalida.Name = "btn_ordensalida"
        Me.btn_ordensalida.Size = New System.Drawing.Size(131, 39)
        Me.btn_ordensalida.TabIndex = 33
        Me.btn_ordensalida.Text = "Ord.Sal."
        Me.btn_ordensalida.UseVisualStyleBackColor = False
        '
        'Timer_principal
        '
        Me.Timer_principal.Enabled = True
        Me.Timer_principal.Interval = 300000
        '
        'btn_OrdProd
        '
        Me.btn_OrdProd.BackColor = System.Drawing.Color.Teal
        Me.btn_OrdProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OrdProd.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_OrdProd.Location = New System.Drawing.Point(819, 0)
        Me.btn_OrdProd.Name = "btn_OrdProd"
        Me.btn_OrdProd.Size = New System.Drawing.Size(143, 39)
        Me.btn_OrdProd.TabIndex = 34
        Me.btn_OrdProd.Text = "Ord.Prod."
        Me.btn_OrdProd.UseVisualStyleBackColor = False
        '
        'btn_RecuentoStocks
        '
        Me.btn_RecuentoStocks.BackColor = System.Drawing.Color.Teal
        Me.btn_RecuentoStocks.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_RecuentoStocks.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_RecuentoStocks.Location = New System.Drawing.Point(2, 41)
        Me.btn_RecuentoStocks.Name = "btn_RecuentoStocks"
        Me.btn_RecuentoStocks.Size = New System.Drawing.Size(225, 39)
        Me.btn_RecuentoStocks.TabIndex = 35
        Me.btn_RecuentoStocks.Text = "Recuento Stocks"
        Me.btn_RecuentoStocks.UseVisualStyleBackColor = False
        '
        'btn_RegistrarAccion
        '
        Me.btn_RegistrarAccion.BackColor = System.Drawing.Color.Orange
        Me.btn_RegistrarAccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_RegistrarAccion.Location = New System.Drawing.Point(723, 45)
        Me.btn_RegistrarAccion.Name = "btn_RegistrarAccion"
        Me.btn_RegistrarAccion.Size = New System.Drawing.Size(143, 30)
        Me.btn_RegistrarAccion.TabIndex = 36
        Me.btn_RegistrarAccion.Text = "Reg. acción"
        Me.btn_RegistrarAccion.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(1162, 49)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 22)
        Me.Label9.TabIndex = 109
        Me.Label9.Text = "Maq.:"
        '
        'lab_maquina
        '
        Me.lab_maquina.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_maquina.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lab_maquina.Location = New System.Drawing.Point(1221, 49)
        Me.lab_maquina.Name = "lab_maquina"
        Me.lab_maquina.Size = New System.Drawing.Size(36, 30)
        Me.lab_maquina.TabIndex = 108
        Me.lab_maquina.Text = "0"
        Me.lab_maquina.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btn_RecogerOFs
        '
        Me.btn_RecogerOFs.BackColor = System.Drawing.Color.Teal
        Me.btn_RecogerOFs.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_RecogerOFs.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_RecogerOFs.Location = New System.Drawing.Point(227, 40)
        Me.btn_RecogerOFs.Name = "btn_RecogerOFs"
        Me.btn_RecogerOFs.Size = New System.Drawing.Size(143, 39)
        Me.btn_RecogerOFs.TabIndex = 110
        Me.btn_RecogerOFs.Text = "Recog.OFs"
        Me.btn_RecogerOFs.UseVisualStyleBackColor = False
        '
        'frm_principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1264, 727)
        Me.Controls.Add(Me.btn_RecogerOFs)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lab_maquina)
        Me.Controls.Add(Me.btn_RegistrarAccion)
        Me.Controls.Add(Me.btn_RecuentoStocks)
        Me.Controls.Add(Me.btn_OrdProd)
        Me.Controls.Add(Me.lab_nomoperario)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_ordensalida)
        Me.Controls.Add(Me.lab_idoperario)
        Me.Controls.Add(Me.btn_contenedor)
        Me.Controls.Add(Me.btn_desg_mont)
        Me.Controls.Add(Me.btn_inventarios)
        Me.Controls.Add(Me.btn_ordenalta)
        Me.Controls.Add(Me.btn_reaprov)
        Me.Controls.Add(Me.btn_articulo)
        Me.Controls.Add(Me.btn_mostrar_ubicacion)
        Me.Controls.Add(Me.p_principal)
        Me.Name = "frm_principal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "  "
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents p_principal As System.Windows.Forms.Panel
    Friend WithEvents btn_mostrar_ubicacion As System.Windows.Forms.Button
    Friend WithEvents btn_articulo As System.Windows.Forms.Button
    Friend WithEvents btn_reaprov As System.Windows.Forms.Button
    Friend WithEvents lab_nomoperario As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_ordenalta As System.Windows.Forms.Button
    Friend WithEvents lab_idoperario As System.Windows.Forms.Label
    Friend WithEvents btn_inventarios As System.Windows.Forms.Button
    Friend WithEvents btn_desg_mont As System.Windows.Forms.Button
    Friend WithEvents btn_contenedor As System.Windows.Forms.Button
    Friend WithEvents btn_ordensalida As System.Windows.Forms.Button
    Friend WithEvents Timer_principal As System.Windows.Forms.Timer
    Friend WithEvents btn_OrdProd As System.Windows.Forms.Button
    Friend WithEvents btn_RecuentoStocks As Button
    Friend WithEvents btn_RegistrarAccion As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents lab_maquina As Label
    Friend WithEvents btn_RecogerOFs As Button
End Class
