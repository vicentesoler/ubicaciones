﻿Public Class frm_ordensalida
    Dim conexion As New Conector

    Private Sub frm_ordensalida_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        limpiar()
        llenar_ordenes()
    End Sub


    Public Sub llenar_ordenes()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer

        list_codorden.DataSource = Nothing
        list_codorden.Items.Clear()
        sql = "select codorden from orden_salida where procesado ='NO' and afectastock=1 order by codorden asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        cont = 0
        list_codorden.Text = ""
        list_codorden.Items.Clear()
        Do While cont < vdatos.Rows.Count
            list_codorden.Items.Add(vdatos.Rows(cont).Item("codorden"))
            cont += 1
        Loop

    End Sub

    Private Sub list_codorden_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_codorden.SelectedIndexChanged
        If list_codorden.SelectedIndex > -1 Then cargar_lineas()
    End Sub
    Public Sub cargar_lineas()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0


        sql = "select codlinea as 'Lin.',lin.codarticulo as 'Ref.',art.descripcion as 'Descripcion',"
        sql &= "isnull(lin.codalmacen,'') as 'Alm.',isnull(ubi.ubicacion,'99-99-99') as 'Ubi.',"
        sql &= "lin.cantidad as 'Cant.',cantidad_recogida as 'Recog.' "
        sql &= "from orden_salida_lin lin "
        sql &= "inner join orden_salida cab on cab.codorden =lin.codorden "
        sql &= "inner join inase.dbo.GesArticulos art on art.codempresa=1 and art.codarticulo collate Modern_Spanish_CI_AS = lin.codarticulo "
        sql &= "left join ubicaciones ubi on lin.codarticulo collate Modern_Spanish_CI_AS=ubi.codarticulo and ubi.tipo='Picking' and ubi.ubicacion not in ('" & constantes.consolidacion_A01 & "','" & constantes.consolidacion_A07 & "','" & constantes.ubicacion_devoluciones_A01 & "','" & constantes.ubicacion_devoluciones_A07 & "','" & constantes.traslados_A01 & "','" & constantes.traslados_A07 & "') "
        sql &= "and ubi.id = (select min(id) from ubicaciones ubi2 where ubi2.codarticulo=ubi.codarticulo and ubi2.tipo='Picking') "
        sql &= "where lin.codorden=" & list_codorden.Text & " "
        sql &= "order by convert(integer,left(isnull(ubi.ubicacion,'99-99-99'),2)) asc, "
        sql &= "case when convert(integer,left(isnull(ubi.ubicacion,'99-99-99'),2)) in (1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99) then convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),4,2)) "
        sql &= "else 99-convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),4,2)) end asc, "
        sql &= "case when convert(integer,left(isnull(ubi.ubicacion,'99-99-99'),2)) in (1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79,81,83,85,87,89,91,93,95,97,99) then convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),7,2)) "
        sql &= "else 99-convert(integer,substring(isnull(ubi.ubicacion,'99-99-99'),7,2)) end asc,lin.codarticulo asc "

        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_lineas.DataSource = vdatos
            dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 25)
            dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 25, FontStyle.Bold)
            'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_lineas.Columns(0).Width = 75
            dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(1).Width = 170
            dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dg_lineas.Columns(2).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15)
            dg_lineas.Columns(2).Width = 420
            dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(3).Width = 100
            dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(4).Width = 160
            dg_lineas.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(5).Width = 110
            dg_lineas.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(6).Width = 130

            pintar_lineas_grid()
            llenar_datos_linea(0)
        Else
            MsgBox("La orden no tiene lineas", MsgBoxStyle.Critical)
            limpiar()
            list_codorden.SelectAll()
        End If

       
    End Sub
    Public Sub pintar_lineas_grid()
        Dim cont As Integer
        cont = 0
        While cont < dg_lineas.RowCount
            Select Case dg_lineas.Rows(cont).Cells("Recog.").Value
                Case 0
                Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
                Case Is > dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Red
                Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
            End Select

            'Situar el cursor en la linea q acabamos de marcar
            'If dg_lineasalb.Rows(cont).Cells("Referencia").Value = ult_ref_embalada And (linea_click = 0 Or dg_lineasalb.Rows(cont).Cells("Lin").Value = linea_click) Then
            '    dg_lineasalb.Item(0, dg_lineasalb.RowCount - 1).Selected = True
            '    dg_lineasalb.Item(0, cont).Selected = True
            'End If

            cont += 1
        End While
        'If ult_ref_embalada = "" Then dg_lineasalb.Item(0, dg_lineasalb.CurrentRow.Index).Selected = False

    End Sub
   
    Public Sub llenar_alm_articulo(ByVal codarticulo As String, ByVal fila As Integer)
        Dim sql As String
        Dim vdatos As New DataTable

        list_almacen.DataSource = Nothing
        list_almacen.Items.Clear()
        sql = "select codalmacen from ubicaciones where codarticulo='" & codarticulo & "' and tipo='Picking' "
        'sql &= "and codalmacen <> '" & dg_lineas.Rows(fila).Cells("Alm.").Value & "' "
        sql &= "order by codalmacen "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        'If dg_lineas.Rows(fila).Cells("Alm.").Value <> "" Then vdatos.Rows.Add({dg_lineas.Rows(fila).Cells("Alm.").Value})
        list_almacen.DisplayMember = "codalmacen"
        list_almacen.ValueMember = "codalmacen"
        list_almacen.DataSource = vdatos
        If fila >= 0 Then list_almacen.SelectedValue = dg_lineas.Rows(fila).Cells("Alm.").Value
        llenar_ubicaciones_articulo(codarticulo, fila)
    End Sub

    Public Sub llenar_ubicaciones_articulo(ByVal codarticulo As String, ByVal fila As Integer)
        Dim sql As String
        Dim vdatos As New DataTable

        list_ubicacion.DataSource = Nothing
        list_ubicacion.Items.Clear()

        'Añadir como primera opcion la ubicacion que pone en el grid, q es la que viene de la consulta
        sql = "select ubicacion from ubicaciones where codarticulo ='" & codarticulo & "' and codalmacen='" & list_almacen.SelectedValue & "' "
        sql &= "and tipo='Picking' "
        sql &= "and ubicacion not in ('" & constantes.ubicacion_devoluciones_A01 & "','" & constantes.ubicacion_devoluciones_A07 & "','" & constantes.consolidacion_A01 & "','" & constantes.consolidacion_A07 & "','" & constantes.traslados_A01 & "','" & constantes.traslados_A07 & "') "
        'sql &= "and ubicacion <> '" & dg_lineas.Rows(fila).Cells("Ubi.").Value & "' " 'Para que no muestre esta ubicacion repetida
        sql &= "order by tipo desc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        'If vdatos.Rows.Count = 0 Then
        '    vdatos.Rows.Add({""})
        'End If
        'If dg_lineas.Rows(fila).Cells("Ubi.").Value <> "SIN UBI." Then vdatos.Rows.Add({dg_lineas.Rows(fila).Cells("Ubi.").Value})

        list_ubicacion.DisplayMember = "ubicacion"
        list_ubicacion.ValueMember = "ubicacion"
        list_ubicacion.DataSource = vdatos

        'If fila >= 0 Then list_ubicacion.SelectedValue = dg_lineas.Rows(fila).Cells("Ubi.").Value

    End Sub

    Private Sub txt_cantidad_Click(sender As System.Object, e As System.EventArgs) Handles txt_cantidad.Click
        txt_cantidad.SelectAll()
    End Sub

    Private Sub btn_ok_Click(sender As System.Object, e As System.EventArgs) Handles btn_ok.Click
        If dg_lineas.SelectedRows.Count = 0 Then Exit Sub
        confirmar_linea(dg_lineas.SelectedRows(0).Index, dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString, dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, txt_cantidad.Text, list_almacen.SelectedValue, list_ubicacion.SelectedValue)
    End Sub
    Public Sub confirmar_linea(ByVal linsel As Integer, ByVal linea As Integer, ByVal codarticulo As String, ByVal cantidad As Integer, ByVal codalmacen As String, ByVal ubicacion As String)
        Dim sql As String = ""
        Dim sql2 As String = ""
        Dim resultado As String
        Dim vcant, vimporte, vubi As New DataTable
        Dim cantant As Integer = 0



        If ubicacion = "" Then
            MsgBox("La referencia no tiene ubicación de picking, no se puede procesar esta referencia. Dar de alta una ubicación para la referencia.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Los desplegables de almacen y ubicación los hde dejado inactivos. Si permito que se cambie el almacen, tengo que hacer un movimiento de ajuste
        'en el mov que hace ivan al crear la orden, ya que el almacén no es el mismo. Tambien puede pasar que se confirme una linea y luego se anule en otro
        'almacen o ubicacione con lo que hay que tener en cuenta esto tambien.

        If cantidad = 0 Then
            If MsgBox("Seguro que quiere eliminar la cantidad recogida?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

            'Obtener la cantidad que tiene hasta el momento la linea preparada para sumarla al stock y dejar el stock en el picking cuadrado
            sql = "select cantidad_recogida from orden_salida_lin where codorden=" & list_codorden.Text & " and codlinea=" & linea & " "
            vcant = conexion.TablaxCmd(constantes.bd_intranet, sql)
            sql = ""
            If vcant.Rows.Count = 1 Then
                cantant = CInt(vcant.Rows(0)(0))
                'Dejar stock como estaba en la ubicacion. 
                sql &= "update ubicaciones set cantidad+=" & cantant & " where codalmacen='" & codalmacen & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & codalmacen & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' order by cantidad desc ) "

            End If
            'Actualizar la linea de orden alta
            sql &= "update orden_salida_lin set cantidad_recogida=0,ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text & " and codlinea=" & linea & " "
            'Poner la cantidad en el grid
            dg_lineas.Rows(linsel).Cells("Recog.").Value = 0
        Else
            sql = ""

            'Actualizar la ubicacion destino
            sql &= "update ubicaciones set cantidad=isnull(cantidad,0)-(" & CInt(cantidad) & ") where codalmacen='" & codalmacen & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' "
            'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
            sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & codalmacen & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' order by cantidad desc ) "

            'Actualizar la linea de orden alta
            sql &= "update orden_salida_lin set cantidad_recogida=" & CInt(cantidad) + CInt(dg_lineas.Rows(linsel).Cells("Recog.").Value.ToString) & ",ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultmodificacion=getdate() "
            sql &= "where codorden=" & list_codorden.Text & " and codlinea=" & linea & " "
            'Poner la cantidad en el grid
            dg_lineas.Rows(linsel).Cells("Recog.").Value = CInt(cantidad) + CInt(dg_lineas.Rows(linsel).Cells("Recog.").Value.ToString)
        End If

        'Añadir registro
        funciones.añadir_reg_mov_ubicaciones(codarticulo, cantant - CInt(cantidad), list_almacen.SelectedValue, list_ubicacion.SelectedValue, "Ord:" & list_codorden.Text, "Ubicaciones-Orden Salida", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)

        If resultado <> "0" Then
            MsgBox("Se produjo un error al marcar la linea.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        'Pintar fila
        Select Case CInt(dg_lineas.Rows(linsel).Cells("Recog.").Value.ToString)
            Case 0 : dg_lineas.Rows(linsel).DefaultCellStyle.BackColor = Color.White
            Case Is = dg_lineas.Rows(linsel).Cells("Cant.").Value.ToString : dg_lineas.Rows(linsel).DefaultCellStyle.BackColor = Color.Green
            Case Is < dg_lineas.Rows(linsel).Cells("Cant.").Value.ToString : dg_lineas.Rows(linsel).DefaultCellStyle.BackColor = Color.Yellow
            Case Is > dg_lineas.Rows(linsel).Cells("Cant.").Value.ToString : dg_lineas.Rows(linsel).DefaultCellStyle.BackColor = Color.Red
        End Select

        If dg_lineas.CurrentRow.Index < dg_lineas.Rows.Count - 1 Then
            dg_lineas.Item(0, dg_lineas.CurrentRow.Index + 1).Selected = True
            llenar_datos_linea(dg_lineas.CurrentRow.Index)
        Else
            dg_lineas.Item(0, dg_lineas.CurrentRow.Index).Selected = False
            txt_articulo.Text = ""
            txt_articulo.BackColor = Color.White
            txt_descripcion.Text = ""
            txt_cantidad.Text = ""
            list_ubicacion.DataSource = Nothing
            list_ubicacion.Items.Clear()
            btn_ok.Enabled = False
        End If
        btn_finalizar.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub

    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql As String = ""
        Dim resultado As String
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim proxmov As Integer = 0
        Dim numrefmov As String = ""
        Dim lineasprep As Integer = 0
        Dim pedidoincompleto As Boolean = False

        'Revisar si la orden esta completamente terminada
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("Recog.").Value Then
                pedidoincompleto = True
            End If
            If dg_lineas.Rows(cont).Cells("Recog.").Value > 0 Then
                lineasprep += 1
            End If
            cont += 1
        Loop


        If pedidoincompleto = True Then
            If MsgBox("La orden no está completamente terminada, finalizar de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
        End If


        'Marcar como procesado la orden
        sql = "update orden_salida set procesado='SI',ultusuario='" & frm_principal.lab_nomoperario.Text & "',ultmodificacion=getdate() where codorden=" & list_codorden.Text
        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado = "0" Then
            MsgBox("Orden de salida finalizada correctamente", MsgBoxStyle.Information)
        Else
            MsgBox("Se produjo un error al marcar la orden como procesada. Avisar a Informática", MsgBoxStyle.Information)
        End If

        limpiar()
        llenar_ordenes()
    End Sub

    Public Sub limpiar()
        txt_articulo.Text = ""
        txt_articulo.BackColor = Color.White
        txt_descripcion.Text = ""
        txt_cantidad.Text = ""
        list_ubicacion.DataSource = Nothing
        list_ubicacion.Items.Clear()
        btn_ok.Enabled = False
        'btn_finalizar.Enabled = False
        dg_lineas.DataSource = Nothing
        dg_lineas.Rows.Clear()
        imagen.ImageLocation = ""
        list_codorden.SelectedIndex = -1
    End Sub

    Public Sub confirmar_todo()
        Dim cont As Integer
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Recog.").Value < dg_lineas.Rows(cont).Cells("Cant.").Value Then
                dg_lineas.CurrentCell = dg_lineas.Rows(cont).Cells(1)
                confirmar_linea(cont, dg_lineas.Rows(cont).Cells("Lin.").Value.ToString, dg_lineas.Rows(cont).Cells("Ref.").Value.ToString.Trim, dg_lineas.Rows(cont).Cells("Cant.").Value.ToString.Trim, dg_lineas.Rows(cont).Cells("Alm.").Value.ToString.Trim, dg_lineas.Rows(cont).Cells("Ubi.").Value.ToString.Trim)
            End If
            cont += 1
        Loop

    End Sub

    Private Sub btn_confirmar_todo_Click(sender As System.Object, e As System.EventArgs) Handles btn_confirmar_todo.Click
        If MsgBox("Todas las lineas serán confirmadas, seguro que quiere continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            confirmar_todo()
        End If
    End Sub

    Private Sub dg_lineas_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_lineas.CellClick
        llenar_datos_linea(e.RowIndex)
    End Sub

    Public Sub llenar_datos_linea(fila As Integer)
        imagen.ImageLocation = "\\jserver\BAJA\" & dg_lineas.Rows(fila).Cells("Ref.").Value & ".jpg"
        imagen.SizeMode = PictureBoxSizeMode.StretchImage
        txt_articulo.Text = dg_lineas.Rows(fila).Cells("Ref.").Value
        txt_descripcion.Text = dg_lineas.Rows(fila).Cells("Descripcion").Value
        txt_cantidad.Text = dg_lineas.Rows(fila).Cells("Cant.").Value - dg_lineas.Rows(fila).Cells("Recog.").Value
        Select Case dg_lineas.Rows(fila).Cells("Recog.").Value
            Case 0 : txt_articulo.BackColor = Color.White
            Case Is = dg_lineas.Rows(fila).Cells("Cant.").Value : txt_articulo.BackColor = Color.Green
            Case Is < dg_lineas.Rows(fila).Cells("Cant.").Value : txt_articulo.BackColor = Color.Yellow
            Case Is > dg_lineas.Rows(fila).Cells("Cant.").Value : txt_articulo.BackColor = Color.Red
        End Select
        llenar_alm_articulo(dg_lineas.Rows(fila).Cells("Ref.").Value, fila)
        btn_ok.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub


    Private Sub list_almacen_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_almacen.DropDownClosed
        llenar_ubicaciones_articulo(txt_articulo.Text, -1)
    End Sub

End Class