﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_reaprov
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.list_ubicacion_destino = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_nuevoreaprov = New System.Windows.Forms.Button()
        Me.list_ubicacion_origen = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_articulo = New System.Windows.Forms.TextBox()
        Me.lab_descripcion = New System.Windows.Forms.Label()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.txt_cantidad = New System.Windows.Forms.TextBox()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.lab_almdestino = New System.Windows.Forms.Label()
        Me.lab_flecha = New System.Windows.Forms.Label()
        Me.list_codreaprov = New System.Windows.Forms.ComboBox()
        Me.lab_almorigen = New System.Windows.Forms.Label()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.list_ubicacion_destino)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btn_nuevoreaprov)
        Me.Panel1.Controls.Add(Me.list_ubicacion_origen)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txt_articulo)
        Me.Panel1.Controls.Add(Me.lab_descripcion)
        Me.Panel1.Controls.Add(Me.btn_ok)
        Me.Panel1.Controls.Add(Me.txt_cantidad)
        Me.Panel1.Controls.Add(Me.imagen)
        Me.Panel1.Controls.Add(Me.lab_almdestino)
        Me.Panel1.Controls.Add(Me.lab_flecha)
        Me.Panel1.Controls.Add(Me.list_codreaprov)
        Me.Panel1.Controls.Add(Me.lab_almorigen)
        Me.Panel1.Controls.Add(Me.btn_finalizar)
        Me.Panel1.Controls.Add(Me.dg_lineas)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1259, 648)
        Me.Panel1.TabIndex = 24
        '
        'list_ubicacion_destino
        '
        Me.list_ubicacion_destino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_ubicacion_destino.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_ubicacion_destino.FormattingEnabled = True
        Me.list_ubicacion_destino.Location = New System.Drawing.Point(548, 121)
        Me.list_ubicacion_destino.Name = "list_ubicacion_destino"
        Me.list_ubicacion_destino.Size = New System.Drawing.Size(225, 62)
        Me.list_ubicacion_destino.TabIndex = 53
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(543, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(129, 25)
        Me.Label3.TabIndex = 52
        Me.Label3.Text = "Ubi. Destino"
        '
        'btn_nuevoreaprov
        '
        Me.btn_nuevoreaprov.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nuevoreaprov.Location = New System.Drawing.Point(411, 14)
        Me.btn_nuevoreaprov.Name = "btn_nuevoreaprov"
        Me.btn_nuevoreaprov.Size = New System.Drawing.Size(139, 59)
        Me.btn_nuevoreaprov.TabIndex = 51
        Me.btn_nuevoreaprov.Text = "Nuevo"
        Me.btn_nuevoreaprov.UseVisualStyleBackColor = True
        '
        'list_ubicacion_origen
        '
        Me.list_ubicacion_origen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_ubicacion_origen.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_ubicacion_origen.FormattingEnabled = True
        Me.list_ubicacion_origen.Location = New System.Drawing.Point(287, 120)
        Me.list_ubicacion_origen.Name = "list_ubicacion_origen"
        Me.list_ubicacion_origen.Size = New System.Drawing.Size(225, 62)
        Me.list_ubicacion_origen.TabIndex = 50
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(282, 93)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(121, 25)
        Me.Label7.TabIndex = 49
        Me.Label7.Text = "Ubi. Origen"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(827, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 25)
        Me.Label1.TabIndex = 48
        Me.Label1.Text = "Cant.:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(20, 93)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(122, 25)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "Referencia:"
        '
        'txt_articulo
        '
        Me.txt_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_articulo.Location = New System.Drawing.Point(20, 121)
        Me.txt_articulo.MaxLength = 13
        Me.txt_articulo.Name = "txt_articulo"
        Me.txt_articulo.ReadOnly = True
        Me.txt_articulo.Size = New System.Drawing.Size(233, 60)
        Me.txt_articulo.TabIndex = 44
        '
        'lab_descripcion
        '
        Me.lab_descripcion.AutoSize = True
        Me.lab_descripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_descripcion.Location = New System.Drawing.Point(19, 184)
        Me.lab_descripcion.Name = "lab_descripcion"
        Me.lab_descripcion.Size = New System.Drawing.Size(38, 17)
        Me.lab_descripcion.TabIndex = 47
        Me.lab_descripcion.Text = "XXX"
        '
        'btn_ok
        '
        Me.btn_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ok.Location = New System.Drawing.Point(955, 121)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(92, 60)
        Me.btn_ok.TabIndex = 43
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'txt_cantidad
        '
        Me.txt_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad.Location = New System.Drawing.Point(832, 121)
        Me.txt_cantidad.Name = "txt_cantidad"
        Me.txt_cantidad.Size = New System.Drawing.Size(101, 60)
        Me.txt_cantidad.TabIndex = 41
        Me.txt_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(1068, 8)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(175, 175)
        Me.imagen.TabIndex = 40
        Me.imagen.TabStop = False
        '
        'lab_almdestino
        '
        Me.lab_almdestino.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_almdestino.ForeColor = System.Drawing.Color.OrangeRed
        Me.lab_almdestino.Location = New System.Drawing.Point(827, 21)
        Me.lab_almdestino.Name = "lab_almdestino"
        Me.lab_almdestino.Size = New System.Drawing.Size(106, 46)
        Me.lab_almdestino.TabIndex = 39
        Me.lab_almdestino.Text = "Código"
        Me.lab_almdestino.Visible = False
        '
        'lab_flecha
        '
        Me.lab_flecha.AutoSize = True
        Me.lab_flecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_flecha.Location = New System.Drawing.Point(751, 9)
        Me.lab_flecha.Name = "lab_flecha"
        Me.lab_flecha.Size = New System.Drawing.Size(77, 63)
        Me.lab_flecha.TabIndex = 38
        Me.lab_flecha.Text = "->"
        Me.lab_flecha.Visible = False
        '
        'list_codreaprov
        '
        Me.list_codreaprov.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_codreaprov.FormattingEnabled = True
        Me.list_codreaprov.IntegralHeight = False
        Me.list_codreaprov.Location = New System.Drawing.Point(252, 14)
        Me.list_codreaprov.Name = "list_codreaprov"
        Me.list_codreaprov.Size = New System.Drawing.Size(136, 59)
        Me.list_codreaprov.TabIndex = 37
        '
        'lab_almorigen
        '
        Me.lab_almorigen.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_almorigen.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lab_almorigen.Location = New System.Drawing.Point(654, 21)
        Me.lab_almorigen.Name = "lab_almorigen"
        Me.lab_almorigen.Size = New System.Drawing.Size(98, 46)
        Me.lab_almorigen.TabIndex = 36
        Me.lab_almorigen.Text = "Código"
        Me.lab_almorigen.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lab_almorigen.Visible = False
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(1009, 587)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(234, 48)
        Me.btn_finalizar.TabIndex = 33
        Me.btn_finalizar.Text = "Finalizar"
        Me.btn_finalizar.UseVisualStyleBackColor = True
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_lineas.Location = New System.Drawing.Point(20, 207)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(1223, 374)
        Me.dg_lineas.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(231, 46)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "N.Reaprov:"
        '
        'frm_reaprov
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1275, 664)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_reaprov"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_reaprov"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents list_codreaprov As System.Windows.Forms.ComboBox
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents dg_lineas As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lab_almorigen As System.Windows.Forms.Label
    Friend WithEvents lab_almdestino As System.Windows.Forms.Label
    Friend WithEvents lab_flecha As System.Windows.Forms.Label
    Friend WithEvents imagen As System.Windows.Forms.PictureBox
    Friend WithEvents txt_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents list_ubicacion_origen As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_articulo As System.Windows.Forms.TextBox
    Friend WithEvents lab_descripcion As System.Windows.Forms.Label
    Friend WithEvents btn_nuevoreaprov As System.Windows.Forms.Button
    Friend WithEvents list_ubicacion_destino As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
