﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_articulo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.labPendServ = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.labPdtePreparar = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lab_of = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_unipalet = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lab_prev = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lab_pendreaprov = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lab_pendmontdesg = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.list_almacen_trasladar = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_tamaño = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_cantidad_trasladar = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_cant_max = New System.Windows.Forms.TextBox()
        Me.lab_pend_rec = New System.Windows.Forms.Label()
        Me.cb_pend_rec = New System.Windows.Forms.CheckBox()
        Me.btn_eliminar_fila = New System.Windows.Forms.Button()
        Me.dg_ubicaciones_traslado = New System.Windows.Forms.DataGridView()
        Me.list_tipo = New System.Windows.Forms.ComboBox()
        Me.txt_portal = New System.Windows.Forms.TextBox()
        Me.txt_altura = New System.Windows.Forms.TextBox()
        Me.txt_pasillo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_trasladar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btn_limpiar = New System.Windows.Forms.Button()
        Me.txt_descripcion_articulo = New System.Windows.Forms.TextBox()
        Me.dg_ubicaciones = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btn_mostrar_ubicaciones_articulo = New System.Windows.Forms.Button()
        Me.txt_articulo = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        CType(Me.dg_ubicaciones_traslado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_ubicaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Bisque
        Me.Panel2.Controls.Add(Me.labPendServ)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.labPdtePreparar)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.lab_of)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.txt_unipalet)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.lab_prev)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.lab_pendreaprov)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.lab_pendmontdesg)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.list_almacen_trasladar)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.txt_tamaño)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.txt_cantidad_trasladar)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txt_cant_max)
        Me.Panel2.Controls.Add(Me.lab_pend_rec)
        Me.Panel2.Controls.Add(Me.cb_pend_rec)
        Me.Panel2.Controls.Add(Me.btn_eliminar_fila)
        Me.Panel2.Controls.Add(Me.dg_ubicaciones_traslado)
        Me.Panel2.Controls.Add(Me.list_tipo)
        Me.Panel2.Controls.Add(Me.txt_portal)
        Me.Panel2.Controls.Add(Me.txt_altura)
        Me.Panel2.Controls.Add(Me.txt_pasillo)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.btn_trasladar)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.btn_limpiar)
        Me.Panel2.Controls.Add(Me.txt_descripcion_articulo)
        Me.Panel2.Controls.Add(Me.dg_ubicaciones)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.btn_mostrar_ubicaciones_articulo)
        Me.Panel2.Controls.Add(Me.txt_articulo)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1250, 640)
        Me.Panel2.TabIndex = 25
        '
        'labPendServ
        '
        Me.labPendServ.AutoSize = True
        Me.labPendServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labPendServ.Location = New System.Drawing.Point(104, 91)
        Me.labPendServ.Name = "labPendServ"
        Me.labPendServ.Size = New System.Drawing.Size(27, 29)
        Me.labPendServ.TabIndex = 63
        Me.labPendServ.Text = "0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(10, 91)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(92, 29)
        Me.Label20.TabIndex = 62
        Me.Label20.Text = "P.Ser.:"
        '
        'labPdtePreparar
        '
        Me.labPdtePreparar.AutoSize = True
        Me.labPdtePreparar.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labPdtePreparar.Location = New System.Drawing.Point(514, 91)
        Me.labPdtePreparar.Name = "labPdtePreparar"
        Me.labPdtePreparar.Size = New System.Drawing.Size(27, 29)
        Me.labPdtePreparar.TabIndex = 61
        Me.labPdtePreparar.Text = "0"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(393, 91)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(107, 29)
        Me.Label19.TabIndex = 60
        Me.Label19.Text = "P.Prep.:"
        '
        'lab_of
        '
        Me.lab_of.AutoSize = True
        Me.lab_of.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_of.Location = New System.Drawing.Point(104, 120)
        Me.lab_of.Name = "lab_of"
        Me.lab_of.Size = New System.Drawing.Size(27, 29)
        Me.lab_of.TabIndex = 59
        Me.lab_of.Text = "0"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(10, 120)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(99, 29)
        Me.Label18.TabIndex = 58
        Me.Label18.Text = "O.Fab.:"
        '
        'txt_unipalet
        '
        Me.txt_unipalet.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_unipalet.Location = New System.Drawing.Point(273, 117)
        Me.txt_unipalet.MaxLength = 7
        Me.txt_unipalet.Name = "txt_unipalet"
        Me.txt_unipalet.Size = New System.Drawing.Size(70, 35)
        Me.txt_unipalet.TabIndex = 57
        Me.txt_unipalet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(161, 120)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(106, 29)
        Me.Label16.TabIndex = 56
        Me.Label16.Text = "U/Palet:"
        '
        'lab_prev
        '
        Me.lab_prev.AutoSize = True
        Me.lab_prev.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_prev.Location = New System.Drawing.Point(514, 120)
        Me.lab_prev.Name = "lab_prev"
        Me.lab_prev.Size = New System.Drawing.Size(27, 29)
        Me.lab_prev.TabIndex = 55
        Me.lab_prev.Text = "0"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(393, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(108, 29)
        Me.Label15.TabIndex = 54
        Me.Label15.Text = "M.Prev.:"
        '
        'lab_pendreaprov
        '
        Me.lab_pendreaprov.AutoSize = True
        Me.lab_pendreaprov.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pendreaprov.Location = New System.Drawing.Point(514, 151)
        Me.lab_pendreaprov.Name = "lab_pendreaprov"
        Me.lab_pendreaprov.Size = New System.Drawing.Size(27, 29)
        Me.lab_pendreaprov.TabIndex = 53
        Me.lab_pendreaprov.Text = "0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(393, 151)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(125, 29)
        Me.Label14.TabIndex = 52
        Me.Label14.Text = "Reaprov.:"
        '
        'lab_pendmontdesg
        '
        Me.lab_pendmontdesg.AutoSize = True
        Me.lab_pendmontdesg.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pendmontdesg.Location = New System.Drawing.Point(315, 151)
        Me.lab_pendmontdesg.Name = "lab_pendmontdesg"
        Me.lab_pendmontdesg.Size = New System.Drawing.Size(27, 29)
        Me.lab_pendmontdesg.TabIndex = 51
        Me.lab_pendmontdesg.Text = "0"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(160, 151)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(160, 29)
        Me.Label13.TabIndex = 50
        Me.Label13.Text = "Mont./Desg.:"
        '
        'list_almacen_trasladar
        '
        Me.list_almacen_trasladar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_almacen_trasladar.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almacen_trasladar.FormattingEnabled = True
        Me.list_almacen_trasladar.Location = New System.Drawing.Point(1141, 129)
        Me.list_almacen_trasladar.Name = "list_almacen_trasladar"
        Me.list_almacen_trasladar.Size = New System.Drawing.Size(97, 45)
        Me.list_almacen_trasladar.TabIndex = 49
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(1013, 328)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(67, 20)
        Me.Label11.TabIndex = 48
        Me.Label11.Text = "Tamaño"
        '
        'txt_tamaño
        '
        Me.txt_tamaño.Font = New System.Drawing.Font("Microsoft Sans Serif", 23.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_tamaño.Location = New System.Drawing.Point(1006, 280)
        Me.txt_tamaño.MaxLength = 7
        Me.txt_tamaño.Name = "txt_tamaño"
        Me.txt_tamaño.Size = New System.Drawing.Size(72, 42)
        Me.txt_tamaño.TabIndex = 47
        Me.txt_tamaño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(842, 328)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(73, 20)
        Me.Label10.TabIndex = 46
        Me.Label10.Text = "Cantidad"
        '
        'txt_cantidad_trasladar
        '
        Me.txt_cantidad_trasladar.Font = New System.Drawing.Font("Microsoft Sans Serif", 23.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad_trasladar.Location = New System.Drawing.Point(842, 280)
        Me.txt_cantidad_trasladar.MaxLength = 7
        Me.txt_cantidad_trasladar.Name = "txt_cantidad_trasladar"
        Me.txt_cantidad_trasladar.Size = New System.Drawing.Size(72, 42)
        Me.txt_cantidad_trasladar.TabIndex = 45
        Me.txt_cantidad_trasladar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(1146, 234)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 20)
        Me.Label8.TabIndex = 44
        Me.Label8.Text = "Tipo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(924, 328)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 20)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Cant.Max."
        '
        'txt_cant_max
        '
        Me.txt_cant_max.Font = New System.Drawing.Font("Microsoft Sans Serif", 23.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cant_max.Location = New System.Drawing.Point(924, 280)
        Me.txt_cant_max.MaxLength = 7
        Me.txt_cant_max.Name = "txt_cant_max"
        Me.txt_cant_max.Size = New System.Drawing.Size(72, 42)
        Me.txt_cant_max.TabIndex = 41
        Me.txt_cant_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lab_pend_rec
        '
        Me.lab_pend_rec.AutoSize = True
        Me.lab_pend_rec.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_pend_rec.Location = New System.Drawing.Point(943, 45)
        Me.lab_pend_rec.Name = "lab_pend_rec"
        Me.lab_pend_rec.Size = New System.Drawing.Size(176, 39)
        Me.lab_pend_rec.TabIndex = 39
        Me.lab_pend_rec.Text = "Pend Rec."
        '
        'cb_pend_rec
        '
        Me.cb_pend_rec.AutoSize = True
        Me.cb_pend_rec.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_pend_rec.Location = New System.Drawing.Point(607, 45)
        Me.cb_pend_rec.Name = "cb_pend_rec"
        Me.cb_pend_rec.Size = New System.Drawing.Size(318, 43)
        Me.cb_pend_rec.TabIndex = 38
        Me.cb_pend_rec.Text = "Mostrar Pend.Rec."
        Me.cb_pend_rec.UseVisualStyleBackColor = True
        '
        'btn_eliminar_fila
        '
        Me.btn_eliminar_fila.BackColor = System.Drawing.Color.Tomato
        Me.btn_eliminar_fila.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_eliminar_fila.Location = New System.Drawing.Point(607, 129)
        Me.btn_eliminar_fila.Name = "btn_eliminar_fila"
        Me.btn_eliminar_fila.Size = New System.Drawing.Size(221, 51)
        Me.btn_eliminar_fila.TabIndex = 37
        Me.btn_eliminar_fila.Text = "Eliminar fila"
        Me.btn_eliminar_fila.UseVisualStyleBackColor = False
        '
        'dg_ubicaciones_traslado
        '
        Me.dg_ubicaciones_traslado.AllowUserToAddRows = False
        Me.dg_ubicaciones_traslado.AllowUserToDeleteRows = False
        Me.dg_ubicaciones_traslado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_ubicaciones_traslado.Location = New System.Drawing.Point(843, 367)
        Me.dg_ubicaciones_traslado.MultiSelect = False
        Me.dg_ubicaciones_traslado.Name = "dg_ubicaciones_traslado"
        Me.dg_ubicaciones_traslado.ReadOnly = True
        Me.dg_ubicaciones_traslado.Size = New System.Drawing.Size(395, 248)
        Me.dg_ubicaciones_traslado.TabIndex = 36
        '
        'list_tipo
        '
        Me.list_tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_tipo.FormattingEnabled = True
        Me.list_tipo.Location = New System.Drawing.Point(1092, 185)
        Me.list_tipo.Name = "list_tipo"
        Me.list_tipo.Size = New System.Drawing.Size(146, 45)
        Me.list_tipo.TabIndex = 35
        '
        'txt_portal
        '
        Me.txt_portal.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_portal.Location = New System.Drawing.Point(926, 186)
        Me.txt_portal.MaxLength = 7
        Me.txt_portal.Name = "txt_portal"
        Me.txt_portal.Size = New System.Drawing.Size(60, 45)
        Me.txt_portal.TabIndex = 28
        '
        'txt_altura
        '
        Me.txt_altura.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_altura.Location = New System.Drawing.Point(1010, 186)
        Me.txt_altura.MaxLength = 7
        Me.txt_altura.Name = "txt_altura"
        Me.txt_altura.Size = New System.Drawing.Size(60, 45)
        Me.txt_altura.TabIndex = 29
        '
        'txt_pasillo
        '
        Me.txt_pasillo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pasillo.Location = New System.Drawing.Point(840, 186)
        Me.txt_pasillo.MaxLength = 7
        Me.txt_pasillo.Name = "txt_pasillo"
        Me.txt_pasillo.Size = New System.Drawing.Size(60, 45)
        Me.txt_pasillo.TabIndex = 27
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(902, 198)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 29)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "/"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(987, 198)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(21, 29)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "/"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(843, 234)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 20)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Pasillo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(931, 234)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 20)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "Portal"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(1016, 234)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 20)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "Altura"
        '
        'btn_trasladar
        '
        Me.btn_trasladar.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_trasladar.Location = New System.Drawing.Point(1095, 280)
        Me.btn_trasladar.Name = "btn_trasladar"
        Me.btn_trasladar.Size = New System.Drawing.Size(146, 45)
        Me.btn_trasladar.TabIndex = 26
        Me.btn_trasladar.Text = "Trasladar"
        Me.btn_trasladar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(841, 135)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(282, 31)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Trasladar a ubi./alm:"
        '
        'btn_limpiar
        '
        Me.btn_limpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_limpiar.Location = New System.Drawing.Point(358, 36)
        Me.btn_limpiar.Name = "btn_limpiar"
        Me.btn_limpiar.Size = New System.Drawing.Size(160, 45)
        Me.btn_limpiar.TabIndex = 24
        Me.btn_limpiar.Text = "Limpiar"
        Me.btn_limpiar.UseVisualStyleBackColor = True
        '
        'txt_descripcion_articulo
        '
        Me.txt_descripcion_articulo.BackColor = System.Drawing.Color.Bisque
        Me.txt_descripcion_articulo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt_descripcion_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_descripcion_articulo.ForeColor = System.Drawing.Color.DarkGreen
        Me.txt_descripcion_articulo.Location = New System.Drawing.Point(130, 13)
        Me.txt_descripcion_articulo.MaxLength = 2
        Me.txt_descripcion_articulo.Name = "txt_descripcion_articulo"
        Me.txt_descripcion_articulo.Size = New System.Drawing.Size(501, 19)
        Me.txt_descripcion_articulo.TabIndex = 23
        '
        'dg_ubicaciones
        '
        Me.dg_ubicaciones.AllowUserToAddRows = False
        Me.dg_ubicaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_ubicaciones.Location = New System.Drawing.Point(7, 186)
        Me.dg_ubicaciones.MultiSelect = False
        Me.dg_ubicaciones.Name = "dg_ubicaciones"
        Me.dg_ubicaciones.RowTemplate.Height = 40
        Me.dg_ubicaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dg_ubicaciones.Size = New System.Drawing.Size(827, 429)
        Me.dg_ubicaciones.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(10, 158)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(119, 22)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Ubicaciones"
        '
        'btn_mostrar_ubicaciones_articulo
        '
        Me.btn_mostrar_ubicaciones_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_mostrar_ubicaciones_articulo.Location = New System.Drawing.Point(184, 36)
        Me.btn_mostrar_ubicaciones_articulo.Name = "btn_mostrar_ubicaciones_articulo"
        Me.btn_mostrar_ubicaciones_articulo.Size = New System.Drawing.Size(159, 45)
        Me.btn_mostrar_ubicaciones_articulo.TabIndex = 21
        Me.btn_mostrar_ubicaciones_articulo.Text = "Mostrar"
        Me.btn_mostrar_ubicaciones_articulo.UseVisualStyleBackColor = True
        '
        'txt_articulo
        '
        Me.txt_articulo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_articulo.Location = New System.Drawing.Point(15, 36)
        Me.txt_articulo.MaxLength = 13
        Me.txt_articulo.Name = "txt_articulo"
        Me.txt_articulo.Size = New System.Drawing.Size(145, 45)
        Me.txt_articulo.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(9, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(122, 25)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Referencia:"
        '
        'frm_articulo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_articulo"
        Me.Text = "frm_articulo"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dg_ubicaciones_traslado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_ubicaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btn_limpiar As System.Windows.Forms.Button
    Friend WithEvents txt_descripcion_articulo As System.Windows.Forms.TextBox
    Friend WithEvents dg_ubicaciones As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btn_mostrar_ubicaciones_articulo As System.Windows.Forms.Button
    Friend WithEvents txt_articulo As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btn_trasladar As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_portal As System.Windows.Forms.TextBox
    Friend WithEvents txt_altura As System.Windows.Forms.TextBox
    Friend WithEvents txt_pasillo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents list_tipo As System.Windows.Forms.ComboBox
    Friend WithEvents dg_ubicaciones_traslado As System.Windows.Forms.DataGridView
    Friend WithEvents btn_eliminar_fila As System.Windows.Forms.Button
    Friend WithEvents lab_pend_rec As System.Windows.Forms.Label
    Friend WithEvents cb_pend_rec As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_cant_max As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_cantidad_trasladar As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_tamaño As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents list_almacen_trasladar As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lab_pendmontdesg As System.Windows.Forms.Label
    Friend WithEvents lab_pendreaprov As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lab_prev As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_unipalet As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lab_of As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents labPdtePreparar As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents labPendServ As Label
    Friend WithEvents Label20 As Label
End Class
