﻿Public Class frm_act_stock
    Dim conexion As New Conector
    Dim noprep_a01, noprep_a07, noprep_a08 As Integer
    Dim movprevA01, movpreva07, movpreva08 As Integer
    Dim pend_dm, pend_dmA01, pend_dmA07, pend_dmA08 As Integer
    Dim pend_reaprov, pend_reaprovA01, pend_reaprovA07, pend_reaprovA08 As Integer
    Dim pend_of, pend_ofA01, pend_ofa07, pend_ofA08 As Integer
    Private Sub frm_act_stock_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        list_almacen.SelectedIndex = 0
        llenar_datos()
        If frm_principal.frm_abierto = "Recuento de Stock" Or frm_principal.frm_abierto = "Inventario" Then
            'Sergio pidió el 19-05-2020 que se hiciera en Auto también, porque si no tiene ubicacion en A07, nunca lo ajustarán
            'El pensó que si si tiene ubi en A01 y A07, se hará auto en los dos recuentos y no pasrá nada.
            'If frm_principal.frm_abierto = "Recuento de Stock" Then list_almacen.Text = Frm_RecuentoStock.list_alm.SelectedValue
            If frm_principal.frm_abierto = "Recuento de Stock" Then list_almacen.Text = "AUTO"

            If frm_principal.frm_abierto = "Inventario" Then list_almacen.Text = "AUTO" 'frm_inventarios.lab_alm.Text
            btn_aceptar_Click(sender, e)
            Close()
        End If

    End Sub
    Public Sub llenar_datos()
        Dim sql As String
        Dim vdatos, vof, vdesgmon, vreaprov As New DataTable
        Dim cont As Integer
        Dim noprep As Integer
        Dim vmovprev As New DataTable

        movprevA01 = 0
        movpreva07 = 0
        movpreva08 = 0
        noprep_a01 = 0
        noprep_a07 = 0
        noprep_a08 = 0

        imagen.ImageLocation = "\\jserver\BAJA\" & Lab_codarticulo.Text & ".jpg"

        'Stock en ubi
        sql = "select art.descripcion,isnull(sum(case when tipo='Picking' then ubi.cantidad end ),0) as picking, "
        sql &= "isnull(sum(case when tipo='Pulmon' then ubi.cantidad end ),0) as pulmon,  "
        sql &= "isnull(sum(case when tipo like 'Esp.%' then ubi.cantidad end ),0) as especial,  "
        Select Case frm_principal.frm_abierto
            Case "Ubicacion" : sql &= "(select cantidad from ubicaciones where id=" & frm_ubicaciones.dg_contenido_ubicacion.Rows(frm_ubicaciones.dg_contenido_ubicacion.SelectedCells(0).RowIndex).Cells("id").Value & " ) as cant_ant_ubi "
            Case "Articulo" : sql &= "(select cantidad from ubicaciones where id=" & frm_articulo.dg_ubicaciones.Rows(frm_articulo.dg_ubicaciones.SelectedCells(0).RowIndex).Cells("id").Value & " ) as cant_ant_ubi "
            Case "Recuento de Stock" : sql &= "(select cantidad from ubicaciones where id=" & Frm_RecuentoStock.dg_ubi.SelectedRows(0).Cells("id").Value & " ) as cant_ant_ubi "
            Case "Inventario" : sql &= "(select cantidad from ubicaciones where codalmacen='" & lab_alm_pick.Text & "' and codarticulo='" & Lab_codarticulo.Text & "' and ubicacion='" & frm_inventarios.list_ubi.SelectedValue & "') as cant_ant_ubi "
        End Select
        sql &= "from ubicaciones ubi "
        sql &= "inner join inase.dbo.gesarticulos as art on art.codarticulo collate Modern_Spanish_CI_AS=ubi.codarticulo and art.codempresa=1 "
        sql &= "where ubi.codarticulo='" & Lab_codarticulo.Text & "' and ubi.codalmacen in ('A01','A07','A08') "
        sql &= "group by art.descripcion "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count = 0 Then
            MsgBox("Esta referencia no tiene ubicación en A01,A07,A08,P1 . No se puede inventariar.")
            btn_aceptar.Enabled = False
            Exit Sub
        Else
            btn_aceptar.Enabled = True
        End If

        Select Case frm_principal.frm_abierto
            Case "Ubicacion" : lab_cantajustar_ubi.Text = CInt(frm_ubicaciones.dg_contenido_ubicacion.Rows(frm_ubicaciones.dg_contenido_ubicacion.SelectedCells(0).RowIndex).Cells("Cant.").Value) - CInt(vdatos.Rows(0).Item("cant_ant_ubi"))
            Case "Articulo" : lab_cantajustar_ubi.Text = CInt(frm_articulo.dg_ubicaciones.Rows(frm_articulo.dg_ubicaciones.SelectedCells(0).RowIndex).Cells("Cant.").Value) - CInt(vdatos.Rows(0).Item("cant_ant_ubi"))
            Case "Recuento de Stock" : lab_cantajustar_ubi.Text = CInt(Frm_RecuentoStock.dg_ubi.SelectedRows(0).Cells("Recont.").Value) - CInt(vdatos.Rows(0).Item("cant_ant_ubi"))
            Case "Inventario" : lab_cantajustar_ubi.Text = CInt(frm_inventarios.txt_cantidadpick.Text) - CInt(vdatos.Rows(0).Item("cant_ant_ubi"))
        End Select

        lab_descripcion.Text = vdatos.Rows(0).Item("descripcion")
        lab_stock_pick.Text = FormatNumber(vdatos.Rows(0).Item("picking"), 0)
        lab_stock_pul.Text = FormatNumber(vdatos.Rows(0).Item("pulmon"), 0)
        lab_stock_esp.Text = FormatNumber(vdatos.Rows(0).Item("especial"), 0)

        'Cantidad en desglose/montaje
        sql = "select cantidad from v_desgmontpend where codarticulo='" & Lab_codarticulo.Text & "'"
        vdesgmon = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdesgmon.Rows.Count = 1 Then
            pend_dm = vdesgmon.Rows(0)(0)
        Else
            pend_dm = 0
        End If
        lab_desgmont.Text = FormatNumber(pend_dm, 0)

        'Necesto saber de a que almacén asignar esta cantidad pendidente de desg/mont, ya que el ajuste lo tiene que hacer en un almacén concreto.
        'No hay manera de saber que almacén es el que se ha descontado ya esta cantidad, ya que la cantidad puede ser la suma de varias rdenes de desg/mont.
        'Busco el almacen que le corresponderia a esta referencia usando la función que ya uso cuando genero la orden desde el gestor. Es lo más aproximado
        Dim almacen_articulo As String = funciones.almacen_articulo(Lab_codarticulo.Text, "A01")
        Select Case almacen_articulo
            Case "A01" : pend_dmA01 = pend_dm
            Case "A07" : pend_dmA07 = pend_dm
            Case "A08" : pend_dmA08 = pend_dm
        End Select

        lab_stock_ubi.Text = FormatNumber(vdatos.Rows(0).Item("pulmon") + vdatos.Rows(0).Item("picking") + vdatos.Rows(0).Item("especial") + pend_dm, 0)
        lab_stock_ubi_ajustada.Text = FormatNumber(CInt(lab_stock_ubi.Text) + (CInt(lab_cantajustar_ubi.Text)), 0)




        'Albaranes no preparados
        sql = "select lin.codempresa as 'Emp.',lin.codperiodo as 'Periodo',lin.seralbaran as 'Ser.',lin.codalbaran as 'Cod.Alb.',cab.cliente as 'Cliente',"
        sql &= "cab.observaciones as 'Observaciones',"
        sql &= "isnull((select sum(l2.cantidad) from GesAlbaranesLin l2 where l2.codempresa=lin.CodEmpresa and l2.CodPeriodo =lin.codperiodo and l2.seralbaran=lin.SerAlbaran and l2.CodAlbaran =lin.CodAlbaran and l2.Articulo='" & Lab_codarticulo.Text & "' and l2.almacen = lin.almacen),0) as 'Cant.' ,"
        sql &= "isnull(sum(pre.cantidadpreparada),0) as 'Cant.Pre.',lin.almacen "
        sql &= "from GesAlbaranesLin lin "
        sql &= "inner join GesAlbaranes cab on cab.numreferencia=lin.refcabecera  "
        sql &= "left join intranet.dbo.albaranes_preparados pre on pre.codempresa =lin.CodEmpresa and pre.codperiodo =lin.CodPeriodo and pre.seralbaran =lin.SerAlbaran "
        sql &= "and pre.codalbaran =lin.CodAlbaran and pre.codlinea =lin.CodLinea and pre.codarticulo=lin.articulo "
        sql &= "where lin.Articulo ='" & Lab_codarticulo.Text & "' "
        sql &= "and not ((cab.codempresa=1 and cab.cliente='99999' and (upper(cab.observaciones) like 'ALB. JOUMMA A NEXTDOOR%')) or (cab.cliente in ('99974','99964','90013') and cab.codempresa=1) or (cab.codempresa=2 and cab.cliente = '00006') or (cab.codempresa=2 and cab.cliente = '00100' and cab.observaciones='Ventas Web') or (cab.codempresa=3 and cab.cliente in ('00024','99999','99964','90013')) or (cab.observaciones like 'Devolucion-Abono%' or cab.observaciones like 'Devolucion-Cambio%'  or cab.observaciones like 'Devolucion-Reparacion%' or cab.observaciones like 'Devolucion desde TPV%' or cab.observaciones like 'Traspaso Devoluciones%') ) "
        sql &= "And lin.codperiodo >=2017 And cab.seralbaran Not in ('AB','FR') and lin.Almacen in ('A01','A07','A08') " 'Solo los almacenens que se inventarian
        sql &= "group by lin.codempresa,lin.codperiodo ,lin.seralbaran ,lin.codalbaran ,cab.cliente ,cab.observaciones,lin.almacen "
        sql &= "having isnull((select sum(l2.cantidad) from GesAlbaranesLin l2 where l2.codempresa=lin.CodEmpresa and l2.CodPeriodo =lin.codperiodo and l2.seralbaran=lin.SerAlbaran and l2.CodAlbaran =lin.CodAlbaran and l2.Articulo='" & Lab_codarticulo.Text & "' and l2.almacen = lin.almacen),0)  <> isnull(sum(pre.cantidadpreparada),0) "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        dg_noprep.DataSource = vdatos
        dg_noprep.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18, FontStyle.Bold)
        dg_noprep.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18)
        dg_noprep.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dg_noprep.Columns(0).Width = 80
        dg_noprep.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_noprep.Columns(0).ReadOnly = True
        dg_noprep.Columns(1).Width = 120
        dg_noprep.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_noprep.Columns(1).ReadOnly = True
        dg_noprep.Columns(2).Width = 80
        dg_noprep.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_noprep.Columns(2).ReadOnly = True
        dg_noprep.Columns(3).Width = 120
        dg_noprep.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_noprep.Columns(3).ReadOnly = True
        dg_noprep.Columns(4).Width = 120
        dg_noprep.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_noprep.Columns(4).ReadOnly = True
        dg_noprep.Columns(5).Width = 400
        dg_noprep.Columns(5).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 10)
        dg_noprep.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        dg_noprep.Columns(5).ReadOnly = True
        dg_noprep.Columns(6).Width = 120
        dg_noprep.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_noprep.Columns(6).ReadOnly = True
        dg_noprep.Columns(7).Width = 120
        dg_noprep.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_noprep.Columns(8).ReadOnly = True
        dg_noprep.Columns(8).Width = 2
        dg_noprep.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells

        noprep = 0
        cont = 0
        Do While cont < vdatos.Rows.Count
            noprep += vdatos.Rows(cont).Item("Cant.") - vdatos.Rows(cont).Item("Cant.Pre.")
            If vdatos.Rows(cont).Item("almacen").ToString = "A01" Then noprep_a01 += vdatos.Rows(cont).Item("Cant.") - vdatos.Rows(cont).Item("Cant.Pre.")
            If vdatos.Rows(cont).Item("almacen").ToString = "A07" Then noprep_a07 += vdatos.Rows(cont).Item("Cant.") - vdatos.Rows(cont).Item("Cant.Pre.")
            If vdatos.Rows(cont).Item("almacen").ToString = "A08" Then noprep_a08 += vdatos.Rows(cont).Item("Cant.") - vdatos.Rows(cont).Item("Cant.Pre.")
            cont += 1
        Loop
        lab_noprep.Text = FormatNumber(noprep, 0)

        'Guardarme la cantidad que esta en un mov de prev. Esta cantidad hay que descontarla del stock
        sql = "select isnull(sum(case when lin.almacendest='A01' then cantidad end),0) as A01,isnull(sum(case when lin.almacendest='A07' then cantidad end),0) as A07,isnull(sum(case when lin.almacendest='A08' then cantidad end),0) as A08 "
        sql &= "from GesMovAlm cab "
        sql &= "inner join GesMovAlmLin lin on lin.RefCabecera =cab.NumReferencia and lin.articulo='" & Lab_codarticulo.Text & "' "
        sql &= "where cab.codperiodo >= 2018 And cab.Observaciones = 'MOV AUTOMATICO LISTADO MOVIMIENTOS PREVISION MANIPULACION' "
        vmovprev = conexion.TablaxCmd(constantes.bd_inase, sql)
        movprevA01 = vmovprev.Rows(0).Item("A01")
        movpreva07 = vmovprev.Rows(0).Item("A07")
        movpreva08 = vmovprev.Rows(0).Item("A08")
        lab_prev_manipulacion.Text = FormatNumber(movprevA01 + movpreva07 + movpreva08, 0)

        'Cantidad en orden de fabricación. Alb OF pend de rec/fab. La cantidad que esta en pedidoprov pendiente de albaranar , aprece como stock en P1, por tanto no hace falta porque ya se tiene en cuenta
        'sql = "select isnull(sum(isnull(cantidad - (case when cantidad <1 then cantidadvalidada*-1 else cantidadvalidada end),0)),0) "
        sql = "select isnull(sum(case when Almacen='A01' then isnull(cantidad - (case when cantidad <1 then cantidadvalidada*-1 else cantidadvalidada end),0)end),0) as A01OF, "
        sql &= "isnull(sum(case when Almacen='A07' then isnull(cantidad - (case when cantidad <1 then cantidadvalidada *-1 else cantidadvalidada end),0)end),0) as A07OF, "
        sql &= "isnull(sum(case when Almacen='A08' then isnull(cantidad - (case when cantidad <1 then cantidadvalidada *-1 else cantidadvalidada end),0)end),0) as A06OF,"
        sql &= "isnull(sum(isnull(cantidad - (case when cantidad <1 then cantidadvalidada*-1 else cantidadvalidada end), 0)),0)  As totOF "
        sql &= "from GesAlbaranesProvLin lin "
        sql &= "where lin.Articulo ='" & Lab_codarticulo.Text & "' and lin.SerAlbaranProv ='OF' and abs(lin.cantidadvalidada) <> abs(lin.cantidad)  "
        vof = conexion.TablaxCmd(constantes.bd_inase, sql)
        lab_OF.Text = FormatNumber(Math.Abs(vof.Rows(0).Item("totOF")), 0) 'Cuidado con el abs. Lo pongo para que no salga negativo y maree, pero el signo es importante
        pend_of = vof.Rows(0).Item("totOF")
        'La cantidad en Of se usa para regularizar el A01, ya que el total el P1 siempre esta en A01
        'pend_ofA01 = vof.Rows(0).Item("A01OF")
        'pend_ofa07 = vof.Rows(0).Item("A07OF")
        'pend_ofA08 = vof.Rows(0).Item("A06OF")



        'Busco pendientes de reaprov
        sql = "select Reaprovtotal,ReaprovA01 ,ReaprovA07 ,ReaprovA08 from v_reaprovpend where CodArticulo='" & Lab_codarticulo.Text & "' "
        'Esto lo puse porque si el reaprov no está ni recogido ni dep, la cantidad está en neg en un alm y en positivo en el otro y por tanto  el Reaprovtotal es 0
        'Solo tengo que ajustar la cantidad en caso de que la cantidad ya esté recogida y no depositada
        'Antes, en la vista v_reaprovpend, había un having "HAVING  (ISNULL(SUM(CASE WHEN recdep = 'Recoger' THEN cantidad * - 1 WHEN recdep = 'Depositar' THEN cantidad END), 0) > 0)"
        'Para q solo aparecieran las referencias que tenian cantidad en el campo Reaprovtotal, pero como solo uso esta vista con ese proposito aquí, lo hago en la consulta
        'así en el listado Ubicaciones_articulo, puedo usar la vista y  obtener el pendiente de reaprov.
        sql &= "and Reaprovtotal <> 0 "

        vreaprov = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vreaprov.Rows.Count = 1 Then
            pend_reaprov = vreaprov.Rows(0).Item("Reaprovtotal")
            pend_reaprovA01 = vreaprov.Rows(0).Item("ReaprovA01")
            pend_reaprovA07 = vreaprov.Rows(0).Item("ReaprovA07")
            pend_reaprovA08 = vreaprov.Rows(0).Item("ReaprovA08")
        Else
            pend_reaprov = 0
        End If
        lab_reaprov.Text = FormatNumber(pend_reaprov, 0)


        'Stock en alm
        sql = "select isnull(sum(case when codalmacen='A01' then StockActual end),0) as A01, "
        sql &= "isnull(sum(case when codalmacen='A07' then StockActual end),0) as A07, "
        sql &= "isnull(sum(case when codalmacen='A08' then StockActual end),0) as A08, "
        sql &= "isnull(sum(case when codalmacen='P1' then StockActual end),0) as P1 "
        sql &= "from gesstocks where codarticulo='" & Lab_codarticulo.Text & "' and codalmacen in ('A01','A07','A08','P1')  "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        lab_stock_A01.Text = FormatNumber(vdatos.Rows(0).Item("A01") - movprevA01, 0)
        lab_stock_a07.Text = FormatNumber(vdatos.Rows(0).Item("A07") - movpreva07, 0)
        lab_stock_a08.Text = FormatNumber(vdatos.Rows(0).Item("A08") - movpreva08, 0)
        lab_stock_p1.Text = FormatNumber(vdatos.Rows(0).Item("p1"), 0)
        lab_stock_total.Text = FormatNumber(vdatos.Rows(0).Item("p1") + (vdatos.Rows(0).Item("A01") - movprevA01) + (vdatos.Rows(0).Item("A07") - movpreva07) + (vdatos.Rows(0).Item("A08") - movpreva08), 0)
        lab_tot_stock.Text = FormatNumber(vdatos.Rows(0).Item("A01") + vdatos.Rows(0).Item("A07") + vdatos.Rows(0).Item("A08") + vdatos.Rows(0).Item("p1") - pend_of - movprevA01 - movpreva07 - movpreva08 - pend_reaprov, 0)
        'Sergio pidio que al ajustar el stock,el stock en alm. quede igual que el de la ubicacion, restando las unidades pendientes de preparar
        'lab_tot_stock_ajustada.Text = FormatNumber(CInt(lab_tot_stock.Text) + (CInt(lab_cantajustar_ubi.Text)), 0)
        lab_tot_stock_ajustada.Text = FormatNumber(CInt(lab_stock_ubi_ajustada.Text) - CInt(lab_noprep.Text), 0)
        lab_cantajustar_stock.Text = FormatNumber(CInt(lab_tot_stock_ajustada.Text) - CInt(lab_tot_stock.Text), 0)

    End Sub

    Public Sub btn_aceptar_Click(sender As System.Object, e As System.EventArgs) Handles btn_aceptar.Click
        'Ajustar stock en almacen con un movimineto. 
        'El stock de la ubicación se ajusta en el otro formulario que llama a este
        Dim sql As String
        Dim vdatos As New DataTable
        Dim resultado As String
        Dim nummov As Integer = 0
        Dim numrefmov As String = ""
        Dim cant_ajuste_A01 As Integer = 0
        Dim cant_ajuste_A07 As Integer = 0
        Dim cant_ajuste_A08 As Integer = 0
        Dim descripcionMov As String

        'Pedido por Sergio/Domingo. Mirar si una referencia esta en medio de un proceso automático de ajuste hecho por susana
        'Ya tengo en cuenta esta cantidad y por tanto no hace falta mostrar este aviso. Se ajusta siempre
        'sql = "select cab.NumMov from GesMovAlm cab "
        'sql &= "inner join gesmovalmlin lin on lin.refcabecera=cab.numreferencia "
        'sql &= "where lin.articulo='" & Lab_codarticulo.Text & "' and cab.CodEmpresa=1 and cab.Observaciones='MOV AUTOMATICO LISTADO MOVIMIENTOS PREVISION MANIPULACION' "
        'vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        'If vdatos.Rows.Count > 0 Then
        '    MsgBox("Existe un movimiento automático de desglose/montaje sin ajustar, no se puede continuar.")
        '    Exit Sub
        'End If

        If list_almacen.Text = "" Then
            MsgBox("Seleccionar el almacén donde se hará el ajuste.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Esto lo quité porque sino en situaciones en las que el stock era igual al stock de ubicaciones no actualizaba nada. En estos casos puede que si que toque cambiar stock de almacen
        'If lab_cantajustar_stock.Text = 0 Then
        '    Close()
        '    Exit Sub
        'End If

        If frm_principal.frm_abierto = "Recuento de Stock" Then
            descripcionMov = "Cambio stock desde Ubicaciones-Act_Stock(Recuento de Stock)"
        ElseIf frm_principal.frm_abierto = "Inventario" Then
            descripcionMov = "Cambio stock desde Ubicaciones-Act_Stock(Inventario)"
        Else
            descripcionMov = "Cambio stock desde Ubicaciones-Act_Stock"
        End If

        crear_movimiento_inase(1, nummov, descripcionMov, numrefmov, 1)


        'If list_almacen.Text = "AUTO" Then
        cant_ajuste_A01 = 0
        cant_ajuste_A07 = 0
        cant_ajuste_A08 = 0
        'Buscar el stock de ubicación A01 (pick + pulmon + esp) y colocarlo en el stock de A01. Sumarle el stock del p1. mas adelante sumar lo pendiente de recoger. 
        'Se suma al p1 porque de momento todas las ordenes se hacen para que descuente del A01 , pero luego se hara del almacen que toque
        'Buscar el stock de ubicación A07 (pick + pulmon + esp) y colocarlo en el stock de A07
        'Tendre que ver como saber de que almacen se ha hecho el pedido para sumar en a01 o en a07 o en a08...
        'Buscar el stock de ubicación A08 (pick + pulmon + esp) y colocarlo en el stock de A08
        sql = "select isnull(sum(case when ubi.codalmacen='A01' then ubi.cantidad end),0) as ubiA01, "
        sql &= "isnull(sum(case when ubi.codalmacen='A07' then ubi.cantidad end),0) as ubiA07, "
        sql &= "isnull(sum(case when ubi.codalmacen='A08' then ubi.cantidad end),0) as ubiA08, "
        sql &= "isnull((select sum(stockactual) from Inase.dbo.GesStocks where CodArticulo =ubi.codarticulo collate Modern_Spanish_CI_AS and CodAlmacen='A01' and CodEmpresa in (select codempresa from inase.dbo.inaempresas where controlstock=1)),0) as stockA01,"
        sql &= "isnull((select sum(stockactual) from Inase.dbo.GesStocks where CodArticulo =ubi.codarticulo collate Modern_Spanish_CI_AS and CodAlmacen='A07' and CodEmpresa in (select codempresa from inase.dbo.inaempresas where controlstock=1)),0) as stockA07, "
        sql &= "isnull((select sum(stockactual) from Inase.dbo.GesStocks where CodArticulo =ubi.codarticulo collate Modern_Spanish_CI_AS and CodAlmacen='A08' and CodEmpresa in (select codempresa from inase.dbo.inaempresas where controlstock=1)),0) as stockA08, "
        sql &= "isnull((select sum(stockactual) from Inase.dbo.GesStocks where CodArticulo =ubi.codarticulo collate Modern_Spanish_CI_AS and CodAlmacen='P1' and CodEmpresa in (select codempresa from inase.dbo.inaempresas where controlstock=1)),0) as stockP1 "
        sql &= "from Ubicaciones ubi "
        sql &= "where ubi.codalmacen in ('A01','A07','A08','P1') and ubi.codarticulo ='" & Lab_codarticulo.Text & "' "
        sql &= "and ubi.tipo in (select tipo from ubicaciones_tipos where controlstock=1) "
        sql &= "group by ubi.codarticulo "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        sql = ""
        If vdatos.Rows.Count = 1 Then
            If lab_alm_pick.Text = "A01" Then cant_ajuste_A01 = CInt(lab_cantajustar_ubi.Text)
            If lab_alm_pick.Text = "A07" Then cant_ajuste_A07 = CInt(lab_cantajustar_ubi.Text)
            If lab_alm_pick.Text = "A08" Then cant_ajuste_A08 = CInt(lab_cantajustar_ubi.Text)
            cant_ajuste_A01 += vdatos.Rows(0).Item("ubiA01") - vdatos.Rows(0).Item("stockA01") - vdatos.Rows(0).Item("stockP1") + pend_of - noprep_a01 + movprevA01 + pend_reaprovA01 + pend_dmA01
            cant_ajuste_A07 += vdatos.Rows(0).Item("ubiA07") - vdatos.Rows(0).Item("stockA07") - noprep_a07 + movpreva07 + pend_reaprovA07 + pend_dmA07
            cant_ajuste_A08 += vdatos.Rows(0).Item("ubiA08") - vdatos.Rows(0).Item("stockA08") - noprep_a08 + movpreva08 + pend_reaprovA08 + pend_dmA08
            If list_almacen.Text = "A01" Or list_almacen.Text = "AUTO" Then
                sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & 1 & "," & Now.Year & "," & nummov & ",1,'A01','XXX','" & Lab_codarticulo.Text & "'," & cant_ajuste_A01 & ",'" & descripcionMov & "') "
            End If
            If list_almacen.Text = "A07" Or list_almacen.Text = "AUTO" Then
                sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & 1 & "," & Now.Year & "," & nummov & ",2,'A07','XXX','" & Lab_codarticulo.Text & "'," & cant_ajuste_A07 & ",'" & descripcionMov & "') "
            End If
                If list_almacen.Text = "A08" Or list_almacen.Text = "AUTO" Then
                sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & 1 & "," & Now.Year & "," & nummov & ",4,'A08','XXX','" & Lab_codarticulo.Text & "'," & cant_ajuste_A08 & ",'" & descripcionMov & "') "
            End If

            Else
                Close()
            End If
        'Else
        'insertar movimiento en Inase en almacen picking y empresa 1
        'sql = "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
        'sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & 1 & "," & Now.Year & "," & nummov & ",1,'" & list_almacen.Text & "','XXX','" & Lab_codarticulo.Text & "'," & CInt(lab_cantajustar_stock.Text) & ",'Ajuste desde Ubicaciones-Ubicaciones') "
        ' End If
        resultado = conexion.Executetransact(constantes.bd_inase, sql)
        If resultado = "0" Then
            'Si la cantidad ajustada es mayor de 20 unidades avisar a Domingo/Sergio por correo
            If Math.Abs(cant_ajuste_A01) > 20 Then funciones.Enviar_correo("encargados_almacen@joumma.com", "Cantidad ajustada desde programa Ubicaciones superior a 20 unidades", "La referencia " & Lab_codarticulo.Text & " ha sido ajustada en " & CInt(cant_ajuste_A01) & " unidades en almacén " & list_almacen.Text)
            If Math.Abs(cant_ajuste_A07) > 20 Then funciones.Enviar_correo("encargados_almacen@joumma.com", "Cantidad ajustada desde programa Ubicaciones superior a 20 unidades", "La referencia " & Lab_codarticulo.Text & " ha sido ajustada en " & CInt(cant_ajuste_A07) & " unidades en almacén " & list_almacen.Text)
            If Math.Abs(cant_ajuste_A08) > 20 Then funciones.Enviar_correo("encargados_almacen@joumma.com", "Cantidad ajustada desde programa Ubicaciones superior a 20 unidades", "La referencia " & Lab_codarticulo.Text & " ha sido ajustada en " & CInt(cant_ajuste_A08) & " unidades en almacén " & list_almacen.Text)
            If Math.Abs(CInt(lab_cantajustar_stock.Text)) > 20 Then funciones.Enviar_correo("encargados_almacen@joumma.com", "Cantidad ajustada desde programa Ubicaciones superior a 20 unidades", "La referencia " & Lab_codarticulo.Text & " ha sido ajustada en " & CInt(lab_cantajustar_stock.Text) & " unidades en almacén " & list_almacen.Text)
            Close()
        Else
            MsgBox("Se producjo un error, el stock no ha sido modificado.", MsgBoxStyle.Exclamation)
        End If
    End Sub


    Private Sub frm_act_stock_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub

    Private Sub btn_cancelar_Click(sender As System.Object, e As System.EventArgs) Handles btn_cancelar.Click
        Close()
    End Sub
End Class