﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_reaprov_ubi
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_OtraUbi = New System.Windows.Forms.Button()
        Me.p_filtro = New System.Windows.Forms.Panel()
        Me.cb_FiltroRecA01 = New System.Windows.Forms.CheckBox()
        Me.cb_FiltroDepA07 = New System.Windows.Forms.CheckBox()
        Me.txt_busq_ref = New System.Windows.Forms.TextBox()
        Me.cb_FiltroRecA07 = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cb_FiltroDepA01 = New System.Windows.Forms.CheckBox()
        Me.txt_AlbAfect = New System.Windows.Forms.TextBox()
        Me.lab_fecha = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_max = New System.Windows.Forms.TextBox()
        Me.txt_act = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_cerrar = New System.Windows.Forms.Button()
        Me.btn_aparcar = New System.Windows.Forms.Button()
        Me.p_ref = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_articulo = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_cantidad = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.list_alm = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.list_ubicacion = New System.Windows.Forms.ComboBox()
        Me.list_accion = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.list_codreaprov_ubi = New System.Windows.Forms.ComboBox()
        Me.lab_descripcion = New System.Windows.Forms.Label()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_EliminarUbi = New System.Windows.Forms.Button()
        Me.lab_tipo = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.p_filtro.SuspendLayout()
        Me.p_ref.SuspendLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.btn_EliminarUbi)
        Me.Panel1.Controls.Add(Me.btn_OtraUbi)
        Me.Panel1.Controls.Add(Me.p_filtro)
        Me.Panel1.Controls.Add(Me.txt_AlbAfect)
        Me.Panel1.Controls.Add(Me.lab_fecha)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txt_max)
        Me.Panel1.Controls.Add(Me.txt_act)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btn_cerrar)
        Me.Panel1.Controls.Add(Me.btn_aparcar)
        Me.Panel1.Controls.Add(Me.p_ref)
        Me.Panel1.Controls.Add(Me.list_accion)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.list_codreaprov_ubi)
        Me.Panel1.Controls.Add(Me.lab_descripcion)
        Me.Panel1.Controls.Add(Me.imagen)
        Me.Panel1.Controls.Add(Me.btn_finalizar)
        Me.Panel1.Controls.Add(Me.dg_lineas)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1250, 640)
        Me.Panel1.TabIndex = 25
        '
        'btn_OtraUbi
        '
        Me.btn_OtraUbi.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.btn_OtraUbi.Enabled = False
        Me.btn_OtraUbi.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OtraUbi.Location = New System.Drawing.Point(930, 146)
        Me.btn_OtraUbi.Name = "btn_OtraUbi"
        Me.btn_OtraUbi.Size = New System.Drawing.Size(101, 34)
        Me.btn_OtraUbi.TabIndex = 114
        Me.btn_OtraUbi.Text = "Otra Ubi."
        Me.btn_OtraUbi.UseVisualStyleBackColor = False
        '
        'p_filtro
        '
        Me.p_filtro.Controls.Add(Me.cb_FiltroRecA01)
        Me.p_filtro.Controls.Add(Me.cb_FiltroDepA07)
        Me.p_filtro.Controls.Add(Me.txt_busq_ref)
        Me.p_filtro.Controls.Add(Me.cb_FiltroRecA07)
        Me.p_filtro.Controls.Add(Me.Label1)
        Me.p_filtro.Controls.Add(Me.cb_FiltroDepA01)
        Me.p_filtro.Location = New System.Drawing.Point(336, 2)
        Me.p_filtro.Name = "p_filtro"
        Me.p_filtro.Size = New System.Drawing.Size(486, 83)
        Me.p_filtro.TabIndex = 103
        '
        'cb_FiltroRecA01
        '
        Me.cb_FiltroRecA01.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroRecA01.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroRecA01.Location = New System.Drawing.Point(167, 13)
        Me.cb_FiltroRecA01.Name = "cb_FiltroRecA01"
        Me.cb_FiltroRecA01.Size = New System.Drawing.Size(144, 30)
        Me.cb_FiltroRecA01.TabIndex = 99
        Me.cb_FiltroRecA01.Text = "Rec. A01"
        Me.cb_FiltroRecA01.UseVisualStyleBackColor = True
        '
        'cb_FiltroDepA07
        '
        Me.cb_FiltroDepA07.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroDepA07.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroDepA07.Location = New System.Drawing.Point(336, 47)
        Me.cb_FiltroDepA07.Name = "cb_FiltroDepA07"
        Me.cb_FiltroDepA07.Size = New System.Drawing.Size(144, 30)
        Me.cb_FiltroDepA07.TabIndex = 102
        Me.cb_FiltroDepA07.Text = "Dep. A07"
        Me.cb_FiltroDepA07.UseVisualStyleBackColor = True
        '
        'txt_busq_ref
        '
        Me.txt_busq_ref.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.txt_busq_ref.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_busq_ref.Location = New System.Drawing.Point(18, 29)
        Me.txt_busq_ref.Name = "txt_busq_ref"
        Me.txt_busq_ref.Size = New System.Drawing.Size(129, 38)
        Me.txt_busq_ref.TabIndex = 67
        Me.txt_busq_ref.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_FiltroRecA07
        '
        Me.cb_FiltroRecA07.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroRecA07.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroRecA07.Location = New System.Drawing.Point(336, 13)
        Me.cb_FiltroRecA07.Name = "cb_FiltroRecA07"
        Me.cb_FiltroRecA07.Size = New System.Drawing.Size(144, 30)
        Me.cb_FiltroRecA07.TabIndex = 101
        Me.cb_FiltroRecA07.Text = "Rec. A07"
        Me.cb_FiltroRecA07.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label1.Location = New System.Drawing.Point(13, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 20)
        Me.Label1.TabIndex = 91
        Me.Label1.Text = "Buscar Ref."
        '
        'cb_FiltroDepA01
        '
        Me.cb_FiltroDepA01.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroDepA01.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroDepA01.Location = New System.Drawing.Point(167, 47)
        Me.cb_FiltroDepA01.Name = "cb_FiltroDepA01"
        Me.cb_FiltroDepA01.Size = New System.Drawing.Size(144, 30)
        Me.cb_FiltroDepA01.TabIndex = 100
        Me.cb_FiltroDepA01.Text = "Dep. A01"
        Me.cb_FiltroDepA01.UseVisualStyleBackColor = True
        '
        'txt_AlbAfect
        '
        Me.txt_AlbAfect.Location = New System.Drawing.Point(474, 600)
        Me.txt_AlbAfect.Multiline = True
        Me.txt_AlbAfect.Name = "txt_AlbAfect"
        Me.txt_AlbAfect.Size = New System.Drawing.Size(572, 30)
        Me.txt_AlbAfect.TabIndex = 80
        '
        'lab_fecha
        '
        Me.lab_fecha.AutoSize = True
        Me.lab_fecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_fecha.Location = New System.Drawing.Point(448, 578)
        Me.lab_fecha.Name = "lab_fecha"
        Me.lab_fecha.Size = New System.Drawing.Size(90, 17)
        Me.lab_fecha.TabIndex = 79
        Me.lab_fecha.Text = "01/01/2019"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(385, 602)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(83, 17)
        Me.Label11.TabIndex = 78
        Me.Label11.Text = "Alb.Afect.:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(385, 578)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 17)
        Me.Label6.TabIndex = 77
        Me.Label6.Text = "Fecha:"
        '
        'txt_max
        '
        Me.txt_max.Enabled = False
        Me.txt_max.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_max.Location = New System.Drawing.Point(589, 183)
        Me.txt_max.Name = "txt_max"
        Me.txt_max.Size = New System.Drawing.Size(53, 26)
        Me.txt_max.TabIndex = 76
        Me.txt_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_act
        '
        Me.txt_act.Enabled = False
        Me.txt_act.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_act.Location = New System.Drawing.Point(469, 183)
        Me.txt_act.Name = "txt_act"
        Me.txt_act.Size = New System.Drawing.Size(53, 26)
        Me.txt_act.TabIndex = 75
        Me.txt_act.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(542, 186)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 20)
        Me.Label4.TabIndex = 72
        Me.Label4.Text = "Max:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(427, 186)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 20)
        Me.Label3.TabIndex = 67
        Me.Label3.Text = "Act:"
        '
        'btn_cerrar
        '
        Me.btn_cerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cerrar.Location = New System.Drawing.Point(201, 581)
        Me.btn_cerrar.Name = "btn_cerrar"
        Me.btn_cerrar.Size = New System.Drawing.Size(160, 48)
        Me.btn_cerrar.TabIndex = 70
        Me.btn_cerrar.Text = "Cerrar"
        Me.btn_cerrar.UseVisualStyleBackColor = True
        '
        'btn_aparcar
        '
        Me.btn_aparcar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_aparcar.Location = New System.Drawing.Point(20, 581)
        Me.btn_aparcar.Name = "btn_aparcar"
        Me.btn_aparcar.Size = New System.Drawing.Size(166, 48)
        Me.btn_aparcar.TabIndex = 69
        Me.btn_aparcar.Text = "Aparcar"
        Me.btn_aparcar.UseVisualStyleBackColor = True
        '
        'p_ref
        '
        Me.p_ref.Controls.Add(Me.lab_tipo)
        Me.p_ref.Controls.Add(Me.Label12)
        Me.p_ref.Controls.Add(Me.txt_articulo)
        Me.p_ref.Controls.Add(Me.Label7)
        Me.p_ref.Controls.Add(Me.txt_cantidad)
        Me.p_ref.Controls.Add(Me.Label8)
        Me.p_ref.Controls.Add(Me.btn_ok)
        Me.p_ref.Controls.Add(Me.list_alm)
        Me.p_ref.Controls.Add(Me.Label10)
        Me.p_ref.Controls.Add(Me.list_ubicacion)
        Me.p_ref.Enabled = False
        Me.p_ref.Location = New System.Drawing.Point(22, 88)
        Me.p_ref.Name = "p_ref"
        Me.p_ref.Size = New System.Drawing.Size(878, 92)
        Me.p_ref.TabIndex = 68
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(13, 5)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(102, 20)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "Referencia:"
        '
        'txt_articulo
        '
        Me.txt_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_articulo.Location = New System.Drawing.Point(17, 27)
        Me.txt_articulo.MaxLength = 13
        Me.txt_articulo.Name = "txt_articulo"
        Me.txt_articulo.Size = New System.Drawing.Size(233, 60)
        Me.txt_articulo.TabIndex = 44
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(269, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 20)
        Me.Label7.TabIndex = 66
        Me.Label7.Text = "Almacén:"
        '
        'txt_cantidad
        '
        Me.txt_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad.Location = New System.Drawing.Point(644, 27)
        Me.txt_cantidad.Name = "txt_cantidad"
        Me.txt_cantidad.Size = New System.Drawing.Size(118, 60)
        Me.txt_cantidad.TabIndex = 59
        Me.txt_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(397, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(93, 20)
        Me.Label8.TabIndex = 65
        Me.Label8.Text = "Ubicación:"
        '
        'btn_ok
        '
        Me.btn_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ok.Location = New System.Drawing.Point(775, 27)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(92, 60)
        Me.btn_ok.TabIndex = 60
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'list_alm
        '
        Me.list_alm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_alm.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_alm.FormattingEnabled = True
        Me.list_alm.Location = New System.Drawing.Point(273, 26)
        Me.list_alm.Name = "list_alm"
        Me.list_alm.Size = New System.Drawing.Size(109, 62)
        Me.list_alm.TabIndex = 64
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(640, 5)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(86, 20)
        Me.Label10.TabIndex = 61
        Me.Label10.Text = "Cantidad:"
        '
        'list_ubicacion
        '
        Me.list_ubicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_ubicacion.FormattingEnabled = True
        Me.list_ubicacion.Location = New System.Drawing.Point(401, 26)
        Me.list_ubicacion.Name = "list_ubicacion"
        Me.list_ubicacion.Size = New System.Drawing.Size(225, 62)
        Me.list_ubicacion.TabIndex = 62
        '
        'list_accion
        '
        Me.list_accion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_accion.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_accion.FormattingEnabled = True
        Me.list_accion.Items.AddRange(New Object() {"Recoger", "Depositar"})
        Me.list_accion.Location = New System.Drawing.Point(858, 30)
        Me.list_accion.Name = "list_accion"
        Me.list_accion.Size = New System.Drawing.Size(186, 46)
        Me.list_accion.TabIndex = 67
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(854, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 20)
        Me.Label9.TabIndex = 63
        Me.Label9.Text = "Acción:"
        '
        'list_codreaprov_ubi
        '
        Me.list_codreaprov_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_codreaprov_ubi.FormattingEnabled = True
        Me.list_codreaprov_ubi.IntegralHeight = False
        Me.list_codreaprov_ubi.Location = New System.Drawing.Point(141, 14)
        Me.list_codreaprov_ubi.MaxDropDownItems = 10
        Me.list_codreaprov_ubi.Name = "list_codreaprov_ubi"
        Me.list_codreaprov_ubi.Size = New System.Drawing.Size(175, 59)
        Me.list_codreaprov_ubi.TabIndex = 54
        '
        'lab_descripcion
        '
        Me.lab_descripcion.AutoSize = True
        Me.lab_descripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_descripcion.Location = New System.Drawing.Point(19, 192)
        Me.lab_descripcion.Name = "lab_descripcion"
        Me.lab_descripcion.Size = New System.Drawing.Size(38, 17)
        Me.lab_descripcion.TabIndex = 47
        Me.lab_descripcion.Text = "XXX"
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(1068, 15)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(175, 175)
        Me.imagen.TabIndex = 40
        Me.imagen.TabStop = False
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(1068, 581)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(175, 48)
        Me.btn_finalizar.TabIndex = 33
        Me.btn_finalizar.Text = "Finalizar"
        Me.btn_finalizar.UseVisualStyleBackColor = True
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_lineas.Location = New System.Drawing.Point(20, 215)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(1223, 354)
        Me.dg_lineas.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(131, 46)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Num.:"
        '
        'btn_EliminarUbi
        '
        Me.btn_EliminarUbi.BackColor = System.Drawing.Color.OrangeRed
        Me.btn_EliminarUbi.Enabled = False
        Me.btn_EliminarUbi.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_EliminarUbi.Location = New System.Drawing.Point(930, 93)
        Me.btn_EliminarUbi.Name = "btn_EliminarUbi"
        Me.btn_EliminarUbi.Size = New System.Drawing.Size(101, 34)
        Me.btn_EliminarUbi.TabIndex = 115
        Me.btn_EliminarUbi.Text = "Elim. Ubi."
        Me.btn_EliminarUbi.UseVisualStyleBackColor = False
        '
        'lab_tipo
        '
        Me.lab_tipo.AutoSize = True
        Me.lab_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tipo.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lab_tipo.Location = New System.Drawing.Point(496, 10)
        Me.lab_tipo.Name = "lab_tipo"
        Me.lab_tipo.Size = New System.Drawing.Size(49, 13)
        Me.lab_tipo.TabIndex = 116
        Me.lab_tipo.Text = "Picking"
        '
        'frm_reaprov_ubi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_reaprov_ubi"
        Me.Text = "frm_reparov_ubi"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.p_filtro.ResumeLayout(False)
        Me.p_filtro.PerformLayout()
        Me.p_ref.ResumeLayout(False)
        Me.p_ref.PerformLayout()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_articulo As System.Windows.Forms.TextBox
    Friend WithEvents lab_descripcion As System.Windows.Forms.Label
    Friend WithEvents imagen As System.Windows.Forms.PictureBox
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents dg_lineas As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents list_codreaprov_ubi As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents list_alm As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents list_ubicacion As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents txt_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents list_accion As System.Windows.Forms.ComboBox
    Friend WithEvents p_ref As System.Windows.Forms.Panel
    Friend WithEvents btn_aparcar As System.Windows.Forms.Button
    Friend WithEvents btn_cerrar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_busq_ref As System.Windows.Forms.TextBox
    Friend WithEvents txt_max As System.Windows.Forms.TextBox
    Friend WithEvents txt_act As System.Windows.Forms.TextBox
    Friend WithEvents lab_fecha As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_AlbAfect As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cb_FiltroDepA07 As CheckBox
    Friend WithEvents cb_FiltroRecA07 As CheckBox
    Friend WithEvents cb_FiltroDepA01 As CheckBox
    Friend WithEvents cb_FiltroRecA01 As CheckBox
    Friend WithEvents p_filtro As Panel
    Friend WithEvents btn_OtraUbi As Button
    Friend WithEvents btn_EliminarUbi As Button
    Friend WithEvents lab_tipo As Label
End Class
