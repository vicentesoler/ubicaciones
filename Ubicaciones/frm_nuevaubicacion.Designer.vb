﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_nuevaubicacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.list_almacen = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_portal = New System.Windows.Forms.TextBox()
        Me.txt_altura = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_pasillo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_tamaño = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_cantmax = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.list_tipo = New System.Windows.Forms.ComboBox()
        Me.btn_añadir_a_ubi = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lab_ref = New System.Windows.Forms.Label()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.btn_consolidacion = New System.Windows.Forms.Button()
        Me.lab_almacen_aux = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'list_almacen
        '
        Me.list_almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 29.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almacen.FormattingEnabled = True
        Me.list_almacen.Location = New System.Drawing.Point(36, 107)
        Me.list_almacen.Name = "list_almacen"
        Me.list_almacen.Size = New System.Drawing.Size(95, 52)
        Me.list_almacen.TabIndex = 50
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(48, 162)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(71, 20)
        Me.Label18.TabIndex = 49
        Me.Label18.Text = "Almacen"
        '
        'txt_portal
        '
        Me.txt_portal.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_portal.Location = New System.Drawing.Point(253, 107)
        Me.txt_portal.MaxLength = 7
        Me.txt_portal.Name = "txt_portal"
        Me.txt_portal.Size = New System.Drawing.Size(70, 53)
        Me.txt_portal.TabIndex = 39
        '
        'txt_altura
        '
        Me.txt_altura.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_altura.Location = New System.Drawing.Point(356, 107)
        Me.txt_altura.MaxLength = 7
        Me.txt_altura.Name = "txt_altura"
        Me.txt_altura.Size = New System.Drawing.Size(70, 53)
        Me.txt_altura.TabIndex = 40
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(553, 118)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 31)
        Me.Label9.TabIndex = 48
        Me.Label9.Text = "Cm."
        '
        'txt_pasillo
        '
        Me.txt_pasillo.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pasillo.Location = New System.Drawing.Point(150, 107)
        Me.txt_pasillo.MaxLength = 7
        Me.txt_pasillo.Name = "txt_pasillo"
        Me.txt_pasillo.Size = New System.Drawing.Size(70, 53)
        Me.txt_pasillo.TabIndex = 38
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(223, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 39)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "/"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(465, 162)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 20)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "Tamaño"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(326, 114)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(27, 39)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "/"
        '
        'txt_tamaño
        '
        Me.txt_tamaño.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_tamaño.Location = New System.Drawing.Point(451, 107)
        Me.txt_tamaño.Name = "txt_tamaño"
        Me.txt_tamaño.Size = New System.Drawing.Size(99, 53)
        Me.txt_tamaño.TabIndex = 46
        Me.txt_tamaño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(157, 162)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 20)
        Me.Label4.TabIndex = 43
        Me.Label4.Text = "Pasillo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(263, 162)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 20)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Portal"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(366, 162)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 20)
        Me.Label6.TabIndex = 45
        Me.Label6.Text = "Altura"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(242, 276)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(70, 17)
        Me.Label16.TabIndex = 54
        Me.Label16.Text = "Cant. Max"
        '
        'txt_cantmax
        '
        Me.txt_cantmax.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantmax.Location = New System.Drawing.Point(234, 220)
        Me.txt_cantmax.MaxLength = 4
        Me.txt_cantmax.Name = "txt_cantmax"
        Me.txt_cantmax.Size = New System.Drawing.Size(82, 53)
        Me.txt_cantmax.TabIndex = 53
        Me.txt_cantmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(105, 277)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 17)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Tipo"
        '
        'list_tipo
        '
        Me.list_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_tipo.FormattingEnabled = True
        Me.list_tipo.Items.AddRange(New Object() {"Pulmon", "Picking"})
        Me.list_tipo.Location = New System.Drawing.Point(36, 220)
        Me.list_tipo.Name = "list_tipo"
        Me.list_tipo.Size = New System.Drawing.Size(175, 54)
        Me.list_tipo.TabIndex = 51
        '
        'btn_añadir_a_ubi
        '
        Me.btn_añadir_a_ubi.BackColor = System.Drawing.Color.LightGreen
        Me.btn_añadir_a_ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_añadir_a_ubi.Location = New System.Drawing.Point(333, 220)
        Me.btn_añadir_a_ubi.Name = "btn_añadir_a_ubi"
        Me.btn_añadir_a_ubi.Size = New System.Drawing.Size(134, 53)
        Me.btn_añadir_a_ubi.TabIndex = 55
        Me.btn_añadir_a_ubi.Text = "Añadir"
        Me.btn_añadir_a_ubi.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(60, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(357, 31)
        Me.Label2.TabIndex = 56
        Me.Label2.Text = "Nueva ubicación para ref.:"
        '
        'lab_ref
        '
        Me.lab_ref.AutoSize = True
        Me.lab_ref.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_ref.Location = New System.Drawing.Point(423, 9)
        Me.lab_ref.Name = "lab_ref"
        Me.lab_ref.Size = New System.Drawing.Size(126, 31)
        Me.lab_ref.TabIndex = 57
        Me.lab_ref.Text = "0000000"
        '
        'btn_cancelar
        '
        Me.btn_cancelar.BackColor = System.Drawing.Color.Tomato
        Me.btn_cancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancelar.Location = New System.Drawing.Point(502, 220)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(169, 53)
        Me.btn_cancelar.TabIndex = 58
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = False
        '
        'btn_consolidacion
        '
        Me.btn_consolidacion.BackColor = System.Drawing.Color.Cyan
        Me.btn_consolidacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_consolidacion.Location = New System.Drawing.Point(204, 52)
        Me.btn_consolidacion.Name = "btn_consolidacion"
        Me.btn_consolidacion.Size = New System.Drawing.Size(263, 40)
        Me.btn_consolidacion.TabIndex = 59
        Me.btn_consolidacion.Text = "A Consolidación"
        Me.btn_consolidacion.UseVisualStyleBackColor = False
        '
        'lab_almacen_aux
        '
        Me.lab_almacen_aux.AutoSize = True
        Me.lab_almacen_aux.Location = New System.Drawing.Point(659, 9)
        Me.lab_almacen_aux.Name = "lab_almacen_aux"
        Me.lab_almacen_aux.Size = New System.Drawing.Size(26, 13)
        Me.lab_almacen_aux.TabIndex = 60
        Me.lab_almacen_aux.Text = "A01"
        Me.lab_almacen_aux.Visible = False
        '
        'frm_nuevaubicacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(697, 301)
        Me.ControlBox = False
        Me.Controls.Add(Me.lab_almacen_aux)
        Me.Controls.Add(Me.btn_consolidacion)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.lab_ref)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btn_añadir_a_ubi)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txt_cantmax)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.list_tipo)
        Me.Controls.Add(Me.list_almacen)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txt_portal)
        Me.Controls.Add(Me.txt_altura)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txt_pasillo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_tamaño)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_nuevaubicacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frm_nuevaubicacion"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents list_almacen As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_portal As System.Windows.Forms.TextBox
    Friend WithEvents txt_altura As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_pasillo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_tamaño As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_cantmax As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents list_tipo As System.Windows.Forms.ComboBox
    Friend WithEvents btn_añadir_a_ubi As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lab_ref As System.Windows.Forms.Label
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents btn_consolidacion As System.Windows.Forms.Button
    Friend WithEvents lab_almacen_aux As Label
End Class
