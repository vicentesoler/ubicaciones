Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports System.Data.Odbc


Public Class Conector

    Public Function TablaxCmd(ByVal conectstr As String, ByVal comando As String)

        'If My.Computer.Network.IsAvailable = True Then

        '    'MsgBox("Tengo conexi�n a Internet")

        'Else

        '    MsgBox("No tengo conexi�n a Internet")
        '    Exit Function
        'End If

        'Conecto a la base de datos
        Dim conexion As New SqlConnection
        conexion.ConnectionString = conectstr

        'Seteo el Comando sql
        Dim cmd As New SqlCommand
        cmd = conexion.CreateCommand
        cmd.CommandType = CommandType.Text
        cmd.CommandText = comando.ToString()
        cmd.CommandTimeout = 300 'Segundos

        'Seteo el DataAdapter y lleno el DataSet

        Dim da As New SqlDataAdapter
        Dim dt As New DataTable
        da.SelectCommand = cmd

        da.Fill(dt)

        cmd.Dispose()
        conexion.Dispose()
        conexion.Close()

        'Devuelvo un DataTable con los datos
        Return dt

    End Function

    Public Function TablaxSP(ByVal conectstr As String, ByVal comando As String)
        'Conecto a la base de datos
        Dim conexion As New SqlConnection
        conexion.ConnectionString = conectstr

        'Seteo el Comando sql
        Dim cmd As New SqlCommand
        cmd = conexion.CreateCommand
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = comando.ToString()
        cmd.CommandTimeout = 300 'Segundos

        'Seteo el DataAdapter y lleno el DataSet
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable

        da.SelectCommand = cmd
        da.Fill(dt)

        cmd.Dispose()
        conexion.Dispose()
        conexion.Close()

        'Devuelvo un DataTable con los datos
        Return dt
    End Function
    Public Function ODBCTablaxCmd(ByVal conectstr As String, ByVal comando As String)
        'Conecto a la base de datos
        Dim conexion As New OdbcConnection
        conexion.ConnectionString = conectstr

        'Seteo el Comando sql
        Dim cmd As New OdbcCommand
        cmd = conexion.CreateCommand
        cmd.CommandType = CommandType.Text
        cmd.CommandText = comando.ToString()
        cmd.CommandTimeout = 300 'Segundos


        'Seteo el DataAdapter y lleno el DataSet
        Dim da As New OdbcDataAdapter
        Dim dt As New DataTable
        da.SelectCommand = cmd
        da.Fill(dt)

        cmd.Dispose()
        conexion.Dispose()
        conexion.Close()

        'Devuelvo un DataTable con los datos
        Return dt
    End Function
    Public Function ODBCExecuteCmd(ByVal conectstr As String, ByVal comando As String)
        'Conecto a la base de datos
        Dim conexion As New OdbcConnection
        conexion.ConnectionString = conectstr

        'Seteo el Comando sql
        Dim cmd As New OdbcCommand
        cmd = conexion.CreateCommand
        cmd.CommandType = CommandType.Text
        cmd.CommandText = comando.ToString()
        cmd.CommandTimeout = 300 'Segundos

        'bro la conexion, ejecuto el comando y entonces cierro la conexion
        Try
            conexion.Open()
            cmd.ExecuteNonQuery()
            Return "0"
        Catch
            Return ("Error:" & Err.ToString)
        Finally
            cmd.Dispose()
            conexion.Dispose()
            conexion.Close()
            'If conexion.State = ConnectionState.Open Then conexion.Close()
        End Try

    End Function
    Public Function ExecuteCmd(ByVal conectstr As String, ByVal comando As String)
        'Conecto a la base de datos
        Dim conexion As New SqlConnection
        conexion.ConnectionString = conectstr

        'Seteo el Comando sql
        Dim cmd As New SqlCommand
        cmd = conexion.CreateCommand
        cmd.CommandType = CommandType.Text
        cmd.CommandText = comando.ToString()
        cmd.CommandTimeout = 300 'Segundos

        'bro la conexion, ejecuto el comando y entonces cierro la conexion
        Try
            conexion.Open()
            cmd.ExecuteNonQuery()
            Return "0"
        Catch err As Exception
            Return ("Error:" & Err.ToString)
        Finally
            cmd.Dispose()
            conexion.Dispose()
            conexion.Close()
            'If conexion.State = ConnectionState.Open Then conexion.Close()
        End Try

    End Function

    Public Function Executetransact(ByVal conectstr As String, ByVal comando As String)
        'Conecto a la base de datos
        Dim conexion As New SqlConnection
        conexion.ConnectionString = conectstr

        'A�ado tranasct
        comando = "BEGIN TRY BEGIN TRANSACTION " & comando
        comando = comando & " COMMIT TRANSACTION END TRY BEGIN CATCH ROLLBACK TRAN Select ERROR_MESSAGE() END CATCH "
        'Seteo el Comando sql
        Dim cmd As New SqlCommand
        cmd = conexion.CreateCommand
        cmd.CommandType = CommandType.Text
        cmd.CommandText = comando.ToString()
        cmd.CommandTimeout = 300 'Segundos

        'Seteo el DataAdapter y lleno el DataSet
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable

        da.SelectCommand = cmd
        da.Fill(dt)

        cmd.Dispose()
        conexion.Dispose()
        conexion.Close()

        'Devuelvo un string con el resultado de la transaccion
        If dt.Rows.Count > 0 Then Return dt.Rows(0)(0) Else Return "0"
    End Function

End Class
