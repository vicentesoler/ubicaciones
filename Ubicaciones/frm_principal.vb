﻿Imports System.IO
Public Class frm_principal
    Public codempresa As Integer
    Public frm_abierto As String
    Public usuario_inase As Integer = 0
    Public encargado As Boolean
    'Variables de permisos menus 
    Public PermUbicaciones, PermArticulo, PermReaprov, PermOrdenAlta, PermInventarios, PermDesgMont, PermContenedor, PermOrdenSalida, PermOrdProd, PermRecuentoStock, PermRecogerOFs As Boolean
    'Variables permisos acciones
    Public PermOrdProdModOFFinalizadas, PermArticuloEliminarFila, PermArticuloModStock, PermArticuloModStockPlus, PermInventariosNuevo, PermInventariosCantAlta, PermOrdenAltaVerTiempo As Boolean
    Public PermReaprovUbiModMaxAct, PermUbicacionesEliminarFila, PermUbicacionesActStock, PermContenedorAbrirFinalizado As Boolean
    Public maquina As String = ""

    Private Sub frm_principal_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Strings.Left(constantes.bd_inase, 20) = "Data Source=atlantis" Then
            If lab_idoperario.Text <> 8 Then
                MsgBox("Aplicación en modo pruebas, no se puede usar. Avisar a Informática")
                End
            Else
                MsgBox("Está ejecutando el progrma en modo pruebas. La base de datos usada no es real.")
                Me.Text = "UBICACIONES - BD PRUEBAS"
            End If
        Else
            Me.Text = "UBICACIONES - " & File.GetLastWriteTime("Ubicaciones.exe")
        End If

        Obtener_datos_del_ini()

        codempresa = 1
        frm_abierto = "Ubicacion"
        'MsgBox("No se puede continuar, esta pendiente de probar el mov de ajuste en orden montaje/desglose cuando una cantidad validada es diferente a la inicial")
        'Close()
        'Desaactivar boton de inventario hasta que arregle que se pueda poner una cantidad de picking por ubi de pick, ya que hay mas de una ubic. de pick
        'If lab_idoperario.Text = 8 Then btn_inventarios.Enabled = True Else btn_inventarios.Enabled = False
    End Sub

    Public Sub Obtener_datos_del_ini()
        Dim sLine As String = ""
        Dim archivo As String

        'Por si no tiene el archivo ini
        globales.etiquetadoras = "Zebras Sala Chinas"

        Try
            archivo = "Ubicaciones.ini"
            Dim str As New StreamReader(archivo)
            Do
                sLine = str.ReadLine()
                If Not sLine Is Nothing Then
                    If Strings.Left(sLine, 14) = "Etiquetadoras=" Then
                        globales.etiquetadoras = Trim(Strings.Mid(sLine, 15))
                    End If
                End If
            Loop Until sLine Is Nothing
        Catch ex As Exception

        End Try


    End Sub

    Public Function EstaAbierto(ByVal Myform As Form) As Boolean
        Dim objForm As Form
        Dim blnAbierto As Boolean = False
        blnAbierto = False
        For Each objForm In My.Application.OpenForms
            If (Trim(objForm.Name) = Trim(Myform.Name)) Then
                blnAbierto = True
            End If
        Next
        Return blnAbierto
    End Function

    Private Sub frm_principal_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Dim sql As String
        Dim conexion As New Conector
        Dim tiempoempleado As Integer = 0
        If frm_abierto = "Recuento de Stock" Then
            tiempoempleado = DateDiff(DateInterval.Second, Frm_RecuentoStock.fecha_inicio, Now())
            sql = "insert into albaranes_tiempos (idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(" & lab_idoperario.Text & ",'Recuento Stock','" & Frm_RecuentoStock.fecha_inicio & "','" & Now() & "'," & tiempoempleado & ",0,'Cerrar' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)

        End If
        If frm_abierto = "OrdenAlta" Then
            If frm_ordenalta.list_codorden.Text <> "" Then conexion.ExecuteCmd(constantes.bd_intranet, "Update orden_alta set estado='Cerrada',ultmodificacion=getdate(),ultusuario='" & lab_nomoperario.Text & "' where codorden=" & frm_ordenalta.list_codorden.Text)
        End If

        If frm_abierto = "Desglose/Montaje" Then
            If frm_Desg_mont.list_codorden.Text <> "" Then conexion.TablaxCmd(constantes.bd_intranet, "update orden_desg_mont set estado='' where codorden=" & frm_Desg_mont.list_codorden.Text)
        End If

        If frm_abierto = "Reaprovisionamiento Ubicaciones" Then
            If frm_reaprov_ubi.list_codreaprov_ubi.Text <> "" Then conexion.Executetransact(constantes.bd_intranet, "update reaprov_ubi set estado='Cerrado' where codreaprov=" & frm_reaprov_ubi.list_codreaprov_ubi.Text)
        End If

        If frm_abierto = "Orden Produccion" Then
            frm_OrdenProduccion.LiberarOF()
        End If

        If frm_abierto = "Recoger OFs" Then
            Frm_RecogerOFs.Guardar_datos_tiempo("Salir")
            'Desmarcar el operario
            sql = "update operarios_almacen set IdUltimaAccion=0,FechaUltimaAccion=getdate() where id=" & lab_idoperario.Text
            conexion.Executetransact(constantes.bd_intranet, sql)
        End If

        'Desmarcar Máquina y log
        If lab_maquina.Text <> "" And lab_maquina.Text <> "0" Then
            sql = "Update MaquinasAlmacen set IdOperario=" & lab_idoperario.Text & ", FechaFinUso=getdate(),Estado='Libre' "
            sql &= "where id='" & lab_maquina.Text & "'; "
            sql &= "insert into MaquinasAlmacenLog(IdMaquina,IdOperario,Accion,Observaciones) "
            sql &= "values('" & lab_maquina.Text & "'," & lab_idoperario.Text & ",'Finalizar Preparación','Salir de Ubicacaiones')"

            conexion.Executetransact(constantes.bd_intranet, sql)
        End If

    End Sub

    Public Sub botones_no_marcados()
        frm_articulo.Close()
        frm_reaprov_ubi.Close()
        frm_ubicaciones.Close()
        frm_ordenalta.Close()
        frm_inventarios.Close()
        frm_Desg_mont.Close()
        frm_ordensalida.Close()
        frm_OrdenProduccion.Close()
        frm_contenedor.Close()
        Frm_RecuentoStock.Close()
        Frm_RecogerOFs.Close()
        btn_mostrar_ubicacion.BackColor = Color.Teal
        btn_mostrar_ubicacion.ForeColor = Color.White
        btn_articulo.BackColor = Color.Teal
        btn_articulo.ForeColor = Color.White
        btn_reaprov.BackColor = Color.Teal
        btn_reaprov.ForeColor = Color.White
        btn_ordenalta.BackColor = Color.Teal
        btn_ordenalta.ForeColor = Color.White
        btn_inventarios.BackColor = Color.Teal
        btn_inventarios.ForeColor = Color.White
        btn_desg_mont.BackColor = Color.Teal
        btn_desg_mont.ForeColor = Color.White
        btn_contenedor.BackColor = Color.Teal
        btn_contenedor.ForeColor = Color.White
        btn_ordensalida.BackColor = Color.Teal
        btn_ordensalida.ForeColor = Color.White
        btn_OrdProd.BackColor = Color.Teal
        btn_OrdProd.ForeColor = Color.White
        btn_RecuentoStocks.BackColor = Color.Teal
        btn_RecuentoStocks.ForeColor = Color.White
        btn_RecogerOFs.BackColor = Color.Teal
        btn_RecogerOFs.ForeColor = Color.White

    End Sub

    Private Sub btn_RegistrarAccion_Click(sender As Object, e As EventArgs) Handles btn_RegistrarAccion.Click
        frm_registrarAccion.ShowDialog()
    End Sub

    Private Sub btn_RecogerOFs_Click(sender As Object, e As EventArgs) Handles btn_RecogerOFs.Click
        If PermRecogerOFs = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(Frm_RecogerOFs) = False Then
            botones_no_marcados()
            Frm_RecogerOFs.TopLevel = False
            Frm_RecogerOFs.Dock = DockStyle.None
            p_principal.Controls.Add(Frm_RecogerOFs)
            btn_RecogerOFs.BackColor = Color.LightGreen
            btn_RecogerOFs.ForeColor = Color.Black
            frm_abierto = "Recoger OFs"
            Frm_RecogerOFs.Show()
            Frm_RecogerOFs.list_filtro_tipo.Text = "Gener."
            Frm_RecogerOFs.llenar_referencias()
            If Frm_RecogerOFs.dg_lineas.Rows.Count > 0 Then Frm_RecogerOFs.dg_lineas.CurrentCell = Nothing
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If
    End Sub

    Public Sub btn_mostrar_ubicacion_Click(sender As System.Object, e As System.EventArgs) Handles btn_mostrar_ubicacion.Click
        If PermUbicaciones = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(frm_ubicaciones) = False Then
            botones_no_marcados()
            frm_ubicaciones.TopLevel = False
            frm_ubicaciones.Dock = DockStyle.None
            p_principal.Controls.Add(frm_ubicaciones)
            btn_mostrar_ubicacion.BackColor = Color.LightGreen
            btn_mostrar_ubicacion.ForeColor = Color.Black
            frm_abierto = "Ubicacion"
            frm_ubicaciones.Show()
        End If
        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If
    End Sub


    Public Sub btn_articulo_Click(sender As System.Object, e As System.EventArgs) Handles btn_articulo.Click
        If PermArticulo = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If

        If EstaAbierto(frm_articulo) = False Then
            botones_no_marcados()
            frm_articulo.TopLevel = False
            frm_articulo.Dock = DockStyle.None
            p_principal.Controls.Add(frm_articulo)
            btn_articulo.BackColor = Color.LightGreen
            btn_articulo.ForeColor = Color.Black
            frm_abierto = "Articulo"
            frm_articulo.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If

    End Sub


    Public Sub btn_reaprov_Click(sender As System.Object, e As System.EventArgs) Handles btn_reaprov.Click
        If PermReaprov = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(frm_reaprov_ubi) = False Then
            botones_no_marcados()
            frm_reaprov_ubi.TopLevel = False
            frm_reaprov_ubi.Dock = DockStyle.None
            p_principal.Controls.Add(frm_reaprov_ubi)
            btn_reaprov.BackColor = Color.LightGreen
            btn_reaprov.ForeColor = Color.Black
            frm_abierto = "Reaprovisionamiento Ubicaciones"
            frm_reaprov_ubi.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If

    End Sub

    Public Sub btn_ordenalta_Click(sender As System.Object, e As System.EventArgs) Handles btn_ordenalta.Click
        If PermOrdenAlta = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If

        If EstaAbierto(frm_ordenalta) = False Then
            botones_no_marcados()
            frm_ordenalta.TopLevel = False
            frm_ordenalta.Dock = DockStyle.None
            p_principal.Controls.Add(frm_ordenalta)
            btn_ordenalta.BackColor = Color.LightGreen
            btn_ordenalta.ForeColor = Color.Black
            frm_abierto = "OrdenAlta"
            frm_ordenalta.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If

    End Sub

    Private Sub cerrar()
        Me.Close()
        End
    End Sub
    Private Sub btn_inventarios_click(sender As System.Object, e As System.EventArgs) Handles btn_inventarios.Click
        If PermInventarios = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(frm_inventarios) = False Then
            'Sergio pidió que activara a todos.
            'Se desactivo a todos menos sergio y domingo
            'Segio pidio que lo activara a todos para el inventario. quitado

            botones_no_marcados()
            frm_inventarios.TopLevel = False
            frm_inventarios.Dock = DockStyle.None
            p_principal.Controls.Add(frm_inventarios)
            btn_inventarios.BackColor = Color.LightGreen
            btn_inventarios.ForeColor = Color.Black
            frm_abierto = "Inventario"
            frm_inventarios.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If

    End Sub

    Private Sub btn_desg_mont_Click(sender As System.Object, e As System.EventArgs) Handles btn_desg_mont.Click
        If PermDesgMont = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(frm_Desg_mont) = False Then
            botones_no_marcados()
            frm_Desg_mont.TopLevel = False
            frm_Desg_mont.Dock = DockStyle.None
            p_principal.Controls.Add(frm_Desg_mont)
            btn_desg_mont.BackColor = Color.LightGreen
            btn_desg_mont.ForeColor = Color.Black
            frm_abierto = "Desglose/Montaje"
            frm_Desg_mont.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If
    End Sub

    Private Sub btn_contenedor_Click(sender As System.Object, e As System.EventArgs) Handles btn_contenedor.Click
        If PermContenedor = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(frm_contenedor) = False Then
            botones_no_marcados()
            frm_contenedor.TopLevel = False
            frm_contenedor.Dock = DockStyle.None
            p_principal.Controls.Add(frm_contenedor)
            btn_contenedor.BackColor = Color.LightGreen
            btn_contenedor.ForeColor = Color.Black
            frm_abierto = "Contenedor"
            frm_contenedor.Show()

        End If


        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If
    End Sub

    Private Sub frm_principal_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        End
    End Sub

    Private Sub btn_ordensalida_Click(sender As System.Object, e As System.EventArgs) Handles btn_ordensalida.Click
        If PermOrdenSalida = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(frm_ordensalida) = False Then
            botones_no_marcados()
            frm_ordensalida.TopLevel = False
            frm_ordensalida.Dock = DockStyle.None
            p_principal.Controls.Add(frm_ordensalida)
            btn_ordensalida.BackColor = Color.LightGreen
            btn_ordensalida.ForeColor = Color.Black
            frm_abierto = "Orden Salida"
            frm_ordensalida.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If
    End Sub


    Private Sub Timer_principal_Tick(sender As System.Object, e As System.EventArgs) Handles Timer_principal.Tick
        comprobar_mont_desg_pend()
        comprobar_reaprov_ubi_pend()
        comprobar_OF_pend()
        comprobar_ubicaciones_vacias()
    End Sub
    Public Sub comprobar_mont_desg_pend()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim conexion As New Conector

        'Mirar si hay ordenes de mont/desglose pendientes
        sql = "Select count(*) from orden_desg_mont where (recogido=0 Or depositado=0) And codorden >=1698 "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows(0)(0) <> 0 Then
            btn_desg_mont.ForeColor = Color.OrangeRed
        Else
            btn_desg_mont.ForeColor = Color.White
        End If
    End Sub
    Public Sub comprobar_ubicaciones_vacias()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim conexion As New Conector

        sql = "Select isnull(count(ubicacion),0) As 'cant' from ubicaciones where cantidad <=0 "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows(0).Item("cant") <> 0 Then
            btn_mostrar_ubicacion.ForeColor = Color.OrangeRed
        Else
            btn_mostrar_ubicacion.ForeColor = Color.White
        End If
    End Sub
    Public Sub comprobar_reaprov_ubi_pend()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim conexion As New Conector

        sql = "select isnull(count(codreaprov),0) as 'cant' from reaprov_ubi where procesado=0 "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows(0).Item("cant") <> 0 Then
            btn_reaprov.ForeColor = Color.OrangeRed
        Else
            btn_reaprov.ForeColor = Color.White
        End If
    End Sub
    Public Sub comprobar_OF_pend()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim conexion As New Conector

        sql = "select isnull(count(CodAlbaranProv),0) as 'cant' from GesAlbaranesProv where SerAlbaranProv ='OF' and (isnull(FinDepositada,0) =0 or isnull(FinRecogida,0) =0) "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        If vdatos.Rows(0).Item("cant") <> 0 Then
            btn_OrdProd.ForeColor = Color.OrangeRed
        Else
            btn_OrdProd.ForeColor = Color.White
        End If
    End Sub
    Private Sub p_principal_Paint(sender As System.Object, e As System.Windows.Forms.PaintEventArgs) Handles p_principal.Paint
        comprobar_mont_desg_pend()
        comprobar_reaprov_ubi_pend()
        comprobar_OF_pend()
        comprobar_ubicaciones_vacias()
    End Sub

    Private Sub btn_OrdProd_Click(sender As System.Object, e As System.EventArgs) Handles btn_OrdProd.Click
        If PermOrdProd = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(frm_OrdenProduccion) = False Then
            botones_no_marcados()
            frm_OrdenProduccion.TopLevel = False
            frm_OrdenProduccion.Dock = DockStyle.None
            p_principal.Controls.Add(frm_OrdenProduccion)
            btn_OrdProd.BackColor = Color.LightGreen
            btn_OrdProd.ForeColor = Color.Black
            frm_abierto = "Orden Produccion"
            frm_OrdenProduccion.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If
    End Sub
    Private Sub btn_RecuentoStocks_Click(sender As Object, e As EventArgs) Handles btn_RecuentoStocks.Click
        If PermRecuentoStock = False Then
            MsgBox("Esta opción no está disponible para este usuario.")
            Exit Sub
        End If
        If EstaAbierto(Frm_RecuentoStock) = False Then
            botones_no_marcados()
            Frm_RecuentoStock.TopLevel = False
            Frm_RecuentoStock.Dock = DockStyle.None
            p_principal.Controls.Add(Frm_RecuentoStock)
            btn_RecuentoStocks.BackColor = Color.LightGreen
            btn_RecuentoStocks.ForeColor = Color.Black
            frm_abierto = "Recuento de Stock"
            Frm_RecuentoStock.Show()
        End If

        ' Lo pongo aqui porque el proceso se quedaba en memoria. Puede que sea porque esta a mitad funcion
        If comprobar_nueva_version() = "SI" Then
            cerrar()
        End If
    End Sub
End Class