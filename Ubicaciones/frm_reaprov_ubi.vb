﻿Public Class frm_reaprov_ubi
    Dim conexion As New Conector
    Dim cargainicial As Boolean
    Dim fecha_inicio, fecha_fin As DateTime

    Private Sub frm_reaprov_ubi_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        limpiar(False)
        llenar_reaprov()
        list_accion.SelectedIndex = -1
        dg_lineas.Enabled = False
        txt_max.Enabled = False
        txt_act.Enabled = False
        lab_tipo.Text = ""
    End Sub

    Public Sub llenar_reaprov()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer

        list_codreaprov_ubi.DataSource = Nothing
        list_codreaprov_ubi.Items.Clear()
        sql = "select cab.codreaprov from reaprov_ubi cab "
        sql &= "inner join reaprov_ubi_lin lin on lin.codreaprov=cab.codreaprov "
        If txt_busq_ref.Text <> "" Then sql &= "and lin.codarticulo='" & txt_busq_ref.Text & "' "

        'If cb_FiltroRecA01.Checked Or cb_FiltroDepA01.Checked Or cb_FiltroRecA07.Checked Or cb_FiltroDepA07.Checked Then
        '    sql &= "and (1=0 "
        '    If cb_FiltroRecA01.Checked Then sql &= "or lin.codalmacen_recogida='A01' "
        '    If cb_FiltroDepA01.Checked Then sql &= "or lin.codalmacen_depositada='A01' "
        '    If cb_FiltroRecA07.Checked Then sql &= "or lin.codalmacen_recogida='A07' "
        '    If cb_FiltroDepA07.Checked Then sql &= "or lin.codalmacen_depositada='A07' "
        '    sql &= ") "
        'End If
        '14/07/2020 - Sergio me pidió que lo cambiara para que fuera and y no or
        If cb_FiltroRecA01.Checked Then sql &= "and lin.codalmacen_recogida='A01' "
        If cb_FiltroDepA01.Checked Then sql &= "and lin.codalmacen_depositada='A01' "
        If cb_FiltroRecA07.Checked Then sql &= "and lin.codalmacen_recogida='A07' "
        If cb_FiltroDepA07.Checked Then sql &= "and lin.codalmacen_depositada='A07' "


        sql &= "where cab.procesado=0 "
        sql &= "group by cab.codreaprov,cab.urgente "
        sql &= "order by cab.urgente desc,codreaprov asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        cont = 0
        list_codreaprov_ubi.Text = ""
        list_codreaprov_ubi.Items.Clear()
        Do While cont < vdatos.Rows.Count
            list_codreaprov_ubi.Items.Add(vdatos.Rows(cont).Item("codreaprov"))
            cont += 1
        Loop

        If txt_busq_ref.Text <> "" Or cb_FiltroDepA01.Checked Or cb_FiltroDepA07.Checked Or cb_FiltroRecA01.Checked Or cb_FiltroRecA07.Checked Then list_codreaprov_ubi.BackColor = Color.SteelBlue Else list_codreaprov_ubi.BackColor = Color.White

    End Sub
    Public Sub datos_cabecera()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0
        Dim resultado As String

        'Guardar el momento de abrir el albarán
        fecha_inicio = Now

        p_filtro.Enabled = False

        sql = "select procesado,fechaalta,albaranesafectados,confirmado "
        sql &= "from reaprov_ubi cab  "
        sql &= "where cab.codreaprov=" & list_codreaprov_ubi.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows.Count = 1 Then
            If vdatos.Rows(0).Item("confirmado") = "False" Then
                MsgBox("Este Reaprov. no ha sido confirmado. Avisar al encargado.")
                limpiar(True, "Este Reaprov. no ha sido confirmado.")
                Exit Sub
            End If
            If vdatos.Rows(0).Item("procesado") = "True" Then
                If MsgBox("Este Reaprov. ya ha sido procesado, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    limpiar(True, "Este Reaprov. ya ha sido procesado.")
                    Exit Sub
                Else
                    btn_finalizar.Enabled = False
                    btn_aparcar.Enabled = False
                    btn_cerrar.Enabled = True
                End If
            Else
                btn_finalizar.Enabled = True
                btn_aparcar.Enabled = True
                btn_cerrar.Enabled = False
            End If
            sql = "update reaprov_ubi set estado='Abierto',ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codreaprov=" & list_codreaprov_ubi.Text
            resultado = conexion.Executetransact(constantes.bd_intranet, sql)

            lab_fecha.Text = CDate(vdatos.Rows(0).Item("fechaalta")).ToShortDateString
            txt_AlbAfect.Text = vdatos.Rows(0).Item("albaranesafectados").ToString
            cargar_lineas()
        Else
            MsgBox("No existe el reaprovisionamiento seleccionado!.", MsgBoxStyle.Exclamation)
            limpiar(True, "No existe el reaprovisionamiento seleccionado.")
            Exit Sub
        End If
        If frm_principal.lab_idoperario.Text = 8 Then btn_cerrar.Enabled = True
    End Sub
    Public Sub cargar_lineas()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont = 0

        sql = "select codlinea as 'Lin.',lin.codarticulo as 'Ref.',art.descripcion as 'Descripcion',lin.cantidad as 'Cant.',"
        sql &= "lin.codalmacen_recogida as 'Alm.Rec',lin.ubicacion_recogida as 'Ubi.Rec',lin.cantidad_recogida as 'C.Rec', "
        sql &= "lin.codalmacen_depositada as 'Alm.Dep',lin.ubicacion_depositada as 'Ubi.Dep',lin.cantidad_depositada as 'C.Dep', "
        sql &= "isnull((select sum(pend.cantpendprep) from V_pendientePreparar pend where pend.codarticulo=lin.codarticulo and pend.codempresa <> 3),0) as 'P.Prep.',"
        sql &= "lin.tipo_recogida as 'TR',isnull(lin.tipo_depositada,'Picking') as 'TD' "
        sql &= "from reaprov_ubi_lin lin "
        sql &= "inner join reaprov_ubi cab on cab.codreaprov =lin.codreaprov "
        sql &= "inner join inase.dbo.GesArticulos art on art.codempresa=1 and art.codarticulo collate Modern_Spanish_CI_AS = lin.codarticulo "
        sql &= "where lin.codreaprov=" & list_codreaprov_ubi.Text & " "
        sql &= "order by lin.codalmacen_recogida asc,lin.codarticulo asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_lineas.DataSource = vdatos
            dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 20)
            dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
            'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_lineas.Columns(0).Width = 1
            dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(1).Width = 130
            dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dg_lineas.Columns(2).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 10, FontStyle.Bold)
            dg_lineas.Columns(2).Width = 220
            dg_lineas.Columns(2).DefaultCellStyle.WrapMode = DataGridViewTriState.True
            dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(3).Width = 70
            dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(4).Width = 105
            dg_lineas.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(5).Width = 125
            dg_lineas.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(6).Width = 85
            dg_lineas.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(7).Width = 110
            dg_lineas.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(8).Width = 125
            dg_lineas.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(9).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(9).Width = 90
            dg_lineas.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(10).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_lineas.Columns(10).Width = 90
            dg_lineas.Columns(10).DefaultCellStyle.Format = "N0"
            dg_lineas.Columns(11).Width = 1
            dg_lineas.Columns(12).Width = 1




        Else
            MsgBox("No se encontro el reaprov. especificado.", MsgBoxStyle.Critical)
            list_codreaprov_ubi.SelectAll()
        End If

        pintar_lineas_grid()
        txt_articulo.Focus()
        txt_articulo.SelectAll()
        dg_lineas.ClearSelection()
        dg_lineas.CurrentCell = Nothing
    End Sub

    Public Sub pintar_lineas_grid()
        Dim cont As Integer
        cont = 0
        While cont < dg_lineas.RowCount

            If CInt(dg_lineas.Rows(cont).Cells("c.Rec").Value.ToString) = CInt(dg_lineas.Rows(cont).Cells("c.dep").Value.ToString) And CInt(dg_lineas.Rows(cont).Cells("Cant.").Value.ToString) = CInt(dg_lineas.Rows(cont).Cells("c.dep").Value.ToString) Then
                dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
                dg_lineas.Rows(cont).Cells("c.Rec").Style.BackColor = Color.Green
                dg_lineas.Rows(cont).Cells("c.dep").Style.BackColor = Color.Green
            Else
                dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.White
                Select Case dg_lineas.Rows(cont).Cells("c.Rec").Value
                    Case 0
                    Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).Cells("c.Rec").Style.BackColor = Color.Yellow
                    Case Is > dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).Cells("c.Rec").Style.BackColor = Color.Red
                    Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).Cells("c.Rec").Style.BackColor = Color.Green
                End Select

                Select Case dg_lineas.Rows(cont).Cells("c.dep").Value
                    Case 0
                    Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).Cells("c.dep").Style.BackColor = Color.Yellow
                    Case Is > dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).Cells("c.dep").Style.BackColor = Color.Red
                    Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).Cells("c.dep").Style.BackColor = Color.Green
                End Select

            End If

            'Situar el cursor en la linea q acabamos de marcar
            'If dg_lineasalb.Rows(cont).Cells("Referencia").Value = ult_ref_embalada And (linea_click = 0 Or dg_lineasalb.Rows(cont).Cells("Lin").Value = linea_click) Then
            '    dg_lineasalb.Item(0, dg_lineasalb.RowCount - 1).Selected = True
            '    dg_lineasalb.Item(0, cont).Selected = True
            'End If

            cont += 1
        End While

    End Sub


    Private Sub txt_cantidad_Click(sender As System.Object, e As System.EventArgs)
        txt_cantidad.SelectAll()
    End Sub

    Private Sub btn_ok_Click(sender As System.Object, e As System.EventArgs) Handles btn_ok.Click
        Dim sql As String = ""
        Dim sql2 As String = ""
        Dim sql_añadir_ubi As String = ""
        Dim resultado As String
        Dim vcant, vimporte, vubi, vdatosant As New DataTable
        Dim cantant As Integer = 0
        Dim nuevalinea As Boolean = False

        If dg_lineas.SelectedRows.Count = 0 Then Exit Sub

        If list_ubicacion.SelectedValue = "" Then
            MsgBox("Seleccionar una ubicación.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If list_accion.Text = "Depositar" And CInt(dg_lineas.SelectedRows(0).Cells("c.Rec").Value) < CInt(dg_lineas.SelectedRows(0).Cells("c.Dep").Value) + CInt(txt_cantidad.Text) And CInt(txt_cantidad.Text) > 0 Then
            MsgBox("No se puede depositar mas cantidad de la recogida", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If validar_ubicacion(txt_articulo.Text, list_alm.SelectedValue, list_ubicacion.SelectedValue) = False Then
            'monto el sql para luego añadirlo antes del update  
            sql_añadir_ubi = "insert into ubicaciones(codalmacen,codarticulo,ubicacion,tipo,cantidad,observaciones) "
            sql_añadir_ubi &= "values ('" & list_alm.SelectedValue & "','" & txt_articulo.Text & "','" & list_ubicacion.SelectedValue & "','Picking',0,'Ubicaciones/reaprov. no existe ubi.') "
        End If


        If txt_cantidad.Text = 0 Then
            If MsgBox("Seguro que quiere eliminar la cantidad preparada?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

            'Obtener la cantidad que tiene hasta el momento la linea preparada para sumarla al stock y dejar el stock en el picking cuadrado
            If list_accion.Text = "Recoger" Then sql = "select cantidad_recogida "
            If list_accion.Text = "Depositar" Then sql = "select cantidad_depositada "
            sql &= " from reaprov_ubi_lin where codreaprov=" & list_codreaprov_ubi.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
            vcant = conexion.TablaxCmd(constantes.bd_intranet, sql)
            sql = ""
            If vcant.Rows.Count = 1 Then
                cantant = CInt(vcant.Rows(0)(0))
                'Dejar stock como estaba en la ubicacion 
                If list_accion.Text = "Recoger" Then sql = "update ubicaciones set cantidad=cantidad+(" & cantant & ") "
                If list_accion.Text = "Depositar" Then sql = " update ubicaciones set cantidad=cantidad-(" & cantant & ") "
                sql &= "where codalmacen='" & list_alm.SelectedValue & "' and ubicacion='" & list_ubicacion.SelectedValue & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & list_alm.SelectedValue & "' and ubicacion='" & list_ubicacion.SelectedValue & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad asc ) "
            End If

            'Actualizar la linea de reaprov
            If list_accion.Text = "Recoger" Then
                sql &= "update reaprov_ubi_lin set codalmacen_recogida='" & list_alm.SelectedValue & "',ubicacion_recogida='" & list_ubicacion.SelectedValue & "',cantidad_recogida=0,usuario_recogida='" & frm_principal.lab_idoperario.Text & "',fecha_recogida=getdate() where codreaprov=" & list_codreaprov_ubi.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
                'Poner la cantidad en el grid
                dg_lineas.SelectedRows(0).Cells("c.Rec").Value = 0
            End If
            If list_accion.Text = "Depositar" Then
                sql &= "update reaprov_ubi_lin set codalmacen_depositada='" & list_alm.SelectedValue & "',ubicacion_depositada='" & list_ubicacion.SelectedValue & "',cantidad_depositada=0,usuario_depositada='" & frm_principal.lab_idoperario.Text & "',fecha_depositada=getdate() where codreaprov=" & list_codreaprov_ubi.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
                'Poner la cantidad en el grid
                dg_lineas.SelectedRows(0).Cells("c.dep").Value = 0
            End If

            '05/03/2020 Si estamos anulando una depositanda que anteriormente estaba reservado desde el mmontorbolsas, hay q marcar esta cantidad como reservada de nuevo y eliminar la cantidad reservada del pick
            Try
                If list_accion.Text = "Depositar" Then
                    Dim cres As Integer
                    Dim vres As New DataTable
                    Dim sqlres, sqlsel As String
                    Dim cantaux As Integer = cantant
                    sqlsel = "select numreferencia,codempresaalbaran,codperiodoalbaran,seralbaran,codalbaran,codlinea,CantreservaHistorico-CantReserva as cantdevolver from MonitorBolsasReservasReaprov "
                    sqlsel &= "where codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' and codreaprov=" & list_codreaprov_ubi.Text & " "
                    sqlsel &= "and CantreservaHistorico <> CantReserva "
                    sqlsel &= "order by fecha asc "
                    vres = conexion.TablaxCmd(constantes.bd_intranet, sqlsel)
                    sqlres = ""
                    cres = 0
                    Do While cres < vres.Rows.Count And cantaux > 0
                        If cantaux >= vres.Rows(cres).Item("cantdevolver") Then
                            sqlres &= "update MonitorBolsasReservasReaprov set observaciones='Cantidad devuelta a reserva al anular cant.depositada.', cantreserva +=" & vres.Rows(cres).Item("cantdevolver") & " where numreferencia='" & vres.Rows(cres).Item("numreferencia").ToString & "'  "
                            sqlres &= "update MonitorBolsasReservas set cantReservadaPick-=" & vres.Rows(cres).Item("cantdevolver") & ", CantReservadaReaprov +=" & vres.Rows(cres).Item("cantdevolver") & " where codempresaalbaran=" & vres.Rows(cres).Item("codempresaalbaran") & " and CodPeriodoAlbaran=" & vres.Rows(cres).Item("CodPeriodoAlbaran") & " and SerAlbaran='" & vres.Rows(cres).Item("SerAlbaran") & "' and CodAlbaran=" & vres.Rows(cres).Item("CodAlbaran") & " and CodLinea=" & vres.Rows(cres).Item("CodLinea") & " "
                            cantaux -= vres.Rows(cres).Item("cantdevolver")
                        Else
                            sqlres &= "update MonitorBolsasReservasReaprov set observaciones='Cantidad devuelta a reserva al anular cant.depositada.', cantreserva +=" & cantaux & " where numreferencia='" & vres.Rows(cres).Item("numreferencia").ToString & "'  "
                            sqlres &= "update MonitorBolsasReservas set cantReservadaPick-=" & cantaux & ",CantReservadaReaprov +=" & cantaux & " where codempresaalbaran=" & vres.Rows(cres).Item("codempresaalbaran") & " and CodPeriodoAlbaran=" & vres.Rows(cres).Item("CodPeriodoAlbaran") & " and SerAlbaran='" & vres.Rows(cres).Item("SerAlbaran") & "' and CodAlbaran=" & vres.Rows(cres).Item("CodAlbaran") & " and CodLinea=" & vres.Rows(cres).Item("CodLinea") & " "
                            cantaux = 0
                        End If
                        cres += 1
                    Loop
                    resultado = conexion.ExecuteCmd(constantes.bd_intranet, sqlres)
                End If

            Catch ex As Exception
                funciones.Enviar_correo("vicentesoler@joumma.com", "Error linea 273 en ubicaciones", ex.ToString)
            End Try



            'Añadir registro. Se hace antes de ejecutar el insert para poder obtener la cantidad anterior antes de que sea modificada
            If list_accion.Text = "Recoger" Then funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, CInt(cantant), list_alm.SelectedValue, list_ubicacion.SelectedValue, "Reaprov_ubi:" & list_codreaprov_ubi.Text, "Ubicaciones-Reaprov_ubi", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
            If list_accion.Text = "Depositar" Then funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, CInt(cantant) * -1, list_alm.SelectedValue, list_ubicacion.SelectedValue, "Reaprov_ubi:" & list_codreaprov_ubi.Text, "Ubicaciones-Reaprov_ubi", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        Else

            'Mirar si la cantidad a ubicar supera la cantidad disponible en la ubicacion
            'Solo si estamos depositando y la ubicacion no es de consolidacion
            If list_accion.Text = "Depositar" And list_ubicacion.Text <> constantes.consolidacion_A07 And list_ubicacion.Text <> constantes.consolidacion_A01 And list_ubicacion.Text <> constantes.consolidacion_A08 Then
                Dim vdatos As New DataTable
                Dim cant_disp As Integer
                sql = "select top 1 cantidad,cantmax from ubicaciones where codalmacen='" & list_alm.SelectedValue & "' and  codarticulo='" & txt_articulo.Text & "' and ubicacion='" & list_ubicacion.SelectedValue & "' order by cantidad  asc "
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vdatos.Rows.Count = 1 Then
                    cant_disp = vdatos.Rows(0).Item("cantmax") - vdatos.Rows(0).Item("cantidad")
                    If cant_disp - txt_cantidad.Text < 0 Then
                        If MsgBox("En la ubicación elegida caben solo " & cant_disp & " unidades, continuar de todas formas?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                            Exit Sub
                        End If
                    End If
                Else
                    MsgBox("No se ha encontrado datos para esta ubicación.")
                    Exit Sub
                End If
            End If

            sql = ""
            'Actualizar la ubicacion 
            If list_accion.Text = "Recoger" Then
                'Recorrer los palets que tiene la ubicacion e ir restando de cada palet la cantidad
                Dim cpal As Integer
                Dim vpal As New DataTable
                Dim sql_pal As String
                Dim cantaux As Integer = CInt(txt_cantidad.Text)
                Dim cantadescontar As Integer
                sql_pal = "select id,cantidad from ubicaciones where codalmacen='" & list_alm.SelectedValue & "' and  codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "

                'sql_pal = "and tipo='" & dg_lineas.SelectedRows(0).Cells("TR").Value.ToString.Trim & "'"
                'Esto lo quite porque me di cuenta de que si la linea de reaprov tenia como tipo recogida picking y luego cuando se ponen a recoger
                'seleccionan una ubicacion de pulmon (esto me lo pidieron) , el tipo no es el que pone en la línea y por tanto la consulta no obtenia ninguna ubicacion
                'Y no descontaba de ningun sitio

                sql_pal &= "and ubicacion='" & list_ubicacion.SelectedValue & "' order by cantidad asc "
                vpal = conexion.TablaxCmd(constantes.bd_intranet, sql_pal)
                If vpal.Rows.Count = 0 Then
                    MsgBox("Ninguna ubicación será actualizada, por favor avisar a informática", MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
                cpal = 0
                Do While cpal < vpal.Rows.Count And cantaux <> 0
                    If vpal.Rows(cpal).Item("cantidad") > cantaux Then cantadescontar = cantaux Else cantadescontar = vpal.Rows(cpal).Item("cantidad")
                    sql &= "update ubicaciones set cantidad=isnull(cantidad,0)-(" & cantadescontar & ") where id=" & vpal.Rows(cpal).Item("id")
                    cantaux -= cantadescontar
                    cpal += 1
                Loop
            End If
            If list_accion.Text = "Depositar" Then
                sql &= sql_añadir_ubi & " update ubicaciones set cantidad=isnull(cantidad,0)+" & CInt(txt_cantidad.Text) & " "
                sql &= "where codalmacen='" & list_alm.SelectedValue & "' and ubicacion='" & list_ubicacion.SelectedValue & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' "
                'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
                sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & list_alm.SelectedValue & "' and ubicacion='" & list_ubicacion.SelectedValue & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' order by cantidad asc ) "

            End If

            'Actualizar la linea de reaprov
            If list_accion.Text = "Recoger" Then
                sql &= "update reaprov_ubi_lin set codalmacen_recogida='" & list_alm.SelectedValue & "',ubicacion_recogida='" & list_ubicacion.SelectedValue & "',cantidad_recogida=cantidad_recogida+" & CInt(txt_cantidad.Text) & ",usuario_recogida='" & frm_principal.lab_idoperario.Text & "',fecha_recogida=getdate() where codreaprov=" & list_codreaprov_ubi.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
                'Poner la cantidad y ubicacion(por si ha elegido otra) en el grid
                dg_lineas.SelectedRows(0).Cells("c.Rec").Value = CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("c.Rec").Value.ToString)
                dg_lineas.SelectedRows(0).Cells("Ubi.Rec").Value = list_ubicacion.SelectedValue
                dg_lineas.SelectedRows(0).Cells("Alm.Rec").Value = list_alm.SelectedValue
            End If
            If list_accion.Text = "Depositar" Then
                sql &= "update reaprov_ubi_lin set codalmacen_depositada='" & list_alm.SelectedValue & "',ubicacion_depositada='" & list_ubicacion.SelectedValue & "',cantidad_depositada=cantidad_depositada+" & CInt(txt_cantidad.Text) & ",usuario_depositada='" & frm_principal.lab_idoperario.Text & "',fecha_depositada=getdate() where codreaprov=" & list_codreaprov_ubi.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "
                'Poner la cantidad y ubicacion(por si ha elegido otra) en el grid
                dg_lineas.SelectedRows(0).Cells("c.dep").Value = CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("c.dep").Value.ToString)
                dg_lineas.SelectedRows(0).Cells("Ubi.Dep").Value = list_ubicacion.SelectedValue
                dg_lineas.SelectedRows(0).Cells("Alm.Dep").Value = list_alm.SelectedValue
            End If
            'Si esta depositando y la cantidad depositada ahora + la que estaba de antes es menor que la cantidad recogida, preguntar si quiere crear una nueva ubicacion para esta cantidad
            'Ahora lo que hago es crear el resto en una nueva linea y preguntar si quieren crear una nueva ubiaccion. Pero el resto siempre a una nueva linea
            'If list_accion.Text = "Depositar" And (CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("c.Dep").Value)) < CInt(dg_lineas.SelectedRows(0).Cells("c.Rec").Value) Then
            'Antes sumaba la cantidad que estamos añadiendo en el textbox mas la cantidad que ya estaba ubicada (en el grid) pero me di cuenta que
            'La cantidad en el grid ya esta actualizada, se actualiza unas lineas mas arriba, por tanto no hay que sumar nada, solo comprobar el grid.
            If list_accion.Text = "Depositar" And CInt(dg_lineas.SelectedRows(0).Cells("c.Dep").Value) < CInt(dg_lineas.SelectedRows(0).Cells("c.Rec").Value) Then
                Dim ubicacion_nueva As String = ""
                If MsgBox("Se añadirá una nueva línea con la cantidad no depositada, quiere crear una nueva ubicación?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    frm_nuevaubicacion.lab_ref.Text = dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim
                    frm_nuevaubicacion.list_tipo.SelectedIndex = -1
                    frm_nuevaubicacion.txt_altura.Text = ""
                    frm_nuevaubicacion.txt_cantmax.Text = ""
                    frm_nuevaubicacion.txt_pasillo.Text = ""
                    frm_nuevaubicacion.txt_portal.Text = ""
                    frm_nuevaubicacion.txt_tamaño.Text = ""
                    frm_nuevaubicacion.lab_almacen_aux.Text = list_alm.SelectedValue
                    frm_nuevaubicacion.ShowDialog()
                    If frm_nuevaubicacion.txt_pasillo.Text <> "" Then
                        ubicacion_nueva = frm_nuevaubicacion.txt_pasillo.Text & "-" & frm_nuevaubicacion.txt_portal.Text & "-" & frm_nuevaubicacion.txt_altura.Text
                    End If
                End If

                'Crear una nueva línea  con el resto de la cantidad
                nuevalinea = True
                Dim vdatos As New DataTable
                Dim cant_rest As Integer = dg_lineas.SelectedRows(0).Cells("C.Rec").Value - txt_cantidad.Text
                vdatos = dg_lineas.DataSource
                vdatos.Rows.Add({dg_lineas.Rows.Count, dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString, dg_lineas.SelectedRows(0).Cells("Descripcion").Value.ToString, cant_rest, dg_lineas.SelectedRows(0).Cells("Alm.Rec").Value.ToString, dg_lineas.SelectedRows(0).Cells("Ubi.Rec").Value.ToString, cant_rest, "", "", 0})
                sql &= "insert into reaprov_ubi_lin(codreaprov,codlinea ,codarticulo ,cantidad,codalmacen_recogida ,ubicacion_recogida ,cantidad_recogida,tipo_recogida,codalmacen_depositada,ubicacion_depositada,tipo_depositada,observaciones) "
                sql &= "values (" & list_codreaprov_ubi.Text & ",(select max(codlinea)+1 from reaprov_ubi_lin where codreaprov=" & list_codreaprov_ubi.Text & "),'" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "'," & cant_rest & ",'" & dg_lineas.SelectedRows(0).Cells("Alm.Rec").Value.ToString & "','" & dg_lineas.SelectedRows(0).Cells("Ubi.Rec").Value.ToString & "'," & cant_rest & ",'" & dg_lineas.SelectedRows(0).Cells("TR").Value.ToString & "','" & dg_lineas.SelectedRows(0).Cells("Alm.Dep").Value.ToString & "','" & ubicacion_nueva & "','" & dg_lineas.SelectedRows(0).Cells("TD").Value.ToString & "','Linea creada al depositar menos cantidad que la recogida' ) "

                'Modificar la linea para que la cantidad y la recogida sea la que se ha depositado. El resto va a la linea que estamos creando
                dg_lineas.SelectedRows(0).Cells("Cant.").Value = txt_cantidad.Text
                dg_lineas.SelectedRows(0).Cells("c.Rec").Value = txt_cantidad.Text
                sql &= "update reaprov_ubi_lin set cantidad=" & txt_cantidad.Text & ",cantidad_recogida=" & txt_cantidad.Text & " where codreaprov=" & list_codreaprov_ubi.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value.ToString & " "

            End If
            'Añadir registro. Se hace antes de ejecutar el insert para poder obtener la cantidad anterior antes de que sea modificada
            If list_accion.Text = "Recoger" Then funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, (CInt(txt_cantidad.Text) * -1), list_alm.SelectedValue, list_ubicacion.SelectedValue, "Reaprov_ubi:" & list_codreaprov_ubi.Text, "Ubicaciones-Reaprov_ubi", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
            If list_accion.Text = "Depositar" Then funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim, CInt(txt_cantidad.Text), list_alm.SelectedValue, list_ubicacion.SelectedValue, "Reaprov_ubi:" & list_codreaprov_ubi.Text, "Ubicaciones-Reaprov_ubi", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)


            '05/03/2020 Si estamos depositando un reaprov que estaba reservado desde el mmontorbolsas, hay q marcar esta cantidad como reservada ya de pick y eliminar la cantidad reservada de reaprov
            'esto es necesario prque sino al estar la cantidad ya depositada, si lanzan una bolsa , esta podría reservar de este stock que acabamos de dejar
            Try
                If list_accion.Text = "Depositar" Then
                    Dim cres As Integer
                    Dim vres As New DataTable
                    Dim sqlres, sqlsel As String
                    Dim cantaux As Integer = txt_cantidad.Text
                    sqlsel = "select numreferencia,codempresaalbaran,codperiodoalbaran,seralbaran,codalbaran,codlinea,cantReserva from MonitorBolsasReservasReaprov "
                    sqlsel &= "where codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim & "' and codreaprov=" & list_codreaprov_ubi.Text & " "
                    sqlsel &= "order by fecha asc "
                    vres = conexion.TablaxCmd(constantes.bd_intranet, sqlsel)
                    sqlres = ""
                    cres = 0
                    Do While cres < vres.Rows.Count And cantaux > 0
                        If cantaux >= vres.Rows(cres).Item("cantreserva") Then
                            sqlres &= "update MonitorBolsasReservasReaprov set observaciones='Cantidad anulada de reserva al depositar el reaprov.', cantreserva -=" & vres.Rows(cres).Item("cantreserva") & " where numreferencia='" & vres.Rows(cres).Item("numreferencia").ToString & "'  "
                            sqlres &= "update MonitorBolsasReservas set cantReservadaPick+=" & vres.Rows(cres).Item("cantreserva") & ", CantReservadaReaprov -=" & vres.Rows(cres).Item("cantreserva") & " where codempresaalbaran=" & vres.Rows(cres).Item("codempresaalbaran") & " and CodPeriodoAlbaran=" & vres.Rows(cres).Item("CodPeriodoAlbaran") & " and SerAlbaran='" & vres.Rows(cres).Item("SerAlbaran") & "' and CodAlbaran=" & vres.Rows(cres).Item("CodAlbaran") & " and CodLinea=" & vres.Rows(cres).Item("CodLinea") & " "
                            cantaux -= vres.Rows(cres).Item("cantreserva")
                        Else
                            sqlres &= "update MonitorBolsasReservasReaprov set observaciones='Cantidad anulada de reserva al depositar el reaprov.', cantreserva -=" & cantaux & " where numreferencia='" & vres.Rows(cres).Item("numreferencia").ToString & "'  "
                            sqlres &= "update MonitorBolsasReservas set cantReservadaPick+=" & cantaux & ",CantReservadaReaprov -=" & cantaux & " where codempresaalbaran=" & vres.Rows(cres).Item("codempresaalbaran") & " and CodPeriodoAlbaran=" & vres.Rows(cres).Item("CodPeriodoAlbaran") & " and SerAlbaran='" & vres.Rows(cres).Item("SerAlbaran") & "' and CodAlbaran=" & vres.Rows(cres).Item("CodAlbaran") & " and CodLinea=" & vres.Rows(cres).Item("CodLinea") & " "
                            cantaux = 0
                        End If
                        cres += 1
                    Loop
                    resultado = conexion.ExecuteCmd(constantes.bd_intranet, sqlres)
                End If

            Catch ex As Exception
                funciones.Enviar_correo("vicentesoler@joumma.com", "Error linea 273 en ubicaciones", ex.ToString)
            End Try

        End If

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)

        If resultado <> "0" Then
            MsgBox("Se produjo un error.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'Pintar  celdas
        If CInt(dg_lineas.SelectedRows(0).Cells("c.Rec").Value.ToString) = CInt(dg_lineas.SelectedRows(0).Cells("c.dep").Value.ToString) And CInt(dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString) = CInt(dg_lineas.SelectedRows(0).Cells("c.dep").Value.ToString) Then
            dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            dg_lineas.SelectedRows(0).Cells("c.Rec").Style.BackColor = Color.Green
            dg_lineas.SelectedRows(0).Cells("c.dep").Style.BackColor = Color.Green
        Else
            dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            If list_accion.Text = "Recoger" Then
                Select Case CInt(dg_lineas.SelectedRows(0).Cells("c.Rec").Value.ToString)
                    Case 0 : dg_lineas.SelectedRows(0).Cells("c.Rec").Style.BackColor = Color.White
                    Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).Cells("c.Rec").Style.BackColor = Color.Green
                    Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).Cells("c.Rec").Style.BackColor = Color.Yellow
                    Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).Cells("c.Rec").Style.BackColor = Color.Red
                End Select
            End If

            If list_accion.Text = "Depositar" Then
                Select Case CInt(dg_lineas.SelectedRows(0).Cells("c.dep").Value.ToString)
                    Case 0 : dg_lineas.SelectedRows(0).Cells("c.dep").Style.BackColor = Color.White
                    Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).Cells("c.dep").Style.BackColor = Color.Green
                    Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).Cells("c.dep").Style.BackColor = Color.Yellow
                    Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString : dg_lineas.SelectedRows(0).Cells("c.dep").Style.BackColor = Color.Red
                End Select
            End If
        End If

        If nuevalinea = True Then
            Dim vlin As New DataTable
            Dim cont As Integer
            'Si se ha insertado una nueva linea por intentar ubicar en un picking mas cantidad de la que cabe, se debe refrescar el grid para que aparezca la línea nueva
            cargar_lineas()
            'Situar en la fila que se acaba de crear
            sql = "select top 1 codlinea from reaprov_ubi_lin where codreaprov=" & list_codreaprov_ubi.Text & " order by codlinea desc"
            vlin = conexion.TablaxCmd(constantes.bd_intranet, sql)
            cont = 0
            Do While cont < dg_lineas.Rows.Count
                If dg_lineas.Rows(cont).Cells("Lin.").Value = vlin.Rows(0)(0) Then
                    dg_lineas.Item(0, cont).Selected = True
                    Exit Do
                End If
                cont += 1
            Loop
        Else
            If dg_lineas.CurrentRow.Index < dg_lineas.Rows.Count - 1 Then
                dg_lineas.Item(0, dg_lineas.CurrentRow.Index + 1).Selected = True
            Else
                dg_lineas.Item(0, dg_lineas.CurrentRow.Index).Selected = False
                txt_articulo.Text = ""
                txt_articulo.BackColor = Color.White
                lab_descripcion.Text = ""
                txt_cantidad.Text = ""
                list_alm.DataSource = Nothing
                list_ubicacion.DataSource = Nothing
                btn_ok.Enabled = False
            End If
        End If


        btn_finalizar.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub

    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql As String = ""
        Dim resultado As String
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim proxmov As Integer = 0
        Dim numrefmov As String = ""
        Dim lineasprep As Integer = 0
        Dim pedidoincompleto As Boolean = False
        Dim cambioalmacen As Boolean = False
        Dim pedidoincoherente As Boolean = False
        Dim lin_mov As Integer


        'Revisar si esta completado y si alguna ref. cambia de almacen
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("c.Rec").Value <> dg_lineas.Rows(cont).Cells("c.dep").Value Then
                MsgBox("La referencia " & dg_lineas.Rows(cont).Cells("Ref.").Value & " no tiene la misma cantidad recogida que depositada, no se puede finalizar.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            If dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("c.Rec").Value Or dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("c.dep").Value Then
                pedidoincompleto = True
            End If
            If dg_lineas.Rows(cont).Cells("c.Rec").Value > 0 Then
                lineasprep += 1
            End If
            If dg_lineas.Rows(cont).Cells("Alm.Rec").Value <> dg_lineas.Rows(cont).Cells("Alm.Dep").Value And dg_lineas.Rows(cont).Cells("c.Rec").Value <> 0 Then
                cambioalmacen = True
            End If
            cont += 1
        Loop


        If pedidoincompleto = True Then
            If MsgBox("El reaprov. no está completamente terminado, finalizar de todos modos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
        End If


        If cambioalmacen = True Then
            'Cabecera de movimiento. 
            resultado = funciones.crear_movimiento_inase(frm_principal.codempresa, proxmov, "Movimiento para Reaprovisionamiento ubicaciones", numrefmov)
            If resultado <> "OK" Then
                MsgBox("Se produjo un error al crear la cabecara del movimiento", MsgBoxStyle.Critical)
                Exit Sub
            End If

            cont = 0
            lin_mov = 1
            sql = ""
            Do While cont < dg_lineas.Rows.Count
                'If dg_lineas.Rows(cont).Cells("Ref.").Value = "3172961" Or dg_lineas.Rows(cont).Cells("Ref.").Value = "4269561" Or dg_lineas.Rows(cont).Cells("Ref.").Value = "5191961" Or dg_lineas.Rows(cont).Cells("Ref.").Value = "6079861" Or dg_lineas.Rows(cont).Cells("Ref.").Value = "66824A0" Then
                '    cont = cont
                'End If

                If dg_lineas.Rows(cont).Cells("Alm.Rec").Value <> dg_lineas.Rows(cont).Cells("Alm.Dep").Value Then
                    'Lineas Movimiento almacen
                    sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacenori,ubicacionori,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                    sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & frm_principal.codempresa & "," & Now.Year & "," & proxmov & "," & cont + 1 & ",'" & dg_lineas.Rows(cont).Cells("Alm.Rec").Value & "','XXX','" & dg_lineas.Rows(cont).Cells("Alm.Dep").Value & "','XXX','" & dg_lineas.Rows(cont).Cells("Ref.").Value & "'," & dg_lineas.Rows(cont).Cells("c.Rec").Value & ",'Salida por Reaprov.') "
                    'Eliminar la referencia de la ubicacion origen si se ha quedado a 0. La cantidad ya se ha restado al marcar la linea.
                    'Solo si la ubicacion que se ha quedado a 0 es un pulmon. Pedido por Sergio
                    sql &= "delete intranet.dbo.ubicaciones where tipo='Pulmon' and codarticulo='" & dg_lineas.Rows(cont).Cells("Ref.").Value & "' and codalmacen='" & dg_lineas.Rows(cont).Cells("Alm.Rec").Value & "' and ubicacion='" & dg_lineas.Rows(cont).Cells("Ubi.Rec").Value & "' and cantidad=0 "
                    lin_mov += 1
                End If
                cont += 1
            Loop

            resultado = conexion.Executetransact(constantes.bd_inase, sql)
            If resultado <> "0" Then
                resultado = funciones.borrar_movimiento_inase(frm_principal.codempresa, numrefmov)
                MsgBox("Se produjo un error al insertar las lineas del movimiento. El Reaprov.Ubi. no ha sido finalizado.", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        'Eliniar las ubicaciones que se han quedado a 0. Confirmado con Sergio
        sql = "delete from ubicaciones where cantidad=0 and tipo='Pulmon' and codarticulo collate Modern_Spanish_CI_AS in (select codarticulo from reaprov_ubi_lin where codreaprov =" & list_codreaprov_ubi.Text & ") "

        'Marcar como procesado el reaprov
        sql &= "update reaprov_ubi set estado='Cerrado',procesado=1,ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codreaprov=" & list_codreaprov_ubi.Text
        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado = "0" Then
            MsgBox("Movimiento de reaprovisionamiento finalizado correctamente", MsgBoxStyle.Information)

            'Ya no hace falta. Pedido por Susana
            'Enviar confirmación a Susana
            'sql = "select albaranesafectados from reaprov_ubi where codreaprov=" & list_codreaprov_ubi.Text
            'vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

            'Enviar_correo("lanzamiento.pedidos@joumma.com", "Reaprovisionamiento " & list_codreaprov_ubi.Text & " finalizado.", "Los albaranes que estaban afectados por este reaprovisionamiento son " & vdatos.Rows(0)(0).ToString)
        Else
            MsgBox("Se produjo un error al marcar el reaprov. como procesado. Avisar a Informática.", MsgBoxStyle.Information)
        End If

        limpiar(True, "Finalizar")
        llenar_reaprov()
    End Sub

    Public Sub Guardar_datos_tiempo(Optional ByVal ObsGuardarTiempos As String = "")
        Dim TiempoEmpleado As Integer
        Dim sql As String
        Try
            TiempoEmpleado = DateDiff(DateInterval.Second, fecha_inicio, fecha_fin)
            sql = "insert into albaranes_tiempos (CodReaprov,idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(" & list_codreaprov_ubi.Text & "," & frm_principal.lab_idoperario.Text & ",'Reaprovisionamiento','" & fecha_inicio & "','" & fecha_fin & "'," & TiempoEmpleado & ",0,'" & ObsGuardarTiempos & "' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        Catch ex As Exception

        End Try

    End Sub

    Public Sub limpiar(Optional ByVal GuardarTiempos As Boolean = True, Optional ByVal ObsGuardarTiempos As String = "")
        'Guardar datos de operario
        fecha_fin = Now
        If GuardarTiempos Then Guardar_datos_tiempo(ObsGuardarTiempos)

        If list_codreaprov_ubi.Text <> "" Then conexion.Executetransact(constantes.bd_intranet, "update reaprov_ubi set estado='Cerrado' where codreaprov=" & list_codreaprov_ubi.Text)
        p_filtro.Enabled = True
        txt_articulo.Text = ""
        txt_articulo.BackColor = Color.White
        lab_descripcion.Text = ""
        txt_cantidad.Text = ""
        list_ubicacion.DataSource = Nothing
        list_ubicacion.Items.Clear()
        btn_ok.Enabled = False
        btn_finalizar.Enabled = False
        btn_cerrar.Enabled = False
        btn_aparcar.Enabled = False
        dg_lineas.DataSource = Nothing
        dg_lineas.Rows.Clear()
        dg_lineas.Enabled = False
        imagen.ImageLocation = ""
        list_codreaprov_ubi.Enabled = True
        txt_busq_ref.Enabled = True
        list_codreaprov_ubi.SelectedIndex = -1
        list_accion.SelectedIndex = -1
        list_accion.Enabled = False
        lab_fecha.Text = ""
        txt_AlbAfect.Text = ""
    End Sub


    Private Sub list_codreaprov_ubi_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_codreaprov_ubi.SelectedIndexChanged
        If list_codreaprov_ubi.SelectedIndex = -1 Then
            limpiar(False)
        Else
            cargainicial = True
            datos_cabecera()
            cargainicial = False
            If dg_lineas.Rows.Count > 0 Then
                list_codreaprov_ubi.Enabled = False
                txt_busq_ref.Enabled = False
                list_accion.Enabled = True
            Else
                list_codreaprov_ubi.Enabled = True
                txt_busq_ref.Enabled = True
                list_accion.Enabled = False
            End If
        End If


    End Sub

    Private Sub list_codreaprov_ubi_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles list_codreaprov_ubi.KeyUp
        If e.KeyValue = Keys.Enter Then
            If list_codreaprov_ubi.Text.Length = 13 Then
                list_codreaprov_ubi.Text = list_codreaprov_ubi.Text.Substring(7, 5)
            End If
            cargainicial = True
            datos_cabecera()
            cargainicial = False
            If dg_lineas.Rows.Count > 0 Then
                list_codreaprov_ubi.Enabled = False
                txt_busq_ref.Enabled = False
                list_accion.Enabled = True
            Else
                list_codreaprov_ubi.Enabled = True
                txt_busq_ref.Enabled = True
                list_accion.Enabled = False
            End If
        End If
    End Sub


    Private Sub list_accion_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_accion.SelectedIndexChanged
        dg_lineas.Enabled = True
        p_ref.Enabled = True
        list_alm.DataSource = Nothing
        list_ubicacion.DataSource = Nothing
        txt_articulo.Text = ""
        txt_cantidad.Text = 0
        dg_lineas.ClearSelection()
        dg_lineas.CurrentCell = Nothing
        txt_act.Text = 0
        txt_max.Text = 0
        If list_accion.Text = "Depositar" And (frm_principal.PermReaprovUbiModMaxAct = True) Then
            txt_max.Enabled = True
            txt_act.Enabled = True
        Else
            txt_max.Enabled = False
            txt_act.Enabled = False
        End If
        btn_OtraUbi.Enabled = False
        btn_EliminarUbi.Enabled = False
        txt_articulo.Focus()
    End Sub

    Private Sub dg_lineas_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_lineas.CellEnter

        If cargainicial = True Then Exit Sub

        imagen.ImageLocation = "\\jserver\BAJA\" & dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value & ".jpg"
        imagen.SizeMode = PictureBoxSizeMode.StretchImage
        txt_articulo.Text = dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value
        lab_descripcion.Text = dg_lineas.Rows(e.RowIndex).Cells("Descripcion").Value
        If list_accion.Text = "Recoger" Then
            If dg_lineas.Rows(e.RowIndex).Cells("c.Rec").Value <> 0 Then list_ubicacion.Enabled = False Else list_ubicacion.Enabled = True
            If dg_lineas.Rows(e.RowIndex).Cells("c.Rec").Value <> 0 Then list_alm.Enabled = False Else list_alm.Enabled = True
            txt_cantidad.Text = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value - dg_lineas.Rows(e.RowIndex).Cells("c.Rec").Value
            Select Case dg_lineas.Rows(e.RowIndex).Cells("c.Rec").Value
                Case 0 : txt_articulo.BackColor = Color.White
                Case Is = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Green
                Case Is < dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Yellow
                Case Is > dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Red
            End Select
            funciones.llenar_almacenes(list_alm)
            list_alm.SelectedValue = dg_lineas.Rows(e.RowIndex).Cells("Alm.Rec").Value
            funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.Rows(e.RowIndex).Cells("TR").Value, "", "", "SI")
            list_ubicacion.SelectedValue = dg_lineas.Rows(e.RowIndex).Cells("Ubi.Rec").Value
            If list_ubicacion.SelectedValue = "" Then lab_tipo.Text = ""
        End If
        If list_accion.Text = "Depositar" Then
            If dg_lineas.Rows(e.RowIndex).Cells("c.dep").Value <> 0 Then list_ubicacion.Enabled = False Else list_ubicacion.Enabled = True
            If dg_lineas.Rows(e.RowIndex).Cells("c.dep").Value <> 0 Then list_alm.Enabled = False Else list_alm.Enabled = True
            txt_cantidad.Text = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value - dg_lineas.Rows(e.RowIndex).Cells("c.dep").Value
            Select Case dg_lineas.Rows(e.RowIndex).Cells("c.dep").Value
                Case 0 : txt_articulo.BackColor = Color.White
                Case Is = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Green
                Case Is < dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Yellow
                Case Is > dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value : txt_articulo.BackColor = Color.Red
            End Select
            funciones.llenar_almacenes(list_alm)
            list_alm.SelectedValue = dg_lineas.Rows(e.RowIndex).Cells("Alm.Dep").Value
            If list_alm.SelectedValue = "A07" Then
                'Añado la ubicacion de consolidacion en variable optional de la funcion llenar_ubicaciones
                ' porque en reaprov de A07, se dejará siempre en la ubicacion de consolidacion
                funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.Rows(e.RowIndex).Cells("TD").Value, constantes.consolidacion_A07, "Picking")
            ElseIf list_alm.SelectedValue = "A08" Then
                'Añado la ubicacion de consolidacion en variable optional de la funcion llenar_ubicaciones
                ' porque en reaprov de A08, se dejará siempre en la ubicacion de consolidacion
                funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.Rows(e.RowIndex).Cells("TD").Value, constantes.consolidacion_A08, "Picking")
            Else
                'Añado la ubicacion de consolidación en variable optional de la funcion llenar_ubicaciones
                'porque en reaprov de A01, se puede dejar en consolidacion.
                funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.Rows(e.RowIndex).Cells("TD").Value, constantes.consolidacion_A01, "Picking")
            End If
            list_ubicacion.SelectedValue = dg_lineas.Rows(e.RowIndex).Cells("Ubi.Dep").Value
            btn_OtraUbi.Enabled = True
        End If
        btn_EliminarUbi.Enabled = True

        'Para que no se pueda poner una cantidad negativa. 
        If txt_cantidad.Text < 0 Then txt_cantidad.Text = 0


        btn_ok.Enabled = True
        txt_articulo.Focus()
        txt_articulo.SelectAll()
    End Sub

    Private Sub list_alm_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_alm.DropDownClosed
        If list_accion.Text = "Depositar" Then
            funciones.llenar_ubicaciones(list_ubicacion, txt_articulo.Text, list_alm.SelectedValue, "Picking")
        End If
        If list_accion.Text = "Recoger" Then
            funciones.llenar_ubicaciones(list_ubicacion, txt_articulo.Text, list_alm.SelectedValue, "Pulmon")
        End If

    End Sub

    Private Sub btn_aparcar_Click(sender As System.Object, e As System.EventArgs) Handles btn_aparcar.Click
        Dim sql As String
        Dim resultado As String

        sql = "update reaprov_ubi set estado='Aparcado',ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codreaprov=" & list_codreaprov_ubi.Text
        resultado = conexion.Executetransact(constantes.bd_intranet, sql)

        limpiar(True, "Aparcar")

    End Sub

    Private Sub btn_cerrar_Click(sender As System.Object, e As System.EventArgs) Handles btn_cerrar.Click
        limpiar(True, "Cerrar")
    End Sub


  
    Private Sub dg_lineas_Sorted(sender As System.Object, e As System.EventArgs) Handles dg_lineas.Sorted
        pintar_lineas_grid()
    End Sub

    Private Sub list_ubicacion_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_ubicacion.SelectedIndexChanged
        Dim sql As String
        Dim vdatos As New DataTable

        If list_ubicacion.SelectedValue <> "" Then
            sql = "select isnull(sum(cantidad),0) as cantidad,isnull(max(cantmax),0) as cantmax,isnull(max(tipo),'') as tipo "
            sql &= "from ubicaciones where codalmacen='" & list_alm.SelectedValue & "' and codarticulo='" & txt_articulo.Text & "' "
            sql &= "and ubicacion='" & list_ubicacion.SelectedValue & "' "
            If list_accion.SelectedValue = "Recoger" Then sql &= "and tipo='Pulmon' "
            If list_accion.SelectedValue = "Depositar" Then sql &= "and tipo='Picking' "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count > 0 Then
                txt_act.Text = vdatos.Rows(0).Item("cantidad")
                txt_max.Text = vdatos.Rows(0).Item("cantmax")
                lab_tipo.Text = vdatos.Rows(0).Item("tipo")
            End If
        End If
    End Sub


    Private Sub txt_busq_ref_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_busq_ref.KeyUp
        Dim codarticulo As String = txt_articulo.Text

        txt_busq_ref.Text = txt_busq_ref.Text.Trim.ToUpper
        If dg_lineas.Rows.Count > 0 Then
            MsgBox("Cerrar el reaprov. en curso antes de buscar.")
            Exit Sub
        End If

        'If e.KeyCode = Keys.Enter Then
        '    llenar_reaprov()
        'End If

        If e.KeyCode = Keys.Enter Then
            If txt_busq_ref.Text.Length = 7 Or txt_busq_ref.Text.Length = 13 Then
                funciones.buscar_descripcion(txt_busq_ref.Text) ' Solo para recuperar el codarticulo.
                llenar_reaprov()
            End If
        End If
    End Sub

    Private Sub txt_act_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_act.KeyUp
        Dim sql As String
        Dim vdatos As New DataTable
        Dim resultado As String
        If e.KeyCode = Keys.Enter Then
            If MsgBox("Se va a actualizar la cantidad actual en la ubicación, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                'Consultar si la ubicación solo tiene un palet, en caso de q no, no dejará actualizar
                sql = "select id from ubicaciones where tipo='" & dg_lineas.SelectedRows(0).Cells("TD").Value & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "' and codalmacen='" & dg_lineas.SelectedRows(0).Cells("Alm.Dep").Value & "' and ubicacion='" & list_ubicacion.SelectedValue & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vdatos.Rows.Count = 0 Then
                    MsgBox("La ubicación no existe, revisar los datos.", MsgBoxStyle.Information)
                ElseIf vdatos.Rows.Count = 1 Then
                    funciones.añadir_reg_mov_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString, txt_act.Text, dg_lineas.SelectedRows(0).Cells("Alm.Dep").Value.ToString, list_ubicacion.SelectedValue, "Cambio valor", "Ubicaciones-Reaprov.Ubi.", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
                    sql = "update ubicaciones set cantidad=" & txt_act.Text & " where id=" & vdatos.Rows(0).Item("id")
                    resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                Else
                    MsgBox("La ubicación continene mas de un palet, no se puede actualizar la información.", MsgBoxStyle.Exclamation)
                End If
            End If
        End If
    End Sub


    Private Sub txt_max_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_max.KeyUp
        Dim sql As String
        Dim vdatos As New DataTable
        Dim resultado As String
        If e.KeyCode = Keys.Enter Then
            If MsgBox("Se va a actualizar la cantidad máxima en la ubicación, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                'Consultar si la ubicación solo tiene un palet, en caso de q no, no dejará actualizar
                sql = "select id from ubicaciones where tipo='" & dg_lineas.SelectedRows(0).Cells("TD").Value & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "' and codalmacen='" & dg_lineas.SelectedRows(0).Cells("Alm.Dep").Value & "' and ubicacion='" & list_ubicacion.SelectedValue & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vdatos.Rows.Count = 0 Then
                    MsgBox("La ubicación no existe, revisar los datos.", MsgBoxStyle.Information)
                ElseIf vdatos.Rows.Count = 1 Then
                    sql = "update ubicaciones set cantmax=" & txt_max.Text & ",fechaultmodificacion=getdate() where id=" & vdatos.Rows(0).Item("id")
                    resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                Else
                    MsgBox("La ubicación continene mas de un palet, no se puede actualizar la información.", MsgBoxStyle.Exclamation)
                End If
            End If
        End If
    End Sub

    Private Sub txt_act_Click(sender As System.Object, e As System.EventArgs) Handles txt_act.Click
        txt_act.SelectAll()
    End Sub

    Private Sub txt_max_Click(sender As System.Object, e As System.EventArgs) Handles txt_max.Click
        txt_max.SelectAll()
    End Sub

    Private Sub frm_reaprov_ubi_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        conexion.ExecuteCmd(constantes.bd_intranet, "Update reaprov_ubi set estado='Cerrado' where codreaprov=" & list_codreaprov_ubi.Text)
    End Sub

    Private Sub cb_FiltroRecA01_Click(sender As Object, e As EventArgs) Handles cb_FiltroRecA01.Click
        llenar_reaprov()
    End Sub

    Private Sub cb_FiltroRecA07_Click(sender As Object, e As EventArgs) Handles cb_FiltroRecA07.Click
        llenar_reaprov()
    End Sub

    Private Sub cb_FiltroDepA07_Click(sender As Object, e As EventArgs) Handles cb_FiltroDepA07.Click
        llenar_reaprov()
    End Sub

    Private Sub cb_FiltroDepA01_Click(sender As Object, e As EventArgs) Handles cb_FiltroDepA01.Click
        llenar_reaprov()
    End Sub

    Private Sub btn_OtraUbi_Click(sender As Object, e As EventArgs) Handles btn_OtraUbi.Click
        frm_nuevaubicacion.lab_ref.Text = dg_lineas.SelectedRows(0).Cells("Ref.").Value
        frm_nuevaubicacion.ShowDialog()
        If list_alm.SelectedValue = "A07" Then
            'Añado la ubicacion de consolidacion en variable optional de la funcion llenar_ubicaciones
            ' porque en reaprov de A07, se dejará siempre en la ubicacion de consolidacion
            funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.SelectedRows(0).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.SelectedRows(0).Cells("TD").Value, constantes.consolidacion_A07, "Picking")
        ElseIf list_alm.SelectedValue = "A08" Then
            'Añado la ubicacion de consolidacion en variable optional de la funcion llenar_ubicaciones
            ' porque en reaprov de A08, se dejará siempre en la ubicacion de consolidacion
            funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.SelectedRows(0).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.SelectedRows(0).Cells("TD").Value, constantes.consolidacion_A08, "Picking")
        Else
            'Añado la ubicacion de consolidación en variable optional de la funcion llenar_ubicaciones
            'porque en reaprov de A01, se puede dejar en consolidacion.
            funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.SelectedRows(0).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.SelectedRows(0).Cells("TD").Value, constantes.consolidacion_A01, "Picking")
        End If
        list_ubicacion.SelectedValue = dg_lineas.SelectedRows(0).Cells("Ubi.Dep").Value
    End Sub

    Private Sub txt_articulo_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_articulo.KeyUp
        Dim codarticulo As String = txt_articulo.Text
        Dim cont As Integer
        Dim evento

        If e.KeyCode = Keys.Enter Then
            If txt_articulo.Text.Length = 7 Or txt_articulo.Text.Length = 13 Then
                lab_descripcion.Text = funciones.buscar_descripcion(codarticulo)
                'Mirara si pertenece al pedido
                cont = 0
                Do While cont < dg_lineas.Rows.Count
                    If dg_lineas.Rows(cont).Cells("Ref.").Value = codarticulo Then
                        dg_lineas.CurrentCell = dg_lineas.Rows(cont).Cells(1)
                        dg_lineas.Rows(cont).Selected = True
                        evento = New DataGridViewCellEventArgs(0, cont)
                        dg_lineas_CellEnter(sender, evento)
                        btn_ok.Focus()
                        Exit Sub
                    End If
                    cont += 1
                Loop
                MsgBox("Esta referencia no pertence a la orden.", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub btn_EliminarUbi_Click(sender As Object, e As EventArgs) Handles btn_EliminarUbi.Click
        Dim sql As String = ""
        Dim resultado As String

        If MsgBox("Seguro que desea eliminar la ubicación " & list_ubicacion.SelectedValue & " para el artículo " & txt_articulo.Text & "?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            If txt_act.Text <> 0 Then
                MsgBox("Esta ubicación no está vacía, no se puede eliminar.")
                Exit Sub
            Else
                Sql &= "insert into ubicaciones_borradas(codalmacen,ubicacion,codarticulo,tipo,usuario,fecha,programa) "
                sql &= "values ('" & list_alm.SelectedValue & "','" & list_ubicacion.SelectedValue & "','" & txt_articulo.Text & "','" & lab_tipo.Text & "','" & frm_principal.lab_nomoperario.Text & "',getdate(),'Ubicaciones/Reaprov Ubi'); "
                sql &= "delete ubicaciones where codalmacen='" & list_alm.SelectedValue & "' and codarticulo='" & txt_articulo.Text & "' and tipo='" & lab_tipo.Text & "' and ubicacion='" & list_ubicacion.SelectedValue & "' "
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, Sql)
                If resultado <> "0" Then
                    MsgBox("Se producjo un error al eliminar la/s filas", MsgBoxStyle.Critical)
                Else
                    funciones.añadir_reg_mov_ubicaciones(txt_articulo.Text, 0, list_alm.SelectedValue, list_ubicacion.SelectedValue, "Eliminar Ubicación", "Ubicaciones-Reaprov Ubi", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text, 0)

                    If list_accion.Text = "Recoger" Then
                        funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.SelectedRows(0).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.SelectedRows(0).Cells("TR").Value, "", "", "SI")
                        list_ubicacion.SelectedValue = dg_lineas.SelectedRows(0).Cells("Ubi.Rec").Value
                    End If
                    If list_accion.Text = "Depositar" Then
                        If list_alm.SelectedValue = "A07" Then
                            funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.SelectedRows(0).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.SelectedRows(0).Cells("TD").Value, constantes.consolidacion_A07, "Picking")
                        ElseIf list_alm.SelectedValue = "A08" Then
                            funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.SelectedRows(0).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.SelectedRows(0).Cells("TD").Value, constantes.consolidacion_A08, "Picking")
                        Else
                            funciones.llenar_ubicaciones(list_ubicacion, dg_lineas.SelectedRows(0).Cells("Ref.").Value, list_alm.SelectedValue, dg_lineas.SelectedRows(0).Cells("TD").Value, constantes.consolidacion_A01, "Picking")
                        End If
                        list_ubicacion.SelectedValue = dg_lineas.SelectedRows(0).Cells("Ubi.Dep").Value
                    End If
                End If
            End If

        End If
    End Sub

    Private Sub dg_lineas_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_lineas.CellDoubleClick
        If e.ColumnIndex = 1 Then
            frm_ventana_ubi_art.txt_articulo.Text = txt_articulo.Text
            frm_ventana_ubi_art.ShowDialog()
        End If
    End Sub


End Class
