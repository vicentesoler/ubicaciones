﻿Public Class Frm_ImpresionDigital
    Dim contador As Integer
    Dim conexion As New Conector

    Private Sub Frm_ImpresionDigital_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer_tiempo.Enabled = False
        llenar_motivos()
        llenar_log()
        RecuperarDatosGuardados
    End Sub
    Public Sub RecuperarDatosGuardados()
        Dim vdatos As New DataTable
        Dim sql As String
        Dim horas, minutos, segundos As Integer

        sql = "select top 1 impresora,TiempoAcum,boton "
        sql &= "from OrdenesFabricacionlog ofl "
        sql &= "where ofl.codempresa=1 and ofl.codperiodo=" & lab_CodPeriodoOF.Text & " and ofl.seralbaranprov='OF' and ofl.codalbaranprov=" & lab_CodAlbaranOF.Text & " "
        sql &= "and accion='IMPRIMIR' and boton <> 'SALIR' " 'Elimino las de SALIR porq no tiene datos relevantes y al recuperar molesta
        sql &= "order by id desc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count = 1 Then
            list_impresoras.Text = vdatos.Rows(0).Item("impresora")

            'Quieren poder cambiar de impresora después de haber sido pausada y reanudada
            'list_impresoras.Enabled = False 

            btn_finalizar.Text = "SALIR"
            btn_finalizar.Visible = True
            If vdatos.Rows(0).Item("boton") = "FINALIZAR" Then
                btn_iniciar.Visible = False
                btn_pausar.Visible = False
                list_motivos.Visible = False
            Else
                btn_pausar.Text = "CONTINUAR"
                btn_pausar.Visible = True
                list_motivos.Visible = False
                btn_iniciar.Visible = False
            End If

            contador = CInt(vdatos.Rows(0).Item("TiempoAcum"))
            horas = contador \ 3600
            minutos = (contador - (horas * 3600)) \ 60
            segundos = contador Mod 60
            Lab_tiempo.Text = Strings.Right("0" & horas, 2) & ":" & Strings.Right("0" & minutos, 2) & ":" & Strings.Right("0" & segundos, 2)
        Else
            btn_iniciar.Visible = True
            btn_finalizar.Visible = True
            btn_finalizar.Text = "SALIR"
            btn_pausar.Visible = False
            list_motivos.Visible = False
            list_motivos.SelectedIndex = -1
            list_impresoras.Enabled = True
            If frm_principal.maquina <> "" Then list_impresoras.Text = frm_principal.maquina
            Lab_tiempo.Text = "00:00:00"
        End If
    End Sub
    Public Sub llenar_motivos()
        Dim vdatos As New DataTable
        Dim sql As String

        sql = "select id,descripcion from AlmTareasTiposPausa where ParaOf=1 order by id asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        list_motivos.DisplayMember = "descripcion"
        list_motivos.ValueMember = "id"
        list_motivos.DataSource = vdatos
        list_motivos.SelectedIndex = -1
    End Sub
    Public Sub llenar_log()
        Dim vdatos As New DataTable
        Dim sql As String
        Dim cont As Integer

        DG_log.GridColor = Color.Gainsboro

        sql = "select op.Nombre as 'Operario' ,ofl.fecha as 'Fecha',ofl.boton as 'Acción' "
        sql &= "from OrdenesFabricacionlog ofl "
        sql &= "inner join operarios_almacen op on op.Id =ofl.idoperario "
        sql &= "where ofl.codempresa=1 and ofl.codperiodo=" & lab_CodPeriodoOF.Text & " and ofl.seralbaranprov='OF' and ofl.codalbaranprov=" & lab_CodAlbaranOF.Text & " "
        sql &= "order by ofl.fecha desc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        DG_log.Visible = True
        DG_log.DataSource = vdatos
        DG_log.BackgroundColor = Color.White
        DG_log.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 9)
        DG_log.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 9, FontStyle.Bold)
        DG_log.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        DG_log.DefaultCellStyle.BackColor = Color.White
        DG_log.Columns(0).Width = 100
        DG_log.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        DG_log.Columns(1).Width = 120
        DG_log.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DG_log.Columns(2).Width = 70
        DG_log.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'DG_log.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        PintarFilasGrid()
        DG_log.CurrentCell = Nothing

    End Sub

    Public Sub PintarFilasGrid()
        Dim cont As Integer
        cont = 0
        Do While cont < DG_log.Rows.Count
            Select Case DG_log.Rows(cont).Cells("Acción").Value
                Case "INICIAR" : DG_log.Rows(cont).DefaultCellStyle.ForeColor = Color.Green
                Case "PAUSAR" : DG_log.Rows(cont).DefaultCellStyle.ForeColor = Color.Blue
                Case "CONTINUAR" : DG_log.Rows(cont).DefaultCellStyle.ForeColor = Color.Green
                Case "SALIR" : DG_log.Rows(cont).DefaultCellStyle.ForeColor = Color.Red
                Case "FINALIZAR" : DG_log.Rows(cont).DefaultCellStyle.ForeColor = Color.Red
            End Select
            cont += 1
        Loop
    End Sub

    Private Sub btn_iniciar_Click(sender As Object, e As EventArgs) Handles btn_iniciar.Click
        Dim sql As String
        Dim vdatos As New DataTable
        Dim resultado As String

        If list_impresoras.SelectedIndex = -1 Then
            MsgBox("Seleccionar la impresora.")
            Exit Sub
        End If

        sql = "insert into OrdenesFabricacionLog(codempresa,codperiodo,seralbaranprov,codalbaranprov,accion,boton,idoperario,impresora,TiempoAcum) "
        sql &= "values(1," & lab_CodPeriodoOF.Text & ",'OF'," & lab_CodAlbaranOF.Text & ",'" & frm_OrdenProduccion.list_accion.Text & "','" & btn_iniciar.Text & "'," & frm_principal.lab_idoperario.Text & ",'" & list_impresoras.Text & "'," & contador & " ) "
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
        If resultado = "0" Then
            Lab_tiempo.ForeColor = Color.Green
            btn_iniciar.Visible = False
            btn_pausar.Visible = True
            list_motivos.Visible = True
            list_motivos.SelectedIndex = -1
            btn_finalizar.Visible = True
            btn_finalizar.Text = "FINALIZAR"
            list_impresoras.Enabled = False
            llenar_log()
            Timer_tiempo.Enabled = True
            Timer_tiempo.Start()
        Else
            MsgBox("Se produjo un error al guardar registro en la BD.")
        End If

    End Sub

    Private Sub Timer_tiempo_Tick(sender As Object, e As EventArgs) Handles Timer_tiempo.Tick
        Dim horas, minutos, segundos As Integer

        horas = contador \ 3600
        minutos = (contador - (horas * 3600)) \ 60
        segundos = contador Mod 60
        contador += 1
        Lab_tiempo.Text = Strings.Right("0" & horas, 2) & ":" & Strings.Right("0" & minutos, 2) & ":" & Strings.Right("0" & segundos, 2)
        Application.DoEvents()
    End Sub

    Private Sub btn_pausar_Click(sender As Object, e As EventArgs) Handles btn_pausar.Click
        Dim sql As String
        Dim vdatos As New DataTable
        Dim resultado As String

        If btn_pausar.Text = "CONTINUAR" Then
            sql = "insert into OrdenesFabricacionLog(codempresa,codperiodo,seralbaranprov,codalbaranprov,accion,boton,idoperario,TiempoAcum,impresora) "
            sql &= "values(1," & lab_CodPeriodoOF.Text & ",'OF'," & lab_CodAlbaranOF.Text & ",'Imprimir','" & btn_pausar.Text & "'," & frm_principal.lab_idoperario.Text & "," & contador & ",'" & list_impresoras.Text & "') "
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If resultado <> "0" Then
                MsgBox("Se produjo un error al guardar registro en la BD.")
                Exit Sub
            End If
            btn_pausar.Text = "PAUSAR"
            list_impresoras.Enabled = False
            list_motivos.Visible = True
            btn_iniciar.Visible = False
            btn_finalizar.Visible = True
            btn_finalizar.Text = "FINALIZAR"
            Lab_tiempo.ForeColor = Color.Green
            Timer_tiempo.Start()
        Else
            If list_motivos.SelectedIndex = -1 Then
                MsgBox("Seleccionar el motivo de la pausa.")
                Exit Sub
            End If
            sql = "insert into OrdenesFabricacionLog(codempresa,codperiodo,seralbaranprov,codalbaranprov,accion,boton,idoperario,MotivoPausa,TiempoAcum,impresora) "
            sql &= "values(1," & lab_CodPeriodoOF.Text & ",'OF'," & lab_CodAlbaranOF.Text & ",'Imprimir','" & btn_pausar.Text & "'," & frm_principal.lab_idoperario.Text & ",'" & list_motivos.SelectedValue & "'," & contador & ",'" & list_impresoras.Text & "') "
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If resultado <> "0" Then
                MsgBox("Se produjo un error al guardar registro en la BD.")
                Exit Sub
            End If
            btn_iniciar.Visible = False
            btn_finalizar.Text = "SALIR"
            btn_finalizar.Visible = True
            btn_pausar.Text = "CONTINUAR"
            list_motivos.Visible = False
            list_motivos.SelectedIndex = -1
            list_impresoras.Enabled = True
            Lab_tiempo.ForeColor = Color.Blue
            Timer_tiempo.Stop()
        End If
        llenar_log()
    End Sub

    Private Sub btn_finalizar_Click(sender As Object, e As EventArgs) Handles btn_finalizar.Click
        Dim sql As String
        Dim vdatos As New DataTable
        Dim resultado As String

        If btn_finalizar.Text = "FINALIZAR" Then
            If MsgBox("Se va a finalizar la impresión para esta orden. No se podrá continuar la impresión posteriormente. Continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If
        End If

        sql = "insert into OrdenesFabricacionLog(codempresa,codperiodo,seralbaranprov,codalbaranprov,accion,boton,idoperario,TiempoAcum,impresora) "
        sql &= "values(1," & lab_CodPeriodoOF.Text & ",'OF'," & lab_CodAlbaranOF.Text & ",'Imprimir','" & btn_finalizar.Text & "'," & frm_principal.lab_idoperario.Text & "," & contador & ",'" & list_impresoras.Text & "') "
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
        If resultado = "0" Then
            Timer_tiempo.Stop()
            Timer_tiempo.Enabled = False
            Dispose()
            Close()
        Else
            MsgBox("Se produjo un error al guardar registro en la BD.")
            Exit Sub
        End If

    End Sub

    Private Sub DG_log_Sorted(sender As Object, e As EventArgs) Handles DG_log.Sorted
        PintarFilasGrid()
    End Sub

    Private Sub btn_otraOF_Click(sender As Object, e As EventArgs) Handles btn_otraOF.Click
        Dim impotraof As New Frm_ImpresionDigital
        Dim sql As String
        Dim vdatos As New DataTable

        impotraof.lab_CodPeriodoOF.Text = InputBox("Indicar el periodo de la OF")
        impotraof.lab_CodAlbaranOF.Text = InputBox("Indicar el código de la OF")
        If IsNumeric(impotraof.lab_CodPeriodoOF.Text) And IsNumeric(impotraof.lab_CodAlbaranOF.Text) Then
            'Mirar si existe la OF
            sql = "select CodAlbaranProv from GesAlbaranesProv where CodEmpresa =1 and CodPeriodo =" & impotraof.lab_CodPeriodoOF.Text & " and SerAlbaranProv ='OF' and CodAlbaranProv =" & impotraof.lab_CodAlbaranOF.Text & " and isnull(FinDepositada,0)=0 "
            vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
            If vdatos.Rows.Count = 1 Then
                impotraof.Show(frm_OrdenProduccion)
            Else
                MsgBox("No existe la OF o ya está filaizada.")
            End If
        Else
            MsgBox("No se han especificado valores para la OF.")
        End If
    End Sub

    Private Sub btn_imprimirEti_Click(sender As Object, e As EventArgs) Handles btn_imprimirEti.Click
        frm_OrdenProduccion.txt_cantImpEti.Text = txt_cantImpEti.Text
        frm_OrdenProduccion.txt_UCaja.Text = txt_UCaja.Text
        frm_OrdenProduccion.ImprimirEtiquetas()
    End Sub
End Class