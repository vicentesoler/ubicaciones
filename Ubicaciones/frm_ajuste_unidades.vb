﻿Public Class frm_ajuste_unidades
    Dim cant_max_ini, unicaja_ini As Integer
    Dim conexion As New Conector
    Private Sub frm_ajuste_unidades_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cant_max_ini = txt_cantmax.Text
        unicaja_ini = txt_unicaja.Text
    End Sub

    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql As String
        Dim resultado As String

        If Not IsNumeric(txt_cantmax.Text) Or Not IsNumeric(txt_unicaja.Text) Then
            MsgBox("El valo de las casillas debe ser numérico", MsgBoxStyle.Critical)
            Exit Sub
        End If

        Try
            If txt_cantmax.Text <> cant_max_ini Then
                sql = "update ubicaciones set cantmax=" & txt_cantmax.Text & ",ultusuario=" & frm_principal.lab_idoperario.Text & ",fechaultmodificacion=getdate(),observaciones='Mod.cantmax desde ubicaciones-contenedor' "
                sql &= "where codarticulo='" & lab_referencia.Text & "' and ubicacion='" & lab_ubipick.Text & "' and codalmacen='" & lab_codalmpick.Text & "' "
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                If resultado <> "0" Then
                    MsgBox("Se produjo un error al guardar la cantidad máxima en la ubicación.", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            End If
            If txt_unicaja.Text <> unicaja_ini Then
                sql = "update gesarticulos set EnvExteriorUd=" & txt_unicaja.Text & " "
                sql &= "where codarticulo='" & lab_referencia.Text & "' "
                resultado = conexion.ExecuteCmd(constantes.bd_inase, sql)
                If resultado <> "0" Then
                    MsgBox("Se produjo un error al guardar las unidades por caja en el artículo.", MsgBoxStyle.Critical)
                    Exit Sub
                Else
                    funciones.Enviar_correo("dpto_logistica@joumma.com", "Cambio en unidades por caja. Ref:" & lab_referencia.Text, "El operario " & frm_principal.lab_nomoperario.Text & " cambió las unidades por caja de la referencia " & lab_referencia.Text & ". La cantidad actual es " & txt_unicaja.Text & ".")
                End If
            End If
        Catch ex As Exception
            MsgBox("Se produjo un error.", MsgBoxStyle.Critical)
            Exit Sub
        End Try

        Close()
    End Sub
End Class