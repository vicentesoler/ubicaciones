﻿Public Class frm_login
    Private Sub frm_login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then
            MsgBox("La aplicación ya esta abierta, no puede abrirla de nuevo")
            Close()
        End If
        lab_alerta.Visible = False
        txt_maquina.Text = ""
        txt_usuario.Text = ""
    End Sub
    Private Sub btn_aceptar_Click(sender As System.Object, e As System.EventArgs) Handles btn_aceptar.Click
        If txt_usuario.Text <> "" And txt_maquina.Text <> "" Then
            validar_operario_maquina()
        End If
    End Sub
    Public Sub validar_operario_maquina()
        Dim sql As String
        Dim conexion As New Conector
        Dim vdatos As New DataTable
        Dim vmaquinausando As New DataTable
        Dim cont As Integer

        sql = "select id,nombre,isnull(usuario_inase,0) as usuario_inase,encargado,"
        sql &= "PermUbicaciones,PermArticulo,PermReaprov,PermOrdenAlta,PermOrdenSalida,PermDesgMont,PermOrdProd,PermContenedor,PermInventarios, "
        sql &= "PermArticuloModStock,PermOrdProdModOFFinalizadas, PermArticuloEliminarFila, PermArticuloModStockPlus, PermInventariosNuevo, PermInventariosCantAlta, "
        sql &= "PermOrdenAltaVerTiempo, PermReaprovUbiModMaxAct, PermUbicacionesEliminarFila, PermUbicacionesActStock,PermContenedorAbrirFinalizado,PermRecuentoStock,PermRecogerOFs "
        sql &= "from operarios_almacen where password='" & txt_usuario.Text.Trim & "'"
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count = 1 Then
            'Validar máquina
            If txt_maquina.Text <> "0" Then
                Dim vmaquina As New DataTable
                Dim resultado As String
                sql = "select id from MaquinasAlmacen where id='" & txt_maquina.Text & "' "
                vmaquina = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vmaquina.Rows.Count = 1 Then
                    'Mirar si el operario se encuentra ya dentro de una máquina. Avisarlo por si quiere salir de ella
                    sql = "select id,Estado from MaquinasAlmacen  where IdOperario =" & vdatos.Rows(0).Item("id") & " and Estado <> 'Libre'"
                    vmaquinausando = conexion.TablaxCmd(constantes.bd_intranet, sql)
                    cont = 0
                    Do While cont < vmaquinausando.Rows.Count
                        If MsgBox("La máquina Nº" & vmaquinausando.Rows(cont).Item("id") & " está siendo usada por usted para '" & vmaquinausando.Rows(cont).Item("Estado").ToString & "' , quere abandonarla?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                            sql = "Update MaquinasAlmacen set FechaFinUso=getdate(),Estado='Libre' "
                            sql &= "where id='" & vmaquinausando.Rows(cont).Item("id") & "'; "
                            sql &= "insert into MaquinasAlmacenLog(IdMaquina,IdOperario,Accion,Observaciones) "
                            sql &= "values('" & vmaquinausando.Rows(cont).Item("id") & "'," & vdatos.Rows(0).Item("id") & ",'Abandonar máquina','Entrar en Ubicaciones y comprobar que está usando otra máquina') "
                            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                        End If

                        cont += 1
                    Loop

                    frm_principal.lab_maquina.Text = txt_maquina.Text
                    'Marca la máquina en uso y añadir a log
                    sql = "Update MaquinasAlmacen set IdOperario=" & vdatos.Rows(0).Item("id") & ", FechaInicioUso=getdate(),FechaFinUso=Null,Estado='Ubicaciones' "
                    sql &= "where id='" & txt_maquina.Text & "'; "
                    sql &= "insert into MaquinasAlmacenLog(IdMaquina,IdOperario,Accion,Observaciones) "
                    sql &= "values('" & txt_maquina.Text & "'," & vdatos.Rows(0).Item("id") & ",'Inicio Preparación','Entrar en Ubicaciones') "
                    resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                Else
                    lab_alerta.Visible = True
                    lab_alerta.Text = "La máquina no existe."
                    txt_maquina.SelectAll()
                    Exit Sub
                End If
            Else
                frm_principal.lab_maquina.Text = txt_maquina.Text
            End If


            frm_principal.lab_nomoperario.Text = vdatos.Rows(0).Item("nombre")
            frm_principal.lab_idoperario.Text = vdatos.Rows(0).Item("id")
            frm_principal.usuario_inase = vdatos.Rows(0).Item("usuario_inase")
            frm_principal.encargado = vdatos.Rows(0).Item("encargado")
            '^Permisos Menus
            frm_principal.PermUbicaciones = vdatos.Rows(0).Item("PermUbicaciones")
            frm_principal.PermArticulo = vdatos.Rows(0).Item("PermArticulo")
            frm_principal.PermReaprov = vdatos.Rows(0).Item("PermReaprov")
            frm_principal.PermOrdenAlta = vdatos.Rows(0).Item("PermOrdenAlta")
            frm_principal.PermInventarios = vdatos.Rows(0).Item("PermInventarios")
            frm_principal.PermDesgMont = vdatos.Rows(0).Item("PermDesgMont")
            frm_principal.PermContenedor = vdatos.Rows(0).Item("PermContenedor")
            frm_principal.PermOrdenSalida = vdatos.Rows(0).Item("PermOrdenSalida")
            frm_principal.PermOrdProd = vdatos.Rows(0).Item("PermOrdProd")
            frm_principal.PermRecuentoStock = vdatos.Rows(0).Item("PermRecuentoStock")
            frm_principal.PermRecogerOFs = vdatos.Rows(0).Item("PermRecogerOFs")
            'PermisosAcciones
            frm_principal.PermOrdProdModOFFinalizadas = vdatos.Rows(0).Item("PermOrdProdModOFFinalizadas")
            frm_principal.PermArticuloModStock = vdatos.Rows(0).Item("PermArticuloModStock")
            frm_principal.PermArticuloEliminarFila = vdatos.Rows(0).Item("PermArticuloEliminarFila")
            frm_principal.PermArticuloModStockPlus = vdatos.Rows(0).Item("PermArticuloModStockPlus")
            frm_principal.PermInventariosNuevo = vdatos.Rows(0).Item("PermInventariosNuevo")
            frm_principal.PermInventariosCantAlta = vdatos.Rows(0).Item("PermInventariosCantAlta")
            frm_principal.PermOrdenAltaVerTiempo = vdatos.Rows(0).Item("PermOrdenAltaVerTiempo")
            frm_principal.PermReaprovUbiModMaxAct = vdatos.Rows(0).Item("PermReaprovUbiModMaxAct")
            frm_principal.PermUbicacionesEliminarFila = vdatos.Rows(0).Item("PermUbicacionesEliminarFila")
            frm_principal.PermUbicacionesActStock = vdatos.Rows(0).Item("PermUbicacionesActStock")
            frm_principal.PermContenedorAbrirFinalizado = vdatos.Rows(0).Item("PermContenedorAbrirFinalizado")
            frm_ubicaciones.TopLevel = False
            frm_ubicaciones.Dock = DockStyle.None
            frm_principal.p_principal.Controls.Add(frm_ubicaciones)
            frm_ubicaciones.Show()
            frm_principal.btn_mostrar_ubicacion.BackColor = Color.LightGreen
            frm_principal.btn_mostrar_ubicacion.ForeColor = Color.Black
            frm_principal.Show()
            Me.Dispose()
        Else
            lab_alerta.Visible = True
            txt_usuario.Text = ""
            txt_usuario.Focus()
        End If
    End Sub


    Private Sub txt_usuario_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_usuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            If txt_usuario.Text = "" Then Exit Sub
            If txt_maquina.Text <> "" Then
                btn_aceptar_Click(sender, e)
            Else
                txt_maquina.Focus()
            End If
        End If
    End Sub

    Private Sub txt_maquina_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_maquina.KeyUp
        If e.KeyCode = Keys.Enter Then
            If txt_maquina.Text = "" Then Exit Sub
            If txt_usuario.Text <> "" Then
                btn_aceptar_Click(sender, e)
            Else
                txt_usuario.Focus()
            End If
        End If
    End Sub
End Class