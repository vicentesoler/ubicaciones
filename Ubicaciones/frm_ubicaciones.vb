﻿Imports Microsoft.VisualBasic
Imports System.Threading

Public Class frm_ubicaciones
    Dim conexion As New Conector

    Private Sub frm_ubicaciones_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'btn_cancel_nuevo_Click(sender, e)
        list_filtro_tipo.SelectedIndex = 0
        funciones.llenar_almacenes(list_almacen, True, True, True)
        funciones.llenar_tipos_ubi(list_añadir_tipo)
        list_añadir_tipo.SelectedIndex = 0
        If frm_principal.PermUbicacionesEliminarFila = True Then
            btn_eliminar_fila.Visible = True
        Else
            btn_eliminar_fila.Visible = False
        End If
        txt_pasillo.Focus()

    End Sub

    Public Sub btn_mostrar_Click(sender As System.Object, e As System.EventArgs) Handles btn_mostrar_ubicacion.Click
        Dim pasillo, portal, altura As String

        If Not IsNumeric(txt_pasillo.Text) And Not IsNumeric(txt_portal.Text) And Not IsNumeric(txt_altura.Text) Then
            MsgBox("Llenar almenos uno de los campos de ubicación", MsgBoxStyle.Exclamation)
            txt_pasillo.Focus()
            Exit Sub
        End If

        If txt_pasillo.Text = "" Then Exit Sub Else pasillo = txt_pasillo.Text
        If txt_portal.Text = "" Then Exit Sub Else portal = txt_portal.Text
        If txt_altura.Text = "" Then Exit Sub Else altura = txt_altura.Text


        mostrar_ubic(pasillo & "-" & portal & "-" & altura)

        p_añadir_a_ubi.Enabled = True
        btn_cancel_nuevo_ubicacion.Text = "Limpiar"
        btn_cancel_nuevo_ubicacion.Visible = True
        btn_modificar_guardar_tamaño_ubicacion.ForeColor = Color.Black
        txt_añadir_articulo.Focus()

    End Sub
    Public Sub mostrar_ubic(ByVal ubicacion As String)
        Dim sql As String
        Dim vdatos As New DataTable
        Dim tamaño_ubi As Integer
        Dim cont As Integer

        dg_contenido_ubicacion.DataSource = Nothing

        If ubicacion <> "VACIAS" Then
            tamaño_ubi = funciones.consultar_tamaño_ubicacion(list_almacen.SelectedValue, txt_pasillo.Text, txt_portal.Text, txt_altura.Text)
            If tamaño_ubi = -1 Then
                'MsgBox("No existe la ubicación", MsgBoxStyle.Information)
                btn_modificar_guardar_tamaño_ubicacion.Visible = True
                btn_modificar_guardar_tamaño_ubicacion.Text = "Guardar"
                btn_cancel_nuevo_ubicacion.Visible = True
                btn_cancel_nuevo_ubicacion.Text = "Cancel."
                txt_tamaño.ReadOnly = False
                List_tipo.Enabled = True
                txt_tamaño.Focus()
                txt_tamaño.SelectAll()
                Exit Sub
            Else
                List_tipo.Text = funciones.consultar_tipo_ubicacion(list_almacen.SelectedValue, txt_pasillo.Text, txt_portal.Text, txt_altura.Text)
                txt_tamaño.Text = tamaño_ubi
                If IsNumeric(txt_tamaño.Text) Then txt_añadir_tamaño.Text = txt_tamaño.Text - funciones.consultar_tamaño_ocupado(txt_pasillo.Text, txt_portal.Text, txt_altura.Text)
                btn_modificar_guardar_tamaño_ubicacion.Visible = True
                If frm_principal.lab_nomoperario.Text = "Sergio" Or frm_principal.lab_nomoperario.Text = "Vicente Soler" Or frm_principal.lab_nomoperario.Text = "Simon" Then btn_eliminar.Visible = True
            End If

        Else
            txt_tamaño.Text = ""
            btn_modificar_guardar_tamaño_ubicacion.Visible = False
            btn_eliminar.Visible = False
        End If

        'Consulta articulos en ubicacion
        sql = "select top 100 ubi.id as 'Id',ubi.codalmacen as 'Alm.',ubi.ubicacion as 'Ubicacion',ubi.codarticulo as 'Ref.',art.descripcion as 'Descripcion',ubi.tipo as 'Tipo',isnull(ubi.tamaño,0) as 'Tam.',isnull(ubi.cantidad,0) as 'Cant.',isnull(ubi.cantmax,0) as 'Max.',"
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A01' then sto.stockactual end ),0)) as A01, "
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A07' then sto.stockactual end ),0)) as A07, "
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A08' then sto.stockactual end ),0)) as A08 "
        sql &= "from intranet.dbo.ubicaciones ubi "
        sql &= "left join gesarticulos art on art.codempresa=1 and art.codarticulo COLLATE Modern_Spanish_CI_AS=ubi.codarticulo "
        sql &= "left join inase.dbo.gesstocks sto on sto.codarticulo COLLATE Modern_Spanish_CI_AS=ubi.codarticulo and sto.codalmacen in ('A01','A07','A08')  "
        sql &= "where ubi.codalmacen='" & list_almacen.SelectedValue & "' "
        If ubicacion = "VACIAS" Then sql &= "and cantidad <=0 " Else sql &= "and ubi.ubicacion like '" & ubicacion & "' "
        If list_filtro_tipo.Text <> "TODOS" Then sql &= "and ubi.tipo='" & list_filtro_tipo.Text & "' "
        sql &= "group by ubi.codalmacen,ubi.id,ubi.ubicacion,ubi.codarticulo,art.descripcion,ubi.tipo,isnull(ubi.tamaño,0),isnull(ubi.cantidad,0),isnull(ubi.cantmax,0) "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        dg_contenido_ubicacion.DataSource = vdatos
        dg_contenido_ubicacion.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 20, FontStyle.Bold)
        dg_contenido_ubicacion.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 20)
        dg_contenido_ubicacion.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dg_contenido_ubicacion.Columns(0).Width = 1
        dg_contenido_ubicacion.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_contenido_ubicacion.Columns(0).ReadOnly = True
        dg_contenido_ubicacion.Columns(1).Width = 60
        dg_contenido_ubicacion.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_contenido_ubicacion.Columns(1).ReadOnly = True
        dg_contenido_ubicacion.Columns(2).Width = 140
        dg_contenido_ubicacion.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_contenido_ubicacion.Columns(2).ReadOnly = True
        dg_contenido_ubicacion.Columns(3).Width = 110
        dg_contenido_ubicacion.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_contenido_ubicacion.Columns(3).ReadOnly = True
        dg_contenido_ubicacion.Columns(4).Width = 250
        dg_contenido_ubicacion.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        dg_contenido_ubicacion.Columns(4).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 10, FontStyle.Bold)
        dg_contenido_ubicacion.Columns(4).ReadOnly = True
        dg_contenido_ubicacion.Columns(5).Width = 80
        dg_contenido_ubicacion.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_contenido_ubicacion.Columns(5).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold)
        dg_contenido_ubicacion.Columns(5).ReadOnly = True
        dg_contenido_ubicacion.Columns(6).Width = 70
        dg_contenido_ubicacion.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_contenido_ubicacion.Columns(7).Width = 80
        dg_contenido_ubicacion.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_contenido_ubicacion.Columns(8).Width = 80
        dg_contenido_ubicacion.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_contenido_ubicacion.Columns(9).Width = 80
        dg_contenido_ubicacion.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_contenido_ubicacion.Columns(9).ReadOnly = True
        dg_contenido_ubicacion.Columns(10).Width = 80
        dg_contenido_ubicacion.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_contenido_ubicacion.Columns(10).ReadOnly = True
        dg_contenido_ubicacion.Columns(11).Width = 80
        dg_contenido_ubicacion.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_contenido_ubicacion.Columns(11).ReadOnly = True
        dg_contenido_ubicacion.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells

        'Pintar las filas de color dependiendo del almacen. pedido por Sergio
        cont = 0
        Do While cont < dg_contenido_ubicacion.Rows.Count
            If dg_contenido_ubicacion.Rows(cont).Cells(1).Value = "A01" Then
                dg_contenido_ubicacion.Rows(cont).DefaultCellStyle.BackColor = Color.Blue
                dg_contenido_ubicacion.Rows(cont).DefaultCellStyle.ForeColor = Color.White
            End If
            If dg_contenido_ubicacion.Rows(cont).Cells(1).Value = "A07" Then
                dg_contenido_ubicacion.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                dg_contenido_ubicacion.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If
            If dg_contenido_ubicacion.Rows(cont).Cells(1).Value = "A08" Then
                dg_contenido_ubicacion.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                dg_contenido_ubicacion.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If
            cont += 1
        Loop
    End Sub
    Private Sub btn_cancel_nuevo_Click(sender As System.Object, e As System.EventArgs) Handles btn_cancel_nuevo_ubicacion.Click
        If btn_cancel_nuevo_ubicacion.Text = "Cancel." Then
            List_tipo.Enabled = False
            txt_tamaño.ReadOnly = True
            btn_modificar_guardar_tamaño_ubicacion.Text = "Modif."
            btn_cancel_nuevo_ubicacion.Text = "Limpiar"
        Else
            txt_pasillo.Text = ""
            txt_portal.Text = ""
            txt_altura.Text = ""
            txt_tamaño.Text = ""
            btn_cancel_nuevo_ubicacion.Text = "Cancel."
            btn_cancel_nuevo_ubicacion.Visible = False
            btn_modificar_guardar_tamaño_ubicacion.Visible = False
            dg_contenido_ubicacion.DataSource = Nothing
        End If
        p_añadir_a_ubi.Enabled = False
        txt_pasillo.Enabled = True
        txt_portal.Enabled = True
        txt_altura.Enabled = True
        btn_modificar_guardar_tamaño_ubicacion.ForeColor = Color.Black
        btn_eliminar.Visible = False
        txt_pasillo.Focus()
        txt_pasillo.SelectAll()
    End Sub

    Private Sub btn_modificar__guardar_tamaño_Click(sender As System.Object, e As System.EventArgs) Handles btn_modificar_guardar_tamaño_ubicacion.Click
        If btn_modificar_guardar_tamaño_ubicacion.Text = "Modif." Then
            'Modif.
            List_tipo.Enabled = True
            txt_tamaño.ReadOnly = False
            btn_modificar_guardar_tamaño_ubicacion.ForeColor = Color.Green
            txt_pasillo.Enabled = False
            txt_portal.Enabled = False
            txt_altura.Enabled = False
            btn_modificar_guardar_tamaño_ubicacion.Text = "Guardar"
            btn_cancel_nuevo_ubicacion.Visible = True
            btn_cancel_nuevo_ubicacion.Text = "Cancel."
            txt_tamaño.Focus()
            txt_tamaño.SelectAll()
        Else
            'Guardar
            If txt_tamaño.Text = "" Then txt_tamaño.Text = 0
            If funciones.guardar_ubicacion_almacen(list_almacen.SelectedValue, txt_pasillo.Text, txt_portal.Text, txt_altura.Text, txt_tamaño.Text, List_tipo.Text) = "OK" Then
                txt_tamaño.ReadOnly = True
                List_tipo.Enabled = False
                'txt_tamaño.BackColor = 
                btn_modificar_guardar_tamaño_ubicacion.Text = "Modif."
                btn_modificar_guardar_tamaño_ubicacion.ForeColor = Color.Black
                btn_cancel_nuevo_ubicacion.Text = "Limpiar"
                txt_pasillo.Enabled = True
                txt_portal.Enabled = True
                txt_altura.Enabled = True
                'btn_mostrar_Click(sender, e)

            End If
        End If
    End Sub


    Private Sub dg_ubicaciones_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
        txt_pasillo.Text = Microsoft.VisualBasic.Left(frm_articulo.dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString, 2)
        txt_portal.Text = Microsoft.VisualBasic.Mid(frm_articulo.dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString, 4, 2)
        txt_altura.Text = Microsoft.VisualBasic.Right(frm_articulo.dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString, 2)
        btn_mostrar_Click(sender, e)

    End Sub

    Private Sub dg_contenido_ubicacion_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_contenido_ubicacion.CellDoubleClick
        Dim articulo As String

        articulo = dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Ref.").Value.ToString


        If e.ColumnIndex = 3 And e.RowIndex > -1 Then
            frm_principal.btn_articulo_Click(sender, e)
            frm_articulo.txt_articulo.Text = articulo
            frm_articulo.btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
        End If
        If e.ColumnIndex = 2 And e.RowIndex > -1 Then
            txt_pasillo.Text = dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString.Substring(0, 2)
            txt_portal.Text = dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString.Substring(3, 2)
            txt_altura.Text = dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString.Substring(6, 2)
            btn_mostrar_Click(sender, e)
        End If
    End Sub
    Private Sub dg_contenido_ubicacion_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_contenido_ubicacion.CellEndEdit
        Dim sql, sql_ant, resultado As String
        Dim vdatosant As New DataTable

        If dg_contenido_ubicacion.Columns(e.ColumnIndex).Name = "Cant." Or dg_contenido_ubicacion.Columns(e.ColumnIndex).Name = "Tam." Or dg_contenido_ubicacion.Columns(e.ColumnIndex).Name = "Max." Then

            If dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Cant.").Value.ToString = "" Then dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Cant.").Value = 0
            If dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Tam.").Value.ToString = "" Then dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Tam.").Value = 0
            If dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Max.").Value.ToString = "" Then dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Max.").Value = 0
            Try
                If IsNumeric(dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Cant.").Value.ToString) Or IsNumeric(dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Tam.").Value.ToString) Or IsNumeric(dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Max.").Value.ToString) Then

                    If dg_contenido_ubicacion.Columns(e.ColumnIndex).Name = "Cant." Then
                        'Serio pidió el 14/01/2019 que solo pudieran actualizar el stok estos usuarios
                        If frm_principal.PermUbicacionesActStock = True Then
                            If MsgBox("Actualizar también el stock del almacén?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                                frm_act_stock.Lab_codarticulo.Text = dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Ref.").Value.ToString
                                frm_act_stock.lab_alm_pick.Text = dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Alm.").Value.ToString
                                frm_act_stock.ShowDialog()
                            End If
                        End If
                        funciones.añadir_reg_mov_ubicaciones(dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Ref.").Value.ToString, dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Cant.").Value, list_almacen.SelectedValue, txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text, "Cambio valor", "Ubicaciones-Ubicaciones", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
                        sql = "update ubicaciones set cantidad=" & dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Cant.").Value.ToString & ",tamaño=" & dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Tam.").Value.ToString & ",cantmax=" & dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Max.").Value.ToString & " "
                        sql &= "where id=" & dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Id").Value & " "
                        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                    Else
                        sql = "update ubicaciones set tamaño=" & dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Tam.").Value.ToString & ",cantmax=" & dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Max.").Value.ToString & " "
                        sql &= "where id=" & dg_contenido_ubicacion.Rows(e.RowIndex).Cells("Id").Value & " "
                        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                    End If
                    'Actualizo el grid porque si han actualizado tambien el stock dealmacen, los datos en lascolumnas de alm. no estan actualizados
                    btn_mostrar_Click(sender, e)
                    If resultado <> "0" Then
                        MsgBox("Se producjo un error al actualizar los datos", MsgBoxStyle.Exclamation)
                    End If
                Else
                    MsgBox("Los datos de las celdas modificadas deben ser numericos", MsgBoxStyle.Information)
                End If
            Catch ex As Exception
                MsgBox("Se producjo un error al actualizar los datos", MsgBoxStyle.Critical)
            End Try
        End If

    End Sub


    Private Sub btn_eliminar_fila_Click(sender As System.Object, e As System.EventArgs) Handles btn_eliminar_fila.Click
        Dim sql, resultado As String
        Dim id As Integer
        Dim cont As Integer
        Try
            cont = 0
            sql = ""
            Do While cont < dg_contenido_ubicacion.SelectedRows.Count
                id = dg_contenido_ubicacion.SelectedRows(cont).Cells("Id").Value
                'Guradar borrado
                sql &= "insert into ubicaciones_borradas(codalmacen,ubicacion,codarticulo,tipo,usuario,fecha,programa) "
                sql &= "select codalmacen,ubicacion,codarticulo,tipo,'" & frm_principal.lab_nomoperario.Text & "',getdate(),'Ubicaciones' from ubicaciones where id=" & id & " "

                sql &= "delete ubicaciones where id=" & id & " "

                funciones.añadir_reg_mov_ubicaciones(dg_contenido_ubicacion.SelectedRows(cont).Cells("Ref.").Value, dg_contenido_ubicacion.SelectedRows(cont).Cells("Cant.").Value, dg_contenido_ubicacion.SelectedRows(cont).Cells("Alm.").Value, dg_contenido_ubicacion.SelectedRows(cont).Cells("Ubicacion").Value, "Borrado linea", "Ubicaciones", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

                cont += 1
            Loop
            resultado = conexion.Executetransact(constantes.bd_intranet, sql)
            If resultado <> "0" Then
                MsgBox("Se producjo un error al eliminar la fila/s", MsgBoxStyle.Exclamation)
            Else
                If txt_altura.Text = "" And txt_pasillo.Text = "" And txt_portal.Text = "" Then
                    'mostrar_ubic("VACIAS")
                    'Sergio pidio que no refrescara el grid , ya que pierde por donde iba
                    dg_contenido_ubicacion.Rows.Remove(dg_contenido_ubicacion.SelectedRows(0))
                Else
                    btn_mostrar_Click(sender, e)
                End If

            End If
        Catch ex As Exception
            MsgBox("Se producjo un error al eliminar la fila", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btn_eliminar_Click(sender As System.Object, e As System.EventArgs) Handles btn_eliminar.Click
        Dim sql As String = ""
        Dim resultado As String
        If txt_pasillo.Text.Trim <> "" And txt_portal.Text.Trim <> "" And txt_altura.Text.Trim <> "" Then
            If MsgBox("Se va a eliminar la ubicación y las referencias ubicadas, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                'Guradar borrado
                sql &= "insert into ubicaciones_borradas(codalmacen,ubicacion,codarticulo,tipo,usuario,fecha,programa) "
                sql &= "select codalmacen,ubicacion,codarticulo,tipo,'" & frm_principal.lab_nomoperario.Text & "',getdate(),'Ubicaciones' from ubicaciones where codalmacen='" & list_almacen.SelectedValue & "' and ubicacion='" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' "

                sql &= "delete ubicaciones_almacen where codalmacen='" & list_almacen.SelectedValue & "' and pasillo='" & txt_pasillo.Text & "' and portal='" & txt_portal.Text & "' and altura='" & txt_altura.Text & "' "
                sql &= "delete ubicaciones where codalmacen='" & list_almacen.SelectedValue & "' and ubicacion='" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' "
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                If resultado <> "0" Then
                    MsgBox("Se producjo un error al eliminar la ubicación", MsgBoxStyle.Critical)
                Else
                    btn_cancel_nuevo_Click(sender, e)
                End If

            End If
        End If
    End Sub

    Private Sub txt_añadir_articulo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_añadir_articulo.KeyUp
        Dim codarticulo As String = txt_añadir_articulo.Text
        btn_añadir_a_ubi.Enabled = False
        If e.KeyCode = Keys.Enter Then
            If txt_añadir_articulo.Text.Length = 7 Or txt_añadir_articulo.Text.Length = 13 Then
                txt_descripcion_ubicacion.Text = funciones.buscar_descripcion(codarticulo)
                If txt_descripcion_ubicacion.Text <> "" Then
                    txt_añadir_articulo.Text = codarticulo
                    txt_añadir_tamaño.Focus()
                    btn_añadir_a_ubi.Enabled = True
                Else
                    btn_añadir_a_ubi.Enabled = False
                    txt_añadir_articulo.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub txt_añadir_tamaño_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_añadir_tamaño.KeyUp
        If e.KeyCode = Keys.Enter And txt_añadir_articulo.Text <> "" Then
            txt_añadir_cantidad.Focus()
        End If
    End Sub

    Private Sub txt_añadir_cantidad_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_añadir_cantidad.KeyUp
        If e.KeyCode = Keys.Enter And txt_añadir_cantidad.Text <> "" Then
            txt_añadir_cantmax.Text = txt_añadir_cantidad.Text
            txt_añadir_cantmax.Focus()
        End If

    End Sub

    Private Sub btn_añadir_a_ubi_Click(sender As System.Object, e As System.EventArgs) Handles btn_añadir_a_ubi.Click
        Dim sql As String
        Dim resultado As String
        If txt_añadir_cantidad.Text = "" Then txt_añadir_cantidad.Text = 0
        If txt_añadir_tamaño.Text = "" Then txt_añadir_tamaño.Text = 0
        If txt_añadir_cantmax.Text = "" Then txt_añadir_cantmax.Text = txt_añadir_cantidad.Text

        If txt_añadir_articulo.Text = "" Or Not IsNumeric(txt_añadir_cantidad.Text) Or Not IsNumeric(txt_añadir_tamaño.Text) Then
            MsgBox("Hay algun campo vacio o con datos erroneos, revisar", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            funciones.añadir_reg_mov_ubicaciones(txt_añadir_articulo.Text.Trim, txt_añadir_cantidad.Text, list_almacen.SelectedValue, txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text, "Añadir linea", "Ubicaciones-Articulo", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

            sql = "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
            sql &= "values ('" & list_almacen.SelectedValue & "','" & txt_añadir_articulo.Text.Trim & "','" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "','" & list_añadir_tipo.SelectedValue & "'" & "," & txt_añadir_tamaño.Text & "," & txt_añadir_cantidad.Text & ",''," & txt_añadir_cantmax.Text & ",'Ubicaciones-Ubicaciones','" & frm_principal.lab_nomoperario.Text & "')"
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If resultado <> "0" Then
                MsgBox("Se producjo un error al actualizar la ubicación", MsgBoxStyle.Critical)
            Else

                btn_mostrar_Click(sender, e)
                txt_añadir_articulo.Text = ""
                txt_añadir_tamaño.Text = ""
                txt_añadir_cantidad.Text = ""
                txt_añadir_cantmax.Text = ""
            End If
        Catch ex As Exception
            MsgBox("Se producjo un error al actualizar la ubicación", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub txt_tamaño_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_tamaño.KeyUp
        If e.KeyCode = Keys.Enter Then
            btn_modificar__guardar_tamaño_Click(sender, e)
        End If
    End Sub


    Private Sub list_filtro_tipo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_filtro_tipo.SelectedIndexChanged
        If Not IsNumeric(txt_pasillo.Text) And Not IsNumeric(txt_portal.Text) And Not IsNumeric(txt_altura.Text) Then
        Else
            btn_mostrar_Click(sender, e)
        End If
    End Sub


    Private Sub txt_pasillo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_pasillo.KeyUp
        Dim aux As String
        If Microsoft.VisualBasic.Left(txt_pasillo.Text, 1) = "U" Then
            If Len(txt_pasillo.Text) = 7 Then
                aux = txt_pasillo.Text
                txt_pasillo.Text = Microsoft.VisualBasic.Mid(aux, 2, 2)
                txt_portal.Text = Microsoft.VisualBasic.Mid(aux, 4, 2)
                txt_altura.Text = Microsoft.VisualBasic.Mid(aux, 6, 2)
                btn_mostrar_Click(sender, e)
            End If
        Else
            If txt_pasillo.Text.Length = 2 Then
                txt_portal.Focus()
                txt_portal.SelectAll()
            End If
        End If
        btn_modificar_guardar_tamaño_ubicacion.Visible = False
        btn_cancel_nuevo_ubicacion.Visible = True
    End Sub


    Private Sub txt_portal_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_portal.KeyUp
        Dim aux As String
        If Microsoft.VisualBasic.Left(txt_portal.Text, 1) = "U" Then
            If Len(txt_portal.Text) = 7 Then
                aux = txt_portal.Text
                txt_pasillo.Text = Microsoft.VisualBasic.Mid(aux, 2, 2)
                txt_portal.Text = Microsoft.VisualBasic.Mid(aux, 4, 2)
                txt_altura.Text = Microsoft.VisualBasic.Mid(aux, 6, 2)
                btn_mostrar_Click(sender, e)
            End If
        Else
            If txt_portal.Text.Length = 2 Then
                txt_altura.Focus()
                txt_altura.SelectAll()
            End If
        End If
        btn_modificar_guardar_tamaño_ubicacion.Visible = False
        btn_cancel_nuevo_ubicacion.Visible = True

    End Sub


    Private Sub txt_altura_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_altura.KeyUp
        Dim aux As String
        If Microsoft.VisualBasic.Left(txt_altura.Text, 1) = "U" Then
            If Len(txt_altura.Text) = 7 Then
                aux = txt_altura.Text
                txt_pasillo.Text = Microsoft.VisualBasic.Mid(aux, 2, 2)
                txt_portal.Text = Microsoft.VisualBasic.Mid(aux, 4, 2)
                txt_altura.Text = Microsoft.VisualBasic.Mid(aux, 6, 2)
                btn_mostrar_Click(sender, e)
            End If
        Else
            If txt_altura.Text.Length = 2 Then
                btn_mostrar_ubicacion.Focus()
            End If
        End If
        btn_modificar_guardar_tamaño_ubicacion.Visible = False
        btn_cancel_nuevo_ubicacion.Visible = True
    End Sub


    Private Sub txt_añadir_cantmax_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_añadir_cantmax.KeyUp
        If e.KeyCode = Keys.Enter And txt_añadir_cantidad.Text <> "" Then
            btn_añadir_a_ubi.Focus()
        End If
    End Sub


    Private Sub list_almacen_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_almacen.SelectedIndexChanged
        'btn_cancel_nuevo_Click(sender, e)
    End Sub


    Private Sub btn_ubic_vacias_Click(sender As System.Object, e As System.EventArgs) Handles btn_ubic_vacias.Click
        mostrar_ubic("VACIAS")
    End Sub
End Class
