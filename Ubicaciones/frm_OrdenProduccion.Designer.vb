﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_OrdenProduccion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txt_UCaja = New System.Windows.Forms.TextBox()
        Me.lab_UCaja = New System.Windows.Forms.Label()
        Me.btn_ImprimirEti = New System.Windows.Forms.Button()
        Me.btn_SubirPalet = New System.Windows.Forms.Button()
        Me.lab_CantPaletA01_UBI = New System.Windows.Forms.Label()
        Me.lab_A01Ubi = New System.Windows.Forms.Label()
        Me.lab_CantPaletA07_CONS = New System.Windows.Forms.Label()
        Me.Lab_A07Cons = New System.Windows.Forms.Label()
        Me.lab_CantPaletA01_CONS = New System.Windows.Forms.Label()
        Me.lab_A01Cons = New System.Windows.Forms.Label()
        Me.lab_prioridad = New System.Windows.Forms.Label()
        Me.btn_OtraUbi = New System.Windows.Forms.Button()
        Me.Lab_AlbDepende = New System.Windows.Forms.Label()
        Me.lab_AlbDependeText = New System.Windows.Forms.Label()
        Me.Dg_producir = New System.Windows.Forms.DataGridView()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.LabPedWeb = New System.Windows.Forms.Label()
        Me.LabTxtPedWeb = New System.Windows.Forms.Label()
        Me.lab_tipo = New System.Windows.Forms.Label()
        Me.LabPdtePrep = New System.Windows.Forms.Label()
        Me.LabPdteServ = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.labStockA08 = New System.Windows.Forms.Label()
        Me.labStockA07 = New System.Windows.Forms.Label()
        Me.labStockA01 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_BorrarLinReg = New System.Windows.Forms.Button()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dg_procesadas = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.list_codperiodo = New System.Windows.Forms.ComboBox()
        Me.P_filtros = New System.Windows.Forms.Panel()
        Me.list_filtro_prioridad = New System.Windows.Forms.ComboBox()
        Me.cb_FiltroB2BWEB = New System.Windows.Forms.CheckBox()
        Me.cb_FiltroPorDepositar = New System.Windows.Forms.CheckBox()
        Me.cb_FiltroStockA07 = New System.Windows.Forms.CheckBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.list_filtro_tipo = New System.Windows.Forms.ComboBox()
        Me.cb_FiltroStockA01 = New System.Windows.Forms.CheckBox()
        Me.cb_FiltroPorRecoger = New System.Windows.Forms.CheckBox()
        Me.btn_limpiar = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_filtro_codarticulo = New System.Windows.Forms.TextBox()
        Me.btn_salirorden = New System.Windows.Forms.Button()
        Me.list_accion = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lab_cantidad = New System.Windows.Forms.Label()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.txt_cantidad = New System.Windows.Forms.TextBox()
        Me.dg_ubi = New System.Windows.Forms.DataGridView()
        Me.lab_ubicaciones = New System.Windows.Forms.Label()
        Me.lab_tit_lineas = New System.Windows.Forms.Label()
        Me.lab_fecha = New System.Windows.Forms.Label()
        Me.list_codorden = New System.Windows.Forms.ComboBox()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_cantImpEti = New System.Windows.Forms.TextBox()
        Me.lab_cantImpEti = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.Dg_producir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_procesadas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.P_filtros.SuspendLayout()
        CType(Me.dg_ubi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.txt_cantImpEti)
        Me.Panel1.Controls.Add(Me.lab_cantImpEti)
        Me.Panel1.Controls.Add(Me.txt_UCaja)
        Me.Panel1.Controls.Add(Me.lab_UCaja)
        Me.Panel1.Controls.Add(Me.btn_ImprimirEti)
        Me.Panel1.Controls.Add(Me.btn_SubirPalet)
        Me.Panel1.Controls.Add(Me.lab_CantPaletA01_UBI)
        Me.Panel1.Controls.Add(Me.lab_A01Ubi)
        Me.Panel1.Controls.Add(Me.lab_CantPaletA07_CONS)
        Me.Panel1.Controls.Add(Me.Lab_A07Cons)
        Me.Panel1.Controls.Add(Me.lab_CantPaletA01_CONS)
        Me.Panel1.Controls.Add(Me.lab_A01Cons)
        Me.Panel1.Controls.Add(Me.lab_prioridad)
        Me.Panel1.Controls.Add(Me.btn_OtraUbi)
        Me.Panel1.Controls.Add(Me.Lab_AlbDepende)
        Me.Panel1.Controls.Add(Me.lab_AlbDependeText)
        Me.Panel1.Controls.Add(Me.Dg_producir)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.LabPedWeb)
        Me.Panel1.Controls.Add(Me.LabTxtPedWeb)
        Me.Panel1.Controls.Add(Me.lab_tipo)
        Me.Panel1.Controls.Add(Me.LabPdtePrep)
        Me.Panel1.Controls.Add(Me.LabPdteServ)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.labStockA08)
        Me.Panel1.Controls.Add(Me.labStockA07)
        Me.Panel1.Controls.Add(Me.labStockA01)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.btn_BorrarLinReg)
        Me.Panel1.Controls.Add(Me.imagen)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.dg_procesadas)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.list_codperiodo)
        Me.Panel1.Controls.Add(Me.P_filtros)
        Me.Panel1.Controls.Add(Me.btn_salirorden)
        Me.Panel1.Controls.Add(Me.list_accion)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lab_cantidad)
        Me.Panel1.Controls.Add(Me.btn_ok)
        Me.Panel1.Controls.Add(Me.txt_cantidad)
        Me.Panel1.Controls.Add(Me.dg_ubi)
        Me.Panel1.Controls.Add(Me.lab_ubicaciones)
        Me.Panel1.Controls.Add(Me.lab_tit_lineas)
        Me.Panel1.Controls.Add(Me.lab_fecha)
        Me.Panel1.Controls.Add(Me.list_codorden)
        Me.Panel1.Controls.Add(Me.btn_finalizar)
        Me.Panel1.Controls.Add(Me.dg_lineas)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1250, 640)
        Me.Panel1.TabIndex = 27
        '
        'txt_UCaja
        '
        Me.txt_UCaja.BackColor = System.Drawing.Color.LemonChiffon
        Me.txt_UCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_UCaja.Location = New System.Drawing.Point(1032, 607)
        Me.txt_UCaja.Name = "txt_UCaja"
        Me.txt_UCaja.Size = New System.Drawing.Size(36, 26)
        Me.txt_UCaja.TabIndex = 123
        Me.txt_UCaja.Text = "1"
        Me.txt_UCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lab_UCaja
        '
        Me.lab_UCaja.AutoSize = True
        Me.lab_UCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_UCaja.Location = New System.Drawing.Point(1030, 589)
        Me.lab_UCaja.Name = "lab_UCaja"
        Me.lab_UCaja.Size = New System.Drawing.Size(42, 15)
        Me.lab_UCaja.TabIndex = 122
        Me.lab_UCaja.Text = "U/Caj"
        '
        'btn_ImprimirEti
        '
        Me.btn_ImprimirEti.BackColor = System.Drawing.Color.LightSteelBlue
        Me.btn_ImprimirEti.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ImprimirEti.Location = New System.Drawing.Point(866, 591)
        Me.btn_ImprimirEti.Name = "btn_ImprimirEti"
        Me.btn_ImprimirEti.Size = New System.Drawing.Size(118, 43)
        Me.btn_ImprimirEti.TabIndex = 121
        Me.btn_ImprimirEti.Text = "Imp.Eti."
        Me.btn_ImprimirEti.UseVisualStyleBackColor = False
        Me.btn_ImprimirEti.Visible = False
        '
        'btn_SubirPalet
        '
        Me.btn_SubirPalet.BackColor = System.Drawing.Color.LightSteelBlue
        Me.btn_SubirPalet.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_SubirPalet.Location = New System.Drawing.Point(533, 595)
        Me.btn_SubirPalet.Name = "btn_SubirPalet"
        Me.btn_SubirPalet.Size = New System.Drawing.Size(190, 36)
        Me.btn_SubirPalet.TabIndex = 120
        Me.btn_SubirPalet.Text = "Mover/Subir palet"
        Me.btn_SubirPalet.UseVisualStyleBackColor = False
        '
        'lab_CantPaletA01_UBI
        '
        Me.lab_CantPaletA01_UBI.AutoSize = True
        Me.lab_CantPaletA01_UBI.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_CantPaletA01_UBI.ForeColor = System.Drawing.Color.Blue
        Me.lab_CantPaletA01_UBI.Location = New System.Drawing.Point(128, 598)
        Me.lab_CantPaletA01_UBI.Name = "lab_CantPaletA01_UBI"
        Me.lab_CantPaletA01_UBI.Size = New System.Drawing.Size(27, 29)
        Me.lab_CantPaletA01_UBI.TabIndex = 119
        Me.lab_CantPaletA01_UBI.Text = "0"
        Me.lab_CantPaletA01_UBI.Visible = False
        '
        'lab_A01Ubi
        '
        Me.lab_A01Ubi.AutoSize = True
        Me.lab_A01Ubi.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_A01Ubi.Location = New System.Drawing.Point(20, 598)
        Me.lab_A01Ubi.Name = "lab_A01Ubi"
        Me.lab_A01Ubi.Size = New System.Drawing.Size(113, 29)
        Me.lab_A01Ubi.TabIndex = 118
        Me.lab_A01Ubi.Text = "A01 UBI:"
        Me.lab_A01Ubi.Visible = False
        '
        'lab_CantPaletA07_CONS
        '
        Me.lab_CantPaletA07_CONS.AutoSize = True
        Me.lab_CantPaletA07_CONS.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_CantPaletA07_CONS.ForeColor = System.Drawing.Color.Green
        Me.lab_CantPaletA07_CONS.Location = New System.Drawing.Point(491, 598)
        Me.lab_CantPaletA07_CONS.Name = "lab_CantPaletA07_CONS"
        Me.lab_CantPaletA07_CONS.Size = New System.Drawing.Size(27, 29)
        Me.lab_CantPaletA07_CONS.TabIndex = 117
        Me.lab_CantPaletA07_CONS.Text = "0"
        Me.lab_CantPaletA07_CONS.Visible = False
        '
        'Lab_A07Cons
        '
        Me.Lab_A07Cons.AutoSize = True
        Me.Lab_A07Cons.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lab_A07Cons.Location = New System.Drawing.Point(352, 598)
        Me.Lab_A07Cons.Name = "Lab_A07Cons"
        Me.Lab_A07Cons.Size = New System.Drawing.Size(145, 29)
        Me.Lab_A07Cons.TabIndex = 116
        Me.Lab_A07Cons.Text = "A07 CONS:"
        Me.Lab_A07Cons.Visible = False
        '
        'lab_CantPaletA01_CONS
        '
        Me.lab_CantPaletA01_CONS.AutoSize = True
        Me.lab_CantPaletA01_CONS.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_CantPaletA01_CONS.ForeColor = System.Drawing.Color.Blue
        Me.lab_CantPaletA01_CONS.Location = New System.Drawing.Point(309, 598)
        Me.lab_CantPaletA01_CONS.Name = "lab_CantPaletA01_CONS"
        Me.lab_CantPaletA01_CONS.Size = New System.Drawing.Size(27, 29)
        Me.lab_CantPaletA01_CONS.TabIndex = 115
        Me.lab_CantPaletA01_CONS.Text = "0"
        Me.lab_CantPaletA01_CONS.Visible = False
        '
        'lab_A01Cons
        '
        Me.lab_A01Cons.AutoSize = True
        Me.lab_A01Cons.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_A01Cons.Location = New System.Drawing.Point(170, 598)
        Me.lab_A01Cons.Name = "lab_A01Cons"
        Me.lab_A01Cons.Size = New System.Drawing.Size(145, 29)
        Me.lab_A01Cons.TabIndex = 114
        Me.lab_A01Cons.Text = "A01 CONS:"
        Me.lab_A01Cons.Visible = False
        '
        'lab_prioridad
        '
        Me.lab_prioridad.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_prioridad.ForeColor = System.Drawing.Color.Red
        Me.lab_prioridad.Location = New System.Drawing.Point(639, 71)
        Me.lab_prioridad.Name = "lab_prioridad"
        Me.lab_prioridad.Size = New System.Drawing.Size(237, 20)
        Me.lab_prioridad.TabIndex = 113
        Me.lab_prioridad.Text = "Prioridad ALTA"
        Me.lab_prioridad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_OtraUbi
        '
        Me.btn_OtraUbi.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.btn_OtraUbi.Enabled = False
        Me.btn_OtraUbi.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OtraUbi.Location = New System.Drawing.Point(749, 391)
        Me.btn_OtraUbi.Name = "btn_OtraUbi"
        Me.btn_OtraUbi.Size = New System.Drawing.Size(92, 31)
        Me.btn_OtraUbi.TabIndex = 112
        Me.btn_OtraUbi.Text = "Otra Ub."
        Me.btn_OtraUbi.UseVisualStyleBackColor = False
        '
        'Lab_AlbDepende
        '
        Me.Lab_AlbDepende.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lab_AlbDepende.ForeColor = System.Drawing.Color.Crimson
        Me.Lab_AlbDepende.Location = New System.Drawing.Point(997, 36)
        Me.Lab_AlbDepende.Name = "Lab_AlbDepende"
        Me.Lab_AlbDepende.Size = New System.Drawing.Size(70, 20)
        Me.Lab_AlbDepende.TabIndex = 111
        Me.Lab_AlbDepende.Text = "0"
        Me.Lab_AlbDepende.Visible = False
        '
        'lab_AlbDependeText
        '
        Me.lab_AlbDependeText.AutoSize = True
        Me.lab_AlbDependeText.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_AlbDependeText.ForeColor = System.Drawing.Color.Crimson
        Me.lab_AlbDependeText.Location = New System.Drawing.Point(900, 36)
        Me.lab_AlbDependeText.Name = "lab_AlbDependeText"
        Me.lab_AlbDependeText.Size = New System.Drawing.Size(80, 20)
        Me.lab_AlbDependeText.TabIndex = 110
        Me.lab_AlbDependeText.Text = "Dep.De.:"
        Me.lab_AlbDependeText.Visible = False
        '
        'Dg_producir
        '
        Me.Dg_producir.AllowUserToAddRows = False
        Me.Dg_producir.AllowUserToDeleteRows = False
        Me.Dg_producir.AllowUserToResizeColumns = False
        Me.Dg_producir.AllowUserToResizeRows = False
        Me.Dg_producir.BackgroundColor = System.Drawing.SystemColors.ScrollBar
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Dg_producir.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle25
        Me.Dg_producir.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Dg_producir.DefaultCellStyle = DataGridViewCellStyle26
        Me.Dg_producir.Enabled = False
        Me.Dg_producir.Location = New System.Drawing.Point(19, 488)
        Me.Dg_producir.MultiSelect = False
        Me.Dg_producir.Name = "Dg_producir"
        Me.Dg_producir.ReadOnly = True
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Dg_producir.RowHeadersDefaultCellStyle = DataGridViewCellStyle27
        Me.Dg_producir.RowTemplate.Height = 25
        Me.Dg_producir.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Dg_producir.Size = New System.Drawing.Size(703, 97)
        Me.Dg_producir.TabIndex = 109
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label14.Location = New System.Drawing.Point(16, 465)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(191, 20)
        Me.Label14.TabIndex = 108
        Me.Label14.Text = "Referencias a producir"
        '
        'LabPedWeb
        '
        Me.LabPedWeb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabPedWeb.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.LabPedWeb.Location = New System.Drawing.Point(997, 9)
        Me.LabPedWeb.Name = "LabPedWeb"
        Me.LabPedWeb.Size = New System.Drawing.Size(70, 20)
        Me.LabPedWeb.TabIndex = 107
        Me.LabPedWeb.Text = "0"
        Me.LabPedWeb.Visible = False
        '
        'LabTxtPedWeb
        '
        Me.LabTxtPedWeb.AutoSize = True
        Me.LabTxtPedWeb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabTxtPedWeb.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.LabTxtPedWeb.Location = New System.Drawing.Point(900, 9)
        Me.LabTxtPedWeb.Name = "LabTxtPedWeb"
        Me.LabTxtPedWeb.Size = New System.Drawing.Size(86, 20)
        Me.LabTxtPedWeb.TabIndex = 106
        Me.LabTxtPedWeb.Text = "Ped.Web:"
        Me.LabTxtPedWeb.Visible = False
        '
        'lab_tipo
        '
        Me.lab_tipo.AutoSize = True
        Me.lab_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tipo.Location = New System.Drawing.Point(345, 71)
        Me.lab_tipo.Name = "lab_tipo"
        Me.lab_tipo.Size = New System.Drawing.Size(271, 18)
        Me.lab_tipo.TabIndex = 105
        Me.lab_tipo.Text = "Pintura/desg/mont/Personalizacion"
        '
        'LabPdtePrep
        '
        Me.LabPdtePrep.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabPdtePrep.Location = New System.Drawing.Point(998, 165)
        Me.LabPdtePrep.Name = "LabPdtePrep"
        Me.LabPdtePrep.Size = New System.Drawing.Size(46, 20)
        Me.LabPdtePrep.TabIndex = 104
        Me.LabPdtePrep.Text = "0"
        Me.LabPdtePrep.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LabPdteServ
        '
        Me.LabPdteServ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabPdteServ.Location = New System.Drawing.Point(997, 140)
        Me.LabPdteServ.Name = "LabPdteServ"
        Me.LabPdteServ.Size = New System.Drawing.Size(47, 20)
        Me.LabPdteServ.TabIndex = 103
        Me.LabPdteServ.Text = "0"
        Me.LabPdteServ.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(900, 165)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(98, 20)
        Me.Label12.TabIndex = 102
        Me.Label12.Text = "Pdte.Prep.:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(900, 140)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(97, 20)
        Me.Label11.TabIndex = 101
        Me.Label11.Text = "Pdte.Serv.:"
        '
        'labStockA08
        '
        Me.labStockA08.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labStockA08.Location = New System.Drawing.Point(997, 115)
        Me.labStockA08.Name = "labStockA08"
        Me.labStockA08.Size = New System.Drawing.Size(47, 20)
        Me.labStockA08.TabIndex = 100
        Me.labStockA08.Text = "0"
        Me.labStockA08.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'labStockA07
        '
        Me.labStockA07.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labStockA07.Location = New System.Drawing.Point(997, 92)
        Me.labStockA07.Name = "labStockA07"
        Me.labStockA07.Size = New System.Drawing.Size(47, 20)
        Me.labStockA07.TabIndex = 99
        Me.labStockA07.Text = "0"
        Me.labStockA07.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'labStockA01
        '
        Me.labStockA01.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labStockA01.Location = New System.Drawing.Point(997, 67)
        Me.labStockA01.Name = "labStockA01"
        Me.labStockA01.Size = New System.Drawing.Size(47, 20)
        Me.labStockA01.TabIndex = 98
        Me.labStockA01.Text = "0"
        Me.labStockA01.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(900, 115)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(97, 20)
        Me.Label10.TabIndex = 97
        Me.Label10.Text = "Stock A08:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(900, 92)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(97, 20)
        Me.Label9.TabIndex = 96
        Me.Label9.Text = "Stock A07:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(900, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(97, 20)
        Me.Label8.TabIndex = 95
        Me.Label8.Text = "Stock A01:"
        '
        'btn_BorrarLinReg
        '
        Me.btn_BorrarLinReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_BorrarLinReg.Location = New System.Drawing.Point(1162, 502)
        Me.btn_BorrarLinReg.Name = "btn_BorrarLinReg"
        Me.btn_BorrarLinReg.Size = New System.Drawing.Size(81, 60)
        Me.btn_BorrarLinReg.TabIndex = 94
        Me.btn_BorrarLinReg.Text = "Borrar Linea"
        Me.btn_BorrarLinReg.UseVisualStyleBackColor = True
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(1073, 7)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(170, 170)
        Me.imagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imagen.TabIndex = 93
        Me.imagen.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(746, 449)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(240, 25)
        Me.Label7.TabIndex = 92
        Me.Label7.Text = "Cantidades procesadas"
        '
        'dg_procesadas
        '
        Me.dg_procesadas.AllowUserToAddRows = False
        Me.dg_procesadas.AllowUserToDeleteRows = False
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_procesadas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle28
        Me.dg_procesadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_procesadas.DefaultCellStyle = DataGridViewCellStyle29
        Me.dg_procesadas.Location = New System.Drawing.Point(751, 477)
        Me.dg_procesadas.MultiSelect = False
        Me.dg_procesadas.Name = "dg_procesadas"
        Me.dg_procesadas.ReadOnly = True
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_procesadas.RowHeadersDefaultCellStyle = DataGridViewCellStyle30
        Me.dg_procesadas.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Gray
        Me.dg_procesadas.RowTemplate.Height = 30
        Me.dg_procesadas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_procesadas.Size = New System.Drawing.Size(405, 108)
        Me.dg_procesadas.TabIndex = 91
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(315, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 46)
        Me.Label6.TabIndex = 90
        Me.Label6.Text = "/"
        '
        'list_codperiodo
        '
        Me.list_codperiodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_codperiodo.FormattingEnabled = True
        Me.list_codperiodo.Location = New System.Drawing.Point(175, 8)
        Me.list_codperiodo.Name = "list_codperiodo"
        Me.list_codperiodo.Size = New System.Drawing.Size(137, 59)
        Me.list_codperiodo.TabIndex = 89
        '
        'P_filtros
        '
        Me.P_filtros.BackColor = System.Drawing.Color.PaleTurquoise
        Me.P_filtros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.P_filtros.Controls.Add(Me.list_filtro_prioridad)
        Me.P_filtros.Controls.Add(Me.cb_FiltroB2BWEB)
        Me.P_filtros.Controls.Add(Me.cb_FiltroPorDepositar)
        Me.P_filtros.Controls.Add(Me.cb_FiltroStockA07)
        Me.P_filtros.Controls.Add(Me.Label13)
        Me.P_filtros.Controls.Add(Me.list_filtro_tipo)
        Me.P_filtros.Controls.Add(Me.cb_FiltroStockA01)
        Me.P_filtros.Controls.Add(Me.cb_FiltroPorRecoger)
        Me.P_filtros.Controls.Add(Me.btn_limpiar)
        Me.P_filtros.Controls.Add(Me.Label5)
        Me.P_filtros.Controls.Add(Me.txt_filtro_codarticulo)
        Me.P_filtros.Location = New System.Drawing.Point(20, 93)
        Me.P_filtros.Name = "P_filtros"
        Me.P_filtros.Size = New System.Drawing.Size(856, 68)
        Me.P_filtros.TabIndex = 88
        '
        'list_filtro_prioridad
        '
        Me.list_filtro_prioridad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_filtro_prioridad.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_filtro_prioridad.FormattingEnabled = True
        Me.list_filtro_prioridad.Location = New System.Drawing.Point(475, 34)
        Me.list_filtro_prioridad.Name = "list_filtro_prioridad"
        Me.list_filtro_prioridad.Size = New System.Drawing.Size(120, 28)
        Me.list_filtro_prioridad.TabIndex = 102
        '
        'cb_FiltroB2BWEB
        '
        Me.cb_FiltroB2BWEB.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroB2BWEB.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroB2BWEB.Location = New System.Drawing.Point(474, 4)
        Me.cb_FiltroB2BWEB.Name = "cb_FiltroB2BWEB"
        Me.cb_FiltroB2BWEB.Size = New System.Drawing.Size(113, 30)
        Me.cb_FiltroB2BWEB.TabIndex = 101
        Me.cb_FiltroB2BWEB.Text = "WEB/B2B"
        Me.cb_FiltroB2BWEB.UseVisualStyleBackColor = True
        '
        'cb_FiltroPorDepositar
        '
        Me.cb_FiltroPorDepositar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroPorDepositar.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroPorDepositar.Location = New System.Drawing.Point(325, 34)
        Me.cb_FiltroPorDepositar.Name = "cb_FiltroPorDepositar"
        Me.cb_FiltroPorDepositar.Size = New System.Drawing.Size(144, 30)
        Me.cb_FiltroPorDepositar.TabIndex = 100
        Me.cb_FiltroPorDepositar.Text = "Por Depositar"
        Me.cb_FiltroPorDepositar.UseVisualStyleBackColor = True
        '
        'cb_FiltroStockA07
        '
        Me.cb_FiltroStockA07.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroStockA07.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroStockA07.Location = New System.Drawing.Point(185, 34)
        Me.cb_FiltroStockA07.Name = "cb_FiltroStockA07"
        Me.cb_FiltroStockA07.Size = New System.Drawing.Size(129, 30)
        Me.cb_FiltroStockA07.TabIndex = 99
        Me.cb_FiltroStockA07.Text = "Stok en A07"
        Me.cb_FiltroStockA07.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(615, 3)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(39, 15)
        Me.Label13.TabIndex = 97
        Me.Label13.Text = "Tipo:"
        '
        'list_filtro_tipo
        '
        Me.list_filtro_tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_filtro_tipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_filtro_tipo.FormattingEnabled = True
        Me.list_filtro_tipo.Items.AddRange(New Object() {"Todas", "Gener.", "Manip.", "Perso."})
        Me.list_filtro_tipo.Location = New System.Drawing.Point(618, 18)
        Me.list_filtro_tipo.Name = "list_filtro_tipo"
        Me.list_filtro_tipo.Size = New System.Drawing.Size(103, 37)
        Me.list_filtro_tipo.TabIndex = 96
        '
        'cb_FiltroStockA01
        '
        Me.cb_FiltroStockA01.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroStockA01.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroStockA01.Location = New System.Drawing.Point(185, 4)
        Me.cb_FiltroStockA01.Name = "cb_FiltroStockA01"
        Me.cb_FiltroStockA01.Size = New System.Drawing.Size(129, 30)
        Me.cb_FiltroStockA01.TabIndex = 98
        Me.cb_FiltroStockA01.Text = "Stok en A01"
        Me.cb_FiltroStockA01.UseVisualStyleBackColor = True
        '
        'cb_FiltroPorRecoger
        '
        Me.cb_FiltroPorRecoger.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroPorRecoger.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroPorRecoger.Location = New System.Drawing.Point(325, 4)
        Me.cb_FiltroPorRecoger.Name = "cb_FiltroPorRecoger"
        Me.cb_FiltroPorRecoger.Size = New System.Drawing.Size(130, 30)
        Me.cb_FiltroPorRecoger.TabIndex = 95
        Me.cb_FiltroPorRecoger.Text = "Por Recoger"
        Me.cb_FiltroPorRecoger.UseVisualStyleBackColor = True
        '
        'btn_limpiar
        '
        Me.btn_limpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_limpiar.Location = New System.Drawing.Point(738, 10)
        Me.btn_limpiar.Name = "btn_limpiar"
        Me.btn_limpiar.Size = New System.Drawing.Size(111, 48)
        Me.btn_limpiar.TabIndex = 89
        Me.btn_limpiar.Text = "Limpiar"
        Me.btn_limpiar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label5.Location = New System.Drawing.Point(11, 4)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 90
        Me.Label5.Text = "Ref.:"
        '
        'txt_filtro_codarticulo
        '
        Me.txt_filtro_codarticulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_filtro_codarticulo.Location = New System.Drawing.Point(14, 18)
        Me.txt_filtro_codarticulo.Name = "txt_filtro_codarticulo"
        Me.txt_filtro_codarticulo.Size = New System.Drawing.Size(147, 45)
        Me.txt_filtro_codarticulo.TabIndex = 89
        Me.txt_filtro_codarticulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btn_salirorden
        '
        Me.btn_salirorden.Enabled = False
        Me.btn_salirorden.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_salirorden.Location = New System.Drawing.Point(749, 591)
        Me.btn_salirorden.Name = "btn_salirorden"
        Me.btn_salirorden.Size = New System.Drawing.Size(92, 43)
        Me.btn_salirorden.TabIndex = 86
        Me.btn_salirorden.Text = "Salir"
        Me.btn_salirorden.UseVisualStyleBackColor = True
        '
        'list_accion
        '
        Me.list_accion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_accion.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_accion.FormattingEnabled = True
        Me.list_accion.Location = New System.Drawing.Point(656, 9)
        Me.list_accion.Name = "list_accion"
        Me.list_accion.Size = New System.Drawing.Size(220, 59)
        Me.list_accion.TabIndex = 84
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(518, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 39)
        Me.Label1.TabIndex = 83
        Me.Label1.Text = "Acción:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(21, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 20)
        Me.Label4.TabIndex = 87
        Me.Label4.Text = "Filtros"
        '
        'lab_cantidad
        '
        Me.lab_cantidad.AutoSize = True
        Me.lab_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_cantidad.Location = New System.Drawing.Point(899, 402)
        Me.lab_cantidad.Name = "lab_cantidad"
        Me.lab_cantidad.Size = New System.Drawing.Size(113, 39)
        Me.lab_cantidad.TabIndex = 63
        Me.lab_cantidad.Text = "Cant.:"
        '
        'btn_ok
        '
        Me.btn_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ok.Location = New System.Drawing.Point(1151, 391)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(92, 56)
        Me.btn_ok.TabIndex = 62
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'txt_cantidad
        '
        Me.txt_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad.Location = New System.Drawing.Point(1018, 391)
        Me.txt_cantidad.Name = "txt_cantidad"
        Me.txt_cantidad.Size = New System.Drawing.Size(112, 53)
        Me.txt_cantidad.TabIndex = 61
        Me.txt_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dg_ubi
        '
        Me.dg_ubi.AllowUserToAddRows = False
        Me.dg_ubi.AllowUserToDeleteRows = False
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_ubi.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle31
        Me.dg_ubi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_ubi.DefaultCellStyle = DataGridViewCellStyle32
        Me.dg_ubi.Location = New System.Drawing.Point(749, 195)
        Me.dg_ubi.MultiSelect = False
        Me.dg_ubi.Name = "dg_ubi"
        Me.dg_ubi.ReadOnly = True
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_ubi.RowHeadersDefaultCellStyle = DataGridViewCellStyle33
        Me.dg_ubi.RowTemplate.Height = 40
        Me.dg_ubi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_ubi.Size = New System.Drawing.Size(494, 190)
        Me.dg_ubi.TabIndex = 60
        '
        'lab_ubicaciones
        '
        Me.lab_ubicaciones.AutoSize = True
        Me.lab_ubicaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_ubicaciones.Location = New System.Drawing.Point(746, 167)
        Me.lab_ubicaciones.Name = "lab_ubicaciones"
        Me.lab_ubicaciones.Size = New System.Drawing.Size(130, 25)
        Me.lab_ubicaciones.TabIndex = 59
        Me.lab_ubicaciones.Text = "Ubicaciones"
        '
        'lab_tit_lineas
        '
        Me.lab_tit_lineas.AutoSize = True
        Me.lab_tit_lineas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tit_lineas.Location = New System.Drawing.Point(15, 167)
        Me.lab_tit_lineas.Name = "lab_tit_lineas"
        Me.lab_tit_lineas.Size = New System.Drawing.Size(340, 25)
        Me.lab_tit_lineas.TabIndex = 57
        Me.lab_tit_lineas.Text = "Referencias a Desglosar / Montar:"
        '
        'lab_fecha
        '
        Me.lab_fecha.AutoSize = True
        Me.lab_fecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_fecha.Location = New System.Drawing.Point(185, 71)
        Me.lab_fecha.Name = "lab_fecha"
        Me.lab_fecha.Size = New System.Drawing.Size(54, 15)
        Me.lab_fecha.TabIndex = 55
        Me.lab_fecha.Text = "Fecha: "
        '
        'list_codorden
        '
        Me.list_codorden.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_codorden.FormattingEnabled = True
        Me.list_codorden.IntegralHeight = False
        Me.list_codorden.Location = New System.Drawing.Point(348, 9)
        Me.list_codorden.Name = "list_codorden"
        Me.list_codorden.Size = New System.Drawing.Size(142, 59)
        Me.list_codorden.TabIndex = 37
        '
        'btn_finalizar
        '
        Me.btn_finalizar.BackColor = System.Drawing.Color.Turquoise
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(1106, 592)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(137, 42)
        Me.btn_finalizar.TabIndex = 33
        Me.btn_finalizar.Text = "Finalizar"
        Me.btn_finalizar.UseVisualStyleBackColor = False
        Me.btn_finalizar.Visible = False
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.AllowUserToResizeColumns = False
        Me.dg_lineas.AllowUserToResizeRows = False
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle34
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_lineas.DefaultCellStyle = DataGridViewCellStyle35
        Me.dg_lineas.Location = New System.Drawing.Point(20, 195)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas.RowHeadersDefaultCellStyle = DataGridViewCellStyle36
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(703, 252)
        Me.dg_lineas.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(155, 46)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "N.Ord.:"
        '
        'txt_cantImpEti
        '
        Me.txt_cantImpEti.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantImpEti.Location = New System.Drawing.Point(990, 607)
        Me.txt_cantImpEti.Name = "txt_cantImpEti"
        Me.txt_cantImpEti.Size = New System.Drawing.Size(36, 26)
        Me.txt_cantImpEti.TabIndex = 125
        Me.txt_cantImpEti.Text = "1"
        Me.txt_cantImpEti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lab_cantImpEti
        '
        Me.lab_cantImpEti.AutoSize = True
        Me.lab_cantImpEti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_cantImpEti.Location = New System.Drawing.Point(988, 589)
        Me.lab_cantImpEti.Name = "lab_cantImpEti"
        Me.lab_cantImpEti.Size = New System.Drawing.Size(40, 15)
        Me.lab_cantImpEti.TabIndex = 124
        Me.lab_cantImpEti.Text = "Cant."
        '
        'frm_OrdenProduccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_OrdenProduccion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Orden de Producción"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Dg_producir, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_procesadas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.P_filtros.ResumeLayout(False)
        Me.P_filtros.PerformLayout()
        CType(Me.dg_ubi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents P_filtros As System.Windows.Forms.Panel
    Friend WithEvents btn_limpiar As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_filtro_codarticulo As System.Windows.Forms.TextBox
    Friend WithEvents btn_salirorden As System.Windows.Forms.Button
    Friend WithEvents list_accion As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lab_cantidad As System.Windows.Forms.Label
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents txt_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents dg_ubi As System.Windows.Forms.DataGridView
    Friend WithEvents lab_ubicaciones As System.Windows.Forms.Label
    Friend WithEvents lab_tit_lineas As System.Windows.Forms.Label
    Friend WithEvents lab_fecha As System.Windows.Forms.Label
    Friend WithEvents list_codorden As System.Windows.Forms.ComboBox
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents dg_lineas As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents list_codperiodo As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dg_procesadas As System.Windows.Forms.DataGridView
    Friend WithEvents imagen As System.Windows.Forms.PictureBox
    Friend WithEvents btn_BorrarLinReg As System.Windows.Forms.Button
    Friend WithEvents cb_FiltroPorRecoger As CheckBox
    Friend WithEvents labStockA08 As Label
    Friend WithEvents labStockA07 As Label
    Friend WithEvents labStockA01 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents LabPdtePrep As Label
    Friend WithEvents LabPdteServ As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents lab_tipo As Label
    Friend WithEvents list_filtro_tipo As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents LabPedWeb As Label
    Friend WithEvents LabTxtPedWeb As Label
    Friend WithEvents Dg_producir As DataGridView
    Friend WithEvents Label14 As Label
    Friend WithEvents cb_FiltroPorDepositar As CheckBox
    Friend WithEvents cb_FiltroStockA07 As CheckBox
    Friend WithEvents cb_FiltroStockA01 As CheckBox
    Friend WithEvents cb_FiltroB2BWEB As CheckBox
    Friend WithEvents Lab_AlbDepende As Label
    Friend WithEvents lab_AlbDependeText As Label
    Friend WithEvents btn_OtraUbi As Button
    Friend WithEvents lab_prioridad As Label
    Friend WithEvents list_filtro_prioridad As ComboBox
    Friend WithEvents lab_CantPaletA01_CONS As Label
    Friend WithEvents lab_A01Cons As Label
    Friend WithEvents lab_CantPaletA07_CONS As Label
    Friend WithEvents Lab_A07Cons As Label
    Friend WithEvents lab_CantPaletA01_UBI As Label
    Friend WithEvents lab_A01Ubi As Label
    Friend WithEvents btn_SubirPalet As Button
    Friend WithEvents btn_ImprimirEti As Button
    Friend WithEvents txt_UCaja As TextBox
    Friend WithEvents lab_UCaja As Label
    Friend WithEvents txt_cantImpEti As TextBox
    Friend WithEvents lab_cantImpEti As Label
End Class
