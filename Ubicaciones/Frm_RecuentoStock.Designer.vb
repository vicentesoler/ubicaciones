﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_RecuentoStock
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dg_ubi = New System.Windows.Forms.DataGridView()
        Me.imagen = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_ubicacion = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.list_alm = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_CantRecontada = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_CantUbicacion = New System.Windows.Forms.TextBox()
        Me.dg_referencias = New System.Windows.Forms.DataGridView()
        Me.txt_articulo = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel2.SuspendLayout()
        CType(Me.dg_ubi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_referencias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Bisque
        Me.Panel2.Controls.Add(Me.dg_ubi)
        Me.Panel2.Controls.Add(Me.imagen)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.txt_ubicacion)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.btn_ok)
        Me.Panel2.Controls.Add(Me.list_alm)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txt_CantRecontada)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txt_CantUbicacion)
        Me.Panel2.Controls.Add(Me.dg_referencias)
        Me.Panel2.Controls.Add(Me.txt_articulo)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1250, 640)
        Me.Panel2.TabIndex = 26
        '
        'dg_ubi
        '
        Me.dg_ubi.AllowUserToAddRows = False
        Me.dg_ubi.AllowUserToDeleteRows = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_ubi.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dg_ubi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_ubi.DefaultCellStyle = DataGridViewCellStyle8
        Me.dg_ubi.Location = New System.Drawing.Point(839, 196)
        Me.dg_ubi.MultiSelect = False
        Me.dg_ubi.Name = "dg_ubi"
        Me.dg_ubi.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_ubi.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dg_ubi.RowTemplate.Height = 50
        Me.dg_ubi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_ubi.Size = New System.Drawing.Size(402, 179)
        Me.dg_ubi.TabIndex = 75
        '
        'imagen
        '
        Me.imagen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.imagen.Location = New System.Drawing.Point(1056, 10)
        Me.imagen.Name = "imagen"
        Me.imagen.Size = New System.Drawing.Size(175, 175)
        Me.imagen.TabIndex = 74
        Me.imagen.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(843, 403)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(247, 25)
        Me.Label8.TabIndex = 73
        Me.Label8.Text = "Ubicación seleccionada:"
        '
        'txt_ubicacion
        '
        Me.txt_ubicacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_ubicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ubicacion.Location = New System.Drawing.Point(1103, 394)
        Me.txt_ubicacion.MaxLength = 13
        Me.txt_ubicacion.Name = "txt_ubicacion"
        Me.txt_ubicacion.ReadOnly = True
        Me.txt_ubicacion.Size = New System.Drawing.Size(138, 38)
        Me.txt_ubicacion.TabIndex = 72
        Me.txt_ubicacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(835, 173)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 20)
        Me.Label7.TabIndex = 71
        Me.Label7.Text = "Ubicaciones:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(198, 20)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Referencias a recontar:"
        '
        'btn_ok
        '
        Me.btn_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ok.Location = New System.Drawing.Point(839, 567)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(402, 46)
        Me.btn_ok.TabIndex = 68
        Me.btn_ok.Text = "Validar Cantidad"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'list_alm
        '
        Me.list_alm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_alm.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_alm.FormattingEnabled = True
        Me.list_alm.Location = New System.Drawing.Point(221, 9)
        Me.list_alm.Name = "list_alm"
        Me.list_alm.Size = New System.Drawing.Size(109, 46)
        Me.list_alm.TabIndex = 66
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(206, 25)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Almacen a recontar:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(843, 504)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 25)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Cantidad recontada:"
        '
        'txt_CantRecontada
        '
        Me.txt_CantRecontada.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CantRecontada.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CantRecontada.Location = New System.Drawing.Point(1129, 495)
        Me.txt_CantRecontada.MaxLength = 13
        Me.txt_CantRecontada.Name = "txt_CantRecontada"
        Me.txt_CantRecontada.Size = New System.Drawing.Size(112, 38)
        Me.txt_CantRecontada.TabIndex = 18
        Me.txt_CantRecontada.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(843, 454)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(234, 25)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Cantidad en ubicación:"
        '
        'txt_CantUbicacion
        '
        Me.txt_CantUbicacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_CantUbicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_CantUbicacion.Location = New System.Drawing.Point(1129, 445)
        Me.txt_CantUbicacion.MaxLength = 13
        Me.txt_CantUbicacion.Name = "txt_CantUbicacion"
        Me.txt_CantUbicacion.ReadOnly = True
        Me.txt_CantUbicacion.Size = New System.Drawing.Size(112, 38)
        Me.txt_CantUbicacion.TabIndex = 16
        Me.txt_CantUbicacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dg_referencias
        '
        Me.dg_referencias.AllowUserToAddRows = False
        Me.dg_referencias.AllowUserToResizeRows = False
        Me.dg_referencias.CausesValidation = False
        Me.dg_referencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_referencias.EnableHeadersVisualStyles = False
        Me.dg_referencias.Location = New System.Drawing.Point(7, 98)
        Me.dg_referencias.MultiSelect = False
        Me.dg_referencias.Name = "dg_referencias"
        Me.dg_referencias.ReadOnly = True
        Me.dg_referencias.RowHeadersVisible = False
        Me.dg_referencias.RowTemplate.Height = 40
        Me.dg_referencias.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dg_referencias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_referencias.Size = New System.Drawing.Size(812, 515)
        Me.dg_referencias.TabIndex = 15
        '
        'txt_articulo
        '
        Me.txt_articulo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_articulo.Location = New System.Drawing.Point(503, 10)
        Me.txt_articulo.MaxLength = 13
        Me.txt_articulo.Name = "txt_articulo"
        Me.txt_articulo.Size = New System.Drawing.Size(145, 45)
        Me.txt_articulo.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(375, 23)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(122, 25)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Referencia:"
        '
        'Frm_RecuentoStock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Frm_RecuentoStock"
        Me.Text = "Frm_RecuentoStock"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dg_ubi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_referencias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel2 As Panel
    Friend WithEvents dg_referencias As DataGridView
    Friend WithEvents txt_articulo As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_CantRecontada As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_CantUbicacion As TextBox
    Friend WithEvents list_alm As ComboBox
    Friend WithEvents btn_ok As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents txt_ubicacion As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents imagen As PictureBox
    Friend WithEvents dg_ubi As DataGridView
End Class
