﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_ImpresionDigital
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.p_tiempo = New System.Windows.Forms.Panel()
        Me.Lab_tiempo = New System.Windows.Forms.Label()
        Me.btn_iniciar = New System.Windows.Forms.Button()
        Me.btn_pausar = New System.Windows.Forms.Button()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.DG_log = New System.Windows.Forms.DataGridView()
        Me.Timer_tiempo = New System.Windows.Forms.Timer(Me.components)
        Me.list_motivos = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.list_impresoras = New System.Windows.Forms.ComboBox()
        Me.btn_otraOF = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lab_CodAlbaranOF = New System.Windows.Forms.Label()
        Me.lab_CodPeriodoOF = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_imprimirEti = New System.Windows.Forms.Button()
        Me.txt_cantImpEti = New System.Windows.Forms.TextBox()
        Me.lab_cantImpEti = New System.Windows.Forms.Label()
        Me.txt_UCaja = New System.Windows.Forms.TextBox()
        Me.lab_UCaja = New System.Windows.Forms.Label()
        Me.p_tiempo.SuspendLayout()
        CType(Me.DG_log, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'p_tiempo
        '
        Me.p_tiempo.BackColor = System.Drawing.Color.Black
        Me.p_tiempo.Controls.Add(Me.Lab_tiempo)
        Me.p_tiempo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.p_tiempo.Location = New System.Drawing.Point(419, 122)
        Me.p_tiempo.Name = "p_tiempo"
        Me.p_tiempo.Size = New System.Drawing.Size(206, 56)
        Me.p_tiempo.TabIndex = 0
        '
        'Lab_tiempo
        '
        Me.Lab_tiempo.AutoSize = True
        Me.Lab_tiempo.Font = New System.Drawing.Font("Microsoft Sans Serif", 33.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lab_tiempo.ForeColor = System.Drawing.SystemColors.InactiveCaption
        Me.Lab_tiempo.Location = New System.Drawing.Point(6, 2)
        Me.Lab_tiempo.Name = "Lab_tiempo"
        Me.Lab_tiempo.Size = New System.Drawing.Size(199, 52)
        Me.Lab_tiempo.TabIndex = 1
        Me.Lab_tiempo.Text = "00:00:00"
        Me.Lab_tiempo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_iniciar
        '
        Me.btn_iniciar.BackColor = System.Drawing.Color.LightGreen
        Me.btn_iniciar.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_iniciar.Location = New System.Drawing.Point(26, 122)
        Me.btn_iniciar.Name = "btn_iniciar"
        Me.btn_iniciar.Size = New System.Drawing.Size(236, 48)
        Me.btn_iniciar.TabIndex = 1
        Me.btn_iniciar.Text = "INICIAR"
        Me.btn_iniciar.UseVisualStyleBackColor = False
        '
        'btn_pausar
        '
        Me.btn_pausar.BackColor = System.Drawing.Color.LightBlue
        Me.btn_pausar.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_pausar.Location = New System.Drawing.Point(26, 204)
        Me.btn_pausar.Name = "btn_pausar"
        Me.btn_pausar.Size = New System.Drawing.Size(236, 48)
        Me.btn_pausar.TabIndex = 2
        Me.btn_pausar.Text = "PAUSAR"
        Me.btn_pausar.UseVisualStyleBackColor = False
        '
        'btn_finalizar
        '
        Me.btn_finalizar.BackColor = System.Drawing.Color.OrangeRed
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(26, 325)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(236, 48)
        Me.btn_finalizar.TabIndex = 3
        Me.btn_finalizar.Text = "FINALIZAR"
        Me.btn_finalizar.UseVisualStyleBackColor = False
        '
        'DG_log
        '
        Me.DG_log.BackgroundColor = System.Drawing.Color.White
        Me.DG_log.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_log.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DG_log.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_log.GridColor = System.Drawing.Color.White
        Me.DG_log.Location = New System.Drawing.Point(300, 203)
        Me.DG_log.Name = "DG_log"
        Me.DG_log.RowHeadersWidth = 5
        Me.DG_log.Size = New System.Drawing.Size(325, 242)
        Me.DG_log.TabIndex = 4
        '
        'Timer_tiempo
        '
        Me.Timer_tiempo.Interval = 1000
        '
        'list_motivos
        '
        Me.list_motivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_motivos.FormattingEnabled = True
        Me.list_motivos.Location = New System.Drawing.Point(26, 264)
        Me.list_motivos.Name = "list_motivos"
        Me.list_motivos.Size = New System.Drawing.Size(236, 33)
        Me.list_motivos.TabIndex = 5
        Me.list_motivos.Text = "Motivo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(55, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(177, 31)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "IMPRESORA"
        '
        'list_impresoras
        '
        Me.list_impresoras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_impresoras.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_impresoras.FormattingEnabled = True
        Me.list_impresoras.Items.AddRange(New Object() {"Impresora China 1", "Impresora China 2", "Impresora China 3", "Impresora China 4", "Rolland 1", "Rolland 2"})
        Me.list_impresoras.Location = New System.Drawing.Point(238, 59)
        Me.list_impresoras.Name = "list_impresoras"
        Me.list_impresoras.Size = New System.Drawing.Size(351, 39)
        Me.list_impresoras.TabIndex = 6
        '
        'btn_otraOF
        '
        Me.btn_otraOF.BackColor = System.Drawing.Color.Thistle
        Me.btn_otraOF.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_otraOF.Location = New System.Drawing.Point(26, 397)
        Me.btn_otraOF.Name = "btn_otraOF"
        Me.btn_otraOF.Size = New System.Drawing.Size(236, 48)
        Me.btn_otraOF.TabIndex = 9
        Me.btn_otraOF.Text = "IMP. OTRA OF"
        Me.btn_otraOF.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(206, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 29)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "OF:"
        '
        'lab_CodAlbaranOF
        '
        Me.lab_CodAlbaranOF.AutoSize = True
        Me.lab_CodAlbaranOF.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lab_CodAlbaranOF.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_CodAlbaranOF.Location = New System.Drawing.Point(370, 9)
        Me.lab_CodAlbaranOF.Name = "lab_CodAlbaranOF"
        Me.lab_CodAlbaranOF.Size = New System.Drawing.Size(69, 29)
        Me.lab_CodAlbaranOF.TabIndex = 11
        Me.lab_CodAlbaranOF.Text = "4587"
        '
        'lab_CodPeriodoOF
        '
        Me.lab_CodPeriodoOF.AutoSize = True
        Me.lab_CodPeriodoOF.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lab_CodPeriodoOF.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_CodPeriodoOF.Location = New System.Drawing.Point(268, 9)
        Me.lab_CodPeriodoOF.Name = "lab_CodPeriodoOF"
        Me.lab_CodPeriodoOF.Size = New System.Drawing.Size(69, 29)
        Me.lab_CodPeriodoOF.TabIndex = 12
        Me.lab_CodPeriodoOF.Text = "2020"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(343, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(21, 29)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "/"
        '
        'btn_imprimirEti
        '
        Me.btn_imprimirEti.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.btn_imprimirEti.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_imprimirEti.Location = New System.Drawing.Point(26, 465)
        Me.btn_imprimirEti.Name = "btn_imprimirEti"
        Me.btn_imprimirEti.Size = New System.Drawing.Size(236, 48)
        Me.btn_imprimirEti.TabIndex = 14
        Me.btn_imprimirEti.Text = "IMP. ETIQUETAS"
        Me.btn_imprimirEti.UseVisualStyleBackColor = False
        '
        'txt_cantImpEti
        '
        Me.txt_cantImpEti.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantImpEti.Location = New System.Drawing.Point(273, 487)
        Me.txt_cantImpEti.Name = "txt_cantImpEti"
        Me.txt_cantImpEti.Size = New System.Drawing.Size(36, 26)
        Me.txt_cantImpEti.TabIndex = 129
        Me.txt_cantImpEti.Text = "1"
        Me.txt_cantImpEti.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lab_cantImpEti
        '
        Me.lab_cantImpEti.AutoSize = True
        Me.lab_cantImpEti.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_cantImpEti.Location = New System.Drawing.Point(271, 469)
        Me.lab_cantImpEti.Name = "lab_cantImpEti"
        Me.lab_cantImpEti.Size = New System.Drawing.Size(40, 15)
        Me.lab_cantImpEti.TabIndex = 128
        Me.lab_cantImpEti.Text = "Cant."
        '
        'txt_UCaja
        '
        Me.txt_UCaja.BackColor = System.Drawing.Color.LemonChiffon
        Me.txt_UCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_UCaja.Location = New System.Drawing.Point(328, 487)
        Me.txt_UCaja.Name = "txt_UCaja"
        Me.txt_UCaja.Size = New System.Drawing.Size(36, 26)
        Me.txt_UCaja.TabIndex = 127
        Me.txt_UCaja.Text = "1"
        Me.txt_UCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lab_UCaja
        '
        Me.lab_UCaja.AutoSize = True
        Me.lab_UCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_UCaja.Location = New System.Drawing.Point(326, 469)
        Me.lab_UCaja.Name = "lab_UCaja"
        Me.lab_UCaja.Size = New System.Drawing.Size(42, 15)
        Me.lab_UCaja.TabIndex = 126
        Me.lab_UCaja.Text = "U/Caj"
        '
        'Frm_ImpresionDigital
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(647, 525)
        Me.ControlBox = False
        Me.Controls.Add(Me.txt_cantImpEti)
        Me.Controls.Add(Me.lab_cantImpEti)
        Me.Controls.Add(Me.txt_UCaja)
        Me.Controls.Add(Me.lab_UCaja)
        Me.Controls.Add(Me.btn_imprimirEti)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lab_CodPeriodoOF)
        Me.Controls.Add(Me.lab_CodAlbaranOF)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_otraOF)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.list_impresoras)
        Me.Controls.Add(Me.list_motivos)
        Me.Controls.Add(Me.DG_log)
        Me.Controls.Add(Me.btn_finalizar)
        Me.Controls.Add(Me.btn_pausar)
        Me.Controls.Add(Me.btn_iniciar)
        Me.Controls.Add(Me.p_tiempo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "Frm_ImpresionDigital"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Impresión digital"
        Me.p_tiempo.ResumeLayout(False)
        Me.p_tiempo.PerformLayout()
        CType(Me.DG_log, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents p_tiempo As Panel
    Friend WithEvents Lab_tiempo As Label
    Friend WithEvents btn_iniciar As Button
    Friend WithEvents btn_pausar As Button
    Friend WithEvents btn_finalizar As Button
    Friend WithEvents DG_log As DataGridView
    Friend WithEvents Timer_tiempo As Timer
    Friend WithEvents list_motivos As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents list_impresoras As ComboBox
    Friend WithEvents btn_otraOF As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents lab_CodAlbaranOF As Label
    Friend WithEvents lab_CodPeriodoOF As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_imprimirEti As Button
    Friend WithEvents txt_cantImpEti As TextBox
    Friend WithEvents lab_cantImpEti As Label
    Friend WithEvents txt_UCaja As TextBox
    Friend WithEvents lab_UCaja As Label
End Class
