﻿Imports System.Net
Public Class frm_contenedor
    Dim conexion As New Conector
    Dim fecha_inicio, fecha_fin As DateTime

    Private Sub frm_contenedor_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim ip As Net.Dns
        Dim nombrePC As String
        Dim entradasIP As Net.IPHostEntry
        Dim direccion_Ip As String
        nombrePC = Dns.GetHostName
        entradasIP = Dns.GetHostEntry(nombrePC)
        direccion_Ip = entradasIP.AddressList.FirstOrDefault(Function(i) i.AddressFamily = Sockets.AddressFamily.InterNetwork).ToString()

        llenar_empresas(list_empresa)
        llenar_periodos_contenedor(list_periodo, list_empresa.SelectedValue)
        'llenar_contenedores(list_contenedor, list_empresa.SelectedValue, list_periodo.SelectedValue, "NO")
        'list_contenedor.SelectedIndex = -1
        If Microsoft.VisualBasic.Mid(direccion_Ip, 6, 1) = 0 Then
            list_almacen.Text = "A01"
        ElseIf Microsoft.VisualBasic.Mid(direccion_Ip, 6, 1) = 2 Then
            list_almacen.Text = "A07"
        ElseIf Microsoft.VisualBasic.Mid(direccion_Ip, 6, 1) = 8 Then
            list_almacen.Text = "A08"
        Else
            list_almacen.Text = "A01"
        End If
        btn_cerrar.Enabled = False
        list_contenedor.Focus()

    End Sub

    Private Sub list_empresa_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_empresa.DropDownClosed
        If IsNumeric(list_empresa.SelectedIndex) Then
            llenar_periodos_contenedor(list_periodo, list_empresa.SelectedValue)
            'llenar_contenedores(list_contenedor, list_empresa.SelectedValue, list_periodo.SelectedValue, "NO")
            'list_contenedor.SelectedIndex = -1
        End If
    End Sub

    Private Sub list_periodo_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_periodo.DropDownClosed
        If IsNumeric(list_periodo.SelectedIndex) Then
            'llenar_contenedores(list_contenedor, list_empresa.SelectedValue, list_periodo.SelectedValue, "NO")
            'list_contenedor.SelectedIndex = -1
        End If
    End Sub

    Public Sub guardar_datos_contenedor()
        Dim sql As String
        Dim vdatos As New DataTable
        sql = "select finalizado from contenedores_ubicados where codempresa=" & list_empresa.SelectedValue & " and codperiodo=" & list_periodo.SelectedValue & " "
        sql &= "and numcontenedor=" & list_contenedor.Text
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows.Count = 0 Then
            sql = "insert into contenedores_ubicados(codempresa,codperiodo,numcontenedor,ultusuario) "
            sql &= "values(" & list_empresa.SelectedValue & "," & list_periodo.SelectedValue & "," & list_contenedor.Text & ",'" & frm_principal.lab_idoperario.Text & "')"
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If


    End Sub
    Public Sub llenar_lineas()
        Dim sql As String
        Dim vdatos, valm As New DataTable
        Dim cont As Integer

        'Guardar el momento de abrir el contenedor
        fecha_inicio = Now

        sql = "select lin.codarticulo as 'Ref.',max(art.descripcion) as 'Descripción',sum(lin.cantidad) as 'Cant.','' as 'Alm.',"
        sql &= "isnull((select sum(cant_depos) from intranet.dbo.contenedores_ubicados_lin ubl where ubl.codempresa=" & list_empresa.SelectedValue & " and ubl.codperiodo=" & list_periodo.SelectedValue.trim & " "
        sql &= "and ubl.numcontenedor=" & list_contenedor.Text & " and ubl.codarticulo=lin.codarticulo),0) as 'Ubicada',max(lin.numreferencia) as 'NR' "
        sql &= "from gescontenedoreslin lin  "
        sql &= "inner join gesarticulos art on art.codempresa=lin.codempresa and art.codarticulo=lin.codarticulo "
        sql &= "where lin.codempresa = " & list_empresa.SelectedValue & " And lin.codperiodo = " & list_periodo.SelectedValue.trim & " And lin.numcontenedor = " & list_contenedor.Text
        sql &= "group by lin.codarticulo,lin.almacen "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        'poner el almacen , porque si se ubica una cantidad en un almacen diferente hay que hacer movimiento de cambio de almacen
        cont = 0
        Do While cont < vdatos.Rows.Count
            'Consultar el almacen de la linea de albaran
            sql = "select linalb.Almacen "
            sql &= "from GesContenedoresLin lincon "
            sql &= "inner join GesContenedores cabcon on cabcon.NumReferencia =lincon.RefCabecera "
            sql &= "inner join GesAlbaranesprov cabalb on cabalb.NumReferencia =cabcon.NumRefAlbaProv "
            sql &= "inner join GesAlbaranesProvlin linalb on linalb.RefCabecera =cabalb.NumReferencia and linalb.Articulo ='" & vdatos.Rows(cont).Item("Ref.").ToString & "' "
            sql &= "where lincon.NumReferencia ='" & vdatos.Rows(cont).Item("NR").ToString & "' "
            valm = conexion.TablaxCmd(constantes.bd_inase, sql)
            If valm.Rows.Count > 0 Then
                vdatos.Rows(cont).Item("Alm.") = valm.Rows(0).Item("almacen").ToString
            Else
                MsgBox("El contenedor (o alguna de sus líneas) no ha sido albaranado o no se puede obtener información del albarán. No se puede continuar.", MsgBoxStyle.Exclamation)
                limpiar(True, "El contenedor no ha sido albaranado o no se puede obtener información del albarán. No se puede continuar.")
                Exit Sub
            End If
            cont += 1
        Loop



        dg_lineas.DataSource = vdatos
        dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 20)
        dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18, FontStyle.Bold)
        'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dg_lineas.Columns(0).Width = 120
        dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas.Columns(1).Width = 285
        dg_lineas.Columns(1).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15)
        dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        dg_lineas.Columns(2).Width = 90
        dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas.Columns(3).Width = 75
        dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas.Columns(4).Width = 120
        dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas.Columns(5).Width = 10
        dg_lineas.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'Pintar filas 
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Cant.").Value = dg_lineas.Rows(cont).Cells("Ubicada").Value Then dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
            If dg_lineas.Rows(cont).Cells("Cant.").Value < dg_lineas.Rows(cont).Cells("Ubicada").Value Then dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Red
            If dg_lineas.Rows(cont).Cells("Cant.").Value > dg_lineas.Rows(cont).Cells("Ubicada").Value And dg_lineas.Rows(cont).Cells("Ubicada").Value > 0 Then dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
            cont += 1
        Loop

        dg_lineas.CurrentCell = Nothing

    End Sub

    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim cont, contmov As Integer
        Dim sql As String
        Dim resultado, sql_insert As String
        Dim vdatosant, vubi As New DataTable
        Dim cantdif As Boolean = False
        Dim proxmov As Integer = 0
        Dim numrefmov As String = ""
        Dim cantidad_ajuste As Integer = 0

        If Not IsNumeric(list_contenedor.Text) Then
            MsgBox("El campo 'Contenedor' está vacío, no se puede continuar. Insertar el número de contenedor.", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Revisar si todas las lineas del contnedor estan completamente ubicadas
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).DefaultCellStyle.BackColor <> Color.Green Then
                If MsgBox("Quedan cantidades por ubicar, seguro que desea continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    cantdif = True
                    Exit Do
                Else
                    Exit Sub
                End If
            End If
            cont += 1
        Loop


        sql_insert = "update contenedores_ubicados set finalizado=1  "
        sql_insert &= "where codempresa=" & list_empresa.SelectedValue & " "
        sql_insert &= "and codperiodo=" & list_periodo.SelectedValue & " and numcontenedor=" & list_contenedor.Text & " "


        If cantdif = True Then
            MsgBox("La cantidad no ubicada será regularizada en almacén mediante un movimiento.")
            If funciones.crear_movimiento_inase(1, proxmov, "Diferencia en descarga contenedor " & list_empresa.SelectedValue.ToString.Trim & "/" & list_periodo.SelectedValue.ToString.Trim & "/" & list_contenedor.Text, numrefmov) = "OK" Then
                cont = 0
                contmov = 0
                Do While cont < dg_lineas.Rows.Count
                    If dg_lineas.Rows(cont).DefaultCellStyle.BackColor <> Color.Green Then
                        cantidad_ajuste = (dg_lineas.Rows(cont).Cells("Cant.").Value - dg_lineas.Rows(cont).Cells("Ubicada").Value) * -1
                        contmov += 1
                        sql_insert &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                        sql_insert &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & proxmov & "," & contmov & ",'" & dg_lineas.Rows(cont).Cells("Alm.").Value & "','XXX','" & dg_lineas.Rows(cont).Cells("Ref.").Value & "'," & cantidad_ajuste & ",'Diferencia en descarga contenedor " & list_empresa.SelectedValue.ToString.Trim & "/" & list_periodo.SelectedValue.ToString.Trim & "/" & list_contenedor.Text & ".') "
                    End If
                    cont += 1
                Loop

            Else
                MsgBox("Se produjo un error a crear la cabecera del mov. para regularizar el stock. Avisar a informática.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
        End If


        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql_insert)

        If resultado = "0" Then
            MsgBox("El contenedor ha sido ubicado correctamente", MsgBoxStyle.Information)
            limpiar(True, "Finalizar")
        Else
            MsgBox("Se produjo un error al marcar el contenedor como finalizdo.", MsgBoxStyle.Exclamation)
            funciones.Enviar_correo("vicentesoler@joumma.com", "Error al finalizar contenedor " & list_contenedor.Text, resultado & " " & sql_insert)
        End If

    End Sub

    Public Sub limpiar(Optional ByVal GuardarTiempos As Boolean = True, Optional ByVal ObsGuardarTiempos As String = "")
        'Guardar datos de operario
        If GuardarTiempos Then
            fecha_fin = Now
            Guardar_datos_tiempo(ObsGuardarTiempos)
        End If


        'llenar_contenedores(list_contenedor, list_empresa.SelectedValue, list_periodo.SelectedValue, "NO")
        btn_finalizar.Enabled = False
        btn_eliminar.Enabled = False
        btn_cerrar.Enabled = False
        list_contenedor.SelectedIndex = -1
        list_contenedor.Enabled = True
        dg_lineas.DataSource = New DataTable
        dg_ubicacion_destino.DataSource = New DataTable
        txt_altura.Text = ""
        txt_pasillo.Text = ""
        txt_portal.Text = ""
        list_contenedor.Text = ""
        list_contenedor.Focus()
    End Sub

    Public Sub Guardar_datos_tiempo(Optional ByVal ObsGuardarTiempos As String = "")
        Dim TiempoEmpleado As Integer
        Dim sql As String
        Try
            TiempoEmpleado = DateDiff(DateInterval.Second, fecha_inicio, fecha_fin)
            sql = "insert into albaranes_tiempos (codempresa,codperiodo,NumContenedor,idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(" & list_empresa.SelectedValue & "," & list_periodo.SelectedValue & ",'" & list_contenedor.Text & "'," & frm_principal.lab_idoperario.Text & ",'Contenedor','" & fecha_inicio & "','" & fecha_fin & "'," & TiempoEmpleado & ",0,'" & ObsGuardarTiempos & "' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        Catch ex As Exception

        End Try

    End Sub

    Public Sub reaprov_ref(ByVal codarticulo As String, ByVal cant_recibida As Integer)
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cantpendservhoy, cantpick, cantmaxpick, unidcaja As Integer
        Dim codalmacen_pick, ubicacion_pick As String
        Dim ubicacion_tras As String
        Dim cantaubicar As Integer

        codarticulo = codarticulo.ToUpper.Trim
        'El 05/01/2018 sergio se quejo de que no se estaba haciendo bien y esto es lo que hacia:
        '- Mira el pendiente de servir hasta hoy y esta cantidad (en multiplo cajas) es la que llena del picking, 
        '- si no caben en el picking las llena en Traslados
        'Ahora quiere:
        '-llene todo el picking (en cajas) y si el pendiente de servir es mayor que la cantidad máxima de picking, que el resto lo deje en Traslados (en cajas)

        'Obtener la cantidad pendiente de servir
        'Simon pidio (confirmado por Domingo) que la cantidad pendiente de servir sea descontando lo que hay en A01, solo cuando se este descargando en A07.
        'Sergio pidio el 05/02 que no hay que coger los pedidos de reserva. Forma de pago 500 y 555 confirmado por Vero

        sql = "select lin.Articulo,isnull(sum(case when convert(date,cab.FechaEntrega) <= getdate() then lin.CantidadPdte end),0) as cant_pend,"
        sql &= "isnull(sum(lin.CantidadPdte),0) as cant_pend_tot,"
        sql &= "max(art.EnvExteriorUd) as unidcaja, "

        sql &= "isnull((select sum(st.StockActual) "
        sql &= "from gesstocks st where st.codarticulo=lin.articulo and st.codempresa in (select codempresa from inaempresas where controlstock=1) and st.codalmacen in ('A01')),0) as stock_a01 "
        sql &= "from GesPedidos cab "
        sql &= "inner join GesPedidosLin lin on lin.RefCabecera =cab.NumReferencia "
        sql &= "inner join gesarticulos art on art.codarticulo=lin.articulo and art.codempresa=1 "
        sql &= "where lin.Articulo ='" & codarticulo & "' and cab.FormaPago not in (555,500) "
        sql &= "group by lin.Articulo "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        If vdatos.Rows.Count = 0 Then Exit Sub

        lab_pendserv_tot.Text = CInt(vdatos.Rows(0).Item("cant_pend_tot"))
        lab_pendserv_hoy.Text = CInt(vdatos.Rows(0).Item("cant_pend"))
        lab_stockA01.Text = vdatos.Rows(0).Item("stock_a01")


        If vdatos.Rows(0).Item("cant_pend_tot") > 0 Then
            cantpendservhoy = vdatos.Rows(0).Item("cant_pend")
            'If cantpendserv > cant_recibida Then cantaubicar = cant_recibida Else cantaubicar = cantpendserv
            cantaubicar = cant_recibida
            unidcaja = vdatos.Rows(0).Item("unidcaja")
        Else
            Exit Sub
        End If

        If dg_lineas.SelectedRows(0).Cells("Alm.").Value = "A07" Then
            'Si estamos descargando en A07, mirar si tiene pick en A07, para avisar de que lo creen. Pedido por Domingo
            sql = "select top 1 * "
            sql &= "from ubicaciones where tipo='Picking' and codArticulo ='" & codarticulo & "' and codalmacen='A07' "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 0 Then
                MsgBox("Esta referencia no tiene ubicación de picking en A07, debería crearla", MsgBoxStyle.Information)
            End If
        End If

        If dg_lineas.SelectedRows(0).Cells("Alm.").Value = "A08" Then
            'Si estamos descargando en A08, mirar si tiene pick en A08, para avisar de que lo creen. Pedido por Domingo
            sql = "select top 1 * "
            sql &= "from ubicaciones where tipo='Picking' and codArticulo ='" & codarticulo & "' and codalmacen='A08' "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 0 Then
                MsgBox("Esta referencia no tiene ubicación de picking en A08, debería crearla", MsgBoxStyle.Information)
            End If
        End If


        'Obtener la cantidad en picking en A01
        'Domingo me dijo que el almacen a llenar el pick siempre sería el A01, ya que se supone que lo vendido se va a servir de allí. 
        sql = "select codalmacen,ubicacion, cantidad,cantmax "
        sql &= "from ubicaciones where tipo='Picking' "
        sql &= "and codArticulo ='" & codarticulo & "' and codalmacen='A01' "
        sql &= "order by id asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        If vdatos.Rows.Count = 0 Then
            MsgBox("Esta referencia no tiene ubicación de picking en A01, no se puede calcular la cantidad a ubicar en el picking A01.", MsgBoxStyle.Information)
            Exit Sub
        Else

            'Mostrar ventana con información de cant. max y un/caja, por si se quiere cambiar
            frm_ajuste_unidades.lab_referencia.Text = codarticulo
            frm_ajuste_unidades.lab_codalmpick.Text = vdatos.Rows(0).Item("codalmacen")
            frm_ajuste_unidades.lab_ubipick.Text = vdatos.Rows(0).Item("ubicacion")
            frm_ajuste_unidades.txt_cantmax.Text = vdatos.Rows(0).Item("cantmax")
            frm_ajuste_unidades.txt_unicaja.Text = unidcaja
            frm_ajuste_unidades.ShowDialog()


            '- Simon quiere que se pueda elegir si hacer o no el reaprov automatico. 19/01/2018
            'Pero quiere que salga en esta posición, ya que lo suyo sería antes de llamar a esta función, pero quiere ver la venta de ajuste_unidades antes
            'de que salga la pregunta
            If MsgBox("Quiere realizar el reaprov. automático del picking/Traslados?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Exit Sub
            End If


            unidcaja = frm_ajuste_unidades.txt_unicaja.Text
            cantpick = vdatos.Rows(0).Item("cantidad")
            cantmaxpick = frm_ajuste_unidades.txt_cantmax.Text
            codalmacen_pick = vdatos.Rows(0).Item("codalmacen")
            ubicacion_pick = vdatos.Rows(0).Item("ubicacion")
            If cantmaxpick = 0 Then
                MsgBox("Esta referencia no tiene puesta la cantidad máxima en la ubicación de picking, no se puede calcular la cantidad a ubicar en Traslados")
                Exit Sub
            Else
                'Llenar primero el picking
                list_almacen.Text = codalmacen_pick
                list_tipo.Text = "Picking"
                txt_pasillo.Text = ubicacion_pick.ToString.Substring(0, 2)
                txt_portal.Text = ubicacion_pick.ToString.Substring(3, 2)
                txt_altura.Text = ubicacion_pick.ToString.Substring(6, 2)
                If cantaubicar <= (cantmaxpick - cantpick) Then
                    txt_cantidad.Text = (cantaubicar \ unidcaja) * unidcaja
                Else
                    txt_cantidad.Text = ((cantmaxpick - cantpick) \ unidcaja) * unidcaja
                End If


                If cantaubicar = 0 Then Exit Sub 'Puede que se reciba menos de la cantidad por caja, por tanto no se puede ubicar ni una caja

                cantaubicar -= txt_cantidad.Text 'Lo pongo aqui, porque al lanzar la funcion de añadir, cambia la cantidad en el textbox y pierdo el dato anterior
                añadir()

                'Si queda por ubicar, se ubica en consolidacion
                'Sergio me pidio por telefono que ubicara en traslados.

                If cantaubicar > 0 And cantaubicar >= unidcaja And cantpendservhoy > cantmaxpick Then ' Domingo/sergio no quieren que se pueda poner uniddes sueltas, han de ser cajas
                    'Avisar si quiere que se reaprovisione en Traslados
                    If MsgBox("Esta referencia tiene un pendiente de servir superior a la cantidad máxima del picking, quiere ubicar una cantdad en Traslados?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

                    If list_almacen.Text = "A01" Then
                        'ubicacion_cons = constantes.consolidacion_A01
                        ubicacion_tras = constantes.traslados_A01
                    End If
                    If list_almacen.Text = "A07" Then
                        'ubicacion_cons = constantes.consolidacion_A07
                        ubicacion_tras = constantes.traslados_A07
                    End If
                    If list_almacen.Text = "A08" Then
                        'ubicacion_cons = constantes.consolidacion_A08
                        ubicacion_tras = constantes.traslados_A08
                    End If


                    list_tipo.Text = "Picking"
                    txt_pasillo.Text = ubicacion_tras.ToString.Substring(0, 2)
                    txt_portal.Text = ubicacion_tras.ToString.Substring(3, 2)
                    txt_altura.Text = ubicacion_tras.ToString.Substring(6, 2)


                    txt_cantidad.Text = ((cantpendservhoy - cantmaxpick) \ unidcaja) * unidcaja

                    If txt_cantidad.Text = 0 Then Exit Sub

                    añadir()
                End If
            End If
        End If

    End Sub

    Public Sub llenar_ubicadas(ByVal indice_sel As Integer)
        Dim cont As Integer
        Dim sql As String
        Dim vdatos As New DataTable
        Dim valm As New DataTable

        sql = "select codalmacen as 'Alm.', ubicacion as 'Ubicación',tipo as 'Tipo',cant_depos as 'Cant.',numreferencia as 'Nref.' "
        sql &= "from contenedores_ubicados_lin where codempresa=" & list_empresa.SelectedValue & " and "
        sql &= "codperiodo=" & list_periodo.SelectedValue & " and numcontenedor=" & list_contenedor.Text & " "
        sql &= "and codarticulo='" & dg_lineas.Rows(indice_sel).Cells("Ref.").Value & "' "
        sql &= "order by ubicacion desc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)


        dg_ubicacion_destino.DataSource = vdatos
        dg_ubicacion_destino.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18)
        dg_ubicacion_destino.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18, FontStyle.Bold)
        'dg_ubicacion_destino.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        dg_ubicacion_destino.Columns(0).Width = 70
        dg_ubicacion_destino.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicacion_destino.Columns(1).Width = 140
        dg_ubicacion_destino.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicacion_destino.Columns(2).Width = 90
        dg_ubicacion_destino.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_ubicacion_destino.Columns(3).Width = 150
        dg_ubicacion_destino.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_ubicacion_destino.Columns(4).Width = 10
        dg_ubicacion_destino.Columns(4).DefaultCellStyle.ForeColor = Color.White

        dg_ubicacion_destino.CurrentCell = Nothing
    End Sub


    Private Sub btn_trasladar_Click(sender As System.Object, e As System.EventArgs) Handles btn_trasladar.Click
        If IsNumeric(list_contenedor.Text) Then
            añadir()
        Else
            MsgBox("El campo 'Contenedor' está vacío, no se puede continuar. Insertar el número de contenedor.", MsgBoxStyle.Information)
        End If

    End Sub

    Public Sub añadir()
        Dim sql, sql2, sqlmov As String
        Dim resultado As String
        Dim vubi As New DataTable
        Dim cantant As Integer
        Dim numrefmov As String
        Dim nummov As Integer

        If txt_pasillo.Text = "" Or txt_portal.Text = "" Or txt_altura.Text = "" Then
            MsgBox("La ubicación no puede ser vacía.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Len(txt_pasillo.Text) <> 2 Or Len(txt_portal.Text) <> 2 Or Len(txt_altura.Text) <> 2 Then
            MsgBox("La Ubicación destino no es correcta.")
            Exit Sub
        End If


        If Not IsNumeric(txt_cantidad.Text) Then
            txt_cantidad.Text = 0
            Exit Sub
        Else
            If txt_cantidad.Text <= 0 Then
                txt_cantidad.Text = 0
                MsgBox("La cantidad debe ser mayor que 0.", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        If list_almacen.Text <> dg_lineas.SelectedRows(0).Cells("Alm.").Value Then
            If MsgBox("Se va a ubicar en un almacén diferente del almacén del contenedor, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
        End If


        'Añadir el registro en la tabla
        sql = "insert into contenedores_ubicados_lin(codempresa,codperiodo,numcontenedor,codarticulo,codalmacen,ubicacion,tipo,cant_cont,cant_depos,ultusuario) "
        sql &= "values(" & list_empresa.SelectedValue & "," & list_periodo.SelectedValue & "," & list_contenedor.Text & ",'" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "',"
        sql &= "'" & list_almacen.Text & "','" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "','" & list_tipo.Text & "'," & dg_lineas.SelectedRows(0).Cells("Cant.").Value & ","
        sql &= "" & txt_cantidad.Text & ",'" & frm_principal.lab_nomoperario.Text & "')"

        'Si es picking, se suma a lo q hay, si es pulmon se añade un nuevo palet en la ubicacion, no se modifica el que hay.
        If list_tipo.Text = "Picking" Then
            'Mirar si existe la ubicacion de picking
            sql2 = "select id,cantidad from ubicaciones where codalmacen='" & list_almacen.Text & "' and  codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.ToUpper & "' and ubicacion='" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' and tipo='Picking' "
            vubi = conexion.TablaxCmd(constantes.bd_intranet, sql2)
            If vubi.Rows.Count = 0 Then
                'Si la ubicacion es de consolidación, se crea
                'Ahora es traslados. Pedido por sergio
                If txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text = constantes.traslados_A01 Or txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text = constantes.traslados_A07 Or txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text = constantes.traslados_A08 Then
                    sql &= "insert into ubicaciones (codalmacen,ubicacion,codarticulo,tipo,cantidad,observaciones,ultusuario) "
                    sql &= "values('" & list_almacen.Text & "','" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "','" & list_tipo.Text & "'," & txt_cantidad.Text & ",'Ubicaciones-contenedor','" & frm_principal.lab_nomoperario.Text & "') "
                Else
                    MsgBox("No existe esta ubicación para la referencia seleccionada, no se añadirá el stock a esta ubicación", MsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                cantant = vubi.Rows(0).Item("cantidad")
                sql &= "update ubicaciones set cantidad =" & cantant & " + " & txt_cantidad.Text & " where id=" & vubi.Rows(0).Item("id") & " "
            End If

        Else
            sql &= "insert into ubicaciones (codalmacen,ubicacion,codarticulo,tipo,cantidad,observaciones,ultusuario) "
            sql &= "values('" & list_almacen.Text & "','" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "','" & list_tipo.Text & "'," & txt_cantidad.Text & ",'Ubicaciones-contenedor','" & frm_principal.lab_nomoperario.Text & "') "
        End If
        'Añadir registro de movimiento
        sql &= "insert into ubicaciones_movimientos(codalmacen,ubicacion,codarticulo,cantidad_ant,cantidad,fecha,aplicacion,documento,usuario,idoperario) "
        sql &= "values ('" & list_almacen.Text & "','" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "'," & cantant & "," & txt_cantidad.Text & ",getdate(),'Ubicaciones-Contenedor','Añadir línea ubicada','" & frm_principal.lab_nomoperario.Text & "'," & frm_principal.lab_idoperario.Text & ") "

        'Si es cambio de almacen hay que generar un movimiento para pasar esta cantidad que se esta ubicando al otro almacén
        If list_almacen.Text <> dg_lineas.SelectedRows(0).Cells("Alm.").Value Then
            funciones.crear_movimiento_inase(list_empresa.SelectedValue, nummov, "Ubicaciones-Ubic.ContenedorMov (al ubicar cant. en almacen dif) Con:" & list_contenedor.Text & " Ref:" & dg_lineas.SelectedRows(0).Cells("Ref.").Value, numrefmov)
            sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacenori,ubicacionori,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
            sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & frm_principal.codempresa & "," & Now.Year & "," & nummov & ",1,'" & dg_lineas.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & list_almacen.Text & "','XXX','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "'," & txt_cantidad.Text & ",'Cambio de almacén desde ubicaciones-contenedor') "
        End If

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado = "0" Then
            'Llenar ubicadas
            llenar_ubicadas(dg_lineas.SelectedRows(0).Index)

            'Act la cantidad ubicada en lineas cont
            dg_lineas.SelectedRows(0).Cells("Ubicada").Value += txt_cantidad.Text

            'Pintar filas lineas cont
            Pintar_filas()

            txt_cantidad.Text = dg_lineas.SelectedRows(0).Cells("Cant.").Value - dg_lineas.SelectedRows(0).Cells("Ubicada").Value
            txt_pasillo.Text = ""
            txt_portal.Text = ""
            txt_altura.Text = ""
        Else
            MsgBox("Se produjo un error, La cantidad no será insertada en la ubicación", MsgBoxStyle.Exclamation)
        End If
    End Sub
    Public Sub Pintar_filas()
        Dim cont As Integer
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            If dg_lineas.Rows(cont).Cells("Cant.").Value = dg_lineas.Rows(cont).Cells("Ubicada").Value Then dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
            If dg_lineas.Rows(cont).Cells("Cant.").Value < dg_lineas.Rows(cont).Cells("Ubicada").Value Then dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Red
            If dg_lineas.Rows(cont).Cells("Cant.").Value > dg_lineas.Rows(cont).Cells("Ubicada").Value And dg_lineas.Rows(cont).Cells("Ubicada").Value > 0 Then dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
            If dg_lineas.Rows(cont).Cells("Ubicada").Value = 0 Then dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.White
            cont += 1
        Loop
    End Sub

    Private Sub txt_cantidad_Click(sender As System.Object, e As System.EventArgs) Handles txt_cantidad.Click
        txt_cantidad.SelectAll()
    End Sub

    Private Sub txt_cantidad_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_cantidad.KeyUp
        If e.KeyCode = Keys.Enter Then
            If IsNumeric(txt_cantidad.Text) Then btn_trasladar.Focus()
        End If
    End Sub

    Private Sub list_contenedor_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles list_contenedor.KeyUp
        Dim sql As String
        Dim vdatos As New DataTable

        If e.KeyCode = Keys.Enter Then
            sql = "select isnull(ubi.finalizado,'') from inase.dbo.gescontenedores con "
            sql &= "left join contenedores_ubicados ubi on ubi.codempresa=con.codempresa "
            sql &= "and ubi.codperiodo=con.codperiodo and ubi.numcontenedor=con.numcontenedor "
            sql &= "where con.codempresa=" & list_empresa.SelectedValue & " and con.codperiodo=" & list_periodo.SelectedValue & " and con.numcontenedor=" & list_contenedor.Text
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 0 Then
                MsgBox("El contenedor no existe.")
                Exit Sub
            Else
                If vdatos.Rows(0)(0) = "1" Then
                    MsgBox("Este contenedor ya ha sido finalizado")
                    If frm_principal.PermContenedorAbrirFinalizado = True Then
                        llenar_lineas()
                        btn_finalizar.Enabled = False
                        btn_eliminar.Enabled = False
                        btn_cerrar.Enabled = True
                    Else
                        Exit Sub
                    End If
                Else
                    btn_finalizar.Enabled = True
                    btn_eliminar.Enabled = True
                    btn_cerrar.Enabled = True
                    list_contenedor.Enabled = False
                    llenar_lineas()
                    If list_contenedor.Text <> "" Then guardar_datos_contenedor()
                End If
            End If

        End If
    End Sub

    Private Sub txt_pasillo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_pasillo.KeyUp
        If txt_pasillo.Text.Length = 2 Then
            txt_portal.Focus()
            txt_portal.SelectAll()
        End If
    End Sub

    Private Sub txt_portal_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_portal.KeyUp
        If txt_portal.Text.Length = 2 Then
            txt_altura.Focus()
            txt_altura.SelectAll()
        End If
    End Sub

    Private Sub txt_altura_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_altura.KeyUp
        If txt_altura.Text.Length = 2 Then
            btn_trasladar.Focus()
        End If
    End Sub



    Private Sub btn_eliminar_Click(sender As System.Object, e As System.EventArgs) Handles btn_eliminar.Click
        Dim sql, sql2 As String
        Dim resultado As String
        Dim vubi As New DataTable
        Dim cantant As Integer
        Dim nummov As Integer
        Dim numrefmov As String

        If Not IsNumeric(list_contenedor.Text) Then
            MsgBox("El campo 'Contenedor' está vacío, no se puede continuar. Insertar el número de contenedor.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If dg_ubicacion_destino.SelectedRows.Count = 0 Then
            MsgBox("Seleccionar la fila a eliminar")
        Else
            'eliminar el registro
            sql = "delete contenedores_ubicados_lin where numreferencia='" & dg_ubicacion_destino.SelectedRows(0).Cells("Nref.").Value.ToString & "'"

            'Eliminar el stock o la ubicacion (si es pick, se resta, si es pulmon se elimina
            'Mirar si existe la ubicacion 
            sql2 = "select id,cantidad from ubicaciones where codalmacen='" & dg_ubicacion_destino.SelectedRows(0).Cells("Alm.").Value & "' and  codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "' and ubicacion='" & dg_ubicacion_destino.SelectedRows(0).Cells("Ubicación").Value & "' and tipo='" & dg_ubicacion_destino.SelectedRows(0).Cells("Tipo").Value & "' "
            If dg_ubicacion_destino.SelectedRows(0).Cells("Tipo").Value = "Pulmon" Then sql2 &= "and cantidad=" & dg_ubicacion_destino.SelectedRows(0).Cells("Cant.").Value & " "

            vubi = conexion.TablaxCmd(constantes.bd_intranet, sql2)
            If vubi.Rows.Count = 0 Then
                MsgBox("No existe esta ubicación para la referencia seleccionada, no se eliminará el stock a esta ubicación", MsgBoxStyle.Information)
                Exit Sub
            Else
                cantant = vubi.Rows(0).Item("cantidad")
                If dg_ubicacion_destino.SelectedRows(0).Cells("Tipo").Value.ToString = "Picking" Then
                    sql &= "update ubicaciones set cantidad =" & cantant & " - " & dg_ubicacion_destino.SelectedRows(0).Cells("Cant.").Value & " where id=" & vubi.Rows(0).Item("id") & " "
                Else
                    sql &= "delete from ubicaciones where id=" & vubi.Rows(0).Item("id") & ""
                End If
                'Eliminar ubicaciones de traslados que se han quedado vacias
                sql &= "delete from ubicaciones where cantidad=0 and tipo='Picking' and ubicacion in ('" & constantes.traslados_A01 & "','" & constantes.traslados_A07 & "','" & constantes.traslados_A08 & "') "
            End If

            'Añadir registro de movimiento
            sql &= "insert into ubicaciones_movimientos(codalmacen,ubicacion,codarticulo,cantidad_ant,cantidad,fecha,aplicacion,documento,usuario,idoperario) "
            sql &= "values ('" & dg_ubicacion_destino.SelectedRows(0).Cells("Alm.").Value & "','" & dg_ubicacion_destino.SelectedRows(0).Cells("Ubicación").Value & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "'," & cantant & "," & dg_ubicacion_destino.SelectedRows(0).Cells("Cant.").Value * -1 & ",getdate(),'Ubicaciones-Contenedor','Eliminar línea ubicada','" & frm_principal.lab_nomoperario.Text & "'," & frm_principal.lab_idoperario.Text & ") "
            resultado = conexion.Executetransact(constantes.bd_intranet, sql)
            If resultado = "0" Then

                'Si lo que se elimina es de un almacen diferente al que tiene el albaran de compra, hay que volver el stock a ese almacen
                If dg_ubicacion_destino.SelectedRows(0).Cells("Alm.").Value <> dg_lineas.SelectedRows(0).Cells("Alm.").Value Then
                    funciones.crear_movimiento_inase(list_empresa.SelectedValue, nummov, "Ubicaciones-Elim Ubic.Contenedor. Mov (al ubicar cant. en almacen dif)", numrefmov)
                    sql = "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacenori,ubicacionori,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                    sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & frm_principal.codempresa & "," & Now.Year & "," & nummov & ",1,'" & dg_ubicacion_destino.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & dg_lineas.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "'," & dg_ubicacion_destino.SelectedRows(0).Cells("Cant.").Value & ",'Cambio de almacén desde ubicaciones-contenedor') "
                End If

                resultado = conexion.Executetransact(constantes.bd_intranet, sql)


                'Act la cantidad ubicada en lineas cont
                dg_lineas.SelectedRows(0).Cells("Ubicada").Value -= dg_ubicacion_destino.SelectedRows(0).Cells("Cant.").Value.ToString

                txt_cantidad.Text = dg_lineas.SelectedRows(0).Cells("Cant.").Value - dg_lineas.SelectedRows(0).Cells("Ubicada").Value

                'Eliminar la fila
                dg_ubicacion_destino.Rows.Remove(dg_ubicacion_destino.SelectedRows(0))

                Pintar_filas()
            Else
                MsgBox("Se produjo un error, La línea no será eliminada de la ubicación", MsgBoxStyle.Exclamation)
            End If

        End If
    End Sub

    Private Sub dg_lineas_Sorted(sender As System.Object, e As System.EventArgs) Handles dg_lineas.Sorted
        Pintar_filas()
    End Sub

    Private Sub list_tipo_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_tipo.DropDownClosed
        txt_pasillo.Text = ""
        txt_altura.Text = ""
        txt_portal.Text = ""
        If list_tipo.Text = "Picking" Then llenar_ubicacion_picking()
    End Sub

    Private Sub list_almacen_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_almacen.DropDownClosed
        txt_pasillo.Text = ""
        txt_altura.Text = ""
        txt_portal.Text = ""
        If list_tipo.Text = "Picking" Then llenar_ubicacion_picking()
    End Sub

    Public Sub llenar_ubicacion_picking()
        Dim sql
        Dim vdatos As New DataTable
        txt_pasillo.Text = ""
        txt_portal.Text = ""
        txt_altura.Text = ""
        'Llenar unbicacion picking

        If list_almacen.Text <> dg_lineas.SelectedRows(0).Cells("Alm.").Value Then
            'Llenar el reaprov. Pedido por Simon
            'Sergio pidio que se llenara el traslado en lugar de consolidacion
            If MsgBox("Quiere ubicar en la ubicación de traslados?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then GoTo salto
            If list_almacen.Text = "A01" Then
                txt_pasillo.Text = constantes.traslados_A01.ToString.Substring(0, 2)
                txt_portal.Text = constantes.traslados_A01.ToString.Substring(3, 2)
                txt_altura.Text = constantes.traslados_A01.ToString.Substring(6, 2)
            End If
            If list_almacen.Text = "A07" Then
                txt_pasillo.Text = constantes.traslados_A07.ToString.Substring(0, 2)
                txt_portal.Text = constantes.traslados_A07.ToString.Substring(3, 2)
                txt_altura.Text = constantes.traslados_A07.ToString.Substring(6, 2)
            End If
            If list_almacen.Text = "A08" Then
                txt_pasillo.Text = constantes.traslados_A08.ToString.Substring(0, 2)
                txt_portal.Text = constantes.traslados_A08.ToString.Substring(3, 2)
                txt_altura.Text = constantes.traslados_A08.ToString.Substring(6, 2)
            End If
        Else
salto:
            sql = "select ubicacion from ubicaciones where codalmacen='" & list_almacen.Text & "' and codarticulo='" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "' and tipo='Picking' "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count > 0 Then
                txt_pasillo.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(0, 2)
                txt_portal.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(3, 2)
                txt_altura.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(6, 2)
                txt_cantidad.Focus()
                txt_cantidad.SelectAll()
            Else
                txt_pasillo.Focus()
                txt_pasillo.SelectAll()
            End If
        End If

       
    End Sub

    Private Sub dg_lineas_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_lineas.CellClick
        'Comprobar si la cantidad pendiente de servir (de la próxima semana) de esta referencia es mayor que la cantidad que hay  en el picking. 
        'Si no hay bastante, se reaprovisiona el picking y la cantidad pend servir - cant picking, se deja en reaprov. (cajas)
        'Solo se hará esta comprobación si todavía no hay cantidad ubicada. Si ya hay cantidad ubicada, significa que ya se ha hecho la ubicacion en el reaprov.

        'El 05/01/2018 sergio se quejo de que no se estaba haciendo bien y esto es lo que hacia:
        '- Mira el pendiente de servir hasta hoy y esta cantidad (en multiplo cajas) es la que llena del picking, 
        '- si no caben en el picking las llena en Traslados
        'Ahora quiere:
        '-llene todo el picking (en cajas) y si el pendiente de servir es mayor que la cantidad máxima de picking, que el resto lo deje en Traslados (en cajas)


        If dg_lineas.Rows(e.RowIndex).Cells("Ubicada").Value = 0 Then
            reaprov_ref(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value)
        End If

        llenar_ubicadas(e.RowIndex)
        txt_cantidad.Text = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value - dg_lineas.Rows(e.RowIndex).Cells("Ubicada").Value
        txt_pasillo.Focus()
        txt_pasillo.SelectAll()
        list_tipo.SelectedIndex = 0
        txt_pasillo.Text = ""
        txt_altura.Text = ""
        txt_portal.Text = ""
    End Sub


    Private Sub btn_cerrar_Click(sender As Object, e As EventArgs) Handles btn_cerrar.Click
        limpiar(True, "Cerrar")
    End Sub
End Class
