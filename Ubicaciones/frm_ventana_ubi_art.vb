﻿Public Class frm_ventana_ubi_art
    Dim conexion As New Conector
    Private Sub frm_ventana_ubi_art_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txt_descripcion_articulo.Text = funciones.buscar_descripcion(txt_articulo.Text)
        mostrar_ubicaciones_articulo()
    End Sub
    Private Sub btn_cerrar_Click(sender As System.Object, e As System.EventArgs) Handles btn_cerrar.Click
        Me.Dispose()
    End Sub

    Public Sub mostrar_ubicaciones_articulo()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer
        txt_articulo.Text = txt_articulo.Text.ToUpper.Trim
        If txt_articulo.Text.Trim = "" Then
            MsgBox("Rellenar el campo artículo.", MsgBoxStyle.Exclamation)
            txt_articulo.Focus()
            Exit Sub
        End If
        vdatos.Rows.Clear()

        'Consulta ubicaciones
        sql = "select  ubi.id as 'Id',ubi.codalmacen as 'Alm.',ubi.codarticulo as 'Ref.',ubi.ubicacion as 'Ubicacion',ubi.tipo as 'Tipo',isnull(ubi.tamaño,0) as 'Tam.' , isnull(ubi.Cantidad,0) as 'Cant.', isnull(ubi.cantmax,0) as 'Max.',"
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A01' then sto.stockactual end ),0)) as A01, "
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A06' then sto.stockactual end ),0)) as A06, "
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A07' then sto.stockactual end ),0)) as A07 "
        sql &= "from ubicaciones ubi "
        sql &= "left join inase.dbo.gesstocks sto on sto.codarticulo COLLATE Modern_Spanish_CI_AS=ubi.codarticulo and sto.codalmacen in ('A01','A06','A07')  "
        sql &= "where ubi.codarticulo like '" & txt_articulo.Text.Trim & "%' "
        sql &= "group by ubi.id,ubi.codalmacen,ubi.codarticulo,ubi.ubicacion,ubi.tipo,ubi.tamaño, ubi.Cantidad,ubi.cantmax "
        sql &= "order by ubi.tipo,ubi.ubicacion "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        dg_ubicaciones.DataSource = vdatos
        dg_ubicaciones.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18, FontStyle.Bold)
        dg_ubicaciones.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18)
        dg_ubicaciones.Columns(0).Width = 2
        dg_ubicaciones.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(0).ReadOnly = True
        dg_ubicaciones.Columns(1).Width = 60
        dg_ubicaciones.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(1).ReadOnly = True
        dg_ubicaciones.Columns(2).Width = 105
        dg_ubicaciones.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicaciones.Columns(2).ReadOnly = True
        dg_ubicaciones.Columns(3).Width = 120
        dg_ubicaciones.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicaciones.Columns(3).ReadOnly = True
        dg_ubicaciones.Columns(4).Width = 80
        dg_ubicaciones.Columns(4).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold)
        dg_ubicaciones.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicaciones.Columns(4).ReadOnly = True
        dg_ubicaciones.Columns(5).Width = 65
        dg_ubicaciones.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(5).ReadOnly = False
        dg_ubicaciones.Columns(6).Width = 70
        dg_ubicaciones.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(6).ReadOnly = False
        dg_ubicaciones.Columns(7).Width = 70
        dg_ubicaciones.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(7).ReadOnly = False
        dg_ubicaciones.Columns(8).Width = 70
        dg_ubicaciones.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(8).ReadOnly = True
        dg_ubicaciones.Columns(9).Width = 70
        dg_ubicaciones.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(9).ReadOnly = True
        dg_ubicaciones.Columns(10).Width = 70
        dg_ubicaciones.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(10).ReadOnly = True
        'dg_ubicaciones.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells

        'Pintar las filas de color dependiendo del almacen. pedido por Sergio
        cont = 0
        Do While cont < dg_ubicaciones.Rows.Count
            If dg_ubicaciones.Rows(cont).Cells(1).Value = "A01" Then
                dg_ubicaciones.Rows(cont).DefaultCellStyle.BackColor = Color.Blue
                dg_ubicaciones.Rows(cont).DefaultCellStyle.ForeColor = Color.White
            End If
            If dg_ubicaciones.Rows(cont).Cells(1).Value = "A07" Then
                dg_ubicaciones.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                dg_ubicaciones.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If
            cont += 1
        Loop


    End Sub

End Class