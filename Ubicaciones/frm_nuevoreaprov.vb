﻿Public Class frm_nuevoreaprov
    Dim conexion As New Conector

    Private Sub frm_nuevoreaprov_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        list_almdestino.Enabled = True
        list_almorigen.Enabled = True
        list_almorigen.SelectedIndex = 0
        list_almdestino.SelectedIndex = 0
        dg_lineas.Rows.Clear()
        cabecera_dg_lineas()
        btn_finalizar.Enabled = False
        txt_ref.Focus()
        txt_ref.SelectAll()

    End Sub
    Public Sub cabecera_dg_lineas()
        If dg_lineas.ColumnCount > 0 Then Exit Sub
        dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 20)
        dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 22, FontStyle.Bold)

        'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        'dg_lineas.AutoResizeColumns()
        dg_lineas.Columns.Add("Articulo", "Articulo")
        dg_lineas.Columns("Articulo").Width = 140
        dg_lineas.Columns("Articulo").ReadOnly = True
        dg_lineas.Columns.Add("Descripcion", "Descripcion")
        dg_lineas.Columns("Descripcion").Width = 500
        dg_lineas.Columns("Descripcion").ReadOnly = True
        dg_lineas.Columns("Descripcion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
        dg_lineas.Columns.Add("Cantidad", "Cant.")
        dg_lineas.Columns("Cantidad").Width = 100
        dg_lineas.Columns("Cantidad").ReadOnly = True
        dg_lineas.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas.Columns.Add("Ubi.Ori.", "Ubi.Ori.")
        dg_lineas.Columns("Ubi.Ori.").Width = 145
        dg_lineas.Columns("Ubi.Ori.").ReadOnly = True
        dg_lineas.Columns("Ubi.Ori.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_lineas.Columns.Add("Ubi.dest.", "Ubi.dest.")
        dg_lineas.Columns("Ubi.dest.").Width = 150
        dg_lineas.Columns("Ubi.dest.").ReadOnly = True
        dg_lineas.Columns("Ubi.dest.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
    End Sub

    Private Sub btn_cancelar_Click(sender As System.Object, e As System.EventArgs) Handles btn_cancelar.Click
        If MsgBox("La orden no será creada, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub btn_añadirref_Click(sender As System.Object, e As System.EventArgs) Handles btn_añadirref.Click
        Dim sql As String
        Dim traslados As String

        If list_almorigen.Text = list_almdestino.Text Then
            MsgBox("El almacén origen no puede ser el mismo que el destino", MsgBoxStyle.Information)
            Exit Sub
        End If

        'Sergio pidió que para los reaprovisionamientos manuales (traslados) la mercancia vaya a parar a una ubicación de "traslados"

        If list_almdestino.Text = "A01" Then traslados = constantes.traslados_A01
        If list_almdestino.Text = "A07" Then traslados = constantes.traslados_A07


        If IsNumeric(txt_cantidad.Text) Then
            If lab_descripcion_articulo.Text <> "" Then
                dg_lineas.Rows.Add(New String() {txt_ref.Text.Trim, lab_descripcion_articulo.Text.Trim.ToUpper, txt_cantidad.Text, list_ubicacionorigen.SelectedValue, traslados})
                list_almdestino.Enabled = False
                list_almorigen.Enabled = False
                btn_finalizar.Enabled = True
                txt_ref.Text = ""
                txt_cantidad.Text = ""
                txt_ref.Focus()
            Else
                MsgBox("No existe la referéncia.", MsgBoxStyle.Exclamation)
            End If
        Else
            MsgBox("La cantidad debe ser un valor numérico.", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql, sqlinsert As String
        Dim resultado As String
        Dim vreaprov As New DataTable
        Dim codreaprov As Integer
        Dim cont As Integer
        Dim ubidestino As String

        If list_almdestino.Text = "A01" Then ubidestino = constantes.traslados_A01
        If list_almdestino.Text = "A07" Then ubidestino = constantes.traslados_A07


        sqlinsert = "insert into reaprov(codalmacenori,codalmacendest,fechaalta,usuarioalta,procesado,origen) values('" & list_almorigen.Text & "','" & list_almdestino.Text & "',getdate(),'" & frm_principal.lab_nomoperario.Text & "','NO','Ubicaciones')"
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sqlinsert)
        'Obtener el codreaprov para insertar las lineas
        sql = "select top 1 codreaprov from reaprov order by codreaprov desc "
        vreaprov = conexion.TablaxCmd(constantes.bd_intranet, sql)
        codreaprov = vreaprov.Rows(0)(0)

        sqlinsert = ""
        cont = 0
        Do While cont < dg_lineas.Rows.Count
            ''añadir la linea al reaprov.
            sqlinsert &= "insert into reaprovlin(codreaprov,codlinea,codarticulo,cantidad,canttrasladada,codalmacen_origen,ubicacion_origen,codalmacen_destino,ubicacion_destino,ultusuario,ultfecha) values(" & codreaprov & "," & cont + 1 & ",'" & dg_lineas.Rows(cont).Cells("Articulo").Value & "'," & dg_lineas.Rows(cont).Cells("Cantidad").Value & ",0,'" & list_almorigen.Text & "','" & dg_lineas.Rows(cont).Cells("Ubi.Ori.").Value & "','" & list_almdestino.Text & "','" & ubidestino & "','" & frm_principal.lab_nomoperario.Text & "',getdate()) "
            cont = cont + 1
        Loop

        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sqlinsert)
        If resultado = "0" Then
            MsgBox("Reaprovisionamiento " & codreaprov & " creado correctamente.", MsgBoxStyle.Exclamation)
            Me.Close()
        Else
            MsgBox("Se produjo un error al insertar las lineas del reaprov.", MsgBoxStyle.Exclamation)
        End If

    End Sub

    Private Sub frm_nuevoreaprov_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frm_reaprov.llenar_reaprov()
    End Sub

    Private Sub txt_ref_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_ref.KeyUp
        Dim codarticulo As String = txt_ref.Text.Trim.ToUpper
        lab_descripcion_articulo.Text = ""
        If e.KeyCode = Keys.Enter Then
            If txt_ref.Text.Length = 7 Or txt_ref.Text.Length = 13 Then
                lab_descripcion_articulo.Text = funciones.buscar_descripcion(codarticulo)
                If lab_descripcion_articulo.Text = "" Then
                    MsgBox("No existe la referencia", MsgBoxStyle.Exclamation)
                Else
                    txt_ref.Text = codarticulo 'La funcion actualiza el dato de codarticulo, ya q se pasa por ref.
                    funciones.llenar_ubicaciones(list_ubicacionorigen, codarticulo, list_almorigen.Text)
                    txt_cantidad.Focus()
                End If
            ElseIf txt_ref.Text.Length > 0 Then
                MsgBox("No existe la referencia", MsgBoxStyle.Exclamation)
            End If
        End If
    End Sub

    Private Sub list_almorigen_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_almorigen.SelectedIndexChanged
        funciones.llenar_ubicaciones(list_ubicacionorigen, txt_ref.Text, list_almorigen.Text)
    End Sub

    Private Sub txt_cantidad_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_cantidad.KeyUp
        If e.KeyCode = Keys.Enter And IsNumeric(txt_cantidad.Text) Then
            btn_añadirref.Focus()
        End If
    End Sub
End Class