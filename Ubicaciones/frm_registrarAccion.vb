﻿Public Class frm_registrarAccion
    Dim conexion As New Conector

    Private Sub frm_registrarAccion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Lab_motivo.Visible = False
        list_motivos.Visible = False
        llenar_acciones
        llenar_motivos()
        list_accion.SelectedIndex = -1
        list_maquinas.SelectedIndex = -1
        lab_IdOperario.Text = frm_principal.lab_idoperario.Text
        lab_NombreOperario.Text = frm_principal.lab_nomoperario.Text
    End Sub
    Public Sub llenar_acciones()
        Dim vdatos As New DataTable
        Dim sql As String

        sql = "select id,descripcion from AlmAcciones "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        list_accion.DisplayMember = "descripcion"
        list_accion.ValueMember = "id"
        list_accion.DataSource = vdatos
        list_accion.SelectedIndex = -1
    End Sub
    Public Sub llenar_motivos()
        Dim vdatos As New DataTable
        Dim sql As String

        sql = "select id,descripcion from AlmTareasTiposPausa where ParaSalaImpDigi=1 order by id asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        list_motivos.DisplayMember = "descripcion"
        list_motivos.ValueMember = "id"
        list_motivos.DataSource = vdatos
        list_motivos.SelectedIndex = -1
    End Sub
    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        Close()
        Dispose()
    End Sub

    Private Sub btn_aceptar_Click(sender As Object, e As EventArgs) Handles btn_aceptar.Click
        Dim sql As String
        Dim resultado As String
        Dim idMotivo As Integer = 0

        If list_accion.SelectedIndex = -1 Then
            MsgBox("Seleccionar la acción.")
            Exit Sub
        End If

        If list_motivos.Visible Then
            If list_motivos.SelectedIndex = -1 Then
                MsgBox("Seleccionar el motivo.")
                Exit Sub
            Else
                idMotivo = list_motivos.SelectedValue
            End If
        End If

        sql = "insert into AlmAccionesRegistro (idAccion,idoperario,idMotivo,Aplicacion) "
        sql &= "values (" & list_accion.SelectedValue & "," & lab_IdOperario.Text & "," & idMotivo & ",'Ubicaciones/Registrar Accion'); "
        sql &= "update operarios_almacen set IdUltimaAccion=" & list_accion.SelectedValue & ",FechaUltimaAccion=getdate() where id=" & lab_IdOperario.Text & " "

        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)

        If resultado = "0" Then
            If list_maquinas.SelectedIndex > -1 Then frm_principal.maquina = list_maquinas.Text
            Dispose()
            'Close()

            'Si ha registrado una salida de la sala de impresión, se debe salir del usuario para que el siguiente operario que quiera usar la aplicacion, tenga que poner su contraseña
            If list_accion.SelectedValue = 2 Then

                If frm_OrdenProduccion.IsHandleCreated Then
                    frm_OrdenProduccion.Close()
                End If


                'frm_principal.Close() 'No se puede cerrar porq desencadena el cierre de la aplicacion, al ser el formulario principal.
                frm_principal.Dispose() 'No hace falta, solo motrar encima el login modal. Lo he activado de nuevo para que se lilmpien todo ya que quedaba iluminado el botón del uñtimo formulario abierto
                frm_login.ShowDialog()
            End If
        Else
            MsgBox("Se produjo un error al registrar la acción.")
        End If

    End Sub

    Private Sub list_accion_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles list_accion.SelectionChangeCommitted
        If list_accion.SelectedValue = 2 Then
            Lab_motivo.Visible = True
            list_motivos.Visible = True
            lab_maquina.Visible = False
            list_maquinas.Visible = False
        Else
            Lab_motivo.Visible = False
            list_motivos.Visible = False
            lab_maquina.Visible = True
            list_maquinas.Visible = True
        End If
    End Sub
End Class