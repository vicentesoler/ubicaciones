﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_nuevoreaprov
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.list_almorigen = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.list_almdestino = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_ref = New System.Windows.Forms.TextBox()
        Me.txt_cantidad = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_añadirref = New System.Windows.Forms.Button()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.btn_cancelar = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.list_ubicacionorigen = New System.Windows.Forms.ComboBox()
        Me.lab_descripcion_articulo = New System.Windows.Forms.Label()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(513, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Generar orden de reaprovisionamiento"
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_lineas.Location = New System.Drawing.Point(17, 114)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(1079, 389)
        Me.dg_lineas.TabIndex = 16
        '
        'list_almorigen
        '
        Me.list_almorigen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_almorigen.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almorigen.FormattingEnabled = True
        Me.list_almorigen.Items.AddRange(New Object() {"A01", "A07"})
        Me.list_almorigen.Location = New System.Drawing.Point(720, 8)
        Me.list_almorigen.Name = "list_almorigen"
        Me.list_almorigen.Size = New System.Drawing.Size(78, 33)
        Me.list_almorigen.TabIndex = 17
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(581, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 25)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Alm. Origen:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(840, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(141, 25)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Alm. Destino:"
        '
        'list_almdestino
        '
        Me.list_almdestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_almdestino.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_almdestino.FormattingEnabled = True
        Me.list_almdestino.Items.AddRange(New Object() {"A07", "A01"})
        Me.list_almdestino.Location = New System.Drawing.Point(987, 8)
        Me.list_almdestino.Name = "list_almdestino"
        Me.list_almdestino.Size = New System.Drawing.Size(78, 33)
        Me.list_almdestino.TabIndex = 19
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(18, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(119, 25)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Añadir ref.:"
        '
        'txt_ref
        '
        Me.txt_ref.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ref.Location = New System.Drawing.Point(143, 55)
        Me.txt_ref.Name = "txt_ref"
        Me.txt_ref.Size = New System.Drawing.Size(127, 38)
        Me.txt_ref.TabIndex = 22
        '
        'txt_cantidad
        '
        Me.txt_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad.Location = New System.Drawing.Point(369, 55)
        Me.txt_cantidad.Name = "txt_cantidad"
        Me.txt_cantidad.Size = New System.Drawing.Size(85, 38)
        Me.txt_cantidad.TabIndex = 24
        Me.txt_cantidad.Text = "1"
        Me.txt_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(292, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 25)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Cant.:"
        '
        'btn_añadirref
        '
        Me.btn_añadirref.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_añadirref.Location = New System.Drawing.Point(851, 55)
        Me.btn_añadirref.Name = "btn_añadirref"
        Me.btn_añadirref.Size = New System.Drawing.Size(138, 38)
        Me.btn_añadirref.TabIndex = 52
        Me.btn_añadirref.Text = "Añadir"
        Me.btn_añadirref.UseVisualStyleBackColor = True
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(978, 512)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(118, 38)
        Me.btn_finalizar.TabIndex = 53
        Me.btn_finalizar.Text = "Finalizar"
        Me.btn_finalizar.UseVisualStyleBackColor = True
        '
        'btn_cancelar
        '
        Me.btn_cancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cancelar.Location = New System.Drawing.Point(18, 509)
        Me.btn_cancelar.Name = "btn_cancelar"
        Me.btn_cancelar.Size = New System.Drawing.Size(127, 38)
        Me.btn_cancelar.TabIndex = 54
        Me.btn_cancelar.Text = "Cancelar"
        Me.btn_cancelar.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(505, 62)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(122, 25)
        Me.Label6.TabIndex = 55
        Me.Label6.Text = "Ubi.Origen:"
        '
        'list_ubicacionorigen
        '
        Me.list_ubicacionorigen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_ubicacionorigen.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_ubicacionorigen.FormattingEnabled = True
        Me.list_ubicacionorigen.Items.AddRange(New Object() {"A01", "A07"})
        Me.list_ubicacionorigen.Location = New System.Drawing.Point(633, 55)
        Me.list_ubicacionorigen.Name = "list_ubicacionorigen"
        Me.list_ubicacionorigen.Size = New System.Drawing.Size(147, 39)
        Me.list_ubicacionorigen.TabIndex = 56
        '
        'lab_descripcion_articulo
        '
        Me.lab_descripcion_articulo.AutoSize = True
        Me.lab_descripcion_articulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_descripcion_articulo.ForeColor = System.Drawing.Color.DarkGreen
        Me.lab_descripcion_articulo.Location = New System.Drawing.Point(140, 94)
        Me.lab_descripcion_articulo.Name = "lab_descripcion_articulo"
        Me.lab_descripcion_articulo.Size = New System.Drawing.Size(34, 15)
        Me.lab_descripcion_articulo.TabIndex = 57
        Me.lab_descripcion_articulo.Text = "XXX"
        '
        'frm_nuevoreaprov
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Bisque
        Me.ClientSize = New System.Drawing.Size(1108, 562)
        Me.Controls.Add(Me.lab_descripcion_articulo)
        Me.Controls.Add(Me.list_ubicacionorigen)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btn_cancelar)
        Me.Controls.Add(Me.btn_finalizar)
        Me.Controls.Add(Me.btn_añadirref)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txt_cantidad)
        Me.Controls.Add(Me.txt_ref)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.list_almdestino)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.list_almorigen)
        Me.Controls.Add(Me.dg_lineas)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_nuevoreaprov"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nuevo Reaprovisionamiento"
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dg_lineas As System.Windows.Forms.DataGridView
    Friend WithEvents list_almorigen As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents list_almdestino As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_ref As System.Windows.Forms.TextBox
    Friend WithEvents txt_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btn_añadirref As System.Windows.Forms.Button
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents btn_cancelar As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents list_ubicacionorigen As System.Windows.Forms.ComboBox
    Friend WithEvents lab_descripcion_articulo As System.Windows.Forms.Label
End Class
