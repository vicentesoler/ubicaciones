﻿Public Class frm_articulo
    Dim conexion As New Conector
    Private Sub frm_articulo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        funciones.llenar_almacenes(list_almacen_trasladar, True, True)
        funciones.llenar_tipos_ubi(list_tipo)
        list_tipo.SelectedIndex = 0
        If frm_principal.PermArticuloEliminarFila = True Then
            btn_eliminar_fila.Visible = True
        Else
            btn_eliminar_fila.Visible = False
        End If

        lab_pend_rec.Text = ""

    End Sub

    Private Sub btn_limpiar_Click(sender As System.Object, e As System.EventArgs)
        txt_articulo.Text = ""
    End Sub

    Public Sub btn_mostrar_ubicaciones_articulo_Click_1(sender As System.Object, e As System.EventArgs) Handles btn_mostrar_ubicaciones_articulo.Click
        mostrar_ubicaciones_articulo()
        If txt_articulo.Text.ToString.Length = 7 Then
            txt_descripcion_articulo.Text = funciones.buscar_descripcion(txt_articulo.Text)
            lab_pendmontdesg.Text = funciones.obtener_pendmontdesg(txt_articulo.Text)
            lab_pendreaprov.Text = funciones.obtener_pendreaprov(txt_articulo.Text, "", "", "")
            lab_of.Text = funciones.obtener_of(txt_articulo.Text)
            If cb_pend_rec.Checked Then lab_pend_rec.Text = funciones.pendiente_recibir(txt_articulo.Text.Trim, "TODAS")
            labPdtePreparar.Text = funciones.pendiente_preparar("TODAS", "TODOS", txt_articulo.Text.Trim)
            labPendServ.Text = funciones.pendiente_servir(txt_articulo.Text.Trim, "TODAS", True)
            obtener_upalet()
        End If
        txt_altura.Text = ""
        txt_portal.Text = ""
        txt_pasillo.Text = ""
        txt_cant_max.Text = ""
        txt_cantidad_trasladar.Text = ""
        txt_tamaño.Text = ""
    End Sub
    Private Sub txt_articulo_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_articulo.TextChanged

        'If txt_articulo.Text.Length = 7 Then
        '    txt_descripcion_articulo.Text = funciones.buscar_descripcion(txt_articulo.Text)
        '    btn_mostrar_ubicaciones_articulo.Focus()
        'End If
        'Lo comenté porque sino con el lector no funciona, al escribir los codigos, al codigo numero 7, salta y no deja escribir los 13
    End Sub
    Private Sub frm_articulo_Shown(sender As System.Object, e As System.EventArgs) Handles MyBase.Shown
        txt_articulo.Focus()
    End Sub
    Public Sub obtener_upalet()
        Dim sql As String
        Dim vdatos As New DataTable
        sql = "select MaxApilable from gesarticulos where codarticulo='" & txt_articulo.Text.Trim & "' and codempresa=1"
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        If vdatos.Rows.Count = 1 Then txt_unipalet.Text = CInt(vdatos.Rows(0)(0))
    End Sub
    Private Sub txt_articulo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_articulo.KeyUp
        Dim codarticulo As String = txt_articulo.Text.ToUpper
        If e.KeyCode = Keys.Enter Then
            lab_pendreaprov.Text = 0
            If txt_articulo.Text.Length = 7 Or txt_articulo.Text.Length = 13 Then
                txt_descripcion_articulo.Text = funciones.buscar_descripcion(codarticulo)
                txt_articulo.Text = codarticulo
                btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
            End If
        End If
    End Sub

    Private Sub dg_ubicaciones_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_ubicaciones.CellEndEdit
        Dim sql, resultado As String
        Dim vdatosant As New DataTable
        Dim vdatos As New DataTable


        If dg_ubicaciones.Rows(e.RowIndex).Cells("Cant.").Value.ToString = "" Then dg_ubicaciones.Rows(e.RowIndex).Cells("Cant.").Value = 0
        If dg_ubicaciones.Rows(e.RowIndex).Cells("Tam.").Value.ToString = "" Then dg_ubicaciones.Rows(e.RowIndex).Cells("Tam.").Value = 0
        If dg_ubicaciones.Rows(e.RowIndex).Cells("Max.").Value.ToString = "" Then dg_ubicaciones.Rows(e.RowIndex).Cells("Max.").Value = 0

        Try
            If IsNumeric(dg_ubicaciones.Rows(e.RowIndex).Cells("Cant.").Value.ToString) And IsNumeric(dg_ubicaciones.Rows(e.RowIndex).Cells("Tam.").Value.ToString) And IsNumeric(dg_ubicaciones.Rows(e.RowIndex).Cells("Max.").Value.ToString) Then

                If e.ColumnIndex = 6 Then
                    'Si la referencia esta dentro de uno de estos albaranes no dejar.
                    'Hasta arreglar preparacion
                    'sql = "select * from gesalbaraneslin where codempresa=3 and codperiodo=2018 and seralbaran='NA' and codalbaran in (5380,5724) and articulo ='" & txt_articulo.Text & "' "
                    'vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                    'If vdatos.Rows.Count > 1 Then
                    '    MsgBox("Esta referencia está dentro de uno de estos albaranes 5380,5724. Hasta que no se arreglen no se puede recontar.")
                    '    btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                    '    Exit Sub
                    'End If


                    'Pedido por Sergio. Si la referencia está dentro de un desgl/mont. no dejar
                    'sql = "select cab.codorden from orden_desg_mont cab "
                    'sql &= "inner join orden_desg_mont_lin lin on lin.codorden =cab.codorden "
                    'sql &= "inner join orden_desg_mont_refs lin2 on lin2.codorden =cab.codorden "
                    'sql &= "where (cab.depositado =0 or cab.recogido=0) "
                    'sql &= "and (lin.codarticulo ='" & txt_articulo.Text & "' or lin2.codarticulo ='" & txt_articulo.Text & "' )"
                    sql = "select cantidad from v_desgmontpend where codarticulo='" & txt_articulo.Text & "' "
                    'Consulta a vista y dejar continuar si es un operario autorizado. Ahora lo tenemos en cuenta para sumar o restar cantidad
                    vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                    If vdatos.Rows.Count > 0 Then
                        If frm_principal.PermArticuloModStockPlus = True Then
                            If MsgBox("Existen desgl./mont. por hacer. Continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                                btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                                Exit Sub
                            End If
                        Else
                            MsgBox("Existen desgl./mont. por terminar. No se puede continuar.")
                            btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                            Exit Sub
                        End If
                    End If



                    'Pedido por Sergio. Si la referencia está dentro de un ord. fab. no dejar
                    'sql = "select articulo from GesPedidosProvLin where SerPedidoProv ='OF' and cantidadpdte<>0 and Articulo ='" & txt_articulo.Text & "' "
                    'vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                    'Se obtiene al mostrar articulo
                    'Lo quitamos porque si no no pueden hacer inventario de muchas referencias. Ahora lo tenemos en cuenta para sumar o restar cantidad
                    If lab_of.Text <> 0 Then
                        If frm_principal.PermArticuloModStockPlus = True Then
                            If MsgBox("Existen ord.fabricación. Continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                                btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                                Exit Sub
                            End If
                        Else
                            MsgBox("Existen ord.fabricación por terminar. No se puede continuar.")
                            btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                            Exit Sub
                        End If
                    End If


                    'Pedido por Sergio. Si la referencia está dentro de un reaprov, no dejar
                    'sql = "select codarticulo  from reaprov_ubi_lin where codreaprov in (select codreaprov  from reaprov_ubi where procesado =0) and codarticulo ='" & txt_articulo.Text & "' "
                    'Consulto la vista
                    sql = "select * from v_reaprovpend where CodArticulo='" & txt_articulo.Text & "' "
                    vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                    If vdatos.Rows.Count > 0 Then
                        If frm_principal.PermArticuloModStockPlus = True Then
                            If MsgBox("Existen Reaprovisionamientos pendientes. Continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                                btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                                Exit Sub
                            End If
                        Else
                            MsgBox("Existen Reaprovisionamientos pendientes. No se puede continuar.")
                            btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                            Exit Sub
                        End If
                    End If



                    'Serio pidió el 14/01/2019 que solo pudieran actualizar el stok estos usuarios
                    If frm_principal.PermArticuloModStock = True Then
                        If MsgBox("Actualizar también el stock del almacén?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                            'Mirar si tiene mas de una ubicación de Pick en el almacén donde está actualizando y avisar. Pedido por Sergio
                            Dim cont As Integer
                            Dim difubi As Integer = 0
                            cont = 0
                            Do While cont < dg_ubicaciones.Rows.Count
                                If dg_ubicaciones.Rows(cont).Cells("Alm.").Value = dg_ubicaciones.Rows(e.RowIndex).Cells("Alm.").Value And dg_ubicaciones.Rows(cont).Cells("Tipo").Value = "Picking" Then
                                    difubi += 1
                                End If
                                cont += 1
                            Loop
                            If difubi > 1 Then MsgBox("Esta referencia tiene más de una ubicación de picking en este almacén. Se tiene que hacer inventario de todas las ubicaciones de picking inmediatamente.")

                            frm_act_stock.Lab_codarticulo.Text = dg_ubicaciones.Rows(e.RowIndex).Cells("Ref.").Value.ToString
                            frm_act_stock.lab_alm_pick.Text = dg_ubicaciones.Rows(e.RowIndex).Cells("Alm.").Value.ToString
                            frm_act_stock.ShowDialog()
                        End If
                    End If
                    funciones.añadir_reg_mov_ubicaciones(dg_ubicaciones.Rows(e.RowIndex).Cells("Ref.").Value.ToString, dg_ubicaciones.Rows(e.RowIndex).Cells("Cant.").Value.ToString, dg_ubicaciones.Rows(e.RowIndex).Cells("Alm.").Value.ToString, dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString, "Cambio valor", "Ubicaciones-Articulo", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
                End If

                sql = "update ubicaciones set cantidad=" & dg_ubicaciones.Rows(e.RowIndex).Cells("Cant.").Value.ToString & ",cantmax=" & dg_ubicaciones.Rows(e.RowIndex).Cells("Max.").Value.ToString & ",tamaño=" & dg_ubicaciones.Rows(e.RowIndex).Cells("Tam.").Value.ToString & " "
                sql &= "where id=" & dg_ubicaciones.Rows(e.RowIndex).Cells("Id").Value
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)

                'Actualizo el grid porque si han actualizado tambien el stock dealmacen, los datos en las columnas de alm. no estan actualizados
                btn_mostrar_ubicaciones_articulo_Click_1(sender, e)

                If resultado <> "0" Then
                    MsgBox("Se producjo un error al actualizar los datos", MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("Los datos de las celdas modificadas deben ser numericos", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox("Se producjo un error al actualizar los datos", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub dg_ubicaciones_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_ubicaciones.CellDoubleClick
        Dim almacen As String
        Dim pasillo, portal, altura As String

        If e.ColumnIndex = 3 And e.RowIndex > -1 Then
            pasillo = dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString.Substring(0, 2)
            portal = dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString.Substring(3, 2)
            altura = dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value.ToString.Substring(6, 2)
            almacen = dg_ubicaciones.Rows(e.RowIndex).Cells("Alm.").Value.ToString
            frm_principal.btn_mostrar_ubicacion_Click(sender, e)
            frm_ubicaciones.txt_pasillo.Text = pasillo
            frm_ubicaciones.txt_portal.Text = portal
            frm_ubicaciones.txt_altura.Text = altura
            frm_ubicaciones.list_almacen.SelectedValue = almacen
            frm_ubicaciones.btn_mostrar_Click(sender, e)
        End If
    End Sub

    Private Sub btn_trasladar_Click(sender As System.Object, e As System.EventArgs) Handles btn_trasladar.Click
        Dim cont As Integer
        Dim sql As String
        Dim sql2 As String = ""
        Dim resultado As String
        Dim vdatos, vdatosant As New DataTable
        Dim proxmov As Integer = 0
        Dim numrefmov As String = ""

        Try
            If dg_ubicaciones.SelectedRows(cont).Cells("Ubicacion").Value = txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text And dg_ubicaciones.SelectedRows(cont).Cells("Alm.").Value = list_almacen_trasladar.SelectedValue Then
                MsgBox("La ubicación destino no puede ser la misma que la origen")
                Exit Sub
            End If

            If Len(txt_pasillo.Text) <> 2 Or Len(txt_portal.Text) <> 2 Or Len(txt_altura.Text) <> 2 Then
                MsgBox("La Ubicación destino no es correcta.")
                Exit Sub
            End If


            If dg_ubicaciones.SelectedRows(cont).Cells("Alm.").Value <> list_almacen_trasladar.SelectedValue Then
                If MsgBox("El almacén destino es diferente al almacén origen, el stock cambiará de almacén, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If

            If Not IsNumeric(txt_cantidad_trasladar.Text) Then
                MsgBox("La cantidad a traspasar debe ser un número.")
                Exit Sub
            End If
            If Not (IsNumeric(txt_pasillo.Text) And IsNumeric(txt_portal.Text) And IsNumeric(txt_altura.Text)) Then
                MsgBox("Rellenar la ubicación destino", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            If CInt(txt_cantidad_trasladar.Text) > CInt(dg_ubicaciones.SelectedRows(cont).Cells("Cant.").Value) Then
                MsgBox("La cantidad a traspasar debe ser menor o igual que la cantidad origen.")
                Exit Sub
            End If

            If Not IsNumeric(txt_cant_max.Text) Then
                MsgBox("Rellenar la cantidad máxima.")
                Exit Sub
            End If

            'Mirar si ya tiene una ubicacion de picking y alertar
            If list_tipo.SelectedValue = "Picking" Then
                sql = "select count(ubicacion) from ubicaciones where codarticulo='" & dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value & "' and tipo='Picking' and codalmacen='" & list_almacen_trasladar.SelectedValue & "' "
                sql &= "and ubicacion <> '" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vdatos.Rows(0)(0) > 0 Then
                    If MsgBox("Esta referencia ya tiene una ubicación de picking, añadir una nueva?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                End If
            End If

            'Mirar si la cantidad máxima o tamño que se ha puesto es diferente de la que ya tiene la ubicació.
            'En teoría no debería dejar ya que al detectar que ya existe la ubicación destino inhabilita las casillas, pero por si a caso
            sql = "select top 1 isnull(cantmax,0) as cantmax,isnull(tamaño,0) as tamaño from ubicaciones where codarticulo='" & dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value & "' and tipo='" & list_tipo.Text & "' and codalmacen='" & list_almacen_trasladar.SelectedValue & "' "
            sql &= "and ubicacion = '" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' "
            sql &= "order by id asc "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 1 Then
                If txt_cant_max.Text <> vdatos.Rows(0).Item("cantmax") Then
                    If MsgBox("La cantidad máxima introducida es diferente de la cantidad máxima que tiene la ubicación actualmente, continuar y actualizar la cantidad máxima?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                End If
                If txt_tamaño.Text <> vdatos.Rows(0).Item("tamaño") Then
                    If MsgBox("La altura introducida es diferente de la altura que tiene la ubicación actualmente, continuar y actualizar la altura?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                End If
            End If


            cont = 0
            Do While cont < dg_ubicaciones.SelectedRows.Count
                If IsNumeric(dg_ubicaciones.SelectedRows(cont).Cells("Id").Value) Then
                    'Mirar si existe ya esta referencia y tipo en esta ubicacio destino.
                    '   Si existe se suma la cantidad al destino y se actualiza la cantmax marcada. Solo si la ubicacion destino es picking. Pedido por sergio el 24/10/2016. Si la ubicacion es esp.CI tambien debe sumar. Pedido por sergio el 20/04/2018
                    '   Si no existe, se crea la nueva ubicacion destino
                    '   Si la cant a trasp es igual a la cant q hay en origen se elimina la ubicacion origen , solo si el origen es un pulmón. Pedido por Sergio
                    '   Si la cant a trasp es menor q la cant q hay en origen se resta la cantidad en origen
                    sql2 = "select id,codalmacen,ubicacion,codarticulo from ubicaciones where codalmacen='" & list_almacen_trasladar.SelectedValue & "' and ubicacion='" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' and tipo='" & list_tipo.SelectedValue & "' and codarticulo='" & dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value & "' "
                    vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql2)

                    sql = ""
                    If vdatos.Rows.Count >= 1 And (list_tipo.SelectedValue = "Picking" Or list_tipo.SelectedValue = "Esp.CI") Then
                        sql &= "update ubicaciones set tamaño=" & CInt(txt_tamaño.Text) & ", cantmax=" & txt_cant_max.Text & " ,cantidad=cantidad + " & CInt(txt_cantidad_trasladar.Text) & " where id=" & vdatos.Rows(0).Item("id") & " "
                    Else
                        sql &= "insert into ubicaciones (codalmacen,ubicacion,codarticulo,tipo,tamaño,cantidad,cantmax,cantmin,observaciones,ultusuario) values('" & list_almacen_trasladar.SelectedValue & "','" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "','" & dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value & "','" & list_tipo.SelectedValue & "'," & txt_tamaño.Text & "," & txt_cantidad_trasladar.Text & "," & txt_cant_max.Text & ",0,'Ubicaciones-articulo','" & frm_principal.lab_nomoperario.Text & "') "
                    End If
                    If txt_cantidad_trasladar.Text = dg_ubicaciones.SelectedRows(cont).Cells("Cant.").Value Then
                        If dg_ubicaciones.SelectedRows(cont).Cells("Tipo").Value = "Pulmon" Then
                            sql &= "delete ubicaciones where id=" & dg_ubicaciones.SelectedRows(cont).Cells("Id").Value & " "
                        Else
                            If MsgBox("La ubicación origen es de tipo " & dg_ubicaciones.SelectedRows(cont).Cells("Tipo").Value & " y se ha quedado vacía, eliminarla?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                                sql &= "delete ubicaciones where id=" & dg_ubicaciones.SelectedRows(cont).Cells("Id").Value & " "
                            Else
                                sql &= "update ubicaciones set cantidad=cantidad - " & CInt(txt_cantidad_trasladar.Text) & " where id=" & dg_ubicaciones.SelectedRows(cont).Cells("Id").Value & " "
                            End If
                        End If
                    End If

                    If txt_cantidad_trasladar.Text < dg_ubicaciones.SelectedRows(cont).Cells("Cant.").Value Then sql &= "update ubicaciones set cantidad=cantidad - " & CInt(txt_cantidad_trasladar.Text) & " where id=" & dg_ubicaciones.SelectedRows(cont).Cells("Id").Value & " "

                    'Origen
                    funciones.añadir_reg_mov_ubicaciones(dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value, CInt(txt_cantidad_trasladar.Text) * -1, dg_ubicaciones.SelectedRows(cont).Cells("Alm.").Value, dg_ubicaciones.SelectedRows(cont).Cells("Ubicacion").Value, "Traslado", "Ubicaciones-Articulo", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

                    'Destino
                    funciones.añadir_reg_mov_ubicaciones(dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value, txt_cantidad_trasladar.Text, list_almacen_trasladar.SelectedValue, txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text, "Traslado", "Ubicaciones-Articulo", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

                    'Si el almacen origen es diferente del destino, pasar stock de almacen con un mov.
                    If dg_ubicaciones.SelectedRows(cont).Cells("Alm.").Value <> list_almacen_trasladar.SelectedValue Then
                        resultado = funciones.crear_movimiento_inase(frm_principal.codempresa, proxmov, "Movimiento a causa de traslado. Ubicaciones-Articulo", numrefmov)
                        If resultado = "OK" Then
                            sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacenori,ubicacionori,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                            sql &= "values('" & numrefmov & "'," & frm_principal.usuario_inase & "," & frm_principal.codempresa & "," & Now.Year & "," & proxmov & "," & 1 & ",'" & dg_ubicaciones.SelectedRows(cont).Cells("Alm.").Value & "','XXX','" & list_almacen_trasladar.SelectedValue & "','XXX','" & dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value & "'," & CInt(txt_cantidad_trasladar.Text) & ",'Traslado. Ubicaciones-Articulo') "
                        Else
                            MsgBox("Se produjo un error al crear el movimiento para cambiar el stock de almacén.")
                            Exit Sub
                        End If
                    End If
                Else
                    MsgBox("Existe un problema para determinar la ubicación origen.")
                    Exit Sub
                End If
                cont += 1
            Loop
            If sql <> "" Then resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If resultado = "0" Then
                btn_mostrar_ubicaciones_articulo_Click_1(sender, e)
                llenar_grid_ubicaciones_traslado()
                txt_pasillo.Text = ""
                txt_portal.Text = ""
                txt_altura.Text = ""
            Else
                MsgBox("Se producjo un error al actualizar la ubicación", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox("Se producjo un error al actualizar la ubicación", MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub txt_pasillo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_pasillo.KeyUp
        Dim aux As String
        If Microsoft.VisualBasic.Left(txt_pasillo.Text, 1) = "U" Then
            If Len(txt_pasillo.Text) = 7 Then
                aux = txt_pasillo.Text
                txt_pasillo.Text = Microsoft.VisualBasic.Mid(aux, 2, 2)
                txt_portal.Text = Microsoft.VisualBasic.Mid(aux, 4, 2)
                txt_altura.Text = Microsoft.VisualBasic.Mid(aux, 6, 2)
                llenar_grid_ubicaciones_traslado()
                list_tipo.Focus()
            End If
        Else
            If txt_pasillo.Text.Length = 2 Then
                txt_portal.Focus()
                txt_portal.SelectAll()
            End If
        End If
    End Sub

    Private Sub txt_portal_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_portal.KeyUp
        Dim aux As String
        If Microsoft.VisualBasic.Left(txt_portal.Text, 1) = "U" Then
            If Len(txt_portal.Text) = 7 Then
                aux = txt_portal.Text
                txt_pasillo.Text = Microsoft.VisualBasic.Mid(aux, 2, 2)
                txt_portal.Text = Microsoft.VisualBasic.Mid(aux, 4, 2)
                txt_altura.Text = Microsoft.VisualBasic.Mid(aux, 6, 2)
                llenar_grid_ubicaciones_traslado()
                list_tipo.Focus()
            End If
        Else
            If txt_portal.Text.Length = 2 Then
                txt_altura.Focus()
                txt_altura.SelectAll()
            End If
        End If

    End Sub

    Private Sub txt_altura_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_altura.KeyUp
        Dim aux As String

        If Microsoft.VisualBasic.Left(txt_altura.Text, 1) = "U" Then
            If Len(txt_altura.Text) = 7 Then
                aux = txt_altura.Text
                txt_pasillo.Text = Microsoft.VisualBasic.Mid(aux, 2, 2)
                txt_portal.Text = Microsoft.VisualBasic.Mid(aux, 4, 2)
                txt_altura.Text = Microsoft.VisualBasic.Mid(aux, 6, 2)
                llenar_grid_ubicaciones_traslado()
                list_tipo.Focus()
            End If
        Else
            If txt_altura.Text.Length = 2 Then
                llenar_grid_ubicaciones_traslado()
                list_tipo.Focus()
            End If
        End If




    End Sub

    Public Sub llenar_grid_ubicaciones_traslado()
        Dim sql As String
        Dim vdatos As New DataTable
        'Consulta ubicaciones
        sql = "select ubi.codarticulo as 'Articulo',ubi.tipo as 'Tipo', ubi.Cantidad as 'Cant.' "
        sql &= "from ubicaciones ubi "
        sql &= " where codalmacen='" & list_almacen_trasladar.SelectedValue & "' and ubi.ubicacion='" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "'"
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_ubicaciones_traslado.DataSource = vdatos
            dg_ubicaciones_traslado.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 18, FontStyle.Bold)
            dg_ubicaciones_traslado.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 17)
            dg_ubicaciones_traslado.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_ubicaciones_traslado.Columns(0).Width = 130
            dg_ubicaciones_traslado.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            dg_ubicaciones_traslado.Columns(0).ReadOnly = True
            dg_ubicaciones_traslado.Columns(1).Width = 100
            dg_ubicaciones_traslado.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubicaciones_traslado.Columns(1).ReadOnly = True
            dg_ubicaciones_traslado.Columns(2).Width = 80
            dg_ubicaciones_traslado.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            dg_ubicaciones_traslado.Columns(2).ReadOnly = False
            dg_ubicaciones_traslado.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        Else
            vdatos.Rows.Clear()
            dg_ubicaciones_traslado.DataSource = vdatos
        End If
    End Sub

    Private Sub btn_limpiar_Click_1(sender As System.Object, e As System.EventArgs) Handles btn_limpiar.Click
        txt_articulo.Text = ""
        txt_cant_max.Text = ""
        txt_altura.Text = ""
        txt_portal.Text = ""
        txt_pasillo.Text = ""
        dg_ubicaciones.DataSource = Nothing
        txt_articulo.Focus()
    End Sub
    Private Sub btn_eliminar_fila_Click(sender As System.Object, e As System.EventArgs) Handles btn_eliminar_fila.Click
        If dg_ubicaciones.SelectedRows.Count > 0 Then
            borrar_ubicacion_articulo()
        Else
            MsgBox("Seleccionar una fila a eliminar", MsgBoxStyle.Exclamation)
        End If

    End Sub
    Public Sub borrar_ubicacion_articulo()
        Dim cont As Integer
        Dim sql As String = ""
        Dim resultado As String
        Try
            If MsgBox("Seguro que quiere eliminar esta linea?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                cont = 0
                Do While cont < dg_ubicaciones.SelectedRows.Count
                    sql = ""
                    If IsNumeric(dg_ubicaciones.SelectedRows(cont).Cells("Id").Value) Then
                        sql &= "insert into ubicaciones_borradas(codalmacen,ubicacion,codarticulo,tipo,usuario,fecha,programa) "
                        sql &= "select codalmacen,ubicacion,codarticulo,tipo,'" & frm_principal.lab_nomoperario.Text & "',getdate(),'Ubicaciones' from ubicaciones where id=" & dg_ubicaciones.SelectedRows(cont).Cells("Id").Value & " "
                        sql &= "delete ubicaciones where id=" & dg_ubicaciones.SelectedRows(cont).Cells("Id").Value & " "
                        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                        If resultado <> "0" Then
                            MsgBox("Se producjo un error al eliminar la/s filas", MsgBoxStyle.Critical)
                        Else
                            funciones.añadir_reg_mov_ubicaciones(dg_ubicaciones.SelectedRows(cont).Cells("Ref.").Value, dg_ubicaciones.SelectedRows(cont).Cells("Cant.").Value * -1, dg_ubicaciones.SelectedRows(cont).Cells("Alm.").Value, dg_ubicaciones.SelectedRows(cont).Cells("Ubicacion").Value, "Eliminar Fila", "Ubicaciones-Articulo", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text, dg_ubicaciones.SelectedRows(cont).Cells("Cant.").Value)
                        End If
                    End If

                    cont += 1
                Loop
            End If
        Catch ex As Exception
            MsgBox("Se producjo un error al actualizar la ubicación", MsgBoxStyle.Critical)
        Finally
            mostrar_ubicaciones_articulo()
        End Try
    End Sub
    Public Sub mostrar_ubicaciones_articulo()
        Dim sql As String
        Dim vdatos, vmovprev As New DataTable
        Dim cont As Integer
        Dim movprevA01, movprevA07, movprevA08 As Integer

        txt_articulo.Text = txt_articulo.Text.ToUpper.Trim
        If txt_articulo.Text.Trim = "" Then
            MsgBox("Rellenar el campo artículo.", MsgBoxStyle.Exclamation)
            txt_articulo.Focus()
            Exit Sub
        End If
        vdatos.Rows.Clear()

        'Guardarme la cantidad que esta en un mov de prev. Esta cantidad hay que descontarla del stock
        sql = "select isnull(sum(case when lin.almacendest='A01' then cantidad end),0) as A01,isnull(sum(case when lin.almacendest='A07' then cantidad end),0) as A07,isnull(sum(case when lin.almacendest='A08' then cantidad end),0) as A08 "
        sql &= "from GesMovAlm cab "
        sql &= "inner join GesMovAlmLin lin on lin.RefCabecera =cab.NumReferencia and lin.articulo='" & txt_articulo.Text.Trim & "' "
        sql &= "where cab.codperiodo >= 2018 And cab.Observaciones = 'MOV AUTOMATICO LISTADO MOVIMIENTOS PREVISION MANIPULACION'"
        vmovprev = conexion.TablaxCmd(constantes.bd_inase, sql)
        movprevA01 = vmovprev.Rows(0).Item("A01")
        movprevA07 = vmovprev.Rows(0).Item("A07")
        movprevA08 = vmovprev.Rows(0).Item("A08")
        lab_prev.Text = movprevA01 + movprevA07 + movprevA08


        'Consulta ubicaciones
        sql = "select  ubi.id as 'Id',ubi.codalmacen as 'Alm.',ubi.codarticulo as 'Ref.',ubi.ubicacion as 'Ubicacion',ubi.tipo as 'Tipo',isnull(ubi.tamaño,0) as 'Tam.' , isnull(ubi.Cantidad,0) as 'Cant.', isnull(ubi.cantmax,0) as 'Max.',"
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A01' then sto.stockactual end ),0)) - (" & movprevA01 & ") as A01, "
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A06' then sto.stockactual end ),0)) as A06, "
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A07' then sto.stockactual end ),0)) - (" & movprevA07 & ") as A07, "
        sql &= "convert(integer,isnull(sum(case when sto.codalmacen='A08' then sto.stockactual end ),0)) - (" & movprevA08 & ") as A08 "
        sql &= "from ubicaciones ubi "
        sql &= "left join inase.dbo.gesstocks sto on sto.codarticulo COLLATE Modern_Spanish_CI_AS=ubi.codarticulo and sto.codalmacen in ('A01','A06','A07','A08')  "
        sql &= "where ubi.codarticulo like '" & txt_articulo.Text.Trim & "%' "
        sql &= "group by ubi.id,ubi.codalmacen,ubi.codarticulo,ubi.ubicacion,ubi.tipo,ubi.tamaño, ubi.Cantidad,ubi.cantmax "
        sql &= "order by ubi.tipo,ubi.ubicacion "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        dg_ubicaciones.DataSource = vdatos
        dg_ubicaciones.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold)
        dg_ubicaciones.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 16)
        dg_ubicaciones.Columns(0).Width = 2
        dg_ubicaciones.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(0).ReadOnly = True
        dg_ubicaciones.Columns(1).Width = 60
        dg_ubicaciones.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(1).ReadOnly = True
        dg_ubicaciones.Columns(2).Width = 105
        dg_ubicaciones.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicaciones.Columns(2).ReadOnly = True
        dg_ubicaciones.Columns(3).Width = 110
        dg_ubicaciones.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicaciones.Columns(3).ReadOnly = True
        dg_ubicaciones.Columns(4).Width = 60
        dg_ubicaciones.Columns(4).DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14, FontStyle.Bold)
        dg_ubicaciones.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_ubicaciones.Columns(4).ReadOnly = True
        dg_ubicaciones.Columns(5).Width = 60
        dg_ubicaciones.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(5).ReadOnly = False
        dg_ubicaciones.Columns(6).Width = 65
        dg_ubicaciones.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(6).ReadOnly = False
        dg_ubicaciones.Columns(7).Width = 65
        dg_ubicaciones.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(7).ReadOnly = False
        dg_ubicaciones.Columns(8).Width = 65
        dg_ubicaciones.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(8).ReadOnly = True
        dg_ubicaciones.Columns(9).Width = 65
        dg_ubicaciones.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(9).ReadOnly = True
        dg_ubicaciones.Columns(10).Width = 65
        dg_ubicaciones.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(10).ReadOnly = True
        dg_ubicaciones.Columns(10).Width = 65
        dg_ubicaciones.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        dg_ubicaciones.Columns(10).ReadOnly = True
        'dg_ubicaciones.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells

        'Pintar las filas de color dependiendo del almacen. pedido por Sergio
        cont = 0
        Do While cont < dg_ubicaciones.Rows.Count
            If dg_ubicaciones.Rows(cont).Cells(1).Value = "A01" Then
                dg_ubicaciones.Rows(cont).DefaultCellStyle.BackColor = Color.Blue
                dg_ubicaciones.Rows(cont).DefaultCellStyle.ForeColor = Color.White
            End If
            If dg_ubicaciones.Rows(cont).Cells(1).Value = "A07" Then
                dg_ubicaciones.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                dg_ubicaciones.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If
            If dg_ubicaciones.Rows(cont).Cells(1).Value = "A08" Then
                dg_ubicaciones.Rows(cont).DefaultCellStyle.BackColor = Color.LightPink
                dg_ubicaciones.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If
            cont += 1
        Loop


    End Sub


    Private Sub cb_pend_rec_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cb_pend_rec.CheckedChanged

        If cb_pend_rec.Checked Then
            lab_pend_rec.Text = funciones.pendiente_recibir(txt_articulo.Text, "TODAS")
        Else
            lab_pend_rec.Text = ""
        End If
    End Sub

    Private Sub txt_cant_max_Enter(sender As System.Object, e As System.EventArgs) Handles txt_cant_max.Enter
        dg_ubicaciones.Rows(dg_ubicaciones.CurrentCell.RowIndex).Selected = True
    End Sub

    Public Sub llenar_ubi(ByVal tipo As String, ByVal cant As Integer)
        Dim sql As String
        Dim vdatos As New DataTable
        sql = "select cantmax,ubicacion from ubicaciones where tipo='" & tipo & "' and codarticulo='" & txt_articulo.Text.Trim & "' and codalmacen='" & list_almacen_trasladar.SelectedValue & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        If vdatos.Rows.Count > 0 Then
            txt_cant_max.Text = vdatos.Rows(0).Item("cantmax").ToString
            txt_pasillo.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(0, 2)
            txt_portal.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(3, 2)
            txt_altura.Text = "" 'Esto es para forzar que se lance el evento
            txt_altura.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(6, 2)
            txt_pasillo.Focus()
            txt_pasillo.SelectAll()
            llenar_grid_ubicaciones_traslado()
        Else
            txt_pasillo.Text = ""
            txt_portal.Text = ""
            txt_altura.Text = ""
            txt_cant_max.Text = CInt(cant)
        End If
    End Sub

    Private Sub txt_pasillo_Enter(sender As System.Object, e As System.EventArgs) Handles txt_pasillo.Enter
        If dg_ubicaciones.Rows.Count > 0 Then dg_ubicaciones.Rows(dg_ubicaciones.CurrentCell.RowIndex).Selected = True
    End Sub

    Private Sub txt_portal_Enter(sender As System.Object, e As System.EventArgs) Handles txt_portal.Enter
        If dg_ubicaciones.Rows.Count > 0 Then dg_ubicaciones.Rows(dg_ubicaciones.CurrentCell.RowIndex).Selected = True
    End Sub

    Private Sub txt_altura_Enter(sender As System.Object, e As System.EventArgs) Handles txt_altura.Enter
        If dg_ubicaciones.Rows.Count > 0 Then dg_ubicaciones.Rows(dg_ubicaciones.CurrentCell.RowIndex).Selected = True
    End Sub

    Private Sub list_almacen_trasladar_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_almacen_trasladar.DropDownClosed
        Dim sql As String
        Dim vdatos As New DataTable

        If dg_ubicaciones.Rows.Count = 0 Then Exit Sub
        If dg_ubicaciones.SelectedRows.Count = 0 Then Exit Sub

        'Si el almacén elegido es de saldos, poner la ubicacion de saldos
        If list_almacen_trasladar.SelectedValue = "S01" Then
            txt_pasillo.Text = constantes.saldos_A01.ToString.Substring(0, 2)
            txt_portal.Text = constantes.saldos_A01.ToString.Substring(3, 2)
            txt_altura.Text = constantes.saldos_A01.ToString.Substring(6, 2)
        ElseIf list_almacen_trasladar.SelectedValue = "S07" Then
            txt_pasillo.Text = constantes.saldos_A07.ToString.Substring(0, 2)
            txt_portal.Text = constantes.saldos_A07.ToString.Substring(3, 2)
            txt_altura.Text = constantes.saldos_A07.ToString.Substring(6, 2)
        ElseIf list_almacen_trasladar.SelectedValue = "S08" Then
            txt_pasillo.Text = constantes.saldos_A08.ToString.Substring(0, 2)
            txt_portal.Text = constantes.saldos_A08.ToString.Substring(3, 2)
            txt_altura.Text = constantes.saldos_A08.ToString.Substring(6, 2)
        Else
            If dg_ubicaciones.SelectedRows(0).Cells("Alm.").Value <> list_almacen_trasladar.SelectedValue Then
                If list_almacen_trasladar.SelectedValue = "A01" Then
                    txt_pasillo.Text = constantes.traslados_A01.ToString.Substring(0, 2)
                    txt_portal.Text = constantes.traslados_A01.ToString.Substring(3, 2)
                    txt_altura.Text = constantes.traslados_A01.ToString.Substring(6, 2)
                ElseIf list_almacen_trasladar.SelectedValue = "A07" Then
                    txt_pasillo.Text = constantes.traslados_A07.ToString.Substring(0, 2)
                    txt_portal.Text = constantes.traslados_A07.ToString.Substring(3, 2)
                    txt_altura.Text = constantes.traslados_A07.ToString.Substring(6, 2)
                ElseIf list_almacen_trasladar.SelectedValue = "A08" Then
                    txt_pasillo.Text = constantes.traslados_A08.ToString.Substring(0, 2)
                    txt_portal.Text = constantes.traslados_A08.ToString.Substring(3, 2)
                    txt_altura.Text = constantes.traslados_A08.ToString.Substring(6, 2)
                Else
                    txt_pasillo.Text = ""
                    txt_portal.Text = ""
                    txt_altura.Text = ""
                End If
                list_tipo.SelectedValue = "Picking"
            Else
                'Mostrar la ubicacion del picking destino
                sql = "select cantmax,ubicacion from ubicaciones where tipo='Picking' and codarticulo='" & txt_articulo.Text.Trim & "' and codalmacen='" & list_almacen_trasladar.SelectedValue & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vdatos.Rows.Count > 0 Then
                    txt_cant_max.Text = vdatos.Rows(0).Item("cantmax").ToString
                    txt_pasillo.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(0, 2)
                    txt_portal.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(3, 2)
                    txt_altura.Text = vdatos.Rows(0).Item("ubicacion").ToString.Substring(6, 2)
                Else
                    txt_cant_max.Text = 0
                    txt_pasillo.Text = ""
                    txt_portal.Text = ""
                    txt_altura.Text = ""
                End If

            End If

        End If
        txt_pasillo.Focus()
        txt_pasillo.SelectAll()
        llenar_grid_ubicaciones_traslado()
    End Sub


    Private Sub txt_altura_TextChanged(sender As System.Object, e As System.EventArgs) Handles txt_altura.TextChanged
        Dim sql As String
        Dim vdatos As New DataTable

        If txt_altura.Text.Length = 2 And dg_ubicaciones.SelectedRows.Count = 1 And list_almacen_trasladar.Text <> "" Then
            'Mirar si la ubicación destino existe, si existe hay q poner los datos de la ubicacion que ya existe. Si no existe
            sql = "select top 1 isnull(cantmax,0) as cantmax,isnull(tamaño,0) as tamaño from ubicaciones "
            sql &= " where codalmacen='" & list_almacen_trasladar.SelectedValue & "' and ubicacion='" & txt_pasillo.Text & "-" & txt_portal.Text & "-" & txt_altura.Text & "' "
            sql &= "and codarticulo='" & dg_ubicaciones.SelectedRows(0).Cells("Ref.").Value & "' and tipo='" & list_tipo.Text & "' "
            sql &= "order by id asc "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 1 Then
                txt_cant_max.Text = vdatos.Rows(0).Item("cantmax")
                txt_tamaño.Text = vdatos.Rows(0).Item("tamaño")
                txt_cant_max.Enabled = False
                txt_tamaño.Enabled = False
            Else
                txt_cant_max.Enabled = True
                txt_tamaño.Enabled = True
            End If

        End If
    End Sub


    Private Sub txt_unipalet_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_unipalet.KeyUp
        Dim sql As String
        Dim resultado As String
        If e.KeyCode = Keys.Enter Then
            If IsNumeric(txt_unipalet.Text) Then
                If MsgBox("Guardar datos?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    sql = "update gesarticulos set MaxApilable=" & txt_unipalet.Text & " where codarticulo='" & txt_articulo.Text.Trim & "' and codempresa=1"
                    resultado = conexion.ExecuteCmd(constantes.bd_inase, sql)
                    If resultado <> "0" Then MsgBox("Se produjo un error, los datos no serán guardados.", MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("El dato introducido no es correcto.", MsgBoxStyle.Exclamation)
            End If
        End If

    End Sub

    Private Sub txt_unipalet_Enter(sender As System.Object, e As System.EventArgs) Handles txt_unipalet.Enter
        txt_unipalet.SelectAll()
    End Sub

    Private Sub txt_unipalet_Click(sender As System.Object, e As System.EventArgs) Handles txt_unipalet.Click
        txt_unipalet.SelectAll()
    End Sub

    Private Sub list_tipo_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_tipo.DropDownClosed
        If dg_ubicaciones.SelectedRows.Count = 1 And Strings.Left(list_almacen_trasladar.SelectedValue, 1) <> "S" Then llenar_ubi(list_tipo.SelectedValue, dg_ubicaciones.SelectedRows(0).Cells("Max.").Value)
    End Sub

    Private Sub dg_ubicaciones_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dg_ubicaciones.CellClick

        If e.RowIndex = -1 Then Exit Sub 'Si pincha en cabecera
        lab_pendreaprov.Text = funciones.obtener_pendreaprov(txt_articulo.Text, dg_ubicaciones.Rows(e.RowIndex).Cells("Alm.").Value, dg_ubicaciones.Rows(e.RowIndex).Cells("Ubicacion").Value, dg_ubicaciones.Rows(e.RowIndex).Cells("Tipo").Value)

        If e.ColumnIndex < 4 Or e.ColumnIndex > 6 Then
            dg_ubicaciones.Rows(e.RowIndex).Selected = True
        End If
        txt_cantidad_trasladar.Text = CInt(dg_ubicaciones.Rows(e.RowIndex).Cells("Cant.").Value)
        If IsNumeric(dg_ubicaciones.Rows(e.RowIndex).Cells("Tam.").Value) Then txt_tamaño.Text = CInt(dg_ubicaciones.Rows(e.RowIndex).Cells("Tam.").Value) Else txt_tamaño.Text = 0

        list_almacen_trasladar.SelectedValue = dg_ubicaciones.Rows(e.RowIndex).Cells("Alm.").Value

        'Llenar la ubi 
        llenar_ubi(list_tipo.SelectedValue, dg_ubicaciones.Rows(e.RowIndex).Cells("Max.").Value)
    End Sub
End Class