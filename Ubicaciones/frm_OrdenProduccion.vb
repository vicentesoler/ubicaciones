﻿
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Net
Imports System.IO
Imports System.Text

Public Class frm_OrdenProduccion
    Dim conexion As New Conector
    Dim estado As String
    Dim carga_inicial As Boolean
    Dim fecha_inicio, fecha_fin As DateTime

    Private Sub frm_OrdenProduccion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        llenar_periodos()
        llenar_filtro_prioridades()
        lab_tipo.Text = ""
        lab_tit_lineas.Text = ""
        If list_codperiodo.Items.Count > 0 Then llenar_ordenes()
        limpiar(False)
        dg_lineas.DefaultCellStyle.SelectionBackColor = Color.Gray
        dg_lineas.DefaultCellStyle.SelectionForeColor = Color.White
        dg_ubi.DefaultCellStyle.SelectionBackColor = Color.Gray
        dg_ubi.DefaultCellStyle.SelectionForeColor = Color.White
        'If frm_principal.lab_idoperario.Text = 8 Then btn_ImprimirEti.Visible = True
    End Sub
    Public Sub llenar_periodos()
        Dim sql As String
        Dim vdatos As New DataTable

        'Llenar_periodos
        list_codperiodo.DataSource = Nothing
        list_codperiodo.Items.Clear()
        'sql = "select distinct cab.CodPeriodo as codperiodo "
        'Sql &= "from GesAlbaranesProv cab "
        'sql &= "inner join GesAlbaranesProvLin lin on lin.RefCabecera =cab.NumReferencia and lin.CantidadValidada <> cantidad "
        'Sql &= "where cab.estado in ('00','01') and cab.SerAlbaranProv ='OF' "
        sql = "select year(GETDATE())-2 as codperiodo union  select year(GETDATE())-1 as codperiodo union  select year(GETDATE()) as codperiodo order by codperiodo desc  "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        list_codperiodo.DisplayMember = "codperiodo"
        list_codperiodo.ValueMember = "codperiodo"
        list_codperiodo.DataSource = vdatos
        'list_codperiodo.SelectedIndex = 0

    End Sub
    Public Sub llenar_ordenes()
        Dim sql As String
        Dim vdatos As New DataTable
        Try
            carga_inicial = True
            list_codorden.DataSource = Nothing
            'list_codorden.Items.Clear()
            sql = "select cab.CodAlbaranProv "
            sql &= "from GesAlbaranesProv cab "
            sql &= "inner join GesAlbaranesProvLin lin on lin.RefCabecera =cab.NumReferencia " 'and abs(lin.CantidadValidada) <> abs(cantidad) "
            sql &= "left join intranet.dbo.PrioridadesOF prio on prio.id=cab.fase "
            sql &= "where cab.estado in ('00','01') and cab.CodPeriodo='" & list_codperiodo.SelectedValue.ToString.Trim & "' and cab.SerAlbaranProv ='OF' "
            sql &= "and (isnull(FinDepositada,0)=0 or isnull(cab.FinDepositada,0)=0)  "
            If cb_FiltroPorRecoger.Checked Then sql &= "and isnull(cab.FinRecogida,0)=0 "
            If cb_FiltroPorDepositar.Checked Then sql &= "and isnull(cab.FinDepositada,0)=0 "
            If cb_FiltroStockA01.Checked Then
                sql &= "and lin.RefCabecera  not in ( "
                sql &= " select lin2.RefCabecera  from GesAlbaranesProvLin lin2 where lin2.RefCabecera =lin.RefCabecera and "
                sql &= "(case when lin2.Cantidad < 0 then isnull((select sum(stockactual) from GesStocks st where st.CodAlmacen ='A01' and st.CodEmpresa in (1,2,3,5,6) and st.CodArticulo =lin2.Articulo),0) else 99999 end) < abs(lin2.Cantidad )) "
            End If
            If cb_FiltroStockA07.Checked Then
                sql &= "and lin.RefCabecera  not in ( "
                sql &= " select lin2.RefCabecera  from GesAlbaranesProvLin lin2 where lin2.RefCabecera =lin.RefCabecera and "
                sql &= "(case when lin2.Cantidad < 0 then isnull((select sum(stockactual) from GesStocks st where st.CodAlmacen ='A07' and st.CodEmpresa in (1,2,3,5,6) and st.CodArticulo =lin2.Articulo),0) else 99999 end) < abs(lin2.Cantidad )) "
            End If
            If cb_FiltroB2BWEB.Checked Then sql &= "and rtrim(ltrim(cab.SuDocumento))<> '' "

            If list_filtro_prioridad.SelectedValue <> 0 Then sql &= "and isnull(prio.id,'')='" & list_filtro_prioridad.SelectedValue & "' "

            If txt_filtro_codarticulo.Text.ToString.Length = 7 Then
                sql &= "and lin.articulo='" & txt_filtro_codarticulo.Text & "' "
            End If

            If list_filtro_tipo.Text = "Gener." Then sql &= "and cab.proveedor=90000 "
            If list_filtro_tipo.Text = "Manip." Then sql &= "and cab.proveedor in (91000,93000) "
            If list_filtro_tipo.Text = "Perso." Then sql &= "and cab.proveedor=92000 "

            sql &= "group by cab.CodAlbaranProv,prio.id "
            sql &= "order by isnull(prio.id,9999) asc,cab.CodAlbaranProv "
            vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

            list_codorden.DisplayMember = "CodAlbaranProv"
            list_codorden.ValueMember = "CodAlbaranProv"
            list_codorden.DataSource = vdatos
            list_codorden.SelectedIndex = -1

            'Si el articulo está en un pedido y todavía no está albaranado , no pueden prepararlo y puede que estén buscando esta ref en una orden y no les salga
            'en el desplegable de ordenes, aviso de que no está albaranado
            If vdatos.Rows.Count = 0 And txt_filtro_codarticulo.Text <> "" Then
                sql = "select articulo from GesPedidosProvLin where SerPedidoProv ='OF' and cantidadpdte<>0 and Articulo ='" & txt_filtro_codarticulo.Text & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                If vdatos.Rows.Count > 0 Then
                    MsgBox("Existen OF pero no están albaranadas, por tanto no están disponibles para preparar")
                End If

            End If

            If txt_filtro_codarticulo.Text <> "" Or cb_FiltroB2BWEB.Checked Or cb_FiltroPorDepositar.Checked Or cb_FiltroPorRecoger.Checked Or cb_FiltroStockA01.Checked Or cb_FiltroStockA07.Checked Or list_filtro_tipo.Text <> "" Then
                list_codorden.BackColor = Color.SteelBlue
            Else
                list_codorden.BackColor = Color.White
            End If

        Catch ex As Exception
            MsgBox("Hubo algún problema al obtener las OF. Revise el filtro aplicado o elimine el filtro y pruebe de nuevo.")
        Finally
            carga_inicial = False
        End Try



    End Sub
    Public Sub llenar_filtro_prioridades()
        Dim vdatos As New DataTable
        Dim sql As String


        sql = "select 0 as id,'TODAS' as descripcion union select id,descripcion from prioridadesOF order by id asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        list_filtro_prioridad.DisplayMember = "descripcion"
        list_filtro_prioridad.ValueMember = "id"
        list_filtro_prioridad.DataSource = vdatos
        list_filtro_prioridad.SelectedIndex = 0

    End Sub
    Public Sub limpiar(Optional ByVal GuardarTiempos As Boolean = True, Optional ByVal ObsGuardarTiempos As String = "")
        'Guardar datos de operario
        fecha_fin = Now
        If GuardarTiempos Then Guardar_datos_tiempo(ObsGuardarTiempos)
        imagen.Image = Nothing
        P_filtros.Enabled = True
        list_accion.SelectedIndex = -1
        dg_lineas.DataSource = Nothing
        dg_lineas.Enabled = False
        dg_ubi.DataSource = Nothing
        dg_ubi.Enabled = False
        dg_procesadas.DataSource = Nothing
        dg_procesadas.Enabled = False
        Dg_producir.Rows.Clear()
        txt_cantidad.Text = 0
        btn_ok.Enabled = False
        btn_finalizar.Enabled = False
        btn_finalizar.Visible = False
        btn_salirorden.Enabled = False
        btn_ImprimirEti.Visible = False
        txt_cantImpEti.Visible = False
        txt_UCaja.Visible = False
        lab_cantImpEti.Visible = False
        lab_UCaja.Visible = False
        lab_fecha.Text = ""
        lab_tipo.Text = ""
        lab_prioridad.Text = ""
        labStockA01.Text = 0
        labStockA07.Text = 0
        labStockA08.Text = 0
        LabPdtePrep.Text = 0
        LabPdteServ.Text = 0
        'lab_CantPaletA01_CONS.Text = 0
        'lab_CantPaletA01_CONS.Visible = False
        lab_CantPaletA07_CONS.Text = 0
        lab_CantPaletA07_CONS.Visible = False
        lab_CantPaletA01_UBI.Text = 0
        lab_CantPaletA01_UBI.Visible = False
        'lab_A01Cons.Visible = False
        lab_A01Ubi.Visible = False
        Lab_A07Cons.Visible = False
        Lab_AlbDepende.Visible = False
        lab_AlbDependeText.Visible = False
        Lab_AlbDepende.Text = ""
        list_accion.Enabled = False
        list_codperiodo.Enabled = True
        list_codorden.Text = ""
        list_codorden.Enabled = True
        list_codorden.Focus()
    End Sub
    Private Sub list_codorden_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_codorden.SelectedIndexChanged
        If carga_inicial = False Then seleccionar_orden()
    End Sub
    Public Sub seleccionar_orden()
        Dim sql As String
        Dim vdatos, vrefs, vdepende, vaux As New DataTable
        Dim cont As Integer
        Dim TotCantADep As Integer
        If Not IsNumeric(list_codorden.Text) Then
            MsgBox("El campo N.Ord. debe tener un valor numérico.")
            Exit Sub
        End If

        'Guardar el momento de abrir la orden
        fecha_inicio = Now

        dg_lineas.DataSource = Nothing
        dg_ubi.DataSource = New DataTable
        list_accion.SelectedIndex = -1
        btn_ok.Enabled = False
        txt_cantidad.Text = 0
        btn_finalizar.Enabled = False
        btn_finalizar.Visible = False
        btn_OtraUbi.Enabled = False

        'Mirar si la orden (Albarán) ya ha sido completamente procesada o si está abierta por algún operario
        sql = "select cab.estado,oa.nombre,cab.FechaAlbaranProv,cab.ultoperario,cab.Proveedor,cab.SuDocumento,lin.cantidad,lin.articulo,lin.deslinea,NumRefDepende,isnull(prio.descripcion,'') as prioridadOF,art.EnvExteriorUd unicaja "
        sql &= "from GesAlbaranesProvLin lin  "
        sql &= "inner join gesarticulos art on art.codempresa=1 and art.codarticulo=lin.articulo "
        sql &= "inner join GesAlbaranesProv cab on cab.numreferencia=lin.refcabecera "
        sql &= "left join intranet.dbo.prioridadesOF prio on prio.id=cab.fase "
        sql &= "left join intranet.dbo.operarios_almacen oa on oa.id=cab.ultoperario "
        sql &= "left join GesPedidosProvLin linped on linped.codempresa =lin.CodEmpresaPed And linped.CodPeriodo =lin.CodPeriodoPed And linped.SerPedidoProv =lin.SerPedidoProv And linped.CodPedidoProv=lin.CodPedidoProv And linped.CodLinea=lin.LinPedido "
        sql &= "where lin.CodPeriodo=" & list_codperiodo.SelectedValue & " And lin.SerAlbaranProv='OF' and lin.CodAlbaranProv = " & list_codorden.Text
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        If vdatos.Rows.Count = 0 Then
            MsgBox("No existe esta orden.", MsgBoxStyle.Exclamation)
            Exit Sub
        Else
            'Obtener las observaciones
            sql = "select ObservacionesPedido,ObservacionesAlbaran from planificadorOf where codempresaAlbaran=1 and CodPeriodoAlbaran=" & list_codperiodo.SelectedValue & " and serAlbaran='OF' and CodAlbaran=" & list_codorden.Text & " and (ObservacionesPedido <>'' or ObservacionesAlbaran<>'') "
            vaux = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vaux.Rows.Count = 1 Then
                If vaux.Rows(0).Item("ObservacionesPedido").trim <> "" Then
                    Frm_Mensaje.lab_titulo.Text = "Observaciones Pedido"
                    Frm_Mensaje.txt_mensaje.Text = vaux.Rows(0).Item("ObservacionesPedido").trim
                    Frm_Mensaje.ShowDialog()
                End If
                If vaux.Rows(0).Item("ObservacionesAlbaran").trim <> vaux.Rows(0).Item("ObservacionesPedido").trim And vaux.Rows(0).Item("ObservacionesAlbaran").trim <> "" Then
                    Frm_Mensaje.lab_titulo.Text = "Observaciones Albarán"
                    Frm_Mensaje.txt_mensaje.Text = vaux.Rows(0).Item("ObservacionesAlbaran").trim
                    Frm_Mensaje.ShowDialog()
                End If

            End If

            'Mirar si esta OF esta asociada con otra. Por tanto no se puede preparar hasta que no esté terminada la otra. Pedido por Chelo
            'Si la orden se ha generado desde el monitor de pedidos, el campo NumRefDepende contiene la OF asociada
            If vdatos.Rows(0).Item("NumRefDepende").ToString <> "" Then
                sql = "select CodEmpresa as 'codempresaAlbaran' ,CodPeriodo as 'codperiodoalbaran',SerAlbaranProv as 'seralbaran' ,CodAlbaranProv as 'codalbaran' from inase.dbo.GesAlbaranesProv where numreferencia='" & vdatos.Rows(0).Item("NumRefDepende").ToString & "' "
            Else
                sql = "select codempresaAlbaran,codperiodoalbaran,seralbaran,codalbaran from planificadorOf where id= (select idDepende from planificadorOf where codempresaAlbaran=1 and CodPeriodoAlbaran=" & list_codperiodo.SelectedValue & " and serAlbaran='OF' and CodAlbaran=" & list_codorden.Text & ") "
            End If
            vaux = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vaux.Rows.Count = 1 Then
                Lab_AlbDepende.Visible = True
                lab_AlbDependeText.Visible = True
                Lab_AlbDepende.Text = vaux.Rows(0).Item("codalbaran")
                'Buscar si la OF existe y está terminada
                sql = "select isnull(FinRecogida,0) as FinRecogida ,isnull(FinDepositada,0) as FinDepositada from GesAlbaranesProv "
                sql &= "where CodEmpresa =" & vaux.Rows(0).Item("codempresaAlbaran") & " And CodPeriodo =" & vaux.Rows(0).Item("codperiodoalbaran") & " And SerAlbaranProv ='" & vaux.Rows(0).Item("seralbaran") & "' and CodAlbaranProv =" & vaux.Rows(0).Item("codalbaran") & " "
                vdepende = conexion.TablaxCmd(constantes.bd_inase, sql)
                If vdepende.Rows.Count = 1 Then
                    If vdepende.Rows(0).Item("FinDepositada") = 0 Then
                        MsgBox("Esta OF está asociada a la OF de Mont./Desg. " & vaux.Rows(0).Item("codalbaran") & " que no está todavía finalizada. No se puede abrir la OF indicada hasta que no termine la OF asociada.")
                        Exit Sub
                    End If
                Else
                    MsgBox("Esta OF estaba asociada a la OF de Mont./Desg." & vaux.Rows(0).Item("codalbaran") & " pero esta OF de Mont./Desg. ya no existe.")
                End If
            Else
                Lab_AlbDepende.Visible = False
                lab_AlbDependeText.Visible = False
                Lab_AlbDepende.Text = ""
            End If

            'lab_A01Cons.Visible = False
            lab_A01Ubi.Visible = False
            Lab_A07Cons.Visible = False
            'lab_CantPaletA01_CONS.Visible = False
            lab_CantPaletA01_UBI.Visible = False
            lab_CantPaletA07_CONS.Visible = False

            estado = vdatos.Rows(0).Item("estado").ToString.Trim
            list_accion.Items.Clear()
            If vdatos.Rows(0).Item("Proveedor") = 90000 Then
                lab_tipo.Text = "Genéricos"
                lab_tipo.ForeColor = Color.Purple
                'lab_A01Cons.Visible = True
                lab_A01Ubi.Visible = True
                Lab_A07Cons.Visible = True
                'lab_CantPaletA01_CONS.Visible = True
                lab_CantPaletA01_UBI.Visible = True
                lab_CantPaletA07_CONS.Visible = True
                list_accion.Items.Add("Recoger")
                list_accion.Items.Add("Imprimir")
                list_accion.Items.Add("Depositar")
                list_accion.Items.Add("Recuperar")
            End If
            If vdatos.Rows(0).Item("Proveedor") = 91000 Then
                lab_tipo.Text = "Manipulación"
                lab_tipo.ForeColor = Color.Orange
                list_accion.Items.Add("Recoger")
                list_accion.Items.Add("Depositar")
            End If
            If vdatos.Rows(0).Item("Proveedor") = 93000 Then
                lab_tipo.Text = "Manipulación"
                lab_tipo.ForeColor = Color.Orange
                list_accion.Items.Add("Recoger")
                list_accion.Items.Add("Depositar")
            End If
            If vdatos.Rows(0).Item("Proveedor") = 92000 Then
                lab_tipo.Text = "Personalización"
                lab_tipo.ForeColor = Color.Green
                list_accion.Items.Add("Recoger")
                list_accion.Items.Add("Imprimir")
                list_accion.Items.Add("Depositar")
                list_accion.Items.Add("Recuperar")
            End If

            If vdatos.Rows(0).Item("SuDocumento") <> "" Then
                LabPedWeb.Visible = True
                LabTxtPedWeb.Visible = True
                LabPedWeb.Text = vdatos.Rows(0).Item("SuDocumento").ToString.Trim
                Select Case vdatos.Rows(0).Item("Proveedor")
                    Case "91000" : MsgBox("Esta OF está asociada a un pedido.")
                    Case "92000" : MsgBox("Esta OF pertenece a una personalización de producto de la web.")
                End Select

            Else
                LabPedWeb.Visible = False
                LabTxtPedWeb.Visible = False
                LabPedWeb.Text = ""
            End If

            If estado = "01" Then
                MsgBox("Esta orden ya está abierta por el usuario " & vdatos.Rows(0).Item("nombre").ToString, MsgBoxStyle.Information)
                llenar_ordenes()
                limpiar(True, "Esta orden ya está abierta por el usuario")
                Exit Sub
            End If
            If vdatos.Rows(0).Item("estado").ToString.Trim = "02" Then
                If MsgBox("Esta orden ya ha sido finalizada, abrirla de todos modos? ", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    btn_finalizar.Enabled = False
                    btn_ok.Enabled = False
                Else
                    llenar_ordenes()
                    limpiar(True, "Esta orden ya ha sido finalizada")
                    Exit Sub
                End If
            End If

            If vdatos.Rows(0).Item("estado").ToString.Trim = "00" Then
                btn_finalizar.Visible = True
                list_codorden.Enabled = False
                list_codperiodo.Enabled = False
            End If

            lab_prioridad.Text = "PRIORIDAD " & vdatos.Rows(0).Item("prioridadOF")

            Select Case vdatos.Rows(0).Item("prioridadOF")
                Case "ALTA" : lab_prioridad.ForeColor = Color.OrangeRed
                Case "MEDIA/ALTA" : lab_prioridad.ForeColor = Color.Orange
                Case "MEDIA" : lab_prioridad.ForeColor = Color.Yellow
                Case "MEDIA/BAJA" : lab_prioridad.ForeColor = Color.YellowGreen
                Case "BAJA" : lab_prioridad.ForeColor = Color.Green
                Case Else
                    lab_prioridad.Text = "SIN PRIORIDAD"
                    lab_prioridad.ForeColor = Color.Black
            End Select

            If lab_tipo.Text = "Genéricos" Then
                txt_UCaja.Text = CInt(vdatos.Rows(cont).Item("unicaja"))
                txt_cantImpEti.Text = CInt(vdatos.Rows(cont).Item("cantidad"))
            End If

            '06/03/2020 David Rios:Llenar las referencias que se producen. Solo como información.
            TotCantADep = 0
            Dg_producir.Rows.Clear()
            Dg_producir.Columns.Clear()
            Dg_producir.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 13, FontStyle.Regular)
            Dg_producir.Columns.Add("Referencia", "Referencia")
            Dg_producir.Columns(0).Width = 120
            Dg_producir.Columns.Add("Descripción", "Descripción")
            Dg_producir.Columns(1).Width = 420
            Dg_producir.Columns.Add("Cantidad", "Cantidad")
            Dg_producir.Columns(2).Width = 100
            cont = 0
            Do While cont < vdatos.Rows.Count
                If vdatos.Rows(cont).Item("Cantidad") > 0 Then
                    Dg_producir.Rows.Add(New String() {vdatos.Rows(cont).Item("articulo").ToString.Trim, vdatos.Rows(cont).Item("Deslinea").ToString.Trim, CInt(vdatos.Rows(cont).Item("Cantidad"))})
                    TotCantADep += vdatos.Rows(cont).Item("Cantidad")
                End If
                cont += 1
            Loop
            Dg_producir.CurrentCell = Nothing

        End If
        lab_fecha.Text = "Fecha: " & CDate(vdatos.Rows(0).Item("FechaAlbaranProv").ToString).ToShortDateString

        'Mirar si esta OF está hecha para algún albarán/es y ver en que almacén se van a cerrar, para luego cuando depositemos, saber en que Palet dejar para trasladar
        If lab_tipo.Text = "Genéricos" Then
            Dim vcantalm As New DataTable
            sql = "select bol.CodAlmacen,sum(mof.CantReserva) as CantReserva  "
            sql &= "from MonitorBolsasReservasOF mof "
            sql &= "inner join MonitorBolsas bol on bol.IdBolsa =mof.idbolsa "
            sql &= "where mof.CodEmpresaOF=1 And mof.CodperiodoOF=" & list_codperiodo.SelectedValue & " And mof.SerAlbaranOF='OF' and mof.CodAlbaranOF =" & list_codorden.Text
            sql &= "group by bol.CodAlmacen "
            vcantalm = conexion.TablaxCmd(constantes.bd_intranet, sql)
            cont = 0
            Do While cont < vcantalm.Rows.Count
                'If vcantalm.Rows(cont).Item("CodAlmacen") = "A01" Then lab_CantPaletA01_CONS.Text = vcantalm.Rows(cont).Item("CantReserva")
                'Ahora solo hay dos palets, A01 y A07, por tanto lo q va para el A01, lo sumo al palet A01 de Ubicacion
                If vcantalm.Rows(cont).Item("CodAlmacen") = "A01" Then lab_CantPaletA01_UBI.Text = vcantalm.Rows(cont).Item("CantReserva")
                If vcantalm.Rows(cont).Item("CodAlmacen") = "A07" Then lab_CantPaletA07_CONS.Text = vcantalm.Rows(cont).Item("CantReserva")
                TotCantADep -= vcantalm.Rows(cont).Item("CantReserva")
                cont += 1
            Loop

            If TotCantADep > 0 Then lab_CantPaletA01_UBI.Text += TotCantADep
        End If

        'Marcar como abierta para que nadie pueda entrar
        sql = "update GesAlbaranesProv set estado='01',UltOperario='" & frm_principal.lab_idoperario.Text & "',UltModificacion=getdate() where CodPeriodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv='OF' and CodAlbaranProv=" & list_codorden.Text
        estado = "01"
        conexion.ExecuteCmd(constantes.bd_inase, sql)


        If lab_tipo.Text = "Genéricos" Then
            btn_ImprimirEti.Visible = True
            txt_UCaja.Visible = True
            lab_UCaja.Visible = True
            txt_cantImpEti.Visible = True
            lab_cantImpEti.Visible = True
        Else
            btn_ImprimirEti.Visible = False
            txt_UCaja.Visible = False
            lab_UCaja.Visible = False
            txt_cantImpEti.Visible = False
            lab_cantImpEti.Visible = False
        End If


        P_filtros.Enabled = False
        btn_salirorden.Enabled = True
        list_accion.Enabled = True
        btn_finalizar.Enabled = False ' HAsta que no seleccionen si están recogiendo o finalizando, no activo

    End Sub
    Public Sub cargar_lineas()
        Dim sql As String
        Dim vdatos, vpick, vpulm As New DataTable
        Dim cont = 0

        sql = "select lin.CodLinea as 'Lin.', lin.Articulo as 'Ref.',DesLinea as 'Descripción' ,abs(cast(Cantidad as int)) as 'Cant.' ,cast(CantidadValidada as int) as 'Val.' "
        sql &= "from GesAlbaranesProvLin lin "
        sql &= "where lin.codperiodo=" & list_codperiodo.SelectedValue & " and lin.SerAlbaranProv ='OF' and lin.CodAlbaranProv = " & list_codorden.Text & " "
        If list_accion.Text = "Recoger" Then sql &= "and cantidad < 0 "
        If list_accion.Text = "Depositar" Then sql &= "and cantidad > 0 "
        sql &= "order by convert(integer,codlinea) asc "

        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        If vdatos.Rows.Count() > 0 Then
            dg_lineas.DataSource = vdatos
            dg_lineas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15)
            dg_lineas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
            'dg_lineas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_lineas.Columns(0).Width = 50
            dg_lineas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(1).Width = 100
            dg_lineas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_lineas.Columns(2).Width = 350
            dg_lineas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            dg_lineas.Columns(3).Width = 80
            dg_lineas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_lineas.Columns(4).Width = 80
            dg_lineas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


            'Pintar
            cont = 0
            Do While cont < dg_lineas.Rows.Count
                Select Case dg_lineas.Rows(cont).Cells("Val.").Value
                    Case Is = 0 : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.White
                    Case Is = dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Green
                    Case Is > dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Red
                    Case Is < dg_lineas.Rows(cont).Cells("Cant.").Value : dg_lineas.Rows(cont).DefaultCellStyle.BackColor = Color.Yellow
                End Select
                cont += 1
            Loop

            dg_lineas.CurrentCell = Nothing
        Else
            MsgBox("La orden no tiene lineas", MsgBoxStyle.Critical)
            list_codorden.SelectAll()
        End If



    End Sub


    Private Sub btn_ok_Click(sender As System.Object, e As System.EventArgs) Handles btn_ok.Click
        Dim sql, ssel As String
        Dim resultado As String
        Dim vcant, vdatos As New DataTable
        Dim cantant As Integer
        Dim codarticulo As String
        Dim cont As Integer

        'Obligo a solo depositar o recoger de una ubicacion. La cantidad está toda en una línea de albarán o de mov. Por tanto como el stock de almacen (de momento solo al depositar) se actualiza
        'al pulsar el ok, esta cantidad es enteramente confirmada y genera el cambio dstock desde P1 hasta el almacen destino. No se puede depositar o recoger parcialmente, aunque en un principio si
        'que lo programé así, me di cuenta que es error, ya que si la cantidad es 10 y depositan 5, está cambiando el almacén en la linea y lo que hace es confirmar las 10 y no las 5.
        'Si depositan una cantidad diferente, lo que hago es modificar el albarán o mov para poner la cantidad que realmente han depositado
        'Lo soluciono poniendo el campo de cantidad en no editable, así obligo a que la cantidad sea siempre la que tiene la orden. Si quieren cambiarla, han de avisar a
        'para que les modifiquen la cantidad a fabricar
        'lo estoy cambiando para que puedan validara la cantidad que quieran. Siempre la cantidad depositada deberá ser la misma que la recogida

        If dg_lineas.SelectedRows.Count = 0 Then
            MsgBox("Debe seleccionar la línea.", MsgBoxStyle.Exclamation)
            Exit Sub
        Else
            codarticulo = dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim.ToUpper
        End If


        If dg_ubi.SelectedRows.Count = 0 Then
            MsgBox("Debe seleccionar la ubicación", MsgBoxStyle.Exclamation)
            Exit Sub
        End If


        If CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Val.").Value.ToString) > CInt(dg_lineas.SelectedRows(0).Cells("Cant.").Value.ToString) Then
            MsgBox("La cantidad a validar no puede ser superior a la cantidad de la linea.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'Si ya ha sido recogido/depositado algo en una ubicación, el almacén donde se va a depositar/ubicar ahora debe ser el mismo. Si no obliga a modificar el albarán en dos líneas
        If dg_procesadas.Rows.Count > 0 Then
            If dg_ubi.SelectedRows(0).Cells("Alm.").Value <> dg_procesadas.Rows(0).Cells("Alm.").Value Then
                MsgBox("Ya ha sido procesada una cantidad en otro almacén diferente al que está procesando ahora. No se permite.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
        End If

        'En el caso de estár recogiendo para un montaje, La cantidad debe ser múltiplo de las unidadesaconsumir
        '14-11-2019 hay casos en los que la referencia a consumir no es la que aparece en la FT, ya que se cambio en el planificador(lo permite el de Iván)
        'En estos casos debo buscar si hay cambio de material en el planificador
        Dim UnidadesAConsumir As Integer
        Dim codpack As String = ""
        Dim BaseAReemplazar As String = ""
        If list_accion.Text = "Recoger" Then
            sql = "select linped.TipoLinea,linped.refcabecera  "
            sql &= "from GesAlbaranesProvLin linalb "
            sql &= "inner join GesPedidosProvLin linped on  linped.CodEmpresa =linalb.CodEmpresaPed And linped.CodPeriodo =linalb.CodPeriodoPed And linped.SerPedidoProv =linalb.SerPedidoProv "
            sql &= " And linped.CodPedidoProv = linalb.CodPedidoProv And linped.CodLinea = linalb.LinPedido "
            sql &= "where linalb.CodEmpresa=1 and linalb.CodPeriodo =" & list_codperiodo.SelectedValue & " and linalb.SerAlbaranProv ='OF' and linalb.CodAlbaranProv =" & list_codorden.Text & " and linalb.CodLinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & " "
            vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

            If IsNumeric(vdatos.Rows(0).Item("tipolinea").ToString) Then 'Montaje
                UnidadesAConsumir = 1 'Quedamos Iván y yo que si no se encuentra, podnríamos 1 unidad.
                'Buscar el codpack
                sql = "select articulo from GesPedidosProvLin where refcabecera='" & vdatos.Rows(0).Item("refcabecera").ToString & "' and codlinea=" & vdatos.Rows(0).Item("tipolinea") & " "
                vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                codpack = vdatos.Rows(0).Item("articulo")
                'Buscar las UnidadesAConsumir 
                sql = "select UnidadesAConsumir from intranet.dbo.ArticulosPackslin where CodPack ='" & codpack & "' and CodArticulo ='" & codarticulo & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vdatos.Rows.Count > 0 Then
                    UnidadesAConsumir = vdatos.Rows(0)(0)
                Else
                    'Buscar en el planificador el cambio de material
                    sql = "select BaseAReemplazar from intranet.dbo.planificadorOF where CodEmpresaAlbaran=1 and CodPeriodoAlbaran='" & list_codperiodo.SelectedValue & "' and SerAlbaran='OF' and CodAlbaran=" & list_codorden.Text & " "
                    sql &= "and basenueva='" & codarticulo & "' "
                    vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                    If vdatos.Rows.Count = 1 Then
                        BaseAReemplazar = vdatos.Rows(0).Item("BaseAReemplazar")
                        sql = "select UnidadesAConsumir from intranet.dbo.ArticulosPackslin where CodPack ='" & codpack & "' and CodArticulo ='" & BaseAReemplazar & "' "
                        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                        If vdatos.Rows.Count > 0 Then UnidadesAConsumir = vdatos.Rows(0).Item("UnidadesAConsumir")
                    End If
                End If
                If txt_cantidad.Text Mod UnidadesAConsumir <> 0 Then
                    'Desactivado por Iván de forma provisional para evitar errores en casos que la OF no corresponde con las unidades de la FT
                    'MsgBox("La cantidad recogida no es válida, debe ser múltiplo de las 'UnidadesAConsumir' definida en el montaje.")
                    'Exit Sub
                End If
            End If
        End If


        'Si va a depositar una OF de Genéricos , la cantidad depositada + la cantidad q va a depositar no puede exceder de la cantidad máxima a ubicar en cada palet A01/A07
        If list_accion.Text = "Depositar" And lab_tipo.Text = "Genéricos" Then
            Dim cantProcesada As Integer = 0

            'Mirar la cantidad ya depositada en la ubicacion
            cont = 0
            Do While cont < dg_procesadas.Rows.Count
                If dg_procesadas.Rows(cont).Cells("Ubicacion").Value = dg_ubi.SelectedRows(0).Cells("Ubicacion").Value Then
                    cantProcesada = dg_procesadas.Rows(cont).Cells("Cant.").Value
                    Exit Do
                End If
                cont += 1
            Loop

            'If dg_ubi.SelectedRows(0).Cells("Ubicacion").Value = constantes.ImpDigital_A01_CONS Then
            '    If cantProcesada + txt_cantidad.Text > lab_CantPaletA01_CONS.Text Then
            '        MsgBox("Ha sobrepasado la cantidad a depositar en Palet A01 CONS, modifique la cantidad o deposite en el palet A07.")
            '        Exit Sub
            '    End If
            'End If
            If dg_ubi.SelectedRows(0).Cells("Ubicacion").Value = constantes.ImpDigital_A01_UBI Then
                If cantProcesada + txt_cantidad.Text > lab_CantPaletA01_UBI.Text Then
                    MsgBox("Ha sobrepasado la cantidad a depositar en Palet A01 UBI, modifique la cantidad o deposite en el palet A07.")
                    Exit Sub
                End If
            End If
            If dg_ubi.SelectedRows(0).Cells("Ubicacion").Value = constantes.ImpDigital_A07_CONS Then
                If cantProcesada + txt_cantidad.Text > lab_CantPaletA07_CONS.Text Then
                    MsgBox("Ha sobrepasado la cantidad a depositar en Palet A07 CONS, modifique la cantidad o deposite en el palet A01.")
                    Exit Sub
                End If
            End If

        End If

        If txt_cantidad.Text = 0 Then
            MsgBox("La cantidad debe ser superior a 0.", MsgBoxStyle.Information)
        Else
            'Actualizar la ubicacion 

            'En OF de pintar, la referencia pintada se puede ubicar en una ubicación que no existe previamente (38-15-01,38-14-01), por tanto hay que crear la ubicacion si no existe
            'Ver si existe la ubicación
            sql = "select * from ubicaciones where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
            If vdatos.Rows.Count = 0 Then
                sql = "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
                sql &= "values ('" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "','" & codarticulo & "','" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "','Picking'" & ",0,0,'',0,'Ubicaciones-OF (No existe Ubi al depositar) ','" & frm_principal.lab_nomoperario.Text & "')"
                conexion.TablaxCmd(constantes.bd_intranet, sql)
            End If


            If list_accion.Text = "Recoger" Then sql = "update ubicaciones set cantidad=isnull(cantidad,0)-" & CInt(txt_cantidad.Text) & " "
            If list_accion.Text = "Depositar" Then sql = "update ubicaciones set cantidad=isnull(cantidad,0)+" & CInt(txt_cantidad.Text) & " "
            sql &= "where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' "
            'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
            sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' order by cantidad asc ) "

            'Añado un registro a la tabla OrdenesFabricacion o actualizo el que ya existe
            ssel = "select cantidad,codalmacen from OrdenesFabricacion where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv='OF' and CodAlbaranProv=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & " and CodAlmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and Ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' "
            vdatos = conexion.TablaxCmd(constantes.bd_intranet, ssel)
            If vdatos.Rows.Count = 1 Then
                'Actualizo
                sql &= "update OrdenesFabricacion set cantidad=cantidad+(" & txt_cantidad.Text & "),ultusuario='" & frm_principal.lab_idoperario.Text & "',ultmodificacion=getdate() where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv='OF' and CodAlbaranProv=" & list_codorden.Text & " and CodLinea='" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & "' and CodAlmacen='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' and Ubicacion='" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "' "
            Else
                'Inserto
                sql &= "insert into OrdenesFabricacion(codempresa,codperiodo,seralbaranprov,codalbaranprov,codlinea,codalmacen,ubicacion,codarticulo,cantidad,ultusuario,accion) values(1," & list_codperiodo.SelectedValue & ",'OF'," & list_codorden.Text & "," & dg_lineas.SelectedRows(0).Cells("Lin.").Value & ",'" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "','" & dg_ubi.SelectedRows(0).Cells("Ubicacion").Value & "','" & dg_lineas.SelectedRows(0).Cells("Ref.").Value & "'," & txt_cantidad.Text & ",'" & frm_principal.lab_idoperario.Text & "','" & list_accion.Text & "') "
            End If

            'Actualizar la linea del albaran
            sql &= "update inase.dbo.gesalbaranesprovlin set  CantidadValidada=CantidadValidada + (" & txt_cantidad.Text & ") where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv='OF' and CodAlbaranProv=" & list_codorden.Text & " and CodLinea='" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & "' "



            'Actualizo el stock . 
            'Solo si están depositando, ya que en la recogida no se actualiza el stock. El stock ya se ha descontado al generar el albaran.
            'Esto lo podria mejorar para que el stock no se descuenta hasta que no se recoja en la orden pero para esto hay q hablar con inase y el almacen que se usa en 
            'el albaran sea el z01 y así al recoger cambiaria el almacén por el P1 y en este momento se descontaria el stock

            'si estamos recogiendo , deberia actualizar el movimiento que se hace que descuenta el stock del almacen A01/A07 y lo traslada al p1, ya que
            'si se recoge de otro almacen al que se ha elegido al hacer el pedido, estaría mal el movimiento.
            'Antes solo se actualizaba el almacen cuando estaban depositando, pero creo que esta mal

            'Ahora ya se hace tanto en recogida como en deposito

            'si el almacen destino es diferente, se actualiza
            'Si es un desglose :
            '       -La ref a desglosar (origen) genera un movimiento, este mov es el que hay q cambiar
            '       -Las ref. desglosadas (destino) tienen el almacen en la línea de pedido/albaran
            'Si es un montaje
            '       -La referencia montada (final) tiene el almacen en la linea de pedido/albaran
            '       -Las referencias que se usan para montar (origen) tienen el almacén en un movimiento

            'Simplemente tengo que comprobar si la línea del pedido tiene nummov, en tal caso el almacén está en el movimiento , sino está en el pedido
            'probar bien y activar



            '08/05/2019
            'Antes modificaba el almacén del albarán/movimiento para que el stock fuera al almacén correcto. 
            'Esto provoca que si depositan solo una parte, al modificar el almacén, se modifica el stock total y no solo de la cantidad que han depositado parcial
            'Ahora lo que hago es hacer un movimiento con la cantidad que acaban de recoger/depositar.

            Dim nummov As Integer
            Dim numrefmov As String

            'Consulta para obtener algunos datos que me hacen falta luego
            ssel = "select numreferencia,codlinea,articulo,isnull(cantidadvalidada,0) as cantidadvalidada,isnull(cantidad,0) as cantidad,almacen,CodEmpresaPed,CodPeriodoPed,SerPedidoProv,CodPedidoProv,linpedido "
            ssel &= "from GesAlbaranesProvlin where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codAlbaranProv=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value
            vdatos = conexion.TablaxCmd(constantes.bd_inase, ssel)

            'Buscar el pedidocompra
            Dim vped, vmov As New DataTable
            ssel = "select nummov,articulo,almacen from GesPedidosProvLin where codempresa=" & vdatos.Rows(0).Item("CodEmpresaPed") & " and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and serpedidoprov='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and codpedidoprov=" & vdatos.Rows(0).Item("CodPedidoProv") & " and codlinea= " & vdatos.Rows(0).Item("linpedido") & " "
            vped = conexion.TablaxCmd(constantes.bd_inase, ssel)

            'Si tiene un mov es porque es la que se consume. en este caso no habria que hacer nada , 
            'pero como puede darse el caso de que el almacén de recogida no sea el que tiene puesto el mov, lo cambio para que se haga la bajada de stock del alm. que toca
            '15/10/2020 Solo si no están haciendo la accion de imprimir
            If list_accion.Text <> "Imprimir" And list_accion.Text <> "Recuperar" Then
                If vped.Rows(0).Item("nummov") <> 0 Then
                    'Buscar en el movimiento para ver si el almacen es el mismo
                    ssel = "select nummov,codlinea,AlmacenOri from gesmovalmlin where codempresa=1 and nummov=" & vped.Rows(0).Item("nummov") & " and articulo='" & vped.Rows(0).Item("articulo") & "' "
                    vmov = conexion.TablaxCmd(constantes.bd_inase, ssel)
                    If dg_ubi.SelectedRows(0).Cells("Alm.").Value <> vmov.Rows(0).Item("AlmacenOri") Then
                        'Hasta el 08/05/2019 se hacia así
                        'sql &= "update inase.dbo.gesmovalmlin set AlmacenOri='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' where codempresa=1 and nummov=" & vmov.Rows(0).Item("nummov") & " and codlinea=" & vmov.Rows(0).Item("codlinea") & " "
                        funciones.crear_movimiento_inase(1, nummov, "Mov.creado al " & list_accion.Text & " ref. en alm. distinto al mov. en " & list_codperiodo.SelectedValue & "/OF/" & list_codorden.Text, numrefmov)
                        sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,AlmacenOri,UbicacionOri,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                        sql &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & nummov & ",1,'" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & vmov.Rows(0).Item("AlmacenOri") & "','XXX','" & codarticulo & "'," & txt_cantidad.Text & ",'Mov. causado por Alm. dif. al " & list_accion.Text & ". OF:" & list_codperiodo.SelectedValue & "/" & list_codorden.Text & ".') "
                    End If
                Else
                    funciones.crear_movimiento_inase(1, nummov, "Mov.creado al " & list_accion.Text & " ref. " & list_codperiodo.SelectedValue & "/OF/" & list_codorden.Text, numrefmov)
                    'sql &= "update inase.dbo.GesAlbaranesProvlin set ultmodificacion=getdate(), Almacen ='" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "' where numreferencia='" & vdatos.Rows(0).Item("numreferencia").ToString & "' "
                    sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,AlmacenOri,UbicacionOri,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                    sql &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & nummov & ",1,'P1','XXX','" & dg_ubi.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & codarticulo & "'," & txt_cantidad.Text & ",'Mov. causado al " & list_accion.Text & ". OF:" & list_codperiodo.SelectedValue & "/" & list_codorden.Text & ".') "
                End If
            End If


            'Poner la cantidad 
            dg_lineas.SelectedRows(0).Cells("Val.").Value = CInt(txt_cantidad.Text) + CInt(dg_lineas.SelectedRows(0).Cells("Val.").Value.ToString)

        End If


        'Añadir registro. Se hace antes de ejecutar el insert para poder obtener la cantidad anterior antes de que sea modificada
        If list_accion.Text = "Recoger" Then funciones.añadir_reg_mov_ubicaciones(codarticulo, (CInt(txt_cantidad.Text) - cantant) * -1, dg_ubi.SelectedRows(0).Cells("Alm.").Value, dg_ubi.SelectedRows(0).Cells("Ubicacion").Value, "OF:" & list_codorden.Text, "Ubicaciones-Orden Prod", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
        If list_accion.Text = "Depositar" Then funciones.añadir_reg_mov_ubicaciones(codarticulo, CInt(txt_cantidad.Text) - cantant, dg_ubi.SelectedRows(0).Cells("Alm.").Value, dg_ubi.SelectedRows(0).Cells("Ubicacion").Value, "OF:" & list_codorden.Text, "Ubicaciones-Orden Prod", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado <> "0" Then
            MsgBox("Se produjo un error, avisar a Informática. Err 20180116", MsgBoxStyle.Exclamation)
        End If
        Select Case dg_lineas.SelectedRows(0).Cells("Val.").Value
            Case Is = 0 : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Red
            Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow
        End Select

        'Llenar de nuevo ubicaciones para actualizar las cantidades por ubicacion.
        cargar_lineas_ubicaciones(codarticulo)
        'Llenar el registro de ubicaciones usadas
        cargar_lineas_registro(codarticulo, dg_lineas.SelectedRows(0).Cells("Lin.").Value)

    End Sub
    Public Sub cargar_lineas_ubicaciones(ByVal codarticulo As String)
        Dim sql As String = ""
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim fila_encontrada As Boolean = False
        Dim resultado As String
        Dim valmacenes As New DataTable

inicio:


        'Si es pintada, pero no de personallización

        If list_accion.Text = "Depositar" And lab_tipo.Text = "Genéricos" Then
            'sql &= "select 'A01' as codalmacen,'" & constantes.ubicacion_ImpDigital_A01 & "' as Ubicacion,'Picking' as Tipo,0 as Cantidad,1 as orden union all "
            'If lab_CantPaletA01_CONS.Text > 0 Then
            '    If sql <> "" Then sql &= " Union "
            '    sql &= "select 'A01' as 'Alm.','" & constantes.ImpDigital_A01_CONS & "' as 'Ubicacion','Picking' as Tipo ,0 as Cantidad,'A01 CONS' as 'Palet' "
            'End If
            If lab_CantPaletA01_UBI.Text > 0 Then
                If sql <> "" Then sql &= " Union "
                sql &= "select 'A01' as 'Alm.','" & constantes.ImpDigital_A01_UBI & "' as 'Ubicacion','Picking' as Tipo ,0 as Cantidad,'A01 UBI' as 'Palet' "
            End If
            If lab_CantPaletA07_CONS.Text > 0 Then
                If sql <> "" Then sql &= " Union "
                sql &= "select 'A01' as 'Alm.','" & constantes.ImpDigital_A07_CONS & "' as 'Ubicacion','Picking' as Tipo ,0 as Cantidad,'A07 CONS' as 'Palet' "
            End If
        Else
            sql = "select codalmacen as 'Alm.',Ubicacion as 'Ubicacion',Tipo as 'Tipo',sum(cantidad) as 'Cant.' ,min(orden) as orden from ( "
            'Si están depositando, debe aparecer la ubicación especial si o si
            'Depende del tipo de OF, saldrá una u otra. Pedido por Javier

            'Si es personalizacion es
            If list_accion.Text = "Depositar" And lab_tipo.Text = "Personalización" Then
                sql &= "select 'A01' as codalmacen,'38-14-01' as Ubicacion,'Picking' as Tipo,0 as Cantidad,1 as orden union all "
                sql &= "select 'A01' as codalmacen,'38-15-01' as 'Ubicacion','Picking' as Tipo,0 as Cantidad,2 as orden  union all "
            End If



            sql &= "select codalmacen ,ubicacion,tipo,cantidad,99 as orden "
            sql &= "from ubicaciones "
            sql &= "where codarticulo='" & codarticulo.ToUpper & "'  "

            'Pedido por Sergio. Q no tenga en cuenta devoluciones,saldos o muestras. 18/07/2019
            sql &= "and substring(codalmacen,1,1) not in ('D','S','M') "

            'Si es una personalizacion o una impresión , solo deben salir las ubicaciones especiales
            If list_accion.Text = "Depositar" And lab_tipo.Text = "Personalización" Then
                sql &= "and codalmacen='A01' and ubicacion in ('38-14-01','38-15-01') "
            End If


            sql &= ") as tabla "


            sql &= "group by codalmacen,Ubicacion,Tipo  "
            If LabPedWeb.Text <> "" Then
                'Ana pidio que si es una personalización, debe aparecer primero la ubicación especial (P1)
                sql &= "order by orden asc,codalmacen desc,tipo asc,ubicacion asc "
            Else
                sql &= "order by orden asc,tipo asc,Codalmacen,ubicacion asc "
            End If
        End If


        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        'Lo desactivé porque no veo claro que les ofrezca dar de alta una ubicación cuando están recogiendo, ya que no es real.
        'Lo activle solo para depositar. Pedido por Sergio 18/07/2019
        If vdatos.Rows.Count = 0 And list_accion.Text = "Depositar" Then
            If MsgBox("La referencia no tiene ubicación, quiere ubicarla en consolidación de A01.", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                funciones.añadir_reg_mov_ubicaciones(codarticulo, 0, "A01", constantes.consolidacion_A01, "En Ubicaciaones/OF y no encontrar ubi.", "Ubicaciones-OF", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

                sql = "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
                sql &= "values ('A01','" & codarticulo & "','" & constantes.consolidacion_A01 & "','Picking'" & ",0,0,'',0,'Ubicaciones-OF','" & frm_principal.lab_nomoperario.Text & "')"
                resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                If resultado <> "0" Then
                    MsgBox("Se producjo un error al añadir la referencia a la ubicación de consolidación.", MsgBoxStyle.Critical)
                    Exit Sub
                Else
                    GoTo inicio
                End If
            Else
                Exit Sub
            End If
        Else

            dg_ubi.DataSource = vdatos
            dg_ubi.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 14
                                                    )
            dg_ubi.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold)
            'dg_ubi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_ubi.Columns(0).Width = 55
            dg_ubi.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_ubi.Columns(1).Width = 100
            dg_ubi.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubi.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_ubi.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubi.Columns(2).Width = 80
            dg_ubi.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubi.Columns(3).Width = 80
            dg_ubi.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_ubi.Columns(4).Width = 120



            'Pintar y habilitar
            btn_ok.Enabled = False
            dg_ubi.Enabled = True
            cont = 0
            Do While cont < dg_ubi.Rows.Count
                If dg_ubi.Rows(cont).Cells("Alm.").Value = "A01" Then
                    dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.Blue
                    dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.White
                End If
                If dg_ubi.Rows(cont).Cells("Alm.").Value = "A07" Then
                    dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                    dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
                End If
                If dg_ubi.Rows(cont).Cells("Alm.").Value = "A08" Then
                    dg_ubi.Rows(cont).DefaultCellStyle.BackColor = Color.LightPink
                    dg_ubi.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
                End If

                cont += 1
            Loop

            If fila_encontrada = False Then dg_ubi.CurrentCell = Nothing

        End If


    End Sub
    Public Sub cargar_lineas_registro(ByVal codarticulo As String, ByVal codlinea As Integer)
        Dim sql As String
        Dim vdatos As New DataTable
        Dim fila_encontrada As Boolean = False

        dg_procesadas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 12)
        dg_procesadas.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold)


        sql = "select codalmacen as 'Alm.',ubicacion as 'Ubicacion',tipo as 'Tipo',cantidad as 'Cant.' "
        If list_accion.Text = "Depositar" And lab_tipo.Text = "Genéricos" Then
            'sql &= ",case when ubicacion ='" & constantes.ImpDigital_A01_CONS & "' then 'A01 CONS' when ubicacion='" & constantes.ImpDigital_A01_UBI & "' then 'A01 UBI' when ubicacion='" & constantes.ImpDigital_A07_CONS & "' then 'A07 CONS' end as Palet "
            sql &= ",case when ubicacion='" & constantes.ImpDigital_A01_UBI & "' then 'A01 UBI' when ubicacion='" & constantes.ImpDigital_A07_CONS & "' then 'A07 CONS' end as Palet "
        End If
        sql &= "from ordenesfabricacion where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue.ToString.Trim & " and seralbaranprov='OF' and codalbaranprov=" & list_codorden.Text & " and codarticulo='" & codarticulo & "' "
        'Lo puse porque vi que hay albaranes que tienen la misma referencia en diferentes líneas. Por ejemplo en impresion digital, se usa la misma base para producir otras
        sql &= "and codlinea=" & codlinea & " "
        sql &= "order by codalmacen,ubicacion asc "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        dg_procesadas.DataSource = vdatos

        If vdatos.Rows.Count() > 0 Then
            'dg_ubi.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            dg_procesadas.Columns(0).Width = 50
            dg_procesadas.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dg_procesadas.Columns(1).Width = 85
            dg_procesadas.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_procesadas.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_procesadas.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_procesadas.Columns(2).Width = 60
            dg_procesadas.Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            dg_procesadas.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dg_procesadas.Columns(3).Width = 55
            If list_accion.Text = "Depositar" And lab_tipo.Text = "Genéricos" Then
                dg_procesadas.Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
                dg_procesadas.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                dg_procesadas.Columns(4).Width = 90
            End If

            'Pintar y habilitar
            btn_ok.Enabled = False
            dg_procesadas.Enabled = True


            pintar_lineas()

        End If


    End Sub

    Public Sub pintar_lineas()
        Dim cont As Integer

        cont = 0
        Do While cont < dg_procesadas.Rows.Count
            If dg_procesadas.Rows(cont).Cells("Alm.").Value = "A01" Then
                dg_procesadas.Rows(cont).DefaultCellStyle.BackColor = Color.Blue
                dg_procesadas.Rows(cont).DefaultCellStyle.ForeColor = Color.White
            End If
            If dg_procesadas.Rows(cont).Cells("Alm.").Value = "A07" Then
                dg_procesadas.Rows(cont).DefaultCellStyle.BackColor = Color.LightGreen
                dg_procesadas.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If
            If dg_procesadas.Rows(cont).Cells("Alm.").Value = "A08" Then
                dg_procesadas.Rows(cont).DefaultCellStyle.BackColor = Color.LightPink
                dg_procesadas.Rows(cont).DefaultCellStyle.ForeColor = Color.Black
            End If

            cont += 1
        Loop

    End Sub
    Private Sub txt_cantidad_Click(sender As System.Object, e As System.EventArgs) Handles txt_cantidad.Click
        txt_cantidad.SelectAll()
    End Sub

    Private Sub txt_cantidad_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_cantidad.KeyUp
        If e.KeyCode = Keys.Enter Then
            If IsNumeric(txt_cantidad.Text) Then btn_ok.Focus()
        End If
    End Sub

    Private Sub list_codorden_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles list_codorden.KeyUp
        If e.KeyCode = Keys.Enter Then
            seleccionar_orden()
        End If
    End Sub

    Private Sub dg_ubi_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg_ubi.CellClick
        If dg_ubi.SelectedRows(0).Cells("Alm.").Value = "A01" Then
            btn_ok.Enabled = True
            txt_cantidad.SelectAll()
            txt_cantidad.Focus()
        Else
            If MsgBox("Esta ubicación no es del A01, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                btn_ok.Enabled = True
                txt_cantidad.SelectAll()
                txt_cantidad.Focus()
            Else
                btn_ok.Enabled = False
                dg_ubi.CurrentCell = Nothing
            End If
        End If
    End Sub

    Private Sub list_accion_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_accion.DropDownClosed
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer
        Dim resultado As String

        If list_accion.SelectedIndex = -1 Then Exit Sub
        imagen.Image = Nothing
        'Revisar si la orden recogida recogida/depositada
        'No lo hago porque hay ordenes marcadas como recogidas o finalizadas y no lo están. Creo que finalizan cuando quieren salir en lugar de cerrar
        'Lo activo de nuevo porque no quiero que puedan finalizar de nuevo una orden ya finalizada. Ahora se hacen muchos mov de ajuste y es peligroso.
        'Tengo que dejar que las modifiquen , porque es la única forma que puedan finalizar parcialmente cuando ya han finalizado antes la recogida 
        'y quieren devolver a su sitio parte de lo recogido.


        sql = "select isnull(FinRecogida,0) as FinRecogida,isnull(FinDepositada,0) as FinDepositada  "
        sql &= "from GesAlbaranesProv where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codalbaranprov=" & list_codorden.Text
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

        'Antes de depositar una orden, esta debe estar previamente finalizada la recogida, sino no debe dejar empezar a depositar.
        'Esto lo pregunté a Domigo/sergio/javier y me dieron el ok.
        'Si hay una cantidad no recogida, ajustará el movimiento para que la cantidad original sea la que realmente han recogido y por tanto es la que quieren depositar.

        'Si es sergio o Domingo, dejar que puedan modificar una orden ya finalizada para devolver cosas a su sitio. Hay un campo en la tabla de operarios para permitir esto

        btn_ok.Enabled = False

        If list_accion.Text = "Recoger" Then
            lab_tit_lineas.Text = "Referencias a recoger:"
            If vdatos.Rows(0).Item("FinRecogida") = False Then
                btn_finalizar.BackColor = Color.Turquoise
                btn_finalizar.Enabled = True
                btn_BorrarLinReg.Enabled = True
                btn_ok.Enabled = True
            ElseIf (vdatos.Rows(0).Item("FinRecogida") = True And frm_principal.PermOrdProdModOFFinalizadas) Then
                MsgBox("Esta Orden ya ha sido Finalizada. No es necesario volver a finalizar, a no ser que se modifiquen cantidades. ")
                btn_finalizar.BackColor = Color.OrangeRed
                btn_finalizar.Enabled = True
                btn_BorrarLinReg.Enabled = True
                btn_ok.Enabled = True
            Else
                btn_finalizar.BackColor = Color.Turquoise
                btn_finalizar.Enabled = False
                btn_BorrarLinReg.Enabled = False
                btn_ok.Enabled = False
            End If
        End If

        If list_accion.Text = "Depositar" Then
            lab_tit_lineas.Text = "Referencias a depositar:"
            If vdatos.Rows(0).Item("FinDepositada") = True Then
                btn_finalizar.BackColor = Color.Turquoise
                btn_finalizar.Enabled = False
                btn_BorrarLinReg.Enabled = False
                btn_ok.Enabled = False
            Else
                If vdatos.Rows(0).Item("FinRecogida") = False Then
                    MsgBox("La orden está pendiente de recoger, por favor finaliza la recogida antes de depositar.")
                    list_accion.SelectedIndex = -1
                    dg_lineas.DataSource = Nothing
                    btn_finalizar.BackColor = Color.Turquoise
                    btn_finalizar.Enabled = False
                    btn_BorrarLinReg.Enabled = False
                    btn_ok.Enabled = False
                    Exit Sub
                Else
                    btn_finalizar.BackColor = Color.Turquoise
                    btn_finalizar.Enabled = True
                    btn_BorrarLinReg.Enabled = True
                    btn_ok.Enabled = True
                End If
            End If
        End If

        If list_accion.Text = "Imprimir" Then
            If lab_tipo.Text = "Genéricos" Or lab_tipo.Text = "Personalización" Then
                '29/09/2020 Comentado porque David me dijo q a veces no no finalizan la recogida ya que es parcial. De esta manera no podrían empezar los tiempos
                'If vdatos.Rows(0).Item("FinRecogida") = False Then
                '    MsgBox("Esta OF no ha sido recogida todavía, no se puede iniciar la impresión.")
                '    Exit Sub
                'Else
                If vdatos.Rows(0).Item("FinDepositada") = True Then
                    MsgBox("Esta OF ha sido ya depositada, no se puede iniciar la impresión.")
                    Exit Sub
                End If

                'Comprobar si el opeario está dentro de la sala, si no lo está no puede registrar impresiones

                'Comprobar si el opeario está dentro de la sala, si no lo está no puede registrar impresiones
                Dim preguntarSiEntrar As Boolean = False
                Dim vultaccion As New DataTable
                sql = "select top 1 IdAccion from AlmAccionesRegistro where idOperario =8 and convert(date,fecha)=convert(date,getdate()) and idAccion in (1,2) order by Fecha desc "
                vultaccion = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vultaccion.Rows.Count = 0 Then
                    preguntarSiEntrar = True
                Else
                    If vultaccion.Rows(0)(0) <> 1 Then preguntarSiEntrar = True
                End If

                If preguntarSiEntrar Then
                    If MsgBox("No tiene registrada la entrada en la sala, debe registrar la entrada para poder imprimir, quiere registrarla?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        sql = "insert into AlmAccionesRegistro (idAccion,idoperario,idmotivo,Aplicacion) "
                        sql &= "values (1," & frm_principal.lab_idoperario.Text & ",0,'Ubicaciones/Registrar Accion'); "
                        sql &= "update operarios_almacen set IdUltimaAccion=1,FechaUltimaAccion=getdate() where id=" & frm_principal.lab_idoperario.Text & " "
                        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
                    Else
                        Exit Sub
                    End If
                End If

                'End If
                Frm_ImpresionDigital.lab_CodPeriodoOF.Text = list_codperiodo.SelectedValue
                Frm_ImpresionDigital.lab_CodAlbaranOF.Text = list_codorden.Text
                Frm_ImpresionDigital.txt_cantImpEti.Text = txt_cantImpEti.Text
                Frm_ImpresionDigital.txt_UCaja.Text = txt_UCaja.Text
                Frm_ImpresionDigital.ShowDialog()
            Else
                MsgBox("Esta OF no es de impresión.")
                Exit Sub
            End If
            list_accion.SelectedIndex = -1
            dg_lineas.DataSource = Nothing
            dg_procesadas.DataSource = Nothing
            dg_ubi.DataSource = Nothing
            btn_finalizar.Enabled = False
            Exit Sub
        End If

        If list_accion.Text = "Recuperar" Then
            frm_Recuperar.list_referencia.Items.Clear()
            cont = 0
            Do While cont < Dg_producir.Rows.Count
                frm_Recuperar.list_referencia.Items.Add(Dg_producir.Rows(cont).Cells("Referencia").Value)
                cont += 1
            Loop
            frm_Recuperar.list_referencia.SelectedIndex = 0
            frm_Recuperar.txt_cantidad.Text = 0
            frm_Recuperar.ShowDialog()
            Exit Sub
        End If

        dg_lineas.Enabled = True
        txt_cantidad.Text = 0
        dg_ubi.DataSource = Nothing
        dg_procesadas.DataSource = Nothing
        btn_OtraUbi.Enabled = False
        cargar_lineas()
        btn_finalizar.Visible = True
    End Sub


    Private Sub btn_finalizar_Click(sender As System.Object, e As System.EventArgs) Handles btn_finalizar.Click
        Dim sql As String = ""
        Dim resultado As String
        Dim cont As Integer
        Dim sql_ajuste As String = ""
        Dim vdatos, valmacen, vped, vmov, vdep, vaux As New DataTable
        Dim almacen_usado As String = ""
        Dim cantidad_ajuste As Integer
        Dim nummov As Integer = 0
        Dim numrefmov As String = ""
        Dim contmov As Integer
        Dim cantidadtotalrecogida As Integer = 0
        Dim CantOrigDep As Integer

        sql_ajuste = ""

        If list_accion.Text = "Recoger" Then
            If MsgBox("Se va a finalizar la recogida, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                'Si hay alguna referencia recogida completamente, el resto de referencias deben estar ta,bien recogidas completamente.
                'Se daba el caso de que en un montaje recogian el trolley completamente y las chapas no. Esto no puede ser.
                Dim algunacompleta, algunaincompleta As Boolean
                cont = 0
                Do While cont < dg_lineas.Rows.Count
                    If dg_lineas.Rows(cont).Cells("Cant.").Value = dg_lineas.Rows(cont).Cells("Val.").Value Then algunacompleta = True
                    If dg_lineas.Rows(cont).Cells("Cant.").Value <> dg_lineas.Rows(cont).Cells("Val.").Value Then algunaincompleta = True
                    cont += 1
                Loop
                If algunacompleta And algunaincompleta Then
                    MsgBox("No se puede finalizar, ya que no puede haber unas referencias recogidas completamente y otras no.")
                    Exit Sub
                End If


                'Revisar si hay algo pendiente de recoger y avisar de que se modificará la orden para que la cantidad inicial a recoger sea la que hay ahora
                cont = 0
                Do While cont < dg_lineas.Rows.Count
                    cantidadtotalrecogida += dg_lineas.Rows(cont).Cells("Val.").Value
                    If dg_lineas.Rows(cont).Cells("Cant.").Value > dg_lineas.Rows(cont).Cells("Val.").Value Then

                        If MsgBox("En la referencia " & dg_lineas.Rows(cont).Cells("Ref.").Value & " ,la cantidad validada no coincide con la cantidad a recoger, la cantidad no recogida será eliminada de la orden y el stock devuelto a su origen, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                            Exit Sub
                        Else
                            'Regularizar OF

                            If dg_lineas.Rows(cont).Cells("Cant.").Value > dg_lineas.Rows(cont).Cells("Val.").Value Then
                                cantidad_ajuste = Math.Abs(dg_lineas.Rows(cont).Cells("Cant.").Value) - Math.Abs(dg_lineas.Rows(cont).Cells("Val.").Value)

                                'Cambiar la cantidad que estamos recogiendo pero también la cantidad de las referencias a depositar, ya que no puede quedar el albarán con una cantidad a recoger diferente de depositar. 

                                'Si es un desglose:
                                ' - obtener la línea de pedido de esta línea de albarán y buscar las lineas de albarán que pertenezcan a lineas de pedido que tengan en el campo tipo de linea este codlinea obtenido y modificar la cantidad
                                'Si es un montaje
                                ' - obtener las líneas de albaran que pertenezcan a lineas de pedido que tengan en "tipolinea" el código de lineapedido que tiene esta línea de albarán

                                sql = "select linalb.CodEmpresaPed,linalb.CodPeriodoPed,linped.SerPedidoProv,linalb.CodPedidoProv, linped.TipoLinea,linped.codlinea,linped.nummov,linped.numreferencia as 'NRP',linalb.numreferencia as 'NRA' "
                                sql &= "from GesAlbaranesProvLin linalb "
                                sql &= "inner join GesPedidosProvLin linped on linped.CodEmpresa =linalb.CodEmpresaPed And linped.CodPeriodo =linalb.CodPeriodoPed And linped.SerPedidoProv =linalb.SerPedidoProv "
                                sql &= "And linped.CodPedidoProv =linalb.CodPedidoProv And linped.CodLinea =linalb.LinPedido "
                                sql &= "where linalb.CodEmpresa =1 And linalb.CodPeriodo =" & list_codperiodo.SelectedValue & " And linalb.SerAlbaranProv ='OF' and linalb.CodAlbaranProv ='" & list_codorden.Text & "' "
                                sql &= "and linalb.CodLinea =" & dg_lineas.Rows(cont).Cells("Lin.").Value & " "
                                vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)

                                If vdatos.Rows.Count = 0 Then
                                    MsgBox("No se han encontrado datos suficientes para continuar con el proceso, avisar a informática.")
                                Else
                                    If vdatos.Rows(0).Item("tipolinea").ToString = "D" Then 'Desglose

                                        'Revisar si alguna de las líneas  a depositar que dependen de esta linea a recoger, ya ha sido depositada, en tal caso  no debe dejar recoger parcial
                                        'Lo hago aquí para aprovechar la consulta de antes
                                        sql = "select * from GesAlbaranesProvLin "
                                        sql &= "where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv ='OF' and CodAlbaranProv = " & list_codorden.Text & " "
                                        sql &= "and linpedido in (select codlinea from GesPedidosProvLin where codempresa='" & vdatos.Rows(0).Item("CodEmpresaPed") & "' and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and SerPedidoProv='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and CodPedidoProv=" & vdatos.Rows(0).Item("CodPedidoProv") & " and tipolinea='" & vdatos.Rows(0).Item("codlinea") & "') "
                                        sql &= "and CantidadValidada <> 0 "
                                        vdep = conexion.TablaxCmd(constantes.bd_inase, sql)
                                        If vdep.Rows.Count > 0 Then
                                            MsgBox("No se puede ajustar la cantidad no recogida porque existen líneas a depositar ya procesadas. Eliminar toda la cantidad depositada y luego finalice de nuevo la recogida.")
                                            Exit Sub
                                        End If


                                        'la linea que estamos modificando, la del set
                                        sql_ajuste &= "update GesAlbaranesProvLin set cantidad=" & Math.Abs(dg_lineas.Rows(cont).Cells("Val.").Value) * -1 & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Orig.:" & dg_lineas.Rows(cont).Cells("Cant.").Value & ".Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' "
                                        sql_ajuste &= "where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv ='OF' and CodAlbaranProv = " & list_codorden.Text & " "
                                        sql_ajuste &= "and codlinea=" & dg_lineas.Rows(cont).Cells("Lin.").Value & " "
                                        'Modificar la cantidad en el pedido de compras. Pedido por Domingo/Sergio en correo
                                        sql_ajuste &= "update GesPedidosProvLin set cantidad +=" & cantidad_ajuste & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' "  'No pongo en observaciones la cantidad original porque puede que no sea la misma que el albarán. Puede que el pedido sea de 10 y el albaran de 5, entonces la cantidad original del pedido no es la misma que la del albaran
                                        sql_ajuste &= "where codempresa='" & vdatos.Rows(0).Item("CodEmpresaPed") & "' and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and SerPedidoProv='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and CodPedidoProv=" & vdatos.Rows(0).Item("CodPedidoProv") & " and codlinea=" & vdatos.Rows(0).Item("codlinea") & " "

                                        'Modificar las lineas de los desgloses resultantes
                                        sql_ajuste &= "update GesAlbaranesProvLin set cantidad=" & Math.Abs(dg_lineas.Rows(cont).Cells("Val.").Value) & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Orig.:" & dg_lineas.Rows(cont).Cells("Cant.").Value & ".Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' "
                                        sql_ajuste &= "where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv ='OF' and CodAlbaranProv = " & list_codorden.Text & " "
                                        sql_ajuste &= "and linpedido in (select codlinea from GesPedidosProvLin where codempresa='" & vdatos.Rows(0).Item("CodEmpresaPed") & "' and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and SerPedidoProv='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and CodPedidoProv=" & vdatos.Rows(0).Item("CodPedidoProv") & " and tipolinea='" & vdatos.Rows(0).Item("codlinea") & "') "
                                        'Modificar la cantidad en el pedido de compras. Pedido por Domingo/Sergio en correo
                                        sql_ajuste &= "update GesPedidosProvLin set cantidad -=" & cantidad_ajuste & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' " 'No pongo en observaciones la cantidad original porque puede que no sea la misma que el albarán. Puede que el pedido sea de 10 y el albaran de 5, entonces la cantidad original del pedido no es la misma que la del albaran
                                        sql_ajuste &= "where codempresa='" & vdatos.Rows(0).Item("CodEmpresaPed") & "' and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and SerPedidoProv='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and CodPedidoProv=" & vdatos.Rows(0).Item("CodPedidoProv") & " and tipolinea='" & vdatos.Rows(0).Item("codlinea") & "' "


                                    End If
                                    If IsNumeric(vdatos.Rows(0).Item("tipolinea").ToString) Then 'Montaje

                                        'Modificar la línea de la referencia a recoger 
                                        sql_ajuste &= "update GesAlbaranesProvLin set cantidad=" & Math.Abs(dg_lineas.Rows(cont).Cells("Val.").Value) * -1 & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Orig.:" & dg_lineas.Rows(cont).Cells("Cant.").Value & ".Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' "
                                        sql_ajuste &= "where numreferencia='" & vdatos.Rows(0).Item("NRA").ToString & "' "
                                        'Modificar la cantidad en el pedido de compras. Pedido por Domingo/Sergio en correo
                                        sql_ajuste &= "update GesPedidosProvLin set cantidad +=" & cantidad_ajuste & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' " 'No pongo en observaciones la cantidad original porque puede que no sea la misma que el albarán. Puede que el pedido sea de 10 y el albaran de 5, entonces la cantidad original del pedido no es la misma que la del albaran
                                        sql_ajuste &= "where numreferencia='" & vdatos.Rows(0).Item("NRP").ToString & "' "

                                        'Modificar la linea del montaje resultante
                                        'La cantidad a producir siempre tiene q ser múltiplo de las 'UnidadesAProducir' del montaje.
                                        'Al igual que la cantidad a consumir debe ser multiplo de las 'UnidadesAConsumir', cosa que compruebo al validar al línea
                                        'Para saber la cantidad que se produce finalmente, divido la cantidad recogida por la cantidadaconsumir y en todas las líneas a consumir debe dar el mismo valor
                                        'Que será el que usaré para multimplicar por la 'cantidadaproducir' para saber la cantidad final a depositar.
                                        Dim multiplicador As Integer
                                        Dim cantidadadepositar As Integer
                                        Dim codpack As String

                                        sql = "select cantidad,articulo from GesPedidosProvLin "
                                        sql &= "where codempresa='" & vdatos.Rows(0).Item("CodEmpresaPed") & "' and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and SerPedidoProv='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and CodPedidoProv=" & vdatos.Rows(0).Item("CodPedidoProv") & " and codlinea=" & vdatos.Rows(0).Item("tipolinea") & " "
                                        vaux = conexion.TablaxCmd(constantes.bd_inase, sql)
                                        CantOrigDep = vaux.Rows(0).Item("cantidad")
                                        codpack = vaux.Rows(0).Item("articulo").ToString

                                        sql = "select UnidadesAConsumir ,UnidadesAProducir  from ArticulosPacksLin lin inner join ArticulosPacks cab on cab.CodPack =lin.CodPack "
                                        sql &= "where cab.CodPack ='" & codpack & "' "
                                        vaux = conexion.TablaxCmd(constantes.bd_intranet, sql)
                                        If vaux.Rows.Count > 0 Then
                                            multiplicador = Math.Abs(dg_lineas.Rows(cont).Cells("Val.").Value) / vaux.Rows(0).Item("UnidadesAConsumir")
                                            cantidadadepositar = multiplicador * vaux.Rows(0).Item("UnidadesAProducir")
                                            cantidad_ajuste = Math.Abs(dg_lineas.Rows(cont).Cells("Cant.").Value) - cantidadadepositar
                                        Else
                                            cantidad_ajuste = cantidad_ajuste
                                        End If

                                        sql_ajuste &= "update GesAlbaranesProvLin set cantidad=" & cantidadadepositar & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Orig.:" & dg_lineas.Rows(cont).Cells("Cant.").Value & ".Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' "
                                        sql_ajuste &= "where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv ='OF' and CodAlbaranProv = " & list_codorden.Text & " "
                                        sql_ajuste &= "and linpedido in (select codlinea from GesPedidosProvLin where codempresa='" & vdatos.Rows(0).Item("CodEmpresaPed") & "' and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and SerPedidoProv='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and CodPedidoProv=" & vdatos.Rows(0).Item("CodPedidoProv") & " and codlinea='" & vdatos.Rows(0).Item("tipolinea") & "' ) "
                                        'Modificar la cantidad en el pedido de compras. Pedido por Domingo/Sergio en correo
                                        'Obtener la cantidad anterior que hay en la linea a depositar.
                                        'Esta cantidad me hace falta porque aquí entrará tantas veces como referencias hacen falta para montar,
                                        'Antes restaba a lacantidad de la línea del montaje resultante, la cantidad a ajustar, pero me di cuenta de que 
                                        'si el montaje tenía 2 referencias , repetia dos veces la modificacion y por tanto estaba mal
                                        'Ahora acutalizo la cantiad y con un = , así aunque lo ejecute 2 veces , siempre pongo la misma cantidad , ya que se hace un transact de todo

                                        sql_ajuste &= "update GesPedidosProvLin set cantidad =" & CantOrigDep - (cantidad_ajuste) & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' " 'No pongo en observaciones la cantidad original porque puede que no sea la misma que el albarán. Puede que el pedido sea de 10 y el albaran de 5, entonces la cantidad original del pedido no es la misma que la del albaran
                                        sql_ajuste &= "where codempresa='" & vdatos.Rows(0).Item("CodEmpresaPed") & "' and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and SerPedidoProv='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and CodPedidoProv=" & vdatos.Rows(0).Item("CodPedidoProv") & " and codlinea=" & vdatos.Rows(0).Item("tipolinea") & " "
                                        'funciones.Enviar_correo("vicentesoler@joumma.com", "Montaje OF finalizado con cantidad preparada diferente a la original,revisar", sql_ajuste)
                                    End If

                                    'Si hay un movimiento asociado, hay que modificar el movimiento también
                                    If vdatos.Rows(0).Item("nummov") <> 0 Then
                                        'No elimino el movimiento, ya que así queda registardao en las observaciones que se modificó por ajuste en of
                                        sql_ajuste &= "update GesMovAlmLin set cantidad -=" & cantidad_ajuste & ",Observaciones='Cant.ajust. al regualrizar OF desde Ubicaciones-OF.Cant.Orig.:" & dg_lineas.Rows(cont).Cells("Cant.").Value & ".Cant.Val:" & dg_lineas.Rows(cont).Cells("Val.").Value & ".Cant.Ajuste:" & cantidad_ajuste & "' where codempresa=1 and nummov=" & vdatos.Rows(0).Item("nummov") & " "
                                    End If

                                End If
                            End If
                        End If
                    End If
                    cont += 1
                Loop


                'Marcar como finalizada y el operario que la finaliza
                'Si la cantidad total recogida es 0, como ya no hay que depositar nada, se marca como depositada para que desaparezca
                If cantidadtotalrecogida = 0 Then
                    sql_ajuste &= "update GesAlbaranesProv set estado='02', ultoperario=" & frm_principal.lab_idoperario.Text & ",FechaRecogida=getdate(),FinRecogida=1,OperarioFinRecogida=" & frm_principal.lab_idoperario.Text & ",FechaFinalizada=getdate(),FechaDepositada=getdate(),FinDepositada=1,OperarioFinDepositada=" & frm_principal.lab_idoperario.Text & " where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codAlbaranProv=" & list_codorden.Text & " "
                Else
                    sql_ajuste &= "update GesAlbaranesProv set estado='00', ultoperario=" & frm_principal.lab_idoperario.Text & ",FechaRecogida=getdate(),FinRecogida=1,OperarioFinRecogida=" & frm_principal.lab_idoperario.Text & " where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codAlbaranProv=" & list_codorden.Text & " "
                End If

                resultado = conexion.Executetransact(constantes.bd_inase, sql_ajuste)
                If resultado <> "0" Then
                    MsgBox("Error al guardar los datos.", MsgBoxStyle.Critical)
                Else
                    llenar_ordenes()
                    limpiar(True, "Finalizar")
                    Exit Sub
                End If

            Else
                Exit Sub
            End If
        End If

        If list_accion.Text = "Depositar" Then
            If MsgBox("Se va a finalizar completamente la orden, continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                sql_ajuste = ""
                contmov = 1
                sql = "select linalb.numreferencia,linalb.codlinea,linalb.articulo,isnull(linalb.cantidadvalidada,0) as cantidadvalidada,"
                sql &= "isnull(linalb.cantidad,0) as cantidad,linalb.almacen,linalb.CodEmpresaPed,linalb.CodPeriodoPed,linalb.SerPedidoProv,linalb.CodPedidoProv,linalb.linpedido, "
                sql &= "linped.TipoLinea,linped.codlinea,linped.nummov "
                sql &= "from GesAlbaranesProvlin linalb "
                sql &= "inner join GesPedidosProvLin linped on linped.CodEmpresa =linalb.CodEmpresaPed And linped.CodPeriodo =linalb.CodPeriodoPed And linped.SerPedidoProv =linalb.SerPedidoProv "
                sql &= "And linped.CodPedidoProv =linalb.CodPedidoProv And linped.CodLinea =linalb.LinPedido "
                sql &= "where linalb.codempresa=1 And linalb.codperiodo=" & list_codperiodo.SelectedValue & " And linalb.seralbaranprov='OF' and linalb.codAlbaranProv=" & list_codorden.Text & " "
                'Solo compruebo las líneas con cantidad >0 , que son las que se van a depositar, ya q estas son las que puede que no hayan depositado completamente y por tanto son
                'las que se hará movimiento a saldos.
                'Detecté un caso que se había hecho movimiento a saldo de una referencia a recoger que no habia sido recogida. Esto no debería pasar porq si no se recoge no deja finalizar
                'la recogida y por tanto no se puede finalizar la depositada. Pero creo q lo q hicieron es finalizar la recogida con todo completo y luego entrar a borrar la cantidad 
                'recogida, por tanto al entrar a finalizar, como anteriormente si que habían finalizado con todo recogido, les dejó finalizar e hizo movimiento a una referencia por recoger.
                sql &= "and isnull(linalb.cantidad,0) > 0 "
                vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                cont = 0
                Do While cont < vdatos.Rows.Count
                    'Comprobar si las líneas están recogidas/depositadas
                    If Math.Abs(vdatos.Rows(cont).Item("cantidad")) > Math.Abs(vdatos.Rows(cont).Item("cantidadvalidada")) Then
                        cantidad_ajuste = Math.Abs(vdatos.Rows(cont).Item("cantidad")) - Math.Abs(vdatos.Rows(cont).Item("cantidadvalidada"))

                        'Esto era antes, ahora el resto va a saldos. Pedido por Sergio/ Domingo. 
                        'Lo comenté con Sergio y prefiere que pregunte si quiere que vaya a saldos o que elimine la cantidad de la referencia original del albarán y pedido
                        'Pense en hacer que si se está depositando, y la cantidad depositada no es completa, preguntar si se quiere devolver la referencia original
                        'a su sitio, para eliminarla luego del albaran y del pedido y que quedara como si la orden incialmente era de esa cantidad, pero se complica
                        'La solucion es que la cantidad depositada la dejen a 0 y luego se metan en la recogida y eliminen la cantidad que no queiren , para luego depositar solo la cantidad 
                        'que quieren depositar


                        If MsgBox("La referencia " & vdatos.Rows(cont).Item("articulo") & " no está depositadas completamente, la cantidad restante se depositará en saldos. continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                            'Pongo en la casilla de cantidad depositada, el total de la cantidad a depositar, ya que el movimiento que hago ahora es para pasar la cantidad que no se ha depositado desde el A01/A07/A08 a saldos
                            vdatos.Rows(cont).Item("cantidadvalidada") = Math.Abs(vdatos.Rows(cont).Item("cantidad"))

                            'Pongo en la linea del albaran la cantidad validada, ya que debe quedar marcada como completa
                            sql_ajuste &= "update GesAlbaranesProvlin set cantidadvalidada = " & vdatos.Rows(cont).Item("cantidadvalidada") & " where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codAlbaranProv=" & list_codorden.Text & " "
                            sql_ajuste &= "and codlinea= " & vdatos.Rows(cont).Item("codlinea") & " "


                            'Movimiento a saldos
                            If nummov = 0 Then
                                funciones.crear_movimiento_inase(1, nummov, "Mov. para depostar OF en saldos", numrefmov)
                            End If
                            sql_ajuste &= "insert into GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,AlmacenOri,UbicacionOri,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                            sql_ajuste &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & nummov & "," & contmov & ",'" & vdatos.Rows(cont).Item("almacen") & "','XXX','S01','XXX','" & vdatos.Rows(cont).Item("articulo") & "'," & cantidad_ajuste & ",'Mov.ajuste cant.depos. <> cant recog. Resto a saldos. (OF:" & list_codperiodo.SelectedValue & "/" & list_codorden.Text & ".Cant.Orig.:" & vdatos.Rows(cont).Item("cantidad") & ".Cant.Val:" & vdatos.Rows(cont).Item("cantidadvalidada") & ".Cant.Ajuste:" & cantidad_ajuste & "') "

                            'Actualizar ubicación. 09-01-2020. Me di cuenta que al pasar a saldos la cantidad no depositada, no actualizaba la ubi.
                            'Añadir registro. Se hace antes de ejecutar el insert para poder obtener la cantidad anterior antes de que sea modificada
                            funciones.añadir_reg_mov_ubicaciones(vdatos.Rows(cont).Item("articulo"), cantidad_ajuste, "S01", constantes.saldos_A01, "OF:" & list_codorden.Text, "Ubicaciones-Orden Prod", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
                            'Actualizo el stock en la ubicacion de saldos 00-00-00
                            sql_ajuste &= "update intranet.dbo.ubicaciones set cantidad=cantidad+" & cantidad_ajuste & " "
                            sql_ajuste &= "where codalmacen='S01' and ubicacion='" & constantes.saldos_A01 & "' and codarticulo='" & vdatos.Rows(cont).Item("articulo") & "' "


                            contmov += 1
                        Else
                            MsgBox("El proceso ha sido cancelado. La orden no será finalizada.")
                            txt_cantidad.Text = 0
                            dg_ubi.DataSource = Nothing
                            dg_procesadas.DataSource = Nothing
                            Exit Sub
                        End If
                    End If

                    cont += 1
                Loop

                'Seleccionar la máquina
                'Solo si es pintura
                If lab_tipo.Text = "Genéricos" Or lab_tipo.Text = "Personalización" Then
                    frm_SelMaqImpresionDigital.list_impresoras.SelectedIndex = -1
                    frm_SelMaqImpresionDigital.ShowDialog()
                End If


                'Marcar como finalizada y el operario que la finaliza
                sql_ajuste &= "update GesAlbaranesProv set Recurso='" & frm_SelMaqImpresionDigital.list_impresoras.Text & "', estado='02', ultoperario=" & frm_principal.lab_idoperario.Text & ",FechaFinalizada=getdate(),FechaDepositada=getdate(),FinDepositada=1,OperarioFinDepositada=" & frm_principal.lab_idoperario.Text & " where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codAlbaranProv=" & list_codorden.Text & " "

                Try
                    'Si hay una OF (en pedido todavía) y depende de esta, se debe marcar en el planificador para que se genere el albarán
                    sql = "select id from PlanificadorOF where idDepende = (select id from PlanificadorOF where CodEmpresaAlbaran =1 and CodPeriodoAlbaran =2020 and SerAlbaran ='OF' and CodAlbaran =" & list_codorden.Text & ") "
                    sql &= "and codalbaran=0 " 'Añado que el albaran sea 0, por si ya se ha generado, aunque no debería...
                    vaux = conexion.TablaxCmd(constantes.bd_intranet, sql)
                    If vaux.Rows.Count = 1 Then
                        sql_ajuste &= "update PlanificadorOF set GenerarAlbaran=1 where id=" & vaux.Rows(0).Item("id") & " "
                    End If
                Catch ex As Exception
                    funciones.Enviar_correo("vicentesoler@joumma.com", "Error al marcar generar albarn en planificador OF al finalizar la OF", ex.ToString & " SQL:" & sql)
                End Try

                resultado = conexion.Executetransact(constantes.bd_inase, sql_ajuste)
                If resultado <> "0" Then
                    MsgBox("Error al guardar los datos.", MsgBoxStyle.Critical)
                Else
                    llenar_ordenes()
                    limpiar(True, "Finalizar")
                    Exit Sub
                End If

            Else
                Exit Sub
            End If
        End If


    End Sub

    Public Sub Guardar_datos_tiempo(Optional ByVal ObsGuardarTiempos As String = "")
        Dim TiempoEmpleado As Integer
        Dim sql As String
        Try
            TiempoEmpleado = DateDiff(DateInterval.Second, fecha_inicio, fecha_fin)
            sql = "insert into albaranes_tiempos (codempresa,codperiodo,seralbaran,codalbaran,idoperario,accion,FechaInicio,FechaFin,tiempo,importe,Observaciones) "
            sql &= "values(1," & list_codperiodo.SelectedValue.ToString.Trim & ",'OF'," & list_codorden.Text & "," & frm_principal.lab_idoperario.Text & ",'OF','" & fecha_inicio & "','" & fecha_fin & "'," & TiempoEmpleado & ",0,'" & ObsGuardarTiempos & "' ) "
            conexion.ExecuteCmd(constantes.bd_intranet, sql)
        Catch ex As Exception

        End Try

    End Sub



    Private Sub btn_salirorden_Click(sender As System.Object, e As System.EventArgs) Handles btn_salirorden.Click
        Dim sql
        If list_codorden.Text.ToString <> "" And estado = "01" Then
            sql = "update GesAlbaranesProv set estado='00' where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue.ToString.Trim & " and SerAlbaranProv='OF' and codAlbaranProv=" & list_codorden.Text & " "
            conexion.ExecuteCmd(constantes.bd_inase, sql)
        End If
        limpiar(True, "Salir")
        llenar_ordenes()
    End Sub


    Private Sub btn_limpiar_Click(sender As System.Object, e As System.EventArgs) Handles btn_limpiar.Click
        txt_filtro_codarticulo.Text = ""
        cb_FiltroPorDepositar.Checked = False
        cb_FiltroPorRecoger.Checked = False
        cb_FiltroStockA01.Checked = False
        cb_FiltroStockA07.Checked = False
        list_filtro_prioridad.SelectedIndex = 0
        list_filtro_tipo.SelectedIndex = -1
        llenar_ordenes()
        limpiar(False)
    End Sub


    Public Sub mostrar_fotos(fila)
        Try
            imagen.ImageLocation = "\\jserver\BAJA\" & dg_lineas.Rows(fila).Cells("Ref.").Value & ".jpg"
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btn_BorrarLinReg_Click(sender As System.Object, e As System.EventArgs) Handles btn_BorrarLinReg.Click
        Dim sql, ssel As String
        Dim resultado As String
        Dim vcant, vdatos As New DataTable
        Dim codarticulo As String
        Dim tipo As String
        Dim ubicacion As String
        Dim nummov As Integer
        Dim numrefmov As String

        sql = ""

        'Si estamos preparando, como el stock ya está descontado al hacerse el albarán, no tenemos que tocar nada del stock, solo en ubicacion . 
        'Esto está por arreglar, ya que el stock debería descontarse al hacer la preapración , pero de momento trabajamos así.
        'Si estamos depositando, el stock si que lo actualizamos al depositar, pero lo que se hace es cambiar el almacén del albaran o del movimiento.
        'Lo que significa que si hemos depositado todo en una ubicacion si que podríamos devolver al sitio lo que eliminamos , ya que cambiando el almacén 
        'del albaran o del mov. ya estaria pero si tenemos depositado en dos ubicaciones , y se elimina una , no podemos devolver parte, ya que todo está en la misma línea.
        'Esto ya no se puede dar, ya que ahora evito que se pueda recoger/depositar de dos ubicaciones diferentes. Lo quité porque al marcar el ok, estamos confirmando el stock de toda la linea, ya que lo 
        'se hace es cambiar el almacen en la linea del mov o en el albaran y como toda la cantidad está en un sitio, no se puede confirmar cantidades parciales.
        'Por tanto , si eliminamos una preparacion, cambiamos de nuevo el almacén y lo dejamos en el P1. 


        If dg_procesadas.SelectedRows.Count = 0 Then
            MsgBox("Seleccionar una línea a eliminar.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If MsgBox("Seguro que quiere eliminar la cantidad preparada?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

        codarticulo = dg_lineas.SelectedRows(0).Cells("Ref.").Value.ToString.Trim.ToUpper

        'Mejora:al intentar borrar una línea primero miro si alguna de las que dependen en la recogida, está ya recogida. Esto puede pasar porque hay usuarios que tienen 
        'permiso para modificar recogidas ya finalizadas, con lo que podría estar ya depositadas parcialmente. 
        If list_accion.Text = "Recoger" Then
            'Por hacer
        End If


        'Antes que nada mirar si la ubicacion existe, sino crearla
        'Esto era antes, comenté con Domingo/Javier y si no esxiste, se tira a consolidado 

        tipo = dg_procesadas.SelectedRows(0).Cells("tipo").Value

        ssel = "select * from ubicaciones where codalmacen='" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & dg_procesadas.SelectedRows(0).Cells("Ubicacion").Value & "' and codarticulo='" & codarticulo & "' "
        'El campo tipo lo cree en la tabla OrdenesFabricacion despues de que ya se estuvieran usando las OF, por tanto habian muchas ordenes que no tienen este dato
        'Por tanto si busco por tipo, no me devolveria nada, ya que las ordenes antiguas estan en blanco
        If tipo <> "" Then ssel &= "and tipo='" & tipo & "'"
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, ssel)

        If vdatos.Rows.Count = 0 Then
            'Esto era antes, comenté con Domingo/Javier y si no esxiste, se tira a consolidado 
            'sql &= "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
            'sql &= "values ('" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "','" & codarticulo & "','" & dg_procesadas.SelectedRows(0).Cells("Ubicacion").Value & "','" & tipo & "'" & ",0,0,'',0,'Ubicaciones-OF','" & frm_principal.lab_nomoperario.Text & "')"
            'resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            If dg_procesadas.SelectedRows(0).Cells("Alm.").Value = "A01" Then ubicacion = constantes.consolidacion_A01
            If dg_procesadas.SelectedRows(0).Cells("Alm.").Value = "A07" Then ubicacion = constantes.consolidacion_A07
            If dg_procesadas.SelectedRows(0).Cells("Alm.").Value = "A08" Then ubicacion = constantes.consolidacion_A07
            If tipo = "" Then tipo = "Picking"
            If ubicacion = "" Then ubicacion = constantes.consolidacion_A01 'Por si el almacén no es ningino de los anteriores.
            sql &= "insert into ubicaciones (codalmacen,codarticulo,ubicacion,tipo,tamaño,cantidad,subaltura,cantmax,observaciones,ultusuario) "
            sql &= "values ('" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "','" & codarticulo & "','" & ubicacion & "','" & tipo & "'" & ",0,0,'',0,'Ubicaciones-OF','" & frm_principal.lab_nomoperario.Text & "')"
            'resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
            'No inserto enseguida porque detecté casos en los que se borraba la ubicación al tener como cantidad =0, ya que hay sitios donde borro las ubicaciones a 0 y coincidia 

            MsgBox("La ubicación de donde se recogió la mercancá ya no existe, se usará la ubicación de consolidación", MsgBoxStyle.Information)
        Else
            ubicacion = dg_procesadas.SelectedRows(0).Cells("Ubicacion").Value
        End If



        'Obtener la cantidad que tiene hasta el momento la linea para sumarla al stock y dejar el stock en el picking cuadrado
        If list_accion.Text = "Recoger" Then
            'Dejar stock como estaba en la ubicacion 
            sql &= "update ubicaciones set cantidad=cantidad+" & dg_procesadas.SelectedRows(0).Cells("Cant.").Value & " "
            sql &= "where codalmacen='" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' "
            'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
            sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' order by cantidad asc ) "
        End If

        If list_accion.Text = "Depositar" Then
            'Dejar stock como estaba en la ubicacion 
            sql &= "update ubicaciones set cantidad=cantidad-" & dg_procesadas.SelectedRows(0).Cells("Cant.").Value & " "
            sql &= "where codalmacen='" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' "
            'Esto es preciso, si no actualiza todos los palets de la misma ubicacion, articulo, almacen. Solo tiene q actualizar uno
            sql &= "and id = (select top 1 id from ubicaciones where codalmacen='" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "' and ubicacion='" & ubicacion & "' and codarticulo='" & codarticulo & "' order by cantidad asc ) "
        End If



        'Cambiar el almacen y poner P1 en la linea. Solo en depositar ya que en recoger el stock ya ha cambiado al generar el albarán. Pendiente de mejorar

        'Si es un desglose :
        '       -Las ref. desglosadas (destino) tienen el almacen en la línea de pedido/albaran. Aqui hay q poner de nuevo el P1
        'Si es un montaje
        '       -La referencia montada (final) tiene el almacen en la linea de pedido/albaran. Aqui hay q poner de nuevo el P1


        'Consulta para obtener algunos datos que me hacen falta luego
        ssel = "select numreferencia,codlinea,articulo,isnull(cantidadvalidada,0) as cantidadvalidada,isnull(cantidad,0) as cantidad,almacen,CodEmpresaPed,CodPeriodoPed,SerPedidoProv,CodPedidoProv,linpedido "
        ssel &= "from GesAlbaranesProvlin where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codAlbaranProv=" & list_codorden.Text & " and codlinea=" & dg_lineas.SelectedRows(0).Cells("Lin.").Value
        vdatos = conexion.TablaxCmd(constantes.bd_inase, ssel)

        'Buscar el pedidocompra
        Dim vped, vmov As New DataTable
        ssel = "select nummov,articulo,almacen from GesPedidosProvLin where codempresa=" & vdatos.Rows(0).Item("CodEmpresaPed") & " and codperiodo=" & vdatos.Rows(0).Item("CodPeriodoPed") & " and serpedidoprov='" & vdatos.Rows(0).Item("SerPedidoProv") & "' and codpedidoprov=" & vdatos.Rows(0).Item("CodPedidoProv") & " and codlinea= " & vdatos.Rows(0).Item("linpedido") & " "
        vped = conexion.TablaxCmd(constantes.bd_inase, ssel)

        If vped.Rows(0).Item("nummov") <> 0 Then
            'Buscar en el movimiento
            ssel = "select nummov,codlinea,AlmacenOri from gesmovalmlin where codempresa=1 and nummov=" & vped.Rows(0).Item("nummov") & " and articulo='" & vped.Rows(0).Item("articulo") & "' "
            vmov = conexion.TablaxCmd(constantes.bd_inase, ssel)
            If dg_procesadas.SelectedRows(0).Cells("Alm.").Value <> vmov.Rows(0).Item("AlmacenOri") Then
                funciones.crear_movimiento_inase(1, nummov, "Mov.creado al eliminar cant. recogida en " & list_codperiodo.SelectedValue & "/OF/" & list_codorden.Text, numrefmov)
                sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,AlmacenOri,UbicacionOri,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
                sql &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & nummov & ",1,'" & vmov.Rows(0).Item("AlmacenOri") & "','XXX','" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "','XXX','" & codarticulo & "'," & dg_procesadas.SelectedRows(0).Cells("Cant.").Value & ",'Mov. causado al eliminar cant. recogida. OF:" & list_codperiodo.SelectedValue & "/" & list_codorden.Text & ".') "
            End If
        Else
            'Antes del 08/05/2019. Ahora hago un movimiento de la cantidad eliminada
            'sql &= "update inase.dbo.GesAlbaranesProvlin set ultmodificacion=getdate(), Almacen ='P1' where numreferencia='" & vdatos.Rows(0).Item("numreferencia").ToString & "' "
            funciones.crear_movimiento_inase(1, nummov, "Mov.creado al eliminar cant. depositada en " & list_codperiodo.SelectedValue & "/OF/" & list_codorden.Text, numrefmov)
            sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,AlmacenOri,UbicacionOri,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
            sql &= "values('" & numrefmov & "',0,1,'" & Now.Year & "'," & nummov & ",1,'" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "','XXX','P1','XXX','" & codarticulo & "'," & dg_procesadas.SelectedRows(0).Cells("Cant.").Value & ",'Mov. causado al eliminar cant. depositada. OF:" & list_codperiodo.SelectedValue & "/" & list_codorden.Text & ".') "
        End If

        'Actualizar la linea del albaran
        sql &= "update inase.dbo.gesalbaranesprovlin set CantidadValidada=CantidadValidada - " & dg_procesadas.SelectedRows(0).Cells("Cant.").Value & " where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv='OF' and CodAlbaranProv=" & list_codorden.Text & " and CodLinea='" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & "' "

        'Borro registro intranet
        sql &= "delete OrdenesFabricacion where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and SerAlbaranProv='OF' and CodAlbaranProv=" & list_codorden.Text & " and CodLinea='" & dg_lineas.SelectedRows(0).Cells("Lin.").Value & "' and CodAlmacen='" & dg_procesadas.SelectedRows(0).Cells("Alm.").Value & "' and Ubicacion='" & dg_procesadas.SelectedRows(0).Cells("Ubicacion").Value & "' "

        'Añadir registro. Se hace antes de ejecutar el insert para poder obtener la cantidad anterior antes de que sea modificada
        If list_accion.Text = "Recoger" Then funciones.añadir_reg_mov_ubicaciones(codarticulo, (CInt(dg_procesadas.SelectedRows(0).Cells("Cant.").Value)), dg_procesadas.SelectedRows(0).Cells("Alm.").Value, dg_procesadas.SelectedRows(0).Cells("Ubicacion").Value, "OF:" & list_codorden.Text, "Ubicaciones-Orden Prod", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)
        If list_accion.Text = "Depositar" Then funciones.añadir_reg_mov_ubicaciones(codarticulo, CInt(dg_procesadas.SelectedRows(0).Cells("Cant.").Value) * -1, dg_procesadas.SelectedRows(0).Cells("Alm.").Value, dg_procesadas.SelectedRows(0).Cells("Ubicacion").Value, "OF:" & list_codorden.Text, "Ubicaciones-Orden Prod", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        resultado = conexion.Executetransact(constantes.bd_intranet, sql)
        If resultado <> "0" Then
            MsgBox("Se produjo un error, avisar a Informática. Err 20180116", MsgBoxStyle.Exclamation)
        End If

        'Actualizar grid lineas.
        dg_lineas.SelectedRows(0).Cells("Val.").Value -= dg_procesadas.SelectedRows(0).Cells("Cant.").Value

        Select Case dg_lineas.SelectedRows(0).Cells("Val.").Value
            Case Is = 0 : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Case Is = dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Green
            Case Is > dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Red
            Case Is < dg_lineas.SelectedRows(0).Cells("Cant.").Value : dg_lineas.SelectedRows(0).DefaultCellStyle.BackColor = Color.Yellow
        End Select

        'Llenar de nuevo ubicaciones para actualizar las cantidades por ubicacion.
        cargar_lineas_ubicaciones(codarticulo)


        'Actualizar datos grid procesadas
        cargar_lineas_registro(codarticulo, dg_lineas.SelectedRows(0).Cells("Lin.").Value)

        pintar_lineas()


    End Sub

    Private Sub dg_lineas_Sorted(sender As System.Object, e As System.EventArgs) Handles dg_lineas.Sorted
        pintar_lineas()
    End Sub

    Private Sub list_codperiodo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles list_codperiodo.SelectedIndexChanged
        llenar_ordenes()
    End Sub

    'Private Sub list_filtro_almacen_DropDownClosed(sender As Object, e As EventArgs) Handles list_filtro_almacen.DropDownClosed
    '    Dim sql
    '    If list_codorden.Text.ToString <> "" Then
    '        sql = "update GesAlbaranesProv set estado='02', ultoperario=" & frm_principal.lab_idoperario.Text & " where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue & " and seralbaranprov='OF' and codAlbaranProv=" & list_codorden.Text & " "
    '        conexion.ExecuteCmd(constantes.bd_inase, sql)
    '    End If
    '    llenar_ordenes()
    '    limpiar(False)
    'End Sub

    Private Sub txt_filtro_codarticulo_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_filtro_codarticulo.KeyUp
        If e.KeyCode = Keys.Enter Then
            If txt_filtro_codarticulo.Text.Length = 13 Then funciones.buscar_descripcion(txt_filtro_codarticulo.Text) 'Guarda en txt el codigo del articulo
            If txt_filtro_codarticulo.Text.ToString.Length = 7 Then
                llenar_ordenes()
                limpiar(False)
            End If
        End If
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click
        btn_finalizar.Enabled = True
    End Sub

    Private Sub Label2_DoubleClick(sender As Object, e As EventArgs) Handles Label2.DoubleClick
        If frm_principal.lab_idoperario.Text = 8 Then btn_finalizar.Enabled = True
    End Sub

    Private Sub cb_FiltroStockA01_Click(sender As Object, e As EventArgs) Handles cb_FiltroStockA01.Click
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub cb_FiltroStockA07_Click(sender As Object, e As EventArgs) Handles cb_FiltroStockA07.Click
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub cb_FiltroPorRecoger_Click(sender As Object, e As EventArgs) Handles cb_FiltroPorRecoger.Click
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub cb_FiltroPorDepositar_Click(sender As Object, e As EventArgs) Handles cb_FiltroPorDepositar.Click
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub list_filtro_tipo_DropDownClosed(sender As Object, e As EventArgs) Handles list_filtro_tipo.DropDownClosed
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub cb_FiltroB2B_Click(sender As Object, e As EventArgs) Handles cb_FiltroB2BWEB.Click
        llenar_ordenes()
        limpiar(False)
    End Sub

    'Private Sub Cb_FiltroPlanificador_Click(sender As Object, e As EventArgs)
    '    llenar_ordenes()
    '    limpiar(False)
    'End Sub

    Private Sub btn_OtraUbi_Click(sender As Object, e As EventArgs) Handles btn_OtraUbi.Click
        frm_nuevaubicacion.lab_ref.Text = dg_lineas.SelectedRows(0).Cells("Ref.").Value
        frm_nuevaubicacion.ShowDialog()
        cargar_lineas_ubicaciones(dg_lineas.SelectedRows(0).Cells("Ref.").Value)
    End Sub

    Private Sub cb_FiltroPrioridadAlta_Click(sender As Object, e As EventArgs)
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub list_filtro_prioridad_DropDownClosed(sender As Object, e As EventArgs) Handles list_filtro_prioridad.DropDownClosed
        llenar_ordenes()
        limpiar(False)
    End Sub

    Private Sub btn_SubirPalet_Click(sender As Object, e As EventArgs) Handles btn_SubirPalet.Click
        frm_ReaprovDesdePaletImpDigital.ShowDialog()
    End Sub

    Private Sub btn_ImprimirEti_Click(sender As Object, e As EventArgs) Handles btn_ImprimirEti.Click
        ImprimirEtiquetas()
    End Sub
    Public Sub ImprimirEtiquetas()
        Dim cont, cont2 As Integer
        Dim sql As String
        Dim vdatos As New DataTable

        Try
            'System.Diagnostics.Process.Start("http://jserver/intranet")

            Dim url As String = "http://jserver/intranet/privado/compras/EtiImpDigi.aspx?Auto=SI&usermanual=" & frm_principal.lab_nomoperario.Text & "&codempresa=1&codperiodo=" & list_codperiodo.SelectedValue & "&serie=OF&CodAlbaran=" & list_codorden.Text & "&Cantidad=" & txt_cantImpEti.Text & "&UCaja=" & txt_UCaja.Text & "&impresora=" & globales.etiquetadoras
            'Dim url As String = "http://atlantis/intranet/privado/compras/EtiImpDigi.aspx?Auto=SI&usermanual=" & frm_principal.lab_nomoperario.Text & "&codempresa=1&codperiodo=" & list_codperiodo.SelectedValue & "&serie=OF&CodAlbaran=" & list_codorden.Text & "&UCaja=" & txt_UCaja.Text & "&impresora=" & globales.etiquetadoras
            Dim wb As New WebBrowser
            wb.Url = New Uri(url)

            '18/12/2020 - Chelo pidió que si lo que se produce es un set, Se deben imprimir las etiquetas de los componentes del set
            If lab_tipo.Text = "Genéricos" Then
                cont = 0
                Do While cont < Dg_producir.Rows.Count
                    If Dg_producir.Rows(cont).Cells("Descripción").Value.ToString.ToUpper.Contains("SET") Then
                        'Buscar los componentes del set
                        sql = "select lin.articulo as Codarticulo from ProFTProductosLin lin where  CodFTOperacion = '" & Dg_producir.Rows(cont).Cells("Referencia").Value & "-S'"
                        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                        cont2 = 0
                        Do While cont2 < vdatos.Rows.Count
                            'Espero 4 segundos, sino la primera etiqueta no sale.
                            System.Threading.Thread.Sleep(4000)
                            url = "http://jserver/intranet/privado/compras/EtiImpDigi.aspx?Auto=SI&usermanual=" & frm_principal.lab_nomoperario.Text & "&CodEmpresa=1&CodArticulo=" & vdatos.Rows(cont2).Item("CodArticulo") & "&Cantidad=" & txt_cantImpEti.Text & "&NoEtiCaja=1&impresora=" & globales.etiquetadoras
                            'url = "http://atlantis/intranet/privado/compras/EtiImpDigi.aspx?Auto=SI&usermanual=" & frm_principal.lab_nomoperario.Text & "&CodEmpresa=1&CodArticulo=" & vdatos.Rows(cont2).Item("CodArticulo") & "&Cantidad=" & Dg_producir.Rows(cont).Cells("Cantidad").Value & "&NoEtiCaja=1&impresora=" & globales.etiquetadoras
                            wb.Url = New Uri(url)
                            cont2 += 1
                        Loop
                    End If
                    cont += 1
                Loop

            End If
        Catch ex As Exception
            MsgBox("Se produjo un error.")
        End Try
    End Sub

    Private Sub frm_OrdenProduccion_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        LiberarOF()
    End Sub
    Public Sub LiberarOF()
        Dim sql
        If list_codorden.Text.ToString <> "" Then
            sql = "update GesAlbaranesProv set estado='00' where codempresa=1 and codperiodo=" & list_codperiodo.SelectedValue.ToString.Trim & " and SerAlbaranProv='OF' "
            'Tiene que ser la propiedad text, ya que selectedvalue ha perdido el dato
            sql &= "and codAlbaranProv=" & list_codorden.Text & " "
            'Solo liberar si tiene estado 01 que es abierta. si esta finalizada, no liberaremos
            sql &= "and estado ='01' "
            conexion.ExecuteCmd(constantes.bd_inase, sql)
        End If

    End Sub
    Private Sub dg_lineas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dg_lineas.CellClick
        If e.RowIndex = -1 Then Exit Sub

        dg_ubi.DataSource = Nothing
        dg_procesadas.DataSource = Nothing

        cargar_lineas_ubicaciones(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value)
        cargar_lineas_registro(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, dg_lineas.Rows(e.RowIndex).Cells("Lin.").Value)

        txt_cantidad.Text = dg_lineas.Rows(e.RowIndex).Cells("Cant.").Value - dg_lineas.Rows(e.RowIndex).Cells("Val.").Value

        mostrar_fotos(e.RowIndex)
        labStockA01.Text = funciones.stock(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", "A01")
        labStockA07.Text = funciones.stock(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", "A07")
        labStockA08.Text = funciones.stock(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", "A08")

        LabPdteServ.Text = funciones.pendiente_servir(dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value, "TODAS", True)
        LabPdtePrep.Text = funciones.pendiente_preparar("TODAS", "TODOS", dg_lineas.Rows(e.RowIndex).Cells("Ref.").Value)

        btn_OtraUbi.Enabled = True
    End Sub


End Class