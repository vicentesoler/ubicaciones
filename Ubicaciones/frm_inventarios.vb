﻿Public Class frm_inventarios
    Dim conexion As New Conector
    Dim codempresa_mov, codperiodo_mov, nummov As Integer
    Dim numref_mov As String

    Private Sub frm_inventarios_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        limpiar()
        llenar_inventarios()
        txt_articulo.Focus()
        funciones.llenar_almacenes(list_almacen)
        p_añadir.Enabled = False
    End Sub

    Public Sub llenar_inventarios()
        Dim sql As String
        Dim vdatos As New DataTable
        Dim cont As Integer

        sql = "select max(codinventario) as codinventario from inventarios where codalmacen='A01' "
        sql &= "union select max(codinventario) as codinventario from inventarios where codalmacen='A07' "
        sql &= "union select max(codinventario) as codinventario from inventarios where codalmacen='A08' "
        sql &="order by codinventario "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        list_inventarios.DisplayMember = "codinventario"
        list_inventarios.ValueMember = "codinventario"
        list_inventarios.DataSource = vdatos
        list_inventarios.SelectedIndex = -1

    End Sub
    Public Sub limpiar()
        txt_articulo.Text = ""
        lab_pickingact.Text = ""
        lab_pulmonact.Text = ""
        lab_stockact_tot.Text = ""
        lab_stockact.Text = ""
        lab_cantnoprep.Text = ""
        lab_canttotal_despues.Text = ""
        lab_almpick.Text = ""
        lab_ubipick.Text = ""
        lab_pickingact_ubi.Text = ""
        'btn_finalizar.Enabled = False
        dg_lineas_inventario.DataSource = Nothing
        dg_lineas_inventario.Rows.Clear()
        'dg_lineas_alb_no_prep.DataSource = Nothing
        'dg_lineas_alb_no_prep.Rows.Clear()
        imagen.ImageLocation = ""
        codempresa_mov = 0
        codperiodo_mov = 0
        nummov = 0
        numref_mov = ""
        list_ubi.DataSource = Nothing
        btn_añadir.Enabled = False
        txt_cantidadpick.Text = ""
    End Sub

    Private Sub btn_nuevo_Click(sender As System.Object, e As System.EventArgs) Handles btn_nuevo.Click
        Dim sql As String
        Dim vdatos As New DataTable
        Dim resultado As String
        Dim nummov As Integer = 0
        Dim numrefmov As String = ""
        Dim inventario As Integer

        If frm_principal.PermInventariosNuevo = False Then
            MsgBox("No está permitido generar un nuevo inventario, contactar con el encargado", MsgBoxStyle.Information)
            Exit Sub
        End If

        If MsgBox("Se va a crear un nuevo Inventario, seguro que desea continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If

        sql = "insert into inventarios(codalmacen,idoperario,fechaalta,fechaultmodificacion) values('" & list_almacen.SelectedValue & "','" & frm_principal.lab_idoperario.Text & "',getdate(),getdate()) "
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)

        'Obtener el codinventario 
        sql = "select isnull(max(codinventario),0) from inventarios "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        inventario = vdatos.Rows(0)(0)

        'Crear movimiento inase
        funciones.crear_movimiento_inase(1, nummov, "Movimiento generado por el operario " & frm_principal.lab_nomoperario.Text & " para el inventario num. " & inventario, numrefmov)
        If numrefmov = "" Then
            MsgBox("Error creando cabecera movimiento", MsgBoxStyle.Information)
            Exit Sub
        Else
            sql = "update inventarios set nummov=" & nummov & ",codperiodo_mov=" & Now.Year & ",codempresa_mov=1,numref_mov='" & numrefmov.ToString & "' where codinventario=" & inventario
            resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)
        End If


        limpiar()
        llenar_inventarios()
    End Sub


    Private Sub txt_articulo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_articulo.KeyUp
        Dim sql As String
        Dim vdatos As New DataTable

        If txt_articulo.Text.Trim = "" Then
            btn_añadir.Enabled = False
        End If

        btn_añadir.Enabled = False
        'dg_lineas_alb_no_prep.DataSource = Nothing
        'dg_lineas_alb_no_prep.Rows.Clear()

        txt_articulo.Text = txt_articulo.Text.Trim
        lab_pickingact.Text = ""
        lab_pulmonact.Text = ""
        lab_stockact_tot.Text = ""
        lab_stockact.Text = ""
        lab_almpick.Text = ""
        lab_ubipick.Text = ""
        lab_otrasact.Text = ""
        lab_pickingact_ubi.Text = ""
        lab_canttotal_despues.Text = ""

        If e.KeyValue = Keys.Enter Then
            txt_articulo.Text = txt_articulo.Text.ToUpper
            If txt_articulo.Text.Length = 13 Then
                sql = "select codarticulo from gesarticulos art "
                sql &= "where art.codempresa=1 "
                sql &= "and art.unidventaean='" & txt_articulo.Text.Trim & "' "
                vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                If vdatos.Rows.Count = 1 Then
                    txt_articulo.Text = vdatos.Rows(0)(0).ToString
                ElseIf vdatos.Rows.Count > 1 Then
                    MsgBox("El codigo de Barras pertence a mas de un producto, introduzca el código del artículo.", MsgBoxStyle.Information)
                    txt_articulo.Text = ""
                    txt_articulo.Focus()
                    Exit Sub
                Else
                    MsgBox("El codigo de Barras no pertenece a ningún artículo", MsgBoxStyle.Information)
                    txt_articulo.Text = ""
                    txt_articulo.Focus()
                    Exit Sub
                End If
            End If

            If txt_articulo.Text.Length <> 7 Then
                Exit Sub
            Else
                Dim vreaprov As New DataTable
                '22/11/2019 - Pedido por Sergio. Si está en medio de un reaprov, no dejar.
                'Hicimos una prueba de una ref que estaba recogida y no depositada y la cantidad recogida la tomaba en cuenta como ya depositada cuando el stock no se había depositado
                'Todavia (no se habia finalizado) por tanto ál finalizar, sumó de nuevo la cantidad en almacén destino. 
                'Decidimos prohibir las referencias que estuvieran en medio de un reaprov, ya  que no deben ser muchas.
                sql = "select Reaprovtotal,ReaprovA01 ,ReaprovA07 ,ReaprovA08 from v_reaprovpend where CodArticulo='" & txt_articulo.Text & "' "
                sql &= "and Reaprovtotal <> 0 "

                vreaprov = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vreaprov.Rows.Count = 1 Then
                    MsgBox("Existe un movimiento de reaprovisionamiento pendiente, no se puede continuar.")
                    Exit Sub
                End If


                'Pedido por Sergio/Domingo. Mirar si una referencia esta en medio de un proceso automático de ajuste hecho por susana
                sql = "select cab.NumMov from GesMovAlm cab "
                sql &= "inner join gesmovalmlin lin on lin.refcabecera=cab.numreferencia "
                sql &= "where lin.articulo='" & txt_articulo.Text & "' and cab.CodEmpresa=1 and cab.Observaciones='MOV AUTOMATICO LISTADO MOVIMIENTOS PREVISION MANIPULACION' "
                vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
                If vdatos.Rows.Count > 0 Then
                    MsgBox("Existe un movimiento automático de desglose/montaje sin ajustar, no se puede continuar.")
                    Exit Sub
                End If

                'Quitado porque al llamar al formulario Act_stock ya tiene en cuenta estos desg/mot. pendientes
                'Pedido por Sergio. Si la referencia está dentro de un desgl/mont. no dejar
                'sql = "select cab.codorden from orden_desg_mont cab "
                'sql &= "inner join orden_desg_mont_lin lin on lin.codorden =cab.codorden "
                'sql &= "inner join orden_desg_mont_refs lin2 on lin2.codorden =cab.codorden "
                'sql &= "where (cab.depositado =0 or cab.recogido=0) "
                'sql &= "and (lin.codarticulo ='" & txt_articulo.Text & "' or lin2.codarticulo ='" & txt_articulo.Text & "' )"
                'vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                'If vdatos.Rows.Count > 0 Then
                '    MsgBox("Existen mont./desglose por terminar. No se puede continuar.")
                '    Exit Sub
                'End If


                'Quitado porque al llamar al formulario Act_stock ya tiene en cuenta estos OF pendientes
                'Pedido por Sergio. Si la referencia está dentro de un ord. fab. no dejar
                'If funciones.obtener_of(txt_articulo.Text) > 0 Then
                '    MsgBox("Existen ord.fabricación por terminar. No se puede continuar.")
                '    Exit Sub
                'End If


                'Mostrar foto
                imagen.ImageLocation = "\\jserver\BAJA\" & txt_articulo.Text & ".jpg"
                imagen.SizeMode = PictureBoxSizeMode.StretchImage

                'Llenar ubicaciones Pick del almacén seleccionado
                '18/12/2019. Sergio pidio que salieran las ubicacione esp.CI
                funciones.llenar_ubicaciones(list_ubi, txt_articulo.Text, lab_alm.Text, "Picking", "", "", "", True)
                If list_ubi.SelectedValue <> "" Then
                    'El inventario se hace siguiendo el recorrido, por tanto no tiene sentido pedir q recuenten otras ubi.
                    'Aún así, Sergio pidió que se alertara de que tiene mas de una ubicación de pick. 17-15-2019
                    If list_ubi.Items.Count > 1 Then MsgBox("Esta referencia tiene mas de una ubicación de picking. Se tienen que recontar todas.")
                    llenar_datos_ref()
                Else
                    MsgBox("La referencia no tiene ubicacaión de picking en el almacén donde se está haciendo el inventario.", MsgBoxStyle.Information)
                    Exit Sub
                End If
                'list_ubi.Focus()
            End If




            'Buscar si la referencia ya ha sido invantariada
            'Quitar este filtro, pedidopor sergio. Poner encontrado =False para que deje continuar
            'encontrado = False
            'cont = 0
            'Do While cont < dg_lineas_inventario.Rows.Count
            '    If dg_lineas_inventario.Rows(cont).Cells("Artículo").Value = txt_articulo.Text Then
            '        encontrado = True
            '        MsgBox("Esta referencia ya ha sido inventariada", MsgBoxStyle.Information)
            '        dg_lineas_inventario.Item(0, dg_lineas_inventario.RowCount - 1).Selected = True
            '        dg_lineas_inventario.Item(0, cont).Selected = True
            '        btn_añadir.Enabled = True
            '        txt_articulo.Text = ""
            '        txt_articulo.Focus()
            '        Exit Sub
            '    End If
            '    cont += 1
            'Loop

            'Lo comenté porque ahora está en un sub para poder llamar tambien cuando cambian de ubicacion
            'If encontrado = False Then
            '    buscar_cantidad_actual(txt_articulo.Text)
            '    If lab_ubipick.Text = "" Then
            '        MsgBox("Esta referencia no tiene ubicación en Picking, no se puede actualizar el inventario", MsgBoxStyle.Exclamation)
            '        Exit Sub
            '    End If
            '    txt_cantidadpick.Text = lab_pickingact.Text
            '    lab_canttotal.Text = CInt(lab_pulmonact.Text) + CInt(txt_cantidadpick.Text) + CInt(lab_otrasact.Text)
            '    buscar_albaranes_no_preparados(txt_articulo.Text)
            '    btn_añadir.Enabled = True
            '    txt_cantidadpick.Focus()
            'End If

        End If
    End Sub
    Public Sub llenar_datos_ref()
        buscar_cantidad_actual(txt_articulo.Text)

        txt_cantidadpick.Text = lab_pickingact_ubi.Text
        lab_canttotal_despues.Text = CInt(lab_pulmonact.Text) + CInt(lab_pickingact.Text) + CInt(lab_otrasact.Text) - (CInt(lab_pickingact_ubi.Text) - CInt(txt_cantidadpick.Text))
        lab_almpick.Text = lab_alm.Text
        lab_ubipick.Text = list_ubi.SelectedValue

        buscar_albaranes_no_preparados(txt_articulo.Text)
        btn_añadir.Enabled = True

        'Javier quiere que sea inventario ciego
        txt_cantidadpick.Text = ""

        txt_cantidadpick.Focus()
    End Sub
    Public Sub buscar_cantidad_actual(ByVal codarticulo As String)
        Dim sql As String
        Dim vdatos As New DataTable

        'sql = "select isnull(sum(case when tipo='Pulmon' then cantidad end),0) as pulmon,"
        'sql &= "isnull(sum(case when tipo='Picking' and not(codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) and not(codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "')) and not(codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then cantidad end),0) as picking, "
        'sql &= "isnull(sum(case when (tipo not in ('Picking','Pulmon')) or (codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) or (codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "')) or (codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then cantidad end),0) as otras, "
        'sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) and not(codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "'))  and not(codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then ubicacion end),'') as ubi_pick,"
        'sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) and not(codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "')) and not(codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then codalmacen end),'') as Alm_pick "
        'sql &= "from ubicaciones where codarticulo='" & txt_articulo.Text & "' and codalmacen in ('A01','A07','A08') "


        sql = "select isnull(sum(case when tipo='Pulmon' then cantidad end),0) as pulmon,"
        If lab_alm.Text = "A01" Then
            sql &= "isnull(sum(case when tipo='Picking' and not(codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) then cantidad end),0) as picking, "
            sql &= "isnull(sum(case when (tipo not in ('Picking','Pulmon')) or (codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) then cantidad end),0) as otras, "
            sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) then ubicacion end),'') as ubi_pick,"
            sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A01' and ubicacion in ('" & constantes.consolidacion_A01 & "','" & constantes.traslados_A01 & "','" & constantes.ubicacion_devoluciones_A01 & "')) then codalmacen end),'') as Alm_pick "
            sql &= "from ubicaciones where codarticulo='" & txt_articulo.Text & "' and codalmacen in ('A01') "
        End If
        If lab_alm.Text = "A07" Then
            sql &= "isnull(sum(case when tipo='Picking' and not(codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "')) then cantidad end),0) as picking, "
            sql &= "isnull(sum(case when (tipo not in ('Picking','Pulmon')) or (codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "')) then cantidad end),0) as otras, "
            sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "')) then ubicacion end),'') as ubi_pick,"
            sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A07' and ubicacion in ('" & constantes.consolidacion_A07 & "','" & constantes.traslados_A07 & "','" & constantes.ubicacion_devoluciones_A07 & "')) then codalmacen end),'') as Alm_pick "
            sql &= "from ubicaciones where codarticulo='" & txt_articulo.Text & "' and codalmacen in ('A07')  "
        End If
        If lab_alm.Text = "A08" Then
            sql &= "isnull(sum(case when tipo='Picking' and not(codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then cantidad end),0) as picking, "
            sql &= "isnull(sum(case when (tipo not in ('Picking','Pulmon')) or (codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then cantidad end),0) as otras, "
            sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then ubicacion end),'') as ubi_pick,"
            sql &= "isnull(max(case when tipo='Picking' and not(codalmacen='A08' and ubicacion in ('" & constantes.consolidacion_A08 & "','" & constantes.traslados_A08 & "','" & constantes.ubicacion_devoluciones_A08 & "')) then codalmacen end),'') as Alm_pick "
            sql &= "from ubicaciones where codarticulo='" & txt_articulo.Text & "' and codalmacen in ('A08') "
        End If
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        lab_pickingact.Text = FormatNumber(vdatos.Rows(0).Item("picking"), 0)
        lab_pulmonact.Text = FormatNumber(vdatos.Rows(0).Item("pulmon"), 0)
        'lab_pulmonact.Text = 2
        lab_otrasact.Text = FormatNumber(vdatos.Rows(0).Item("otras"), 0)
        'lab_otrasact.Text = 1
        'Cantidad actual de la ubicacion elegida
        sql = "select sum(cantidad) from ubicaciones where codalmacen='" & lab_alm.Text & "' and ubicacion='" & list_ubi.SelectedValue & "' and codarticulo='" & txt_articulo.Text & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        lab_pickingact_ubi.Text = FormatNumber(vdatos.Rows(0)(0), 0)

        'Esto lo lleno ahora al seleccionar la ubicacion en el desplegable.
        'Ahora se hace inventario por almacen y por tanto ya se ha seleccionado el alamcen y ubicacion
        'If vdatos.Rows(0).Item("ubi_pick").ToString <> "" Then
        '    lab_almpick.Text = vdatos.Rows(0).Item("Alm_pick").ToString
        '    lab_ubipick.Text = vdatos.Rows(0).Item("ubi_pick").ToString
        'End If

        sql = "select isnull(sum(case when codalmacen = '" & lab_alm.Text & "' then stockactual end),0) as stock,sum(stockactual) as stocktot "
        sql &= "from gesstocks where codarticulo='" & txt_articulo.Text & "' and codalmacen in ('A01','A07','A08') and codempresa in (select CodEmpresa  from InaEmpresas where ControlStock =1) "
        vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        lab_stockact.Text = FormatNumber(vdatos.Rows(0).Item("stock"), 0)
    End Sub
    Public Sub buscar_albaranes_no_preparados(ByVal codarticulo As String)
        Dim sql As String
        Dim valb, vprep As New DataTable
        Dim cont As Integer
        Dim total As Integer

        sql = "select isnull(sum(lin.cantidad),0) as cantalb "
        sql &= "from GesAlbaranesLin lin "
        sql &= "inner join GesAlbaranes cab on cab.numreferencia=lin.refcabecera  "
        sql &= "where lin.Articulo ='" & codarticulo & "' and lin.almacen='" & lab_alm.Text & "' "
        sql &= "and not ((cab.codempresa=1 and cab.cliente='99999' and (upper(cab.observaciones) like 'ALB. JOUMMA A NEXTDOOR%')) or (cab.cliente in ('99974','99964','90013') and cab.codempresa=1) or (cab.codempresa=2 and cab.cliente = '00006') or (cab.codempresa=2 and cab.cliente = '00100' and cab.observaciones='Ventas Web') or (cab.codempresa=3 and cab.cliente in ('00024','99999','99964','90013')) or (cab.observaciones like 'Devolucion-Abono%' or cab.observaciones like 'Devolucion-Cambio%'  or cab.observaciones like 'Devolucion desde TPV%' or cab.observaciones like 'Traspaso Devoluciones%') ) "
        'or cab.observaciones like 'Devolucion-Reparacion%'
        'Cuidado con esto, ya que las reparaciones si que se preparan. Al finalizar estos albaranes en almacen, se marcan como preparadas
        sql &= "And lin.codperiodo >=2017 And cab.seralbaran Not in ('AB','FR') "
        valb = conexion.TablaxCmd(constantes.bd_inase, sql)
        'Response.Write(sql)

        sql = "select isnull(sum(isnull(pre.cantidadpreparada,0)),0) as prep "
        sql &= "from albaranes_preparados pre "
        sql &= "inner join inase.dbo.gesalbaraneslin lin on lin.codempresa=pre.codempresa and lin.codperiodo=pre.codperiodo and lin.seralbaran=pre.seralbaran and lin.codalbaran=pre.codalbaran "
        sql &= "and lin.articulo=pre.codarticulo And lin.codlinea=pre.codlinea and lin.almacen='" & lab_alm.Text & "' "
        sql &= "where pre.codArticulo ='" & codarticulo & "' and pre.codperiodo >=2017  "
        vprep = conexion.TablaxCmd(constantes.bd_intranet, sql)
        'Response.Write(sql)
        lab_cantnoprep.Text = FormatNumber(CInt(valb.Rows(0).Item("cantalb")) - CInt(vprep.Rows(0).Item("prep")), 0)
        'lab_cantnoprep.Text = 2
        lab_stockact_tot.Text = FormatNumber(CInt(lab_stockact.Text) + CInt(lab_cantnoprep.Text), 0)
    End Sub

    Private Sub txt_cantidad_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txt_cantidadpick.KeyUp
        If e.KeyValue = Keys.Enter And IsNumeric(txt_cantidadpick.Text) Then
            btn_añadir.Focus()
        Else
            If IsNumeric(lab_pulmonact.Text) And IsNumeric(txt_cantidadpick.Text) And IsNumeric(lab_otrasact.Text) Then
                'lab_canttotal_despues.Text = CInt(lab_pulmonact.Text) + CInt(txt_cantidadpick.Text) + CInt(lab_otrasact.Text)
                lab_canttotal_despues.Text = CInt(lab_pulmonact.Text) + CInt(lab_pickingact.Text) + CInt(lab_otrasact.Text) - (CInt(lab_pickingact_ubi.Text) - CInt(txt_cantidadpick.Text))

            End If
        End If
    End Sub


    Private Sub txt_articulo_Click(sender As System.Object, e As System.EventArgs) Handles txt_articulo.Click
        txt_articulo.SelectAll()
    End Sub

    Private Sub btn_añadir_Click(sender As System.Object, e As System.EventArgs) Handles btn_añadir.Click
        Dim sql As String
        Dim resultado As String
        Dim vdatos As New DataTable
        Dim cant_a_mod As Integer
        Dim siglinea_inv, siglinea_mov As Integer
        Dim canttotal As Integer
        Dim stockact As Integer



        If lab_ubipick.Text.Trim = "" Then
            MsgBox("No se puede actualizar el stock de una referencia que no tiene picking", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        frm_act_stock.Lab_codarticulo.Text = txt_articulo.Text
        frm_act_stock.lab_alm_pick.Text = lab_alm.Text
        frm_act_stock.ShowDialog()

        funciones.añadir_reg_mov_ubicaciones(txt_articulo.Text, txt_cantidadpick.Text, lab_almpick.Text, list_ubi.SelectedValue, "Cambio valor", "Ubicaciones-Inventario", frm_principal.lab_nomoperario.Text, frm_principal.lab_idoperario.Text)

        sql = "update ubicaciones set cantidad=" & txt_cantidadpick.Text & " where codalmacen='" & lab_almpick.Text & "' and ubicacion='" & list_ubi.SelectedValue & "' and codarticulo='" & txt_articulo.Text & "' "
        resultado = conexion.ExecuteCmd(constantes.bd_intranet, sql)

        If resultado <> "0" Then
            MsgBox("Se producjo un error al actualizar los datos", MsgBoxStyle.Exclamation)
        End If

        'Obtener el siguiente codlinea del inventario
        sql = "select isnull(max(codlinea),0)+1 from inventarios_lin where codinventario='" & list_inventarios.SelectedValue & "' "
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        siglinea_inv = vdatos.Rows(0)(0)

        'Insertar linea inventario
        cant_a_mod = CInt(lab_canttotal_despues.Text) - CInt(lab_stockact_tot.Text)
        sql = "insert into intranet.dbo.inventarios_lin(codinventario,codlinea,idoperario,fecha,codarticulo,codalmacen,ubicacion,"
        sql &= "cantant,cant,pulmonant,pickingant,stockant,pulmon,picking,stock,cantnoprep) "
        sql &= "values (" & list_inventarios.SelectedValue & "," & siglinea_inv & ",'" & frm_principal.lab_idoperario.Text & "',getdate(),"
        sql &= "'" & txt_articulo.Text & "','" & lab_almpick.Text & "','" & lab_ubipick.Text & "',"
        sql &= "" & CInt(lab_pickingact_ubi.Text) & "," & CInt(txt_cantidadpick.Text) & "," & CInt(lab_pulmonact.Text) & "," & CInt(lab_pickingact.Text) & ","
        sql &= "" & CInt(lab_stockact_tot.Text) & "," & CInt(lab_pulmonact.Text) & "," & CInt(lab_pickingact.Text) - (CInt(lab_pickingact_ubi.Text) - CInt(txt_cantidadpick.Text)) & "," & CInt(lab_stockact_tot.Text) + cant_a_mod & "," & CInt(lab_cantnoprep.Text) & ") "
        resultado = conexion.Executetransact(constantes.bd_inase, sql)


        limpiar()
        lineas_inventario(list_inventarios.SelectedValue)

        txt_articulo.Focus()

        'Esto es como se hacía antes. Ahora llamo al formulario Act_stock

        'If lab_canttotal_despues.Text < 0 Then
        '    MsgBox("La cantidad a actualizar debe ser mayor o igual que la cantidad pendiente de preparar.", MsgBoxStyle.Exclamation)
        '    Exit Sub
        'End If


        ''Pedido Por Chelo. No se puede insertar inventario con una dif de mas del 2% sobre la cantidad que habia en stock. 
        ''Siempre y cuando la diferencia sea mas de 5. Pedido por Sergio y validado por telefono coon Chelo
        ''Domingo pidio que pueda continuar si es Sergio, Domingo o Simon
        'stockact = CInt(lab_stockact_tot.Text)
        'canttotal = CInt(lab_canttotal_despues.Text)
        'If Math.Abs(canttotal - stockact) * 100 / stockact > 2 And Math.Abs(canttotal - stockact) > 5 Then
        '    If frm_principal.PermInventariosCantAlta = True Then
        '        If MsgBox("La cantidad inventariada difiere demasiado de la cantidad actual, Continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
        '            txt_cantidadpick.Text = lab_pickingact.Text
        '            Exit Sub
        '        Else
        '            'Avisar a Susana
        '            'Enviar_correo("susanatormo@joumma.com", "Cambio de stock desde Ubicaciones/Inventarios", "El operario " & frm_principal.lab_nomoperario.Text & " ha modificado el stock de la referencia: " & txt_articulo.Text & " en más de 5 unidades")
        '        End If
        '    Else
        '        MsgBox("La cantidad inventariada difiere demasiado de la cantidad actual, avisar al encargado", MsgBoxStyle.Exclamation)
        '        txt_cantidadpick.Text = lab_pickingact.Text
        '        Exit Sub
        '    End If
        'End If


        ''Obtener el siguiente codlinea del mov
        'sql = "select isnull(max(codlinea),0)+1 from GesMovAlmLin where refcabecera='" & numref_mov.ToString & "' "
        'vdatos = conexion.TablaxCmd(constantes.bd_inase, sql)
        'siglinea_mov = vdatos.Rows(0)(0)

        'Obtener el siguiente codlinea del inventario
        'sql = "select isnull(max(codlinea),0)+1 from inventarios_lin where codinventario='" & list_inventarios.SelectedValue & "' "
        'vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
        'siglinea_inv = vdatos.Rows(0)(0)

        ''Insertar movimiento en ubicaciones
        'sql = "insert into intranet.dbo.ubicaciones_movimientos(codalmacen,ubicacion,codarticulo,cantidad_ant,cantidad,fecha,aplicacion,documento,usuario,idoperario) "
        'sql &= "values ('" & lab_almpick.Text & "','" & lab_ubipick.Text & "','" & txt_articulo.Text & "'," & CInt(lab_pickingact_ubi.Text) & "," & CInt(txt_cantidadpick.Text) & ",getdate(),'Ubicaciones-Inventario','Cambio valor (Inventario " & list_inventarios.SelectedValue.ToString & ")','" & frm_principal.lab_nomoperario.Text & "'," & frm_principal.lab_idoperario.Text & ") "

        ''Actualizar stock en ubicacion
        ''Comente en sergio que si una ubicacion tiene mas de un palet , que hacemos, de que palet descontamos.
        ''Me dijo el 10/12/2018 que unificara toda la cantidad en un solo palet.
        ''Lo pense mejor y voy a cambiar la cantidad del palet de menor cantidad ya que sino pierdo los datos al borrar
        'sql &= "update intranet.dbo.ubicaciones set cantidad=" & CInt(txt_cantidadpick.Text) & " where codarticulo='" & txt_articulo.Text & "' and codalmacen='" & lab_almpick.Text & "' and ubicacion='" & lab_ubipick.Text & "' "
        'sql &= "and id = (select top 1 id from intranet.dbo.ubicaciones where codarticulo='" & txt_articulo.Text & "' and codalmacen='" & lab_almpick.Text & "' and ubicacion='" & lab_ubipick.Text & "' order by cantidad asc ) "

        'cant_a_mod = CInt(lab_canttotal_despues.Text) - CInt(lab_stockact_tot.Text)

        'Insertar linea inventario
        'sql &= "insert into intranet.dbo.inventarios_lin(codinventario,codlinea,idoperario,fecha,codarticulo,codalmacen,ubicacion,"
        'sql &= "cantant,cant,pulmonant,pickingant,stockant,pulmon,picking,stock,cantnoprep) "
        'sql &= "values (" & list_inventarios.SelectedValue & "," & siglinea_inv & ",'" & frm_principal.lab_idoperario.Text & "',getdate(),"
        'sql &= "'" & txt_articulo.Text & "','" & lab_almpick.Text & "','" & lab_ubipick.Text & "',"
        'sql &= "" & CInt(lab_pickingact_ubi.Text) & "," & CInt(txt_cantidadpick.Text) & "," & CInt(lab_pulmonact.Text) & "," & CInt(lab_pickingact.Text) & ","
        'sql &= "" & CInt(lab_stockact_tot.Text) & "," & CInt(lab_pulmonact.Text) & "," & CInt(lab_pickingact.Text) - (CInt(lab_pickingact_ubi.Text) - CInt(txt_cantidadpick.Text)) & "," & CInt(lab_stockact_tot.Text) + cant_a_mod & "," & CInt(lab_cantnoprep.Text) & ") "

        ''insertar movimiento en Inase en almacen picking y empresa 1
        ''cant_a_mod = (CInt(lab_pulmonact.Text) + CInt(txt_cantidadpick.Text) + CInt(lab_otrasact.Text) - CInt(lab_cantnoprep.Text)) - CInt(lab_stockact_A01_A07_A08.Text)
        'sql &= "insert into inase.dbo.GesMovAlmLin(refcabecera,ultusuario,codempresa,codperiodo,nummov,codlinea,almacendest,ubicaciondest,articulo,cantidad,deslinea) "
        'sql &= "values('" & numref_mov & "'," & frm_principal.usuario_inase & "," & codempresa_mov & "," & codperiodo_mov & "," & nummov & "," & siglinea_mov & ",'" & lab_almpick.Text & "','XXX','" & txt_articulo.Text & "'," & cant_a_mod & ",'Inventario " & list_inventarios.SelectedValue & "') "

        'resultado = conexion.Executetransact(constantes.bd_inase, sql)

        'If resultado <> "0" Then
        '    MsgBox("Se produjo un error al actualizar el stock", MsgBoxStyle.Exclamation)
        'Else
        '    lineas_inventario(list_inventarios.SelectedValue)
        '    'Situar el cursor en la linea q acabamos de marcar
        '    dg_lineas_inventario.CurrentCell = dg_lineas_inventario.Rows(0).Cells(0)

        '    txt_cantidadpick.Text = ""
        '    lab_pickingact.Text = ""
        '    lab_pulmonact.Text = ""
        '    lab_stockact_tot.Text = ""
        '    lab_cantnoprep.Text = ""
        '    lab_almpick.Text = ""
        '    lab_ubipick.Text = ""
        '    lab_canttotal_despues.Text = ""
        '    lab_pickingact_ubi.Text = ""
        '    lab_stockact.Text = ""
        '    lab_otrasact.Text = ""
        '    'dg_lineas_alb_no_prep.DataSource = Nothing
        '    txt_articulo.Text = ""
        '    list_ubi.DataSource = Nothing
        '    txt_articulo.Focus()
        'End If
    End Sub

    Public Sub lineas_inventario(ByVal inventario As Integer)
        Dim sql As String
        Dim vlin As New DataTable

        sql = "select op.nombre as 'Operario',lin.Fecha,lin.codarticulo as 'Artículo',lin.ubicacion,lin.pickingant as 'Pick.ant.',lin.picking as 'Pick.act.',lin.stockant as 'Stock ant',lin.stock as 'Stock act' "
        sql &= "from inventarios_lin lin  "
        sql &= "left join operarios_almacen op on op.id=lin.idoperario "
        sql &= "where lin.codinventario=" & inventario
        sql &= "order by lin.codlinea desc "
        vlin = conexion.TablaxCmd(constantes.bd_intranet, sql)
        'Response.Write(sql)
        dg_lineas_inventario.DataSource = vlin
        dg_lineas_inventario.ColumnHeadersDefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15, FontStyle.Bold)
        dg_lineas_inventario.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 15)
        dg_lineas_inventario.Columns(0).Width = 120
        dg_lineas_inventario.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_lineas_inventario.Columns(0).ReadOnly = True
        dg_lineas_inventario.Columns(1).Width = 200
        dg_lineas_inventario.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_lineas_inventario.Columns(1).ReadOnly = True
        dg_lineas_inventario.Columns(2).Width = 150
        dg_lineas_inventario.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dg_lineas_inventario.Columns(2).ReadOnly = True
        dg_lineas_inventario.Columns(3).Width = 150
        dg_lineas_inventario.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas_inventario.Columns(3).ReadOnly = True
        dg_lineas_inventario.Columns(4).Width = 150
        dg_lineas_inventario.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas_inventario.Columns(4).ReadOnly = True
        dg_lineas_inventario.Columns(5).Width = 150
        dg_lineas_inventario.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas_inventario.Columns(5).ReadOnly = True
        dg_lineas_inventario.Columns(6).Width = 150
        dg_lineas_inventario.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas_inventario.Columns(6).ReadOnly = True
        dg_lineas_inventario.Columns(7).Width = 200
        dg_lineas_inventario.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dg_lineas_inventario.Columns(7).ReadOnly = True



        If vlin.Rows.Count > 0 Then
            lab_ultUbi.Text = vlin.Rows(vlin.Rows.Count - 1).Item("ubicacion").ToString
        End If
    End Sub


    Private Sub list_ubi_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles list_ubi.DropDownClosed
        If list_ubi.SelectedValue <> "" Then llenar_datos_ref()
    End Sub

    Private Sub list_inventarios_DropDownClosed(sender As Object, e As EventArgs) Handles list_inventarios.DropDownClosed
        Dim sql As String
        Dim vdatos As New DataTable
        If list_inventarios.SelectedIndex = -1 Then
            p_añadir.Enabled = False
            Exit Sub
        End If

        p_añadir.Enabled = True

        sql = "select inv.codalmacen,inv.idoperario,op.nombre,inv.fechaalta,fechaultmodificacion, "
        sql &= "codempresa_mov,codperiodo_mov,nummov,numref_mov "
        sql &= "from inventarios inv "
        sql &= "left join operarios_almacen op on op.id=inv.idoperario "
        sql &= "where inv.codinventario = " & list_inventarios.SelectedValue
        vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)

        limpiar()

        If MsgBox("El inventario elegido es del almacén " & vdatos.Rows(0).Item("codalmacen").ToString & ", pero los stocks serán actualizados en todos los almacenes. Continuar", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If

        codempresa_mov = vdatos.Rows(0).Item("codempresa_mov")
        codperiodo_mov = vdatos.Rows(0).Item("codperiodo_mov")
        nummov = vdatos.Rows(0).Item("nummov")
        numref_mov = vdatos.Rows(0).Item("numref_mov").ToString
        lab_fechacreacion.Text = vdatos.Rows(0).Item("fechaalta").ToString
        lab_creadopor.Text = vdatos.Rows(0).Item("idoperario").ToString & "-" & vdatos.Rows(0).Item("nombre").ToString
        lab_ultmodificacion.Text = vdatos.Rows(0).Item("fechaultmodificacion").ToString
        lab_alm.Text = vdatos.Rows(0).Item("codalmacen").ToString
        lineas_inventario(list_inventarios.SelectedValue)
        lab_txt_stock.Text = "Stock " & vdatos.Rows(0).Item("codalmacen").ToString
        lab_tit_ubi.Text = "Stock en Ubicaciones " & vdatos.Rows(0).Item("codalmacen").ToString
        txt_articulo.Focus()

    End Sub

    Private Sub btn_NuevaUbi_Click(sender As Object, e As EventArgs) Handles btn_NuevaUbi.Click
        If txt_articulo.Text.Trim <> "" Then
            frm_nuevaubicacion.lab_ref.Text = txt_articulo.Text
            frm_nuevaubicacion.list_tipo.SelectedIndex = -1
            frm_nuevaubicacion.txt_altura.Text = ""
            frm_nuevaubicacion.txt_cantmax.Text = ""
            frm_nuevaubicacion.txt_pasillo.Text = ""
            frm_nuevaubicacion.txt_portal.Text = ""
            frm_nuevaubicacion.txt_tamaño.Text = ""
            frm_nuevaubicacion.lab_almacen_aux.Text = lab_alm.Text
            frm_nuevaubicacion.ShowDialog()
            If frm_nuevaubicacion.txt_pasillo.Text <> "" Then

                funciones.llenar_ubicaciones(list_ubi, txt_articulo.Text, lab_alm.Text, "Picking", "", "", "")
                list_ubi.SelectedIndex = list_ubi.Items.Count - 1

                llenar_datos_ref()
            End If
        End If
    End Sub

    Private Sub txt_cantidadpick_Click(sender As Object, e As EventArgs) Handles txt_cantidadpick.Click
        txt_cantidadpick.SelectAll()
    End Sub

    Private Sub list_ubi_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles list_ubi.KeyUp
        Dim sql As String
        Dim vdatos As New DataTable
        If e.KeyValue = Keys.Enter Then
            If Strings.Left(list_ubi.Text, 1) = "U" And Strings.Len(list_ubi.Text.Trim) = 7 Then
                list_ubi.Text = Strings.Mid(list_ubi.Text, 2, 2) & "-" & Strings.Mid(list_ubi.Text, 4, 2) & "-" & Strings.Right(list_ubi.Text, 2)
                'Validar si en esta ubicacion existe la referencia a inventariar
                sql = "select * from ubicaciones where codalmacen='" & lab_alm.Text & "' and codarticulo='" & txt_articulo.Text & "' and ubicacion='" & list_ubi.Text & "'"
                vdatos = conexion.TablaxCmd(constantes.bd_intranet, sql)
                If vdatos.Rows.Count = 0 Then
                    MsgBox("Esta ref. no está ubicada en la ubicación indicada", MsgBoxStyle.Exclamation)
                    Try
                        list_ubi.SelectedIndex = 0
                    Catch ex As Exception

                    End Try
                Else
                    llenar_datos_ref()
                End If
            End If
        End If
    End Sub

    Private Sub list_ubi_Click(sender As System.Object, e As System.EventArgs) Handles list_ubi.Click
        list_ubi.SelectAll()
    End Sub
End Class