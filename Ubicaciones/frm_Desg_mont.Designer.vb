﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Desg_mont
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_OtraUbi = New System.Windows.Forms.Button()
        Me.p_filtro = New System.Windows.Forms.Panel()
        Me.cb_FiltroPorDepositar = New System.Windows.Forms.CheckBox()
        Me.cb_FiltroPorRecoger = New System.Windows.Forms.CheckBox()
        Me.cb_FiltroStockA07 = New System.Windows.Forms.CheckBox()
        Me.cb_FiltroStockA01 = New System.Windows.Forms.CheckBox()
        Me.cb_filtro_urgentes = New System.Windows.Forms.CheckBox()
        Me.btn_filtro_ok = New System.Windows.Forms.Button()
        Me.btn_limpiar = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.list_filtro_almacen = New System.Windows.Forms.ComboBox()
        Me.textbox12 = New System.Windows.Forms.Label()
        Me.txt_filtro_codarticulo = New System.Windows.Forms.TextBox()
        Me.btn_salirorden = New System.Windows.Forms.Button()
        Me.lab_origen = New System.Windows.Forms.Label()
        Me.list_accion = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lab_descripcion = New System.Windows.Forms.Label()
        Me.lab_cantidad = New System.Windows.Forms.Label()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.txt_cantidad = New System.Windows.Forms.TextBox()
        Me.dg_ubi = New System.Windows.Forms.DataGridView()
        Me.lab_ubicaciones = New System.Windows.Forms.Label()
        Me.lab_titulo = New System.Windows.Forms.Label()
        Me.lab_tit_lineas = New System.Windows.Forms.Label()
        Me.lab_fecha = New System.Windows.Forms.Label()
        Me.lab_aplicacion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.list_codorden = New System.Windows.Forms.ComboBox()
        Me.btn_finalizar = New System.Windows.Forms.Button()
        Me.dg_lineas = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.p_filtro.SuspendLayout()
        CType(Me.dg_ubi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.btn_OtraUbi)
        Me.Panel1.Controls.Add(Me.p_filtro)
        Me.Panel1.Controls.Add(Me.btn_salirorden)
        Me.Panel1.Controls.Add(Me.lab_origen)
        Me.Panel1.Controls.Add(Me.list_accion)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.lab_descripcion)
        Me.Panel1.Controls.Add(Me.lab_cantidad)
        Me.Panel1.Controls.Add(Me.btn_ok)
        Me.Panel1.Controls.Add(Me.txt_cantidad)
        Me.Panel1.Controls.Add(Me.dg_ubi)
        Me.Panel1.Controls.Add(Me.lab_ubicaciones)
        Me.Panel1.Controls.Add(Me.lab_titulo)
        Me.Panel1.Controls.Add(Me.lab_tit_lineas)
        Me.Panel1.Controls.Add(Me.lab_fecha)
        Me.Panel1.Controls.Add(Me.lab_aplicacion)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.list_codorden)
        Me.Panel1.Controls.Add(Me.btn_finalizar)
        Me.Panel1.Controls.Add(Me.dg_lineas)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1250, 640)
        Me.Panel1.TabIndex = 26
        '
        'btn_OtraUbi
        '
        Me.btn_OtraUbi.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.btn_OtraUbi.Enabled = False
        Me.btn_OtraUbi.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_OtraUbi.Location = New System.Drawing.Point(747, 494)
        Me.btn_OtraUbi.Name = "btn_OtraUbi"
        Me.btn_OtraUbi.Size = New System.Drawing.Size(92, 31)
        Me.btn_OtraUbi.TabIndex = 113
        Me.btn_OtraUbi.Text = "Otra Ub."
        Me.btn_OtraUbi.UseVisualStyleBackColor = False
        '
        'p_filtro
        '
        Me.p_filtro.BackColor = System.Drawing.Color.PaleTurquoise
        Me.p_filtro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.p_filtro.Controls.Add(Me.cb_FiltroPorDepositar)
        Me.p_filtro.Controls.Add(Me.cb_FiltroPorRecoger)
        Me.p_filtro.Controls.Add(Me.cb_FiltroStockA07)
        Me.p_filtro.Controls.Add(Me.cb_FiltroStockA01)
        Me.p_filtro.Controls.Add(Me.cb_filtro_urgentes)
        Me.p_filtro.Controls.Add(Me.btn_filtro_ok)
        Me.p_filtro.Controls.Add(Me.btn_limpiar)
        Me.p_filtro.Controls.Add(Me.Label5)
        Me.p_filtro.Controls.Add(Me.list_filtro_almacen)
        Me.p_filtro.Controls.Add(Me.textbox12)
        Me.p_filtro.Controls.Add(Me.txt_filtro_codarticulo)
        Me.p_filtro.Location = New System.Drawing.Point(20, 87)
        Me.p_filtro.Name = "p_filtro"
        Me.p_filtro.Size = New System.Drawing.Size(1221, 73)
        Me.p_filtro.TabIndex = 88
        '
        'cb_FiltroPorDepositar
        '
        Me.cb_FiltroPorDepositar.AutoSize = True
        Me.cb_FiltroPorDepositar.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroPorDepositar.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroPorDepositar.Location = New System.Drawing.Point(771, 35)
        Me.cb_FiltroPorDepositar.Name = "cb_FiltroPorDepositar"
        Me.cb_FiltroPorDepositar.Size = New System.Drawing.Size(177, 30)
        Me.cb_FiltroPorDepositar.TabIndex = 99
        Me.cb_FiltroPorDepositar.Text = "Por Depositar"
        Me.cb_FiltroPorDepositar.UseVisualStyleBackColor = True
        '
        'cb_FiltroPorRecoger
        '
        Me.cb_FiltroPorRecoger.AutoSize = True
        Me.cb_FiltroPorRecoger.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroPorRecoger.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroPorRecoger.Location = New System.Drawing.Point(771, 3)
        Me.cb_FiltroPorRecoger.Name = "cb_FiltroPorRecoger"
        Me.cb_FiltroPorRecoger.Size = New System.Drawing.Size(164, 30)
        Me.cb_FiltroPorRecoger.TabIndex = 98
        Me.cb_FiltroPorRecoger.Text = "Por Recoger"
        Me.cb_FiltroPorRecoger.UseVisualStyleBackColor = True
        '
        'cb_FiltroStockA07
        '
        Me.cb_FiltroStockA07.AutoSize = True
        Me.cb_FiltroStockA07.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroStockA07.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroStockA07.Location = New System.Drawing.Point(580, 35)
        Me.cb_FiltroStockA07.Name = "cb_FiltroStockA07"
        Me.cb_FiltroStockA07.Size = New System.Drawing.Size(140, 30)
        Me.cb_FiltroStockA07.TabIndex = 97
        Me.cb_FiltroStockA07.Text = "Stock A07"
        Me.cb_FiltroStockA07.UseVisualStyleBackColor = True
        '
        'cb_FiltroStockA01
        '
        Me.cb_FiltroStockA01.AutoSize = True
        Me.cb_FiltroStockA01.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_FiltroStockA01.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_FiltroStockA01.Location = New System.Drawing.Point(580, 3)
        Me.cb_FiltroStockA01.Name = "cb_FiltroStockA01"
        Me.cb_FiltroStockA01.Size = New System.Drawing.Size(140, 30)
        Me.cb_FiltroStockA01.TabIndex = 96
        Me.cb_FiltroStockA01.Text = "Stock A01"
        Me.cb_FiltroStockA01.UseVisualStyleBackColor = True
        '
        'cb_filtro_urgentes
        '
        Me.cb_filtro_urgentes.AutoSize = True
        Me.cb_filtro_urgentes.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_filtro_urgentes.ForeColor = System.Drawing.Color.SteelBlue
        Me.cb_filtro_urgentes.Location = New System.Drawing.Point(460, 17)
        Me.cb_filtro_urgentes.Name = "cb_filtro_urgentes"
        Me.cb_filtro_urgentes.Size = New System.Drawing.Size(85, 35)
        Me.cb_filtro_urgentes.TabIndex = 92
        Me.cb_filtro_urgentes.Text = "Urg."
        Me.cb_filtro_urgentes.UseVisualStyleBackColor = True
        '
        'btn_filtro_ok
        '
        Me.btn_filtro_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_filtro_ok.Location = New System.Drawing.Point(983, 10)
        Me.btn_filtro_ok.Name = "btn_filtro_ok"
        Me.btn_filtro_ok.Size = New System.Drawing.Size(97, 48)
        Me.btn_filtro_ok.TabIndex = 91
        Me.btn_filtro_ok.Text = "Filtrar"
        Me.btn_filtro_ok.UseVisualStyleBackColor = True
        '
        'btn_limpiar
        '
        Me.btn_limpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_limpiar.Location = New System.Drawing.Point(1094, 10)
        Me.btn_limpiar.Name = "btn_limpiar"
        Me.btn_limpiar.Size = New System.Drawing.Size(114, 48)
        Me.btn_limpiar.TabIndex = 89
        Me.btn_limpiar.Text = "Limpiar"
        Me.btn_limpiar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label5.Location = New System.Drawing.Point(203, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 29)
        Me.Label5.TabIndex = 90
        Me.Label5.Text = "Ref.:"
        '
        'list_filtro_almacen
        '
        Me.list_filtro_almacen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_filtro_almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_filtro_almacen.FormattingEnabled = True
        Me.list_filtro_almacen.Items.AddRange(New Object() {"A01", "A07"})
        Me.list_filtro_almacen.Location = New System.Drawing.Point(89, 14)
        Me.list_filtro_almacen.Name = "list_filtro_almacen"
        Me.list_filtro_almacen.Size = New System.Drawing.Size(90, 41)
        Me.list_filtro_almacen.TabIndex = 89
        '
        'textbox12
        '
        Me.textbox12.AutoSize = True
        Me.textbox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textbox12.ForeColor = System.Drawing.Color.SteelBlue
        Me.textbox12.Location = New System.Drawing.Point(19, 20)
        Me.textbox12.Name = "textbox12"
        Me.textbox12.Size = New System.Drawing.Size(64, 29)
        Me.textbox12.TabIndex = 89
        Me.textbox12.Text = "Alm:"
        '
        'txt_filtro_codarticulo
        '
        Me.txt_filtro_codarticulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_filtro_codarticulo.Location = New System.Drawing.Point(276, 12)
        Me.txt_filtro_codarticulo.Name = "txt_filtro_codarticulo"
        Me.txt_filtro_codarticulo.Size = New System.Drawing.Size(154, 45)
        Me.txt_filtro_codarticulo.TabIndex = 89
        Me.txt_filtro_codarticulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btn_salirorden
        '
        Me.btn_salirorden.Enabled = False
        Me.btn_salirorden.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_salirorden.Location = New System.Drawing.Point(751, 589)
        Me.btn_salirorden.Name = "btn_salirorden"
        Me.btn_salirorden.Size = New System.Drawing.Size(262, 44)
        Me.btn_salirorden.TabIndex = 86
        Me.btn_salirorden.Text = "Salir de la orden"
        Me.btn_salirorden.UseVisualStyleBackColor = True
        '
        'lab_origen
        '
        Me.lab_origen.AutoSize = True
        Me.lab_origen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_origen.Location = New System.Drawing.Point(527, 43)
        Me.lab_origen.Name = "lab_origen"
        Me.lab_origen.Size = New System.Drawing.Size(54, 15)
        Me.lab_origen.TabIndex = 85
        Me.lab_origen.Text = "Origen:"
        '
        'list_accion
        '
        Me.list_accion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.list_accion.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_accion.FormattingEnabled = True
        Me.list_accion.Items.AddRange(New Object() {"Recoger", "Depositar"})
        Me.list_accion.Location = New System.Drawing.Point(1018, 9)
        Me.list_accion.Name = "list_accion"
        Me.list_accion.Size = New System.Drawing.Size(225, 62)
        Me.list_accion.TabIndex = 84
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(876, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 39)
        Me.Label1.TabIndex = 83
        Me.Label1.Text = "Acción:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(21, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 20)
        Me.Label4.TabIndex = 87
        Me.Label4.Text = "Filtros"
        '
        'lab_descripcion
        '
        Me.lab_descripcion.AutoSize = True
        Me.lab_descripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_descripcion.Location = New System.Drawing.Point(744, 163)
        Me.lab_descripcion.Name = "lab_descripcion"
        Me.lab_descripcion.Size = New System.Drawing.Size(49, 29)
        Me.lab_descripcion.TabIndex = 82
        Me.lab_descripcion.Text = "xxx"
        '
        'lab_cantidad
        '
        Me.lab_cantidad.AutoSize = True
        Me.lab_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_cantidad.Location = New System.Drawing.Point(890, 521)
        Me.lab_cantidad.Name = "lab_cantidad"
        Me.lab_cantidad.Size = New System.Drawing.Size(113, 39)
        Me.lab_cantidad.TabIndex = 63
        Me.lab_cantidad.Text = "Cant.:"
        '
        'btn_ok
        '
        Me.btn_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ok.Location = New System.Drawing.Point(1149, 506)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(92, 60)
        Me.btn_ok.TabIndex = 62
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'txt_cantidad
        '
        Me.txt_cantidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cantidad.Location = New System.Drawing.Point(1009, 506)
        Me.txt_cantidad.Name = "txt_cantidad"
        Me.txt_cantidad.Size = New System.Drawing.Size(120, 60)
        Me.txt_cantidad.TabIndex = 61
        Me.txt_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dg_ubi
        '
        Me.dg_ubi.AllowUserToAddRows = False
        Me.dg_ubi.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_ubi.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dg_ubi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_ubi.DefaultCellStyle = DataGridViewCellStyle2
        Me.dg_ubi.Location = New System.Drawing.Point(747, 226)
        Me.dg_ubi.MultiSelect = False
        Me.dg_ubi.Name = "dg_ubi"
        Me.dg_ubi.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_ubi.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dg_ubi.RowTemplate.Height = 50
        Me.dg_ubi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_ubi.Size = New System.Drawing.Size(494, 262)
        Me.dg_ubi.TabIndex = 60
        '
        'lab_ubicaciones
        '
        Me.lab_ubicaciones.AutoSize = True
        Me.lab_ubicaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_ubicaciones.Location = New System.Drawing.Point(744, 195)
        Me.lab_ubicaciones.Name = "lab_ubicaciones"
        Me.lab_ubicaciones.Size = New System.Drawing.Size(130, 25)
        Me.lab_ubicaciones.TabIndex = 59
        Me.lab_ubicaciones.Text = "Ubicaciones"
        '
        'lab_titulo
        '
        Me.lab_titulo.AutoSize = True
        Me.lab_titulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_titulo.Location = New System.Drawing.Point(346, 17)
        Me.lab_titulo.Name = "lab_titulo"
        Me.lab_titulo.Size = New System.Drawing.Size(89, 39)
        Me.lab_titulo.TabIndex = 58
        Me.lab_titulo.Text = "XXX"
        '
        'lab_tit_lineas
        '
        Me.lab_tit_lineas.AutoSize = True
        Me.lab_tit_lineas.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_tit_lineas.Location = New System.Drawing.Point(15, 167)
        Me.lab_tit_lineas.Name = "lab_tit_lineas"
        Me.lab_tit_lineas.Size = New System.Drawing.Size(340, 25)
        Me.lab_tit_lineas.TabIndex = 57
        Me.lab_tit_lineas.Text = "Referencias a Desglosar / Montar:"
        '
        'lab_fecha
        '
        Me.lab_fecha.AutoSize = True
        Me.lab_fecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_fecha.Location = New System.Drawing.Point(527, 17)
        Me.lab_fecha.Name = "lab_fecha"
        Me.lab_fecha.Size = New System.Drawing.Size(54, 15)
        Me.lab_fecha.TabIndex = 55
        Me.lab_fecha.Text = "Fecha: "
        '
        'lab_aplicacion
        '
        Me.lab_aplicacion.AutoSize = True
        Me.lab_aplicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lab_aplicacion.Location = New System.Drawing.Point(251, 594)
        Me.lab_aplicacion.Name = "lab_aplicacion"
        Me.lab_aplicacion.Size = New System.Drawing.Size(56, 31)
        Me.lab_aplicacion.TabIndex = 53
        Me.lab_aplicacion.Text = "xxx"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(18, 594)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(238, 31)
        Me.Label3.TabIndex = 52
        Me.Label3.Text = "Generada desde:"
        '
        'list_codorden
        '
        Me.list_codorden.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_codorden.FormattingEnabled = True
        Me.list_codorden.IntegralHeight = False
        Me.list_codorden.Location = New System.Drawing.Point(174, 8)
        Me.list_codorden.Name = "list_codorden"
        Me.list_codorden.Size = New System.Drawing.Size(155, 59)
        Me.list_codorden.TabIndex = 37
        '
        'btn_finalizar
        '
        Me.btn_finalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_finalizar.Location = New System.Drawing.Point(1066, 586)
        Me.btn_finalizar.Name = "btn_finalizar"
        Me.btn_finalizar.Size = New System.Drawing.Size(175, 48)
        Me.btn_finalizar.TabIndex = 33
        Me.btn_finalizar.Text = "Finalizar"
        Me.btn_finalizar.UseVisualStyleBackColor = True
        Me.btn_finalizar.Visible = False
        '
        'dg_lineas
        '
        Me.dg_lineas.AllowUserToAddRows = False
        Me.dg_lineas.AllowUserToDeleteRows = False
        Me.dg_lineas.AllowUserToResizeColumns = False
        Me.dg_lineas.AllowUserToResizeRows = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dg_lineas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dg_lineas.DefaultCellStyle = DataGridViewCellStyle5
        Me.dg_lineas.Location = New System.Drawing.Point(20, 195)
        Me.dg_lineas.MultiSelect = False
        Me.dg_lineas.Name = "dg_lineas"
        Me.dg_lineas.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dg_lineas.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dg_lineas.RowTemplate.Height = 50
        Me.dg_lineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_lineas.Size = New System.Drawing.Size(703, 392)
        Me.dg_lineas.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(167, 46)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "N. Ord.:"
        '
        'frm_Desg_mont
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1256, 646)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_Desg_mont"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Alta Set/Desglose"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.p_filtro.ResumeLayout(False)
        Me.p_filtro.PerformLayout()
        CType(Me.dg_ubi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dg_lineas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lab_fecha As System.Windows.Forms.Label
    Friend WithEvents lab_aplicacion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents list_codorden As System.Windows.Forms.ComboBox
    Friend WithEvents btn_finalizar As System.Windows.Forms.Button
    Friend WithEvents dg_lineas As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lab_tit_lineas As System.Windows.Forms.Label
    Friend WithEvents lab_titulo As System.Windows.Forms.Label
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents lab_descripcion As System.Windows.Forms.Label
    Friend WithEvents list_accion As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lab_cantidad As System.Windows.Forms.Label
    Friend WithEvents txt_cantidad As System.Windows.Forms.TextBox
    Friend WithEvents dg_ubi As System.Windows.Forms.DataGridView
    Friend WithEvents lab_ubicaciones As System.Windows.Forms.Label
    Friend WithEvents lab_origen As System.Windows.Forms.Label
    Friend WithEvents btn_salirorden As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents p_filtro As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents list_filtro_almacen As System.Windows.Forms.ComboBox
    Friend WithEvents textbox12 As System.Windows.Forms.Label
    Friend WithEvents txt_filtro_codarticulo As System.Windows.Forms.TextBox
    Friend WithEvents btn_limpiar As System.Windows.Forms.Button
    Friend WithEvents btn_filtro_ok As System.Windows.Forms.Button
    Friend WithEvents cb_filtro_urgentes As System.Windows.Forms.CheckBox
    Friend WithEvents cb_FiltroStockA07 As CheckBox
    Friend WithEvents cb_FiltroStockA01 As CheckBox
    Friend WithEvents cb_FiltroPorDepositar As CheckBox
    Friend WithEvents cb_FiltroPorRecoger As CheckBox
    Friend WithEvents btn_OtraUbi As Button
End Class
